import "../sass/ga_hotTopicsSettings.scss";
import  "./view/importsvg";

import uniqid from 'uniqid';
import remove from "./model/deleteCard";
import { initTinymce } from './utils/tinyActions';
import { delfunc, cancelfunc } from "./view/deleteBox";
import { seeShow_adv , showBtnClick} from "./view/seeShowbtns";
import { tagsFunc } from "./view/tags";
import { loadLargeSpinner, removeSpinner } from './view/spinner';
import { autoComplete } from './view/autoCompleteTags';

///////////////////////////////////////////////
////---- Universal Selectors ----////
///////////////////////////////////////////////
const editSection = document.querySelector('.edit');
const topicsSection = document.querySelector('.hot-topics');
const animeBox = document.getElementById('animations');
const pageUniqId = uniqid();
const backBtn = editSection.querySelector('.edit__show-cards');
const hotTopicForm = editSection.querySelector('.edit__form-container')

const csrf = document.querySelector('#csrf').getAttribute('data-csrf');
const guestpertId = document.querySelector('#guestpertId').getAttribute('data-guestpertId')

///////////////////////////////////////////////
////---- Tags Functionality ----////
///////////////////////////////////////////////
tagsFunc(editSection);


/////////////////////////////////////////
////---- On clicking create btn ----////
////////////////////////////////////////
window.addEventListener('click', e => {
    if (!e.target.classList.contains('btn-createNew')) return;
    hotTopicForm.setAttribute('action', `/guestpert/createHotTopic?_csrf=${csrf}&&tempId=${pageUniqId}&&guestpertId=${guestpertId}`)
    seeShow_adv([topicsSection], editSection)
})

///////////////////////////////////////
////---- On clicking back btn ----////
//////////////////////////////////////
backBtn.addEventListener('click', async function () {

    // Remove Edit Fields
    if (this.classList.contains('editMode')) {
    
        // Remove class from button
        this.classList.remove('editMode');
    }

    // Remove Fields
    removeFields();

    // Render Spinner
    loadLargeSpinner(animeBox);

    // Delete the array of images with the tempId
    const removeTempArr = new remove(`/guestpert/tempHotTopicsArr/delete?tempId=${pageUniqId}&&_csrf=${csrf}`);
    await removeTempArr.getResults();

    // Render Spinner
    removeSpinner(animeBox);

    // display main Section
    showBtnClick([topicsSection], editSection);
})

///////////////////////////////////////////////
////---- On clicking delete btn ----////
///////////////////////////////////////////////
for (const delBtn of document.querySelectorAll('.hot-topics__card-btn-delete')) {
    delfunc(delBtn)
}

///////////////////////////////////////////////
////---- On clicking cancel btn ----////
///////////////////////////////////////////////
cancelfunc();

/////////////////////////////////////////
////---- Tags AutoComplete ----////
///////////////////////////////////////
autoComplete();



////////////////////////////////////////
///////////////////////////////////////
/////---- REST Features ----///////////
//////////////////////////////////////
//////////////////////////////////////

import Search from './model/search';
import { renderTagsUI } from './view/rest-autocomplete';
import { prepareForLoad} from './view/spinner';
import { renderEdit, renderError, removeFields } from './view/pg-ga_manageHotTopics';

// State of the home Page
const state = {};

// Clicking on Edit Button
topicsSection.addEventListener('click', async (e) => {

    // Preventing from clicking outside
    if (!e.target.classList.contains('hot-topics__card-btn-edit')) { return };

    // store the button
    const btn = e.target;
    if (!btn) { return };

    // store the id
    const id = btn.getAttribute('data-id');

    // Render Spinner
    prepareForLoad(topicsSection, animeBox);

    // Search
    state.search = new Search(`/guestpert/manageHotTopics/edit?id=${id}`);
    state.result = await state.search.getResults();

    // If error occours
    if (!state.result) {
        // Remove spinner
        removeSpinner(animeBox);

        // call error function
        renderError(topicsSection);

        // stop next
        return
    }

    // Remove spinner
    removeSpinner(animeBox);

    // Render Edit
    renderEdit(state.result.data);

    hotTopicForm.setAttribute('action', `/guestpert/editHotTopic?_csrf=${csrf}&&tempId=${pageUniqId}&&guestpertId=${guestpertId}&&hotTopicId=${id}`);
})



///////////////////////////
// --- AutoComplete ---//
/////////////////////////

const regEx = /^[\w]{1,60}$/i;

document.querySelector('.tags-input__main-input').addEventListener('keyup', async function (e) {

    // Return if not available
    if (!document.activeElement.classList.contains('tags-input__main-input') || this.value.length < 2) { return };

    const chk = ['', 8, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 96, 97, 98, 99, 100, 101, 102, 103, 104, 105, 106, 222, 186]
    const k = chk.indexOf(e.keyCode);

    if (k < 0 || !k) { return };

    // RegExp
    if (!regEx.test(this.value)) { return };

    // // Get Keyword
    const keyword = this.value;

    // // Search
    state.autoComplete = new Search(`/guestpert/search/tagsList?tag=${keyword}`);
    state.autoCompleteResult = await state.autoComplete.getResults();

    if (!state.autoCompleteResult) { return };

    // // Render in UI
    renderTagsUI(state.autoCompleteResult.data, this);
})


///////////////////////////////
// --- tiny mce settings ---//
///////////////////////////////


initTinymce(
    {
        selector: "#openingPoint",
        menubar: !1,
        skin: "CUSTOM",
        content_css: "CUSTOM",
        plugins: "preview lists link fullscreen paste",
        paste_as_text: true,
        toolbar: "undo redo preview fullscreen bold italic alignjustify link"
    }
)
initTinymce(
    {
        selector: "#content",
        menubar: !1,
        skin: "CUSTOM",
        content_css: "CUSTOM",
        plugins: "preview image lists fullscreen link paste",
        paste_as_text: true,
        toolbar: "undo redo image preview bullist fullscreen bold italic alignjustify link",
        advlist_bullet_styles: "disc",
        images_upload_url: `/guestpert/hotTopicUploadPic?tempId=${pageUniqId}&&_csrf=${csrf}`,
        image_dimensions: false,
    }
)


const errMsg = document.querySelector('.message');

window.addEventListener('load', e => {
    setTimeout(() => {
        if (!errMsg) return;
        errMsg.style.display = 'none';
        errMsg.querySelector('.message__txt').textContent = '';
    }, 5000)
})

import { logoutFunc } from './lib/autoLogout'
logoutFunc(() => {
    window.location.href = `/login`
})