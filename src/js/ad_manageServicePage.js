import "../sass/ad_manageServicePage.scss";
import "./view/importsvg";

import {initTinymce} from './utils/tinyActions';

import { seeShow, seeShow_adv } from "./view/seeShowbtns";
import { delfuncRest, cancelfunc, removeDelBox} from "./view/deleteBox";
import { message } from "./view/errorMessage";
import { hamburger, slideMenu } from "./view/sidebar";

///////////////////////////////////////////
///--- Hamburger and Sidebar functionality ---///
///////////////////////////////////////////
hamburger();
slideMenu();

///////////////////////////////////////////////
////---- Universal Selectors ----////
///////////////////////////////////////////////
const cardsSection = document.querySelector('.services');
const createBtn = document.querySelector('.btn-createNew');
const animeBox = document.querySelector('#animation');
const editSection = document.querySelector('.edit');
const form = editSection.querySelector('.edit__form-container');
const backBtn = editSection.querySelector('.edit__show-cards');
const delBox = document.querySelector('.popup__del-box');
const csrf = document.querySelector('#csrf').getAttribute('data-csrf');

///////////////////////////////////////////////
////---- On clicking create and see btns ----////
///////////////////////////////////////////////
seeShow(cardsSection);
createBtn.addEventListener('click', e=>{
    form.setAttribute('action', `/admin/manageServices/create?_csrf=${csrf}`);
})


///////////////////////////////////////////////
////---- On clicking delete btn ----////
///////////////////////////////////////////////
cardsSection.addEventListener('click', e=>{

    // Protect from clicking outside
    if (!e.target.classList.contains('services__card-btn-remove')) { return };

    // Store the card
    state.activeCard = e.target.closest('.services__cards')

    // call delfunc Rest
    delfuncRest(e.target);
})

///////////////////////////////////////////////
////---- On clicking cancel btn ----////
///////////////////////////////////////////////
cancelfunc();


///////////////////////////////////////////////
///////////////////////////////////////////////
////---- Rest Features ----//// ----------
///////////////////////////////////////////////
///////////////////////////////////////////////

import Search from './model/search';
import remove from './model/deleteCard';
import { prepareForLoad, loadLargeSpinner, removeSpinner } from './view/spinner';
import { renderEdit, clearFields } from './view/pg-ad_manageServices';

// State of the Page
const state = {};

///////////////////////////////////////////////
////---- Pressing Edit Btn ----////
///////////////////////////////////////////////
cardsSection.addEventListener('click', async (e) => {

    // Preventing from clicking outside
    if (!e.target.classList.contains('services__card-btn-edit')) { return };

    // store the id
    const id = e.target.getAttribute('data-id');

    // Render Spinner
    prepareForLoad(cardsSection, animeBox);

    // Search
    state.search = new Search(`/admin/manageServices/getService?id=${id}`);
    state.result = await state.search.getResults();

    // Remove spinner
    removeSpinner(animeBox);

    // If error occours
    if (!state.result) {

        // display topics Section
        cardsSection.style.display = 'flex';

        // display error message
        message(5000, false, 'Could not fetch data. Please try again later');
        // stop next
        return
    }

    // Render Edit
    renderEdit(state.result.data);

    // Set edit mode in back btn
    backBtn.setAttribute('data-mode', 'edit');
    form.setAttribute('action', `/admin/editService/edit?id=${id}&&_csrf=${csrf}`);

    seeShow_adv([cardsSection], editSection);
})


///////////////////////////////////////////////
////---- Pressing Back Btn ----////
///////////////////////////////////////////////
backBtn.addEventListener('click', function () {

    if (this.getAttribute('data-mode') === 'edit') {

        // Call Remove function
        clearFields()

        // Take out the edit Mode
        backBtn.setAttribute('data-mode', '');
        form.setAttribute('action', '');
    } 

    // Show all products
    showBtnClick([cardsSection], editSection);
})



//////////////////////////////////////
////---- Pressing remove Btn ----////
/////////////////////////////////////
delBox.addEventListener('click', async (e) => {

    // Preventing from clicking outside
    if (!e.target.classList.contains('popup__del-btn')) { return };

    // store the id
    const id = e.target.getAttribute('data-id');
    
    // Remove the popup 
    removeDelBox();

    // Render Spinner
    loadLargeSpinner(animeBox);

    // Search
    state.removeService = new remove(`/admin/manageServices/delete?id=${id}&&_csrf=${csrf}`);
    state.removeServiceRes = await state.removeService.getResults();

    // Remove spinner
    removeSpinner(animeBox);

    // If error occours
    if (!state.removeServiceRes) {

        // display error message
        message(5000, false, 'Could not delete this service. Try again later');

        // Cancel active card
        state.activeCard = null;

        // stop next
        return
    }
    
    message(5000, true, 'The srvice have been successfully deleted');

    // Disappear the card
    if(state.activeCard){
        state.activeCard.parentElement.removeChild(state.activeCard);
    }
    
})


///////////////////////////////
// --- tiny mce settings ---//
///////////////////////////////


initTinymce(
    {
        selector: "#description",
        menubar: !1,
        skin: "CUSTOM",
        content_css: "CUSTOM",
        plugins: " preview image lists fullscreen paste",
        paste_as_text: true,
        toolbar: "undo redo image preview fullscreen bold bullist italic alignjustify",
        advlist_bullet_styles: "disc",
        images_upload_url: `/admin/imageUpload/servicePage?_csrf=${csrf}`,
        image_dimensions: false,
    }
)



import { logoutFunc } from './lib/autoLogout'
logoutFunc(() => {
    window.location.href = `/admin_login`
})