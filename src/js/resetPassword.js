import "../sass/resetPassword.scss";
import "./view/importsvg"; 

const resetForm = document.querySelector('.reset__form');
const password = resetForm.querySelector('#password');
const confirmPassword = resetForm.querySelector('#confirmPassword');
const errorMsg = document.querySelector('.reset__msg');

resetForm.addEventListener('submit', (e)=>{

    e.preventDefault();

    if(password.value !== confirmPassword.value){
        errorMsg.textContent = 'Passwords does not match. Try again';
        errorMsg.style.display = 'block';
        return
    }

    resetForm.submit()
})


const logoutBtn = document.querySelector('.header-bar__link-logout')
import { logoutFunc } from './lib/autoLogout'
if(logoutBtn){
    logoutFunc(() => {
        const btnId = logoutBtn.getAttribute('data-id')
        if(btnId=='adtyspnce'){
            window.location.href = `/admin_login`
        }else{
            window.location.href = `/login`
        }
    })
} 