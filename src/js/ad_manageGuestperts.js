import "../sass/ad_manageGuestperts.scss";
import "./view/importsvg";
import "./vendor/form-dropdown_manageGuestperts";

import {initTinymce} from './utils/tinyActions';

import Search from './model/search';
import remove from './model/deleteCard';
import {prepareForLoad , removeSpinner, loadLargeSpinner} from './view/spinner';
import {pagination} from './view/pagination_rest';
import {renderCards, renderEdit, clearEdit} from './view/pg-ad_manageGuestpert';
import {moreDetailGuestpert, closeMoreDetail} from './view/moreDetailsPopup';
import {seeShow_adv, showBtnClick} from './view/seeShowbtns';
import {message} from './view/errorMessage';
import {changeDp_withoutPopup, changeDp} from './view/changeProfPic';
import {delfuncRest, removeDelBox, cancelfunc} from './view/deleteBox';
import { hamburger, slideMenu } from "./view/sidebar";

///////////////////////////////////////////
///--- Hamburger and Sidebar functionality ---///
///////////////////////////////////////////
hamburger();
slideMenu();


///////////////////////////////////////////////
////---- Universal Selectors ----////
///////////////////////////////////////////////
const searchBar = document.querySelector('#search-input');
const hiddenInput = searchBar.parentElement.querySelector('#hidden-input');

const topicsSection = document.querySelector('.guestperts');
const guestpertBox = topicsSection.querySelector('#guestperts-box');
const createBtn = topicsSection.querySelector('#create-btn');

const animation = document.getElementById('animation');
const createsection = document.querySelector('#create-section');
const editSection = document.querySelector('#editSection');
const edit_dp = editSection.querySelector('.edit__image-container');
const delBtn = editSection.querySelector('#delete-guestpert-btn');

const showGuestpertsBtn = document.querySelectorAll('.btn__show-cards');

const pageSection = document.querySelector('.page');
const pageList = pageSection.querySelector('#page__list');

const popup = document.querySelector('.popup');
const delBox = popup.querySelector('.popup__del-box');
const popupProfilePic = popup.querySelector('.popup__change-profile-picture');

const csrf = document.querySelector('#csrf').getAttribute('data-csrf');



/**************************************************************/
//---- Protection against submitting form with large size image

const editForm = popupProfilePic.querySelector('form')
editForm.addEventListener('submit', e=>{

    e.preventDefault()
    const imgInp = editForm.querySelector('#image-input').files[0]

    if(imgInp && imgInp.size>500*1000){
        message(5000, false, 'Too large image . Please reduce image file size')
        return
    }

    e.target.submit()
})



const createForm = createsection.querySelector('.edit__form-container')

createForm.addEventListener('submit', e=>{
    
    e.preventDefault()
    const imgInp = createsection.querySelector('#img-input').files[0]

    if(imgInp && imgInp.size>500*1000){
        message(5000, false, 'Too large image . Please reduce image file size')
        return
    }

    e.target.submit()
})

// -- State of the page -- //
const state = {};


///////////////////////////////////////////
////---- Pagination functionality ----////
//////////////////////////////////////////
state.paginationCls = new pagination(12) // 12 is Element per page

// On clicking other pages
pageList.addEventListener('click', function (e) {
    const target = e.target;
    if (!target.classList.contains('clickable')) { return };

    const pageNum = target.getAttribute('data-num');
    init(pageNum);
});




///////////////////////////////////////////
////---- change Profile pic ----////
//////////////////////////////////////////
changeDp_withoutPopup(createsection);

// When profile picture is changed 
changeDp(edit_dp, popup);



//////////////////////////////
////---- Delete a Guestpert ----////
//////////////////////////////
delBtn.addEventListener('click', function(){

    // Call the del popup box
    delfuncRest(this)
})
cancelfunc();

/////////////////////////////////////
//---- On deleting a Guestpert ----//

delBox.addEventListener('click', async (e)=>{
    
    // Avoid from clicking outside 
    if(!e.target.classList.contains('popup__del-btn'))return;

    
    // Store the id
    state.delId = e.target.getAttribute('data-id');
    if(!state.delId)return;


    // Disappear popup
    removeDelBox();


    // Render Spinner
    loadLargeSpinner(animation);

    // Del request to server
    state.remove = new remove(`/admin/manageGuestperts/deleteGuestpert?id=${state.delId}&&_csrf=${csrf}`);
    state.result = await state.remove.getResults();

    // Remove spinner
    removeSpinner(animation);

    // If error occours
    if (!state.result) {
        
        // display error message
        message(5000, false, 'Your request could not be completed. Please try again later.')
        
        // stop next
        return
    }
    
    // Get active page number
    state.activePage = state.paginationCls.activePageNum();


    // Make the topicSection active
    showBtnClick([topicsSection, pageSection], editSection);

    // Clear the edit section
    clearEdit()

    // Call init
    init(state.activePage);
})



/////////////////////////////////////////
////---- Search bar functionality ----////
/////////////////////////////////////////
const regEx = /^[\w]{1,60}$/i;

searchBar.addEventListener('keyup', function (e) {

    const chk = ['', 8, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 96, 97, 98, 99, 100, 101, 102, 103, 104, 105, 106, 222, 186]
    const k = chk.indexOf(e.keyCode);

    // When ecape is pressed
    if (e.keyCode === 27) {
        e.preventDefault();
        this.value = '';
        hiddenInput.value = '';
        this.blur();
    };

    // Validation 1
    if (!k || k < 0) { return };

    if (this.value.length === 0) {
        hiddenInput.value = '';
        init(1);
    };

    // Validation 2
    if (this.value.length < 2 || !regEx.test(this.value)) { return };

    // Get the value
    const value = this.value;

    // Put text into hidden input
    hiddenInput.value = value;

    // call init
    init(1);
})




///////////////////////////////////////////////
////---- On clicking create and see btns ----////
///////////////////////////////////////////////
createBtn.addEventListener('click', ()=>{
    seeShow_adv([topicsSection, pageSection], createsection);
})

for(const btn of showGuestpertsBtn){
    btn.addEventListener('click', function(){
        showBtnClick([topicsSection, pageSection], this.parentElement.parentElement);
    })
}


///////////////////////////////////////////
////---- On clicking More details ----////
/////////////////////////////////////////
guestpertBox.addEventListener('click', (e)=>{

    // Protect from clicking outside
    if(!e.target.classList.contains('users__card-btn-details'))return;

    moreDetailGuestpert(e.target);
})
closeMoreDetail();




///////////////////////////////////
////---- Editing Guestpert ----////
///////////////////////////////////
guestpertBox.addEventListener('click', async (e)=>{

    // Prevent from clicking outside
    if (!e.target.classList.contains('users__card-btn-edit')) return;

    // store the id
    const id = e.target.getAttribute('data-id');

    // Render Spinner
    loadLargeSpinner(animation);

    // Search
    state.search = new Search(`/admin/manageGuestperts/searchEdit?id=${id}`);
    state.result = await state.search.getResults();
    
    // Remove spinner
    removeSpinner(animation);

    // If error occours
    if (!state.result) {

        // display topics Section
        topicsSection.style.display = 'flex';

        // display error message
        message(5000, false, 'Your request could not be completed. Please try again later.')
        // stop next
        return
    }

    // Render Edit
    renderEdit(state.result.data);

    // Set hrefs in forms
    SetEditForms(state.result.data._id);

    seeShow_adv([topicsSection, pageSection], editSection);
})


//------- Edit Forms -------//

function SetEditForms (id){

    const form__profilePic = popupProfilePic.querySelector('form');
    const form__personal = editSection.querySelector('.edit__form-container--personal');
    const form__contact = editSection.querySelector('.edit__form-container--contact');
    const form__membership = editSection.querySelector('.edit__form-container--memberships');
    const form__credentials = editSection.querySelector('.edit__form-container--credentials');
    const form__biography = editSection.querySelector('.edit__form-container--biography');

    form__profilePic.setAttribute('action', `/admin/editGuestpert/profilePic?id=${id}&&_csrf=${csrf}`)
    form__personal.setAttribute('action', `/admin/editGuestpert/personal?id=${id}&&_csrf=${csrf}`)
    form__contact.setAttribute('action', `/admin/editGuestpert/contact?id=${id}&&_csrf=${csrf}`)
    form__membership.setAttribute('action', `/admin/editGuestpert/membership?id=${id}&&_csrf=${csrf}`)
    form__credentials.setAttribute('action', `/admin/editGuestpert/credentials?id=${id}&&_csrf=${csrf}`)
    form__biography.setAttribute('action', `/admin/editGuestpert/biography?id=${id}&&_csrf=${csrf}`)
}

// --------------------------


///////////////////////////////
////---- init function ----////
///////////////////////////////

// Show cards function
async function init(pageNum) {

    // Get keywords from input
    let keywords = hiddenInput.value;

    // Prepare UI
    prepareForLoad(guestpertBox, animation);

    // Get Data from Server
    state.pageSearch = new Search(`/admin/searchGuestperts/all_info?pageNum=${pageNum}&&keywords=${keywords}`);
    state.pageSearchRes = await state.pageSearch.getResults();
    
    // Remove Spinner
    removeSpinner(animation);

    // Display guestperts cards
    guestpertBox.style.display='flex';
    
    if (!state.pageSearchRes) {

        // Render Error message
        const msg = 'Error in fethcing data'; 
        message(5000, false, msg);

        // Return Error
        return;
    };

    // // Make change in UI
    renderCards(guestpertBox, state.pageSearchRes.data.dataArr);

    // Pagingation
    state.paginationCls.changeVals(+state.pageSearchRes.data.totalItems);
    state.paginationCls.changePage(+state.pageSearchRes.data.pageNum);
}
init(1)


initTinymce({
    selector: "#biography",
    menubar: !1,
    skin: "CUSTOM",
    content_css: "CUSTOM",
    plugins: "preview lists fullscreen link paste",
    paste_as_text: true,
    toolbar: "undo redo preview fullscreen bold italic alignjustify link",
    advlist_bullet_styles: "disc",
})


const errMsg = document.querySelector('.message');
window.addEventListener('load', e=>{
    setTimeout(()=>{
        if(!errMsg)return;
        errMsg.style.display='none';
        errMsg.querySelector('.message__txt').textContent='';
    }, 5000)
})


import { logoutFunc } from './lib/autoLogout'
logoutFunc(() => {
    window.location.href = `/admin_login`
})