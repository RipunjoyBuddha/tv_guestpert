import "../sass/pr_enquiries.scss";
import "./view/importsvg";

import dateformat from 'dateformat';

import Search from './model/search';
import { ratingStar } from './view/star-rating';
import { prepareForLoad, removeSpinner } from './view/spinner';
import { pagination } from './view/pagination_rest';
import { renderCards, showFuture, showPast, showRatingForm, renderEnquiryCards, clearForm } from './view/pg-pr_enquiries';
import { message } from './view/errorMessage';

const state = {}

///////////////////////////////////////////////
////---- Universal Selectors ----////
///////////////////////////////////////////////
const searchBar = document.querySelector('#search-input');
const smileyBox = document.querySelector('.smileybox');
const guestpertBox = document.querySelector('#guestpert-box');
const enquiriesBox = document.querySelector('.allEnquiries');
const pageListGuestperts = document.querySelector('#page__list-guestperts');
const pageListEnquiries = document.querySelector('#page__list-enquiries');
const animation = document.getElementById('animation');
const createBtn = document.querySelector('.btn-createNew');
const backBtnToEnquiries = document.querySelector('.btn-backToEnquiries')
const backBtnToGuestpert = document.querySelector('.btn-backToGuestperts')


const guestpertSection = document.querySelector('.guestperts')
const allEnquiriesSection = document.querySelector('.enquiries-section')
const createSection = document.querySelector('.edit');

const csrf = document.querySelector('#csrf').getAttribute('data-csrf')

///////////////////////////////////////////
////---- Pagination functionality ----////
//////////////////////////////////////////
state.paginationCls_1 = new pagination(24, pageListGuestperts) // 24 is Element per page
state.paginationCls_2 = new pagination(12, pageListEnquiries) // 12 is Element per page

// On clicking other pages
pageListGuestperts.addEventListener('click', function (e) {
    const target = e.target;
    if (!target.classList.contains('clickable')) { return };

    const pageNum = target.getAttribute('data-num');
    init(pageNum);
});

pageListEnquiries.addEventListener('click', function (e) {
    const target = e.target;
    if (!target.classList.contains('clickable')) { return };

    const pageNum = target.getAttribute('data-num');
    getEnquiries(pageNum);
});


//////////////////////////////////////
//---- Clicking Rate Guestpert ----//
allEnquiriesSection.addEventListener('click', e=>{

    if(!e.target.classList.contains('users__card-btn-edit'))return;
    
    const id = e.target.getAttribute('data-id');
    allEnquiriesSection.classList.add('u-display-none');
    showRatingForm(createSection);
    createSection.style.display = 'block';
    createSection.querySelector('.form__submit-btn').textContent = 'update inquiry'
    createSection.querySelector('.edit__form-container--enquiry').setAttribute('action', `/producer/editRating?inquiryId=${id}&&_csrf=${csrf}`);
})


////////////////////////////////////////////
////---- Clicking Guestperts Cards ----////
//////////////////////////////////////////
guestpertBox.addEventListener('click', async e => {

    if (!e.target.closest('.guestpert')) return

    const guestpertId = e.target.closest('.guestpert').getAttribute('data-id')
    if (!guestpertId) return;

    state.selectedGuestpert = guestpertId;

    guestpertSection.classList.add('u-display-none')
    allEnquiriesSection.classList.remove('u-display-none')

    getEnquiries(1)
})



//////////////////////////////////////////////////
////---- Clicking CreateEnquiries Button ----////
////////////////////////////////////////////////
createBtn.addEventListener('click', e => {

    const id = state.selectedGuestpert;
    allEnquiriesSection.classList.add('u-display-none')

    createSection.style.display = 'block'
    createSection.querySelector('.form__submit-btn').textContent = 'send inquiry'
    clearForm(createSection)
    createSection.querySelector('.edit__form-container--enquiry').setAttribute('action', `/producer/createEnquiry?guestpertId=${id}&&_csrf=${csrf}`)
})


////////////////////////////////////////
////---- Clicking Back Buttons ----////
//////////////////////////////////////

backBtnToGuestpert.addEventListener('click', e => {
    guestpertSection.classList.remove('u-display-none')
    allEnquiriesSection.classList.add('u-display-none')

    pageListEnquiries.style.display = 'none'
    enquiriesBox.innerHTML=''

    clearForm(createSection)
})

backBtnToEnquiries.addEventListener('click', () => {
    allEnquiriesSection.classList.remove('u-display-none')
    createSection.style.display = 'none'
})


///////////////////////////////////
////---- Rating of stars ----////
///////////////////////////////////
smileyBox.addEventListener('click', e => {

    if (e.target.classList.contains('rated') || e.target.classList.contains('str-inp')) {

        let input = e.target
        if (e.target.classList.contains('rated')) {
            input = e.target.querySelector('input')
        }
        ratingStar(input)
    }

})

//////////////////////////////////////////////////
////---- On Changing Dates in Create Form ----////
////////////////////////////////////////////////
createSection.querySelector('#airDate').addEventListener('change', e=>{
    const curDate = new Date(e.target.value);
    const today = new Date( dateformat(new Date(), 'yyyy mm dd'));
    const past = curDate<today;
    
    if(past){
        showPast(createSection)
    }else{
        showFuture(createSection)
    }


})


/////////////////////////////////////////
////---- Search bar functionality ----////
/////////////////////////////////////////
const regEx = /^[\w]{1,60}$/i;

searchBar.addEventListener('keyup', function (e) {

    const chk = ['', 8, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 96, 97, 98, 99, 100, 101, 102, 103, 104, 105, 106, 222, 186]
    const k = chk.indexOf(e.keyCode);

    // When ecape is pressed
    if (e.keyCode === 27) {
        e.preventDefault();
        this.value = '';
        searchBar.value = '';
        this.blur();
    };

    // Validation 1
    if (!k || k < 0) { return };

    if (this.value.length === 0) {
        searchBar.value = '';
        init(1);
    };

    // Validation 2
    if (this.value.length < 2 || !regEx.test(this.value)) { return };

    // Get the value
    const value = this.value;

    // Put text into hidden input
    searchBar.value = value;

    // call init
    init(1);
})


////////////////////////////////////////
////---- Get Enquiries function ----////
//////////////////////////////////////
async function getEnquiries(pageNum) {

    let guestpertId = state.selectedGuestpert
    if (!guestpertId || !pageNum) {
        guestpertSection.classList.remove('u-display-none')
        allEnquiriesSection.classList.add('u-display-none')

        clearForm(createSection)
        return
    }

    // Prepare UI
    prepareForLoad(enquiriesBox, animation);

    // Get Data from Server
    const enquirySearch = new Search(`/producer/searchEnquiries?guestpertId=${guestpertId}&&pageNum=${pageNum}`);
    state.enquirySearchRes = await enquirySearch.getResults();

    // Remove Spinner
    removeSpinner(animation);

    if (!state.enquirySearchRes) {

        // Render Error message
        const msg = 'Error in fethcing data';
        message(5000, false, msg);

        guestpertSection.classList.remove('u-display-none')
        allEnquiriesSection.classList.add('u-display-none')

        clearForm(createSection)

        // Return Error
        return;
    };

    renderEnquiryCards(enquiriesBox, state.enquirySearchRes.data.dataArr)
    enquiriesBox.style.display='block'

    // Pagingation
    state.paginationCls_2.changeVals(+state.enquirySearchRes.data.totalItems);
    state.paginationCls_2.changePage(+state.enquirySearchRes.data.pageNum);
}


///////////////////////////////
////---- init function ----////
///////////////////////////////

// Show cards function
async function init(pageNum) {

    // Get keywords from input
    let keywords = searchBar.value;

    // Prepare UI
    prepareForLoad(guestpertBox, animation);

    // Get Data from Server
    state.pageSearch = new Search(`/producer/searchGuestperts/allInfo?pageNum=${pageNum}&&keywords=${keywords}`);
    state.pageSearchRes = await state.pageSearch.getResults();

    // Remove Spinner
    removeSpinner(animation);

    // Display guestperts cards
    guestpertBox.style.display = 'flex';

    if (!state.pageSearchRes) {

        // Render Error message
        const msg = 'Error in fethcing data';
        message(5000, false, msg);

        // Return Error
        return;
    };

    // // Make change in UI
    renderCards(guestpertBox, state.pageSearchRes.data.dataArr);

    // Pagingation
    state.paginationCls_1.changeVals(+state.pageSearchRes.data.totalItems);
    state.paginationCls_1.changePage(+state.pageSearchRes.data.pageNum);
}
init(1)


const errMsg = document.querySelector('.message');
window.addEventListener('load', e => {
    setTimeout(() => {
        if (!errMsg) return;
        errMsg.style.display = 'none';
        errMsg.querySelector('.message__txt').textContent = '';
    }, 5000)
})

import { logoutFunc } from './lib/autoLogout'
logoutFunc(() => {
    window.location.href = `/login`
})