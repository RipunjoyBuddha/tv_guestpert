import "../sass/ad_jaquiesAppearance.scss";
import svg from "./view/importsvg";

import {seeShow} from "./view/seeShowbtns";
import {editAppearance, removeAppearance} from "./view/editField";
import { message } from "./view/errorMessage";
import {delfuncAdv, cancelfunc} from "./view/deleteBox";
import { hamburger, slideMenu } from "./view/sidebar";

///////////////////////////////////////////
///--- Hamburger and Sidebar functionality ---///
///////////////////////////////////////////
hamburger();
slideMenu();

//////////////////////////////////////
////---- Universal Selectors ----////
////////////////////////////////////
const topicsSection = document.querySelector('.slides');
const editsection = document.querySelector('.edit');
const createBtn = document.querySelector('.btn-createNew');
const form = editsection.querySelector('.edit__form-container');
const animeBox = document.getElementById('animations');
const backBtn = editsection.querySelector('.edit__show-cards');
const csrf = document.querySelector('#csrf').getAttribute('data-csrf');

///////////////////////////////////////////////
////---- On clicking create and see btns ----////
///////////////////////////////////////////////
seeShow(topicsSection, editsection);
createBtn.addEventListener('click', e=>{
    form.setAttribute('action', `/admin/createFounderAppearance?_csrf=${csrf}`);
})


/////////////////////////////////////////
////---- On clicking delete btn ----////
///////////////////////////////////////
topicsSection.addEventListener('click', (e)=>{
    
    if(!e.target.classList.contains('slides__delete-btn')){
        return;
    }

    delfuncAdv(e.target);
})

///////////////////////////////////////////////
////---- On clicking cancel btn ----////
///////////////////////////////////////////////
cancelfunc();


///////////////////////////////////////
////---- On clicking edit btn ----////
//////////////////////////////////////
// editAppearance('slides__edit-btn', editsection , topicsSection, form);

const state ={};

import Search from './model/search';
import { prepareForLoad, removeSpinner } from './view/spinner';

// Edit functionality
topicsSection.addEventListener('click', async (e) => {

    // Prevent from clicking outside
    if (!e.target.classList.contains('slides__edit-btn')) return;

    // store the button
    const btn = e.target;
    if (!btn) { return };

    // store the id
    const id = btn.getAttribute('data-id');
    
    // // Render Spinner
    prepareForLoad(topicsSection, animeBox);

    // Search
    state.search = new Search(`/admin/founderAppearance/edit?id=${id}`);
    state.result = await state.search.getResults();

    // Remove spinner
    removeSpinner(animeBox);

    // If error occours
    if (!state.result) {

        // display topics Section
        topicsSection.style.display = 'flex';

        // display error message
        message(5000, false, 'Your request could not be completed. Please try again later.')
        // stop next
        return
    }

    form.setAttribute('action',  `/admin/editFounderAppearance?id=${id}&&_csrf=${csrf}`);

    // Render Edit
    editAppearance(state.result.data.data, editsection, topicsSection);

    // // Change the edit mode
    backBtn.setAttribute('data-mode', 'edit');
})

backBtn.addEventListener('click', e=>{
    if(e.target.getAttribute('data-mode')!== 'edit')return;

    removeAppearance(editsection);
})


const errMsg = document.querySelector('.message');

window.addEventListener('load', e=>{
    setTimeout(()=>{
        if(!errMsg)return;
        errMsg.style.display='none';
        errMsg.querySelector('.message__txt').textContent='';
    }, 5000)
})


import { logoutFunc } from './lib/autoLogout'
logoutFunc(() => {
    window.location.href = `/admin_login`
})