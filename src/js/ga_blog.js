import "../sass/ga_blog.scss";
import "./view/importsvg";
import {seeShow_adv, showBtnClick} from "./view/seeShowbtns";
import { chooseImg } from "./view/choose_img";
import {delfunc, cancelfunc} from "./view/deleteBox";
import {initTinymce} from './utils/tinyActions';
import uniqid from 'uniqid';
import {message} from './view/errorMessage'


//////////////////////////////////////
////---- Universal Selectors ----////
////////////////////////////////////

const createBtn = document.querySelector('.btn-createNew');
const form = document.querySelector('.edit__form-container');
const csrf = document.querySelector('#csrf').getAttribute('data-csrf');
const guestpertId = document.querySelector('#guestpertId').getAttribute('data-guestpertId')

const topicsSection = document.querySelector('.blogs');
const editSection = document.getElementById('edit');
const pageId = uniqid();

/********* Edit Form **********/
form.addEventListener('submit', e=>{

    e.preventDefault()
    const imgFile = form.querySelector('#choose-img-file').files[0]

    if(imgFile && imgFile.size > 500*1000){
        message(5000, false, 'Too large image file. Please reduce image file size')
        return
    }

    e.target.submit()
})

///////////////////////////////////////////////
////---- On clicking create button ----////
///////////////////////////////////////////////

createBtn.addEventListener('click', e=>{
    form.setAttribute('action', `/guestpert/createBlogs?_csrf=${csrf}&&guestpertId=${guestpertId}&&tempId=${pageId}`)
    seeShow_adv([topicsSection], editSection)
})


/////////////////////////////////////////////
//---- choose image functionality ----//
/////////////////////////////////////////////
const topBox = document.getElementById('inpFile-box');
chooseImg(topBox);


///////////////////////////////////////////////
////---- On clicking delete btn ----////
///////////////////////////////////////////////
for (const delBtn of document.querySelectorAll('.blogs__card-btn-delete')) {
    delfunc(delBtn)
}

////////////////////////////////////////
////---- On clicking cancel btn ----////
////////////////////////////////////////
cancelfunc();


///////////////////////////////////////
//////////////////////////////////////
/////---- REST Features ----////
/////////////////////////////////////
////////////////////////////////////

import Search from './model/search';
import remove from './model/deleteCard';
import {prepareForLoad, loadLargeSpinner, removeSpinner} from './view/spinner';
import {renderEdit, removeFields, renderError} from './view/pg-ga_manageBlog';


//////////////////////////////////////
////---- Universal Selectors ----////
/////////////////////////////////////
const blogSection = document.getElementById('blog-box');

const animeBox = document.getElementById('animations');


// State of the Guestpert pannel blog page
const state = {};


///////////////////////////////////
// --- Clicking the edit btn ---//
////////////////////////////////
blogSection.addEventListener('click', async function(e){

    // Don't do anything if clicked outside
    if(!e.target.classList.contains('blogs__card-btn-edit')){return};

    // store the button
    const btn = e.target;
    if(!btn){return};

    // store the id
    const id = btn.getAttribute('data-id');

    // Render Spinner
    prepareForLoad(blogSection.parentElement, animeBox);
    
    // Search
    state.search = new Search(`/guestpert/manageBlogs/edit?id=${id}`);
    state.result = await state.search.getResults();

    // If error occours
    if(!state.result){
        // Remove spinner
        removeSpinner(animeBox);
     
        // call error function
        renderError(blogSection.parentElement);
        
        // stop next
        return
    }

    // Remove spinner
    removeSpinner(animeBox);

    // Render Edit
    renderEdit(editSection, state.result.data);

    // Set form url
    form.setAttribute('action', `/guestpert/editBlog?id=${id}&&_csrf=${csrf}&&guestpertId=${guestpertId}&&tempId=${pageId}`)
});


/////////////////////////////////////////
// --- Clicking the show blogs btn ---//
///////////////////////////////////////

document.querySelector('.edit__show-cards').addEventListener('click', async function(){

    // Remove Fields
    removeFields(editSection);

    // Remove class from button
    this.classList.remove('editMode');

    // Render Spinner
    loadLargeSpinner(animeBox);

    // Delete the array of images with the tempId
    const removeTempArr = new remove(`/guestpert/tempBlogsArr/delete?tempId=${pageId}&&_csrf=${csrf}`);
    await removeTempArr.getResults();

    // Render Spinner
    removeSpinner(animeBox);

    // Setting Form url
    form.setAttribute('action', '')

    showBtnClick([topicsSection], editSection)
})


///////////////////////////////
// --- tiny mce settings ---//
///////////////////////////////


initTinymce(
    { 
        selector: "#content",
        menubar: !1,
        skin: "CUSTOM",
        content_css: "CUSTOM",
        plugins: "preview image lists fullscreen link paste",
        paste_as_text: true,
        toolbar: "undo redo image preview fullscreen bullist bold italic alignjustify link",
        advlist_bullet_styles: "disc",
        images_upload_url: `/guestpert/uploadBlogPic?tempId=${pageId}&&_csrf=${csrf}`,
        image_dimensions: false,
    }
)


const errMsg = document.querySelector('.message');

window.addEventListener('load', e=>{
    setTimeout(()=>{
        if(!errMsg)return;
        errMsg.style.display='none';
        errMsg.querySelector('.message__txt').textContent='';
    }, 5000)
})

import { logoutFunc } from './lib/autoLogout'
logoutFunc(() => {
    window.location.href = `/login`
})