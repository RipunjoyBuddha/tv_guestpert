import "../sass/ad_manageProducts.scss";
import "./view/importsvg";
import {removeThirdParty} from "./vendor/form-dopdown_manageProds";

import { loadSpinner, removeSpinner, loadLargeSpinner } from "./view/spinner";
import Search from "./model/search";
import addCard from "./model/addToRequest";
import { pagination } from "./view/pagination_rest";
import { message } from "./view/errorMessage";
import { renderActiveCards, renderInactiveCards, renderEdit, removeDel, renderRemove } from "./view/pg-ad_manageProducts";
import { seeShow_adv, showBtnClick } from "./view/seeShowbtns";
import {delfuncRest, cancelfunc, removeDelBox} from "./view/deleteBox";
import {chooseImg} from "./view/choose_img";
import { hamburger, slideMenu } from "./view/sidebar";
import {initTinymce} from "./utils/tinyActions";

///////////////////////////////////////////
///--- Hamburger and Sidebar functionality ---///
///////////////////////////////////////////
hamburger();
slideMenu();


//////////////////////////////////
//---- Universal selectors ----//
////////////////////////////////

const activeProds = document.querySelector('.active-products');
const activeSearchInput = activeProds.querySelector('#active-products-search');
const activeHiddenInput = activeProds.querySelector('#active-hidden-input');
const activeBox = activeProds.querySelector('#activeCards-box');
const activePageList = document.querySelector('#page-activeProd-list');

const inactiveProds = document.querySelector('.inactive-products');
const inactiveSearchInput = inactiveProds.querySelector('#inactive-products-search');
const inactiveHiddenInput = inactiveProds.querySelector('#inactive-hidden-input');
const inactiveBox = inactiveProds.querySelector('#inactiveCards-box');
const inactivePageList = document.querySelector('#page-inactiveProd-list');

const editSection = document.querySelector('.edit');
const chooseImgBox = editSection.querySelector('#inpFile-box');
const prodForm = editSection.querySelector('.edit__form-container');
const thirdPartyBox = editSection.querySelector('.thirdparty-box');

const createBtn = document.querySelector('.btn-createNew');
const backBtn = document.querySelector('.edit__show-cards');
const animeBox = document.getElementById('animation');
const popup = document.querySelector('.popup');
const delBox = popup.querySelector('.popup__del-box');
const csrf = document.querySelector('#csrf').getAttribute('data-csrf');



////////////////////////////////////////
//---- State of the current Page ----//
//////////////////////////////////////
const state = {};


//--- Choose Image functionality
chooseImg(chooseImgBox);

editSection.addEventListener('submit', e=>{
    e.preventDefault()

    const imgInput = chooseImgBox.querySelector('#choose-img-file').files[0]
    
    if(imgInput && imgInput.size > 200*1000){
        message(5000, false, 'Too large image file. Please reduce image size')
        return
    }

    e.target.submit()
})

///////////////////////////////////////////
////---- Pagination functionality ----////
//////////////////////////////////////////
state.activePaginationCls = new pagination(12, activePageList) // 12 is Element per page
state.inactivePaginationCls = new pagination(12, inactivePageList) // 12 is Element per page

// On clicking other pages
activePageList.addEventListener('click', function (e) {
    const target = e.target;

    if (!target.classList.contains('clickable')) { return };
    const pageNum = target.getAttribute('data-num');
    initActiveProds(pageNum)
});

inactivePageList.addEventListener('click', function (e) {
    const target = e.target;

    if (!target.classList.contains('clickable')) { return };
    const pageNum = target.getAttribute('data-num');
    initInActiveProds(pageNum)
});




//////////////////////////////////////////
////---- Search bar functionality ----////
/////////////////////////////////////////
const regEx = /^[\w]{1,60}$/i;

activeSearchInput.addEventListener('keyup', function (e) {

    const chk = ['', 8, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 96, 97, 98, 99, 100, 101, 102, 103, 104, 105, 106, 222, 186]
    const k = chk.indexOf(e.keyCode);

    // When ecape is pressed
    if (e.keyCode === 27) {
        e.preventDefault();
        this.value = '';
        activeHiddenInput.value = '';
        this.blur();
    };

    // Validation 1
    if (!k || k < 0) { return };

    if (this.value.length === 0) {
        activeHiddenInput.value = '';
        initActiveProds(1);
    };

    // Validation 2
    if (this.value.length < 2 || !regEx.test(this.value)) { return };

    // Get the value
    const value = this.value;

    // Put text into hidden input
    activeHiddenInput.value = value;

    // call init
    initActiveProds(1);
})

inactiveSearchInput.addEventListener('keyup', function (e) {

    const chk = ['', 8, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 96, 97, 98, 99, 100, 101, 102, 103, 104, 105, 106, 222, 186]
    const k = chk.indexOf(e.keyCode);

    // When ecape is pressed
    if (e.keyCode === 27) {
        e.preventDefault();
        this.value = '';
        inactiveHiddenInput.value = '';
        this.blur();
    };

    // Validation 1
    if (!k || k < 0) { return };

    if (this.value.length === 0) {
        inactiveHiddenInput.value = '';
        initInActiveProds(1);
    };

    // Validation 2
    if (this.value.length < 2 || !regEx.test(this.value)) { return };

    // Get the value
    const value = this.value;

    // Put text into hidden input
    inactiveHiddenInput.value = value;

    // call init
    initInActiveProds(1);
})



/////////////////////////////////////////////
//---- Clicking create Btn & Back btn----//
//////////////////////////////////////////
createBtn.addEventListener('click', () => {

    seeShow_adv([activeProds, inactiveProds, activePageList.parentElement, inactivePageList.parentElement], editSection);
   
    prodForm.setAttribute('action', `/admin/createProduct?_csrf=${csrf}`);
})

backBtn.addEventListener('click', function () {
    
    if (this.getAttribute('data-mode') === 'edit') {
        // Call Remove function
        removeFunc();
        removeThirdParty(thirdPartyBox);
        
    } else if (this.getAttribute('data-mode') === 'add-to-shop') {
        
        // Clear Fields
        renderRemove();
        
        // Set form submit btn
        editSection.querySelector('.form__submit-btn').textContent = 'Save'
        
        // Take out the edit Mode
        backBtn.setAttribute('data-mode', '');
    }

    // Set action to null
    prodForm.setAttribute('action', '');
    
    // Show all products
    showBtnClick([activeProds, inactiveProds, activePageList.parentElement, inactivePageList.parentElement], editSection);
})


const removeFunc = () => {

    // Remove all edit fields
    renderRemove();

    // Take out the edit Mode
    backBtn.setAttribute('data-mode', '');
}



///////////////////////////////////
//---- clicking edit button ----//
/////////////////////////////////
activeBox.addEventListener('click', async (e) => {

    // Prevent from clicking outside
    if (!e.target.classList.contains('products__card-btn-edit')) return;

    // store the id
    const id = e.target.getAttribute('data-id');

    // Render Spinner
    loadLargeSpinner(animeBox);

    // Search
    state.editSearch = new Search(`/admin/manageProducts/edit?id=${id}`);
    state.editSearchRes = await state.editSearch.getResults();

    // Remove spinner
    removeSpinner(animeBox);

    // If error occours
    if (!state.editSearchRes) {

        // display error message
        message(5000, false, 'Your request could not be completed. Please try again later.')
        // stop next
        return
    }

    // Render Edit
    renderEdit(state.editSearchRes.data, thirdPartyBox);

    // Puting data in back btn
    backBtn.setAttribute('data-mode', 'edit');


    // Make edit section visible
    seeShow_adv([activeProds, inactiveProds, activePageList.parentElement, inactivePageList.parentElement], editSection);
    
    prodForm.setAttribute('action', `/admin/editProduct?id=${id}&&_csrf=${csrf}`);
})



/////////////////////////////////////////////
//---- clicking delete product button ----//
///////////////////////////////////////////
activeBox.addEventListener('click' , async e=>{

    // Protect from clicking outside
    if(!e.target.classList.contains('products__card-btn-remove'))return;

    delfuncRest(e.target);
})

cancelfunc();




///////////////////////////////////
//---- clicking remove button ----//
/////////////////////////////////
delBox.addEventListener('click', async (e) => {

    // Prevent from clicking outside
    if (!e.target.classList.contains('popup__del-btn')) return;

    // store the id
    const id = e.target.getAttribute('data-id');

    // Remove Del box
    removeDelBox();

    // Render Spinner
    loadLargeSpinner(animeBox);

    // Search
    state.removeFromShop = new addCard(`/admin/manageProducts/remove?id=${id}&&_csrf=${csrf}`);
    state.removeCardRes = await state.removeFromShop.getResults();

    // Remove spinner
    removeSpinner(animeBox);

    // If error occours
    if (!state.removeCardRes) {

        // display error message
        message(5000, false, 'Your request could not be completed. Please try again later.')
        // stop next
        return
    }

    // Show success message
    message(5000, true, 'Product successfully removed from shop');

    // get active pages

    
    initActiveProds(state.activePaginationCls.activePageNum());
    initInActiveProds(state.inactivePaginationCls.activePageNum());
})




///////////////////////////////////////////
//---- clicking Add to Shop Button ----//
////////////////////////////////////////

inactiveBox.addEventListener('click', async (e) => {

    // Prevent from clicking outside
    if (!e.target.classList.contains('button__oval')) return;

    // store the id
    const id = e.target.getAttribute('data-id');

    // Render Spinner
    loadLargeSpinner(animeBox);

    // Search
    state.editSearch = new Search(`/admin/manageProducts/addBook?id=${id}`);
    state.editSearchRes = await state.editSearch.getResults();

    // Remove spinner
    removeSpinner(animeBox);

    // If error occours
    if (!state.editSearchRes) {

        // display error message
        message(5000, false, 'Your request could not be completed. Please try again later.')
        // stop next
        return
    }

    // Render Edit
    renderEdit(state.editSearchRes.data, thirdPartyBox);
    
    // Puting data in back btn
    backBtn.setAttribute('data-mode', 'add-to-shop');

    // Set form submit btn
    editSection.querySelector('.form__submit-btn').textContent = 'Add to Shop'

    // Make edit section visible
    seeShow_adv([activeProds, inactiveProds, activePageList.parentElement, inactivePageList.parentElement], editSection);
    prodForm.setAttribute('action', `/admin/createProduct?_csrf=${csrf}`);
})


////////////////////////////
//---- Init functions ----//
////////////////////////////
async function initActiveProds(pageNum) {

    // Get keywords from input
    let keywords = activeHiddenInput.value;

    // Prepare UI
    loadSpinner(activeBox);

    // Get Data from Server
    state.activeProdsSearch = new Search(`/admin/searchProducts/active?pageNum=${pageNum}&&keywords=${keywords}`);
    state.activeProdsRes = await state.activeProdsSearch.getResults();

    // Remove Spinner
    removeSpinner(activeBox);

    if (!state.activeProdsRes) {

        // Render Error message
        const msg = 'Couldn\'t fetch active products. Please reload';
        message(5000, false, msg);

        // Return Error
        return;
    };

    // // Make change in UI
    renderActiveCards(activeBox, state.activeProdsRes.data.dataArr);

    // Pagingation
    state.activePaginationCls.changeVals(+state.activeProdsRes.data.totalItems);
    state.activePaginationCls.changePage(+state.activeProdsRes.data.pageNum);
}
initActiveProds(1)



async function initInActiveProds(pageNum) {

    // Get keywords from input
    let keywords = inactiveHiddenInput.value;

    // Prepare UI
    loadSpinner(inactiveBox);

    // Get Data from Server
    state.inactiveProdsSearch = new Search(`/admin/searchProducts/inactive?pageNum=${pageNum}&&keywords=${keywords}`);
    state.inactiveProdsRes = await state.inactiveProdsSearch.getResults();

    // Remove Spinner
    removeSpinner(inactiveBox);

    if (!state.inactiveProdsRes) {

        // Render Error message
        const msg = 'Couldn\'t fetch inactive products. Please reload';
        message(5000, false, msg);

        // Return Error
        return;
    };

    // // Make change in UI
    renderInactiveCards(inactiveBox, state.inactiveProdsRes.data.dataArr);

    // Pagingation
    state.inactivePaginationCls.changeVals(+state.inactiveProdsRes.data.totalItems);
    state.inactivePaginationCls.changePage(+state.inactiveProdsRes.data.pageNum);
}
initInActiveProds(1)



const errMsg = document.querySelector('.message');
window.addEventListener('load', e=>{
    setTimeout(()=>{
        if(!errMsg)return;
        errMsg.style.display='none';
        errMsg.querySelector('.message__txt').textContent='';
    }, 5000)
})


initTinymce(
    {
        selector: '#productDescription',
        menubar: !1,
        skin: "CUSTOM",
        content_css: "CUSTOM",
        plugins: " preview lists fullscreen paste",
        paste_as_text: true,
        toolbar: "undo redo preview fullscreen bold bullist italic alignjustify",
        advlist_bullet_styles: "disc",
    }
);


import { logoutFunc } from './lib/autoLogout'
logoutFunc(() => {
    window.location.href = `/admin_login`
})