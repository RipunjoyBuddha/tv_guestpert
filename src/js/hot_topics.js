import "../sass/hot_topics.scss";
import svg from "./view/importsvg";

import {filter} from "./view/customFilter";

///////////////////////////////////////////////
////---- Universal Selectors ----////
///////////////////////////////////////////////
const searchBar = document.querySelector('#search-input');
const cards = document.querySelectorAll('.hot-topics__cards');
// const notFoundMsg = document.querySelector('.addnew').querySelector('.paragraph__not-found');


///////////////////////////////////////////////
////---- Custom Filter ----////
///////////////////////////////////////////////
filter(searchBar, cards, 99999 , '.hot-topics__title', false);

const logoutBtn = document.querySelector('.header-bar__link-logout')
import { logoutFunc } from './lib/autoLogout'
if(logoutBtn){
    logoutFunc(() => {
        const btnId = logoutBtn.getAttribute('data-id')
        if(btnId=='adtyspnce'){
            window.location.href = `/admin_login`
        }else{
            window.location.href = `/login`
        }
    })
} 