import "../sass/shop.scss";
import "./view/importsvg"; 

import {pagination} from './view/pagination_rest';

// -- State of the page -- //
const state = {};


const pageSection = document.querySelector('.page');
const pageList = pageSection.querySelector('#page__list');

///////////////////////////////////////////
////---- Pagination functionality ----////
//////////////////////////////////////////
state.paginationCls = new pagination(12) // 12 is Element per page

window.addEventListener('load', ()=>{

    // Get Total items
    state.totalItems = document.querySelector('#totalItems').getAttribute('data-totalItems')
    state.activePage = document.querySelector('#activePage').getAttribute('data-activePage')
    
    state.totalItems==null?state.totalItems=0:''
    state.activePage==null?state.activePage=1:''

    state.paginationCls.changeVals(+state.totalItems);
    state.paginationCls.changePage(+state.activePage);

})

const init = (pageNum)=>{
    if(!pageNum)return
    window.location.href = `/shop?pageNum=${pageNum}`;
}

// On clicking other pages
pageList.addEventListener('click', function (e) {
    const target = e.target;
    if (!target.classList.contains('clickable')) { return };

    const pageNum = target.getAttribute('data-num');
    init(pageNum);
});


const logoutBtn = document.querySelector('.header-bar__link-logout')
import { logoutFunc } from './lib/autoLogout'
if(logoutBtn){
    logoutFunc(() => {
        const btnId = logoutBtn.getAttribute('data-id')
        if(btnId=='adtyspnce'){
            window.location.href = `/admin_login`
        }else{
            window.location.href = `/login`
        }
    })
} 