import "../sass/ga_dashboard.scss";
import "./view/importsvg"

const state = { click: !1 }
const csrf = document.querySelector("#csrf").getAttribute("data-csrf")
const infoBox = document.querySelector(".info")

if(infoBox){
    const infoTxt = infoBox.querySelector(".info__msg") 
    const infoBtn = infoBox.querySelector(".info__btn")
    
    infoBox.addEventListener("click", async function (t) { 
        if (!state.click && t.target.classList.contains("info__btn")) {
            state.click = !0, state.result = null; try { state.fetchRes = await fetch("/user/verifyAccount?_csrf=".concat(csrf), { method: "PUT" }), state.result = await state.fetchRes.json() } catch (t) { state.result = null } state.result ? (infoTxt.textContent = "An email have been sent for verification", infoBtn.style.display = "none") : state.click = !1 
        } 
    });
}

import { logoutFunc } from './lib/autoLogout'
logoutFunc(() => {
    window.location.href = `/login`
})