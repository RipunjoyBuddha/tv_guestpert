for (const wrapper of document.querySelectorAll('.custom-select-wrapper')) {
    wrapper.addEventListener('click', function () {
        this.querySelector('.custom-select').classList.toggle('open');
    })
}


for (const select of document.querySelectorAll('.custom-select-wrapper')) {
    for (const option of select.querySelectorAll(".custom-option")) {
        option.addEventListener('click', function () {
            if (!this.classList.contains('selected')) {
                if (this.parentNode.querySelector('.custom-option.selected')) {
                    this.parentNode.querySelector('.custom-option.selected').classList.remove('selected');
                }
                this.classList.add('selected');
                clickTask(this);
            }
        })
    }
}



window.addEventListener('keydown', (e) => {
    if (e.keyCode === 38 || e.keyCode === 40 || e.keyCode === 13) {
        for (const select of document.querySelectorAll('.custom-select-wrapper')) {
            if (select.querySelector('.custom-select').classList.contains('open')) {
                e.preventDefault();

                ////--- When down arrow is pressed ---///
                if (e.keyCode === 40) {
                    const option = select.querySelectorAll('.custom-option');
                    if (option && option.length <= 0) { return };

                    let index = 0;
                    if (index == undefined) { index = 0 };

                    option.forEach((cur, i) => {
                        if (cur.classList.contains('selected')) {
                            if (i < option.length - 1) {
                                index = +i + 1;
                                cur.classList.remove('selected');
                            }
                            else {
                                index = +i;
                            }
                        }
                    })

                    if (index === option.length) { return };
                    keyDowntask(option, index);
                }

                ////--- When up arrow is pressed ---///
                else if (e.keyCode === 38) {
                    const option = select.querySelectorAll('.custom-option');
                    if (option.length <= 0) { return };

                    let index;
                    option.forEach((cur, i) => {
                        if (cur.classList.contains('selected')) {
                            if (i > 0) {
                                index = +i - 1;
                                cur.classList.remove('selected');
                            }
                            else {
                                index = 0;
                            }
                        }
                    });

                    if (index < 0 || index === undefined) { return };
                    keyDowntask(option, index);
                }


                ///--- Enter is pressed ---///
                else if (e.keyCode === 13) {
                    for (const select of document.querySelectorAll('.custom-select')) {
                        if (select.classList.contains('open')) {
                            select.classList.remove('open');
                        }
                    }
                }
            }
        }
    }
})



window.addEventListener('click', function (e) {
    for (const select of document.querySelectorAll('.custom-select-wrapper')) {
        if (!select.contains(e.target)) {
            select.querySelector('.custom-select').classList.remove('open');
        }
    }
});


function clickTask(thisEl) {
    thisEl.closest('.custom-select').querySelector('.custom-select__trigger span').textContent = thisEl.textContent;
    thisEl.parentElement.parentElement.parentElement.parentElement.querySelector('#inp-category').value = thisEl.textContent;
}


function keyDowntask(option, index) {
    if (index >= 0) {
        scroll(option[index].parentElement, index, 'custom-option');
        option[index].classList.add("selected");
        option[index].closest('.custom-select').querySelector('.custom-select__trigger span').textContent = option[index].textContent;
        option[index].parentElement.parentElement.parentElement.parentElement.querySelector('#inp-category').value = option[index].textContent;
    }
}

function scroll(parent, index, childClass) {
    const childGrp = parent.querySelectorAll(`.${childClass}`);
    const childHeight = childGrp[index].offsetHeight;
    const scrollPercentage = Math.round(parent.scrollTop / (parent.scrollHeight - parent.offsetHeight) * 100);
    const childTopul = childGrp[index].offsetTop;
    const childTopll = childTopul + childHeight;
    const scrollArea = parent.scrollHeight - parent.offsetHeight;
    const scrollul = Math.round((scrollPercentage / 100) * scrollArea);
    const scrollll = Math.round((scrollPercentage / 100) * scrollArea + parent.offsetHeight);

    if (childTopll > scrollll) {
        const scrollAmount = childTopll + 3 - parent.offsetHeight;
        parent.scroll(0, scrollAmount);
    }
    else if (childTopul < scrollul) {
        const scrollAmount = childTopul - 2;
        parent.scroll(0, scrollAmount);
    }
}


export class updateContent {
    constructor(parentEl) {
        this.parentEl=parentEl;
        this.hiddenInput = parentEl.parentElement.querySelector('#inp-category');
        this.triggerSelect = parentEl.querySelector('.custom-select__trigger span');
        this.allCustomOpts = parentEl.querySelectorAll('.custom-option');
    }
    
    setContent(data='') {
        
        this.data = data.toLowerCase();
        this.customOption = this.parentEl.querySelector(`.custom-option[data-key="${this.data}"]`);

        if(!this.customOption){
            // console.log('Error . No custom option found');
            return
        }

        // Remove selected class from every option
        this.allCustomOpts.forEach(c=>c.classList.remove('selected'));

        // Set the content
        this.hiddenInput.value = this.data;
        this.triggerSelect.textContent = this.data;
        this.customOption.classList.add('selected');
    }

    removeContent(){
        // Remove the content
        this.hiddenInput.value = '';
        this.triggerSelect.textContent = 'Select a Category';
        this.allCustomOpts.forEach(c=>c.classList.remove('selected'));
    }
}