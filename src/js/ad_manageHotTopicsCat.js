import "../sass/ad_manage_hotTopicsCat.scss";
import "./view/importsvg";

import {pagination, showCards} from "./view/pagination";
import {seeShow} from "./view/seeShowbtns";
import {ht_Categories} from "./view/editField";
import {chooseImg} from "./view/choose_img";
import { hamburger, slideMenu } from "./view/sidebar";
import {message} from './view/errorMessage'

///////////////////////////////////////////
///--- Hamburger and Sidebar functionality ---///
///////////////////////////////////////////
hamburger();
slideMenu();

///////////////////////////////////////////////
////---- Universal Selectors ----////
///////////////////////////////////////////////
const cardsSection = document.querySelector('.ht-categories');
const cards = cardsSection.querySelectorAll('.ht-categories__card');
const pageSection = document.querySelector('.page');
const csrf = document.querySelector('#csrf').getAttribute('data-csrf');
const inputFile = document.querySelector('#inpFile-box');
const mainForm = document.querySelector('#createEdit_form')

mainForm.addEventListener('submit', e=>{

    e.preventDefault()
    const imgInp = mainForm.querySelector('#choose-img-file').files[0]

    if(imgInp && imgInp.size>200*1000){
        message(5000, false, 'Too large Image file, Please reduce it.')
        return
    }
    e.target.submit()
})

///////////////////////////////////////////////
////---- Pagination functionality ----////
///////////////////////////////////////////////
const cardNumb = cards.length;

// Show cards function
const pageFunc = showCards(cards);

// Calling the class pagination 
new pagination(cardNumb, 28, pageFunc) // cardNumb is total cards, 2 is limit to show , Show Cards is the callback

///////////////////////////////////////////////
////---- Choose Img functionality----////
///////////////////////////////////////////////
chooseImg(inputFile)


///////////////////////////////////////////////
////---- On clicking create and see btns ----////
///////////////////////////////////////////////
seeShow(cardsSection);
seeShow(pageSection);



///////////////////////////////////////////////
////---- On clicking edit btn ----////
///////////////////////////////////////////////
const editBtns = cardsSection.querySelectorAll('.btn-ht-edit');
const editSection = document.querySelector('.edit');
const form = editSection.querySelector('#createEdit_form');

ht_Categories(editBtns, editSection, cardsSection, pageSection, csrf )

// Manupulating form action
document.querySelector('.btn-createNew').addEventListener('click', e=>{
    form.setAttribute('action', `/admin/createCategoryHotTopics?_csrf=${csrf}`)
})


import { logoutFunc } from './lib/autoLogout'
logoutFunc(() => {
    window.location.href = `/admin_login`
})