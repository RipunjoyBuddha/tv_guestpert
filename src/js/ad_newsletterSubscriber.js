import "../sass/ad_newsletterSubscriber.scss";
import "./view/importsvg";

import {pagination} from "./view/pagination_rest";
import {loadSpinner, removeSpinner, loadLargeSpinner} from "./view/spinner";
import Search from './model/search';
import remove from './model/deleteCard';
import { message } from './view/errorMessage';
import {delfuncRest, cancelfunc, removeDelBox} from './view/deleteBox';
import {renderCards} from "./view/pg-ad_newsletterSubscribers";
import addCard from "./model/addToRequest";
import { hamburger, slideMenu } from "./view/sidebar";

///////////////////////////////////////////
///--- Hamburger and Sidebar functionality ---///
///////////////////////////////////////////
hamburger();
slideMenu();


///////////////////////////////////////////////
////---- Universal Selectors ----////
///////////////////////////////////////////////
const topicSection = document.querySelector('#newsletter-subs__box');
const searchBar = document.querySelector('#search-input');
const hiddenInput = searchBar.parentElement.querySelector('.form__hidden-input');
const pageList = document.querySelector('.page__list');
const animation = document.getElementById('animation');
const delBox = document.querySelector('.popup__del-box')
const csrf = document.querySelector('#csrf').getAttribute('data-csrf');

// State of the guestpert Page
const state = {};


///////////////////////////////////////////////
////---- Pagination functionality ----////
///////////////////////////////////////////////
state.paginationCls = new pagination(24) // 2 is Element per page
init(1);


// On clicking other pages
pageList.addEventListener('click', function (e) {
    const target = e.target;
    if (!target.classList.contains('clickable')) { return };

    const pageNum = target.getAttribute('data-num');
    init(pageNum);
});


///////////////////////////////////////////
////---- Search bar functionality ----////
/////////////////////////////////////////
const regEx = /^[\w]{1,60}$/i;

searchBar.addEventListener('keyup', function (e) {

    const chk = ['', 8, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 96, 97, 98, 99, 100, 101, 102, 103, 104, 105, 106, 222, 186]
    const k = chk.indexOf(e.keyCode);

    // When ecape is pressed
    if (e.keyCode === 27) {
        e.preventDefault();
        this.value = '';
        hiddenInput.value = '';
        this.blur();
    };

    // Validation 1
    if (!k || k < 0) { return };

    if (this.value.length === 0) {
        hiddenInput.value = '';
        init(1);
    };

    // Validation 2
    if (this.value.length < 2 || !regEx.test(this.value)) { return };

    // Get the value
    const value = this.value;

    // Put text into hidden input
    hiddenInput.value = value;

    // call init
    init(1);
})




///////////////////////////////////
////---- Remove Subscriber ----////
///////////////////////////////////
topicSection.addEventListener('click', (e) => {

    // Prevent from clicking outside
    if (!e.target.classList.contains('newsletter-subscriber__card-btn-remove')) { return };
    
    // Store the card
    state.activeCard = e.target.closest('.newsletter-subscriber__card');
    
    // Call del function
    delfuncRest(e.target)
});

cancelfunc();


///////////////////////////////////
////---- Block Subscriber ----////
///////////////////////////////////

topicSection.addEventListener('click', async (e) => {

    // Prevent from clicking outside
    if (!e.target.classList.contains('newsletter-subscriber__card-btn-block')) { return };
    // Store the id
    const id = e.target.getAttribute('data-id');
    const command = e.target.getAttribute('data-command');
    
    if(!id || !command)return;
    
    
    // Render Spinner
    loadLargeSpinner(animation);

    // Search
    state.updateSubs = new addCard(`/admin/newsletterSubscribers/update?id=${id}&&command=${command}&&_csrf=${csrf}`);
    state.updataSubsRes = await state.updateSubs.getResults();

    // Remove spinner
    removeSpinner(animation);

     // If error occours
     if (!state.updataSubsRes) {

        // display error message
        message(5000, false, 'Could not update the Subscriber. Please try again later');

        // stop next
        return
    }

    message(5000, true, 'The subscriber have been successfully updated');

    init(state.paginationCls.activePageNum());
});



/////////////////////////////////////
////---- Pressing remove Btn ----////
/////////////////////////////////////
delBox.addEventListener('click', async (e) => {

    // Preventing from clicking outside
    if (!e.target.classList.contains('popup__del-btn')) { return };

    // store the id
    const id = e.target.getAttribute('data-id');
    
    // Remove the popup 
    removeDelBox();

    // Render Spinner
    loadLargeSpinner(animation);

    // Search
    state.removeTag = new remove(`/admin/newsletterSubscribers/delete?id=${id}&&_csrf=${csrf}`);
    state.removeTagRes = await state.removeTag.getResults();

    // Remove spinner
    removeSpinner(animation);

    // If error occours
    if (!state.removeTagRes) {

        // display error message
        message(5000, false, 'Could not delete this Subscriber. Please try again later');

        // Cancel active card
        state.activeCard = null;

        // stop next
        return
    }
    
    message(5000, true, 'The subscriber have been successfully deleted');

    // Disappear the card
    if(state.activeCard){
        state.activeCard.parentElement.removeChild(state.activeCard);
    }
    
})




///////////////////////////
////---- Functions ----////
//////////////////////////

// Show cards function
async function init(pageNum) {

    // Get keywords from input
    let keywords = hiddenInput.value;

    // Prepare UI
    loadSpinner(topicSection);
    
    // Get Data from Server
    state.subscriberSearch = new Search(`/admin/newsletterSubscribers/search?pageNum=${pageNum}&&keywords=${keywords}`);
    state.subscriberSearchRes = await state.subscriberSearch.getResults();


    if (!state.subscriberSearchRes) {
        // Render Error page in UI
        removeSpinner(topicSection);

        // Render Error message
        const msg = 'Error in fethcing subscribers';
        message(5000, false, msg);

        // Return Error
        return;
    };

    // Prepare for change
    removeSpinner(topicSection);

    // Make change in UI
    renderCards(topicSection, state.subscriberSearchRes.data.dataArr);

    // Pagingation
    state.paginationCls.changeVals(+state.subscriberSearchRes.data.totalItems);
    state.paginationCls.changePage(+state.subscriberSearchRes.data.pageNum);
}

import { logoutFunc } from './lib/autoLogout'
logoutFunc(() => {
    window.location.href = `/admin_login`
})