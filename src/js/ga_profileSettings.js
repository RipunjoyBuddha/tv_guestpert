import "../sass/ga_profileSettings.scss"; 
import "./view/importsvg" ;
import {changeDp} from "./view/changeProfPic";
import {change} from "./view/changeEmailPhone";
import {initTinymce} from './utils/tinyActions';
import {message} from './view/errorMessage'

const popup = document.querySelector('.popup');
const emailPopupBox = popup.querySelector('.popup__change-email');
const phonePopupBox = popup.querySelector('.popup__change-phone');
const imgInpForm = popup.querySelector('.popup__change-profile-picture').querySelector('form')


imgInpForm.addEventListener('submit', e=>{

    e.preventDefault()

    const imgFile = imgInpForm.querySelector('#image-input').files[0]

    if(imgFile && imgFile.size > 500*1000){
        message(5000, false, 'Too large image file. Maximum 500 KB supported')
        return
    }

    e.target.submit()
})


///////////////////////////////////
//---- State of the whole page ----//
///////////////////////////////////
const state={
    changeEmail: change(popup, emailPopupBox),
    changePhone: change(popup, phonePopupBox)
}

///////////////////////////////////
//---- Change profile picture----//
///////////////////////////////////

const image = document.querySelector('.edit__guestpert-image-container');

//--- When change btn is clicked ---//
changeDp(image, popup);

///////////////////////////////////
//---- Change Email----//
///////////////////////////////////
(function (){
    const changeEmailBtn = document.getElementById('change-email');
    const cancelbtn = emailPopupBox.querySelector('.btn_cancel');
    const popupMsg = emailPopupBox.querySelector('.popup__msg');
    const popupEmailinp = emailPopupBox.querySelector('.popup__email-input');
    const popupVerificationInp = emailPopupBox.querySelector('.popup__verification-input');
    
    //--- When change btn is clicked ---//
    changeEmailBtn.addEventListener('click', function () {
        
        //--- Call the function showUP ---//
        state.changeEmail.showUp();
    })
    
    //--- When cancel btn is clicked ---//
    cancelbtn.addEventListener('click', (e) => {
        
        e.preventDefault()
        //--- Call the function cancel ---//
        state.changeEmail.cancel(popupMsg, popupEmailinp, popupVerificationInp);
    })
    
    
    popup.addEventListener('click', e=>{
        
        if(!e.target.classList.contains('popup'))return;
        
        state.changeEmail.cancel(popupMsg, popupEmailinp, popupVerificationInp);
    })
}())


initTinymce({
    selector: "#biography",
    menubar: !1,
    skin: "CUSTOM",
    content_css: "CUSTOM",
    plugins: "preview lists fullscreen link paste",
    paste_as_text: true,
    toolbar: "undo redo preview fullscreen bold italic underline alignjustify link",
    advlist_bullet_styles: "disc",
})


const errMsg = document.querySelector('.message');
window.addEventListener('load', e=>{
    setTimeout(()=>{
        if(!errMsg)return;
        errMsg.style.display='none';
        errMsg.querySelector('.message__txt').textContent='';
    }, 5000)
})

import { logoutFunc } from './lib/autoLogout'
logoutFunc(() => {
    window.location.href = `/login`
})