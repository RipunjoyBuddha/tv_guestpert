import "../sass/ad_manageTags.scss";
import svg from "./view/importsvg";

import {pagination} from "./view/pagination_rest";
import {seeShow_adv, showBtnClick} from "./view/seeShowbtns";
import Search from "./model/search";
import remove from "./model/deleteCard";
import {renderCards, renderEdit, removeFields} from "./view/pg-ad_manageTags";
import {loadSpinner, removeSpinner, loadLargeSpinner} from "./view/spinner";
import {message} from "./view/errorMessage";
import {delfuncRest, cancelfunc, removeDelBox} from "./view/deleteBox";
import { hamburger, slideMenu } from "./view/sidebar";

///////////////////////////////////////////
///--- Hamburger and Sidebar functionality ---///
///////////////////////////////////////////
hamburger();
slideMenu();



///////////////////////////////////////////////
////---- Universal Selectors ----////
///////////////////////////////////////////////
const topicsSection = document.querySelector('.tags');
const tagsBox= topicsSection.querySelector('#tags-box');
const searchBar = document.querySelector('#search-input');
const hiddenInput = searchBar.parentElement.querySelector('#hidden-input');
const createBtn = topicsSection.querySelector('.btn-createNew');

const editsection = document.querySelector('.edit');
const form = editsection.querySelector('.edit__form-container');
const pageSection = document.querySelector('.page');
const pageList = pageSection.querySelector('#page__list');
const backBtn = editsection.querySelector('.edit__show-cards');
const delBox = document.querySelector('.popup__del-box');
const animeBox = document.querySelector('#animation');
const csrf = document.querySelector('#csrf').getAttribute('data-csrf');


// State of the guestpert Page
const state = {};


///////////////////////////////////////////
////---- When create btn is clicked ----////
//////////////////////////////////////////
createBtn.addEventListener('click', ()=>{

    // Show the edit form
    seeShow_adv([topicsSection, pageSection], editsection);

    form.setAttribute('action', `/admin/createTag?_csrf=${csrf}`)
})

backBtn.addEventListener('click', function(){

    // See if it is in edit mode
    if(this.getAttribute('data-mode')==='edit'){

        // Remove all Fields
        removeFields();

        // clear data-mode
        this.setAttribute('data-mode', '')
    }
    
    form.setAttribute('action', '')
    showBtnClick([topicsSection, pageSection], editsection);
})




///////////////////////////////////////////
////---- Pagination functionality ----////
//////////////////////////////////////////
state.paginationCls = new pagination(24) // 2 is Element per page

// On clicking other pages
pageList.addEventListener('click', function (e) {
    const target = e.target;
    if (!target.classList.contains('clickable')) { return };

    const pageNum = target.getAttribute('data-num');
    init(pageNum);
});



//////////////////////////////////////////
////---- Search bar functionality ----////
/////////////////////////////////////////
const regEx = /^[\w]{1,60}$/i;

searchBar.addEventListener('keyup', function (e) {

    const chk = ['', 8, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 96, 97, 98, 99, 100, 101, 102, 103, 104, 105, 106, 222, 186]
    const k = chk.indexOf(e.keyCode);

    // When ecape is pressed
    if (e.keyCode === 27) {
        e.preventDefault();
        this.value = '';
        hiddenInput.value = '';
        this.blur();
    };

    // Validation 1
    if (!k || k < 0) { return };

    if (this.value.length === 0) {
        hiddenInput.value = '';
        init(1);
    };

    // Validation 2
    if (this.value.length < 2 || !regEx.test(this.value)) { return };

    // Get the value
    const value = this.value;

    // Put text into hidden input
    hiddenInput.value = value;

    // call init
    init(1);
})



///////////////////////////////////////////
////---- When Edit btn is clicked ----////
//////////////////////////////////////////
topicsSection.addEventListener('click', async (e) => {

    // Prevent from clicking outside
    if (!e.target.classList.contains('tags__card-btn-edit')) return;

    // Store card
    state.activeCard = e.target.closest('.thin');

    // Store the id
    const id = e.target.getAttribute('data-id');

    // Store edit data
    state.cardData = {
        keyword: state.activeCard.querySelector('.tag-keyword').textContent,
        active: state.activeCard.querySelector('.tag-activeStats').textContent
    }

    // Make activecard null
    state.activeCard=null;

    // Render Edit
    renderEdit(state.cardData);

    // Puting data in back btn
    backBtn.setAttribute('data-mode', 'edit');

    form.setAttribute('action', `/admin/editTag?id=${id}&&_csrf=${csrf}`);

    // Make edit section visible
    seeShow_adv([topicsSection, pageSection], editsection);
})




///////////////////////////////////////////
////---- When remove btn is clicked ----////
//////////////////////////////////////////

topicsSection.addEventListener('click', async (e) => {

    // Prevent from clicking outside
    if (!e.target.classList.contains('tags__card-btn-remove')) return;

    // Get the id
    const id = e.target.getAttribute('data-id');
    if(!id)return;
    
    // Store card
    state.activeCard = e.target.closest('.thin');

    // call delfunc Rest
    delfuncRest(e.target);
});

cancelfunc();



//////////////////////////////////////
////---- Pressing remove Btn ----////
/////////////////////////////////////
delBox.addEventListener('click', async (e) => {

    // Preventing from clicking outside
    if (!e.target.classList.contains('popup__del-btn')) { return };

    // store the id
    const id = e.target.getAttribute('data-id');
    
    // Remove the popup 
    removeDelBox();

    // Render Spinner
    loadLargeSpinner(animeBox);

    // Search
    state.removeTag = new remove(`/admin/deleteTags?id=${id}&&_csrf=${csrf}`);
    state.removeTagRes = await state.removeTag.getResults();

    // Remove spinner
    removeSpinner(animeBox);

    // If error occours
    if (!state.removeTagRes) {

        // display error message
        message(5000, false, 'Could not delete this tag. Please try again later');

        // Cancel active card
        state.activeCard = null;

        // stop next
        return
    }
    
    message(5000, true, 'The tag have been successfully deleted');

    // Disappear the card
    if(state.activeCard){
        state.activeCard.parentElement.removeChild(state.activeCard);
    }
    
})


//////////////////////////
////---- Functions ----////
//////////////////////////

// Show cards function
async function init(pageNum) {

    // Get keywords from input
    let keywords = hiddenInput.value;

    // Prepare UI
    loadSpinner(tagsBox);
    
    // Get Data from Server
    state.tagsSearch = new Search(`/admin/searchTags?pageNum=${pageNum}&&keywords=${keywords}`);
    state.tagsSearchRes = await state.tagsSearch.getResults();

    // Remove Spinner
    removeSpinner(tagsBox);
    
    if (!state.tagsSearchRes) {

        // Render Error message
        const msg = 'Error in fethcing tags';
        message(5000, false, msg);

        // Return Error
        return;
    };
   
    // // Make change in UI
    renderCards(tagsBox, state.tagsSearchRes.data.dataArr);
    
    // Pagingation
    state.paginationCls.changeVals(+state.tagsSearchRes.data.totalItems);
    state.paginationCls.changePage(+state.tagsSearchRes.data.pageNum);
}
init(1);


const errMsg = document.querySelector('.message');

window.addEventListener('load', e=>{
    setTimeout(()=>{
        if(!errMsg)return;
        errMsg.style.display='none';
        errMsg.querySelector('.message__txt').textContent='';
    }, 5000)
})


import { logoutFunc } from './lib/autoLogout'
logoutFunc(() => {
    window.location.href = `/admin_login`
})