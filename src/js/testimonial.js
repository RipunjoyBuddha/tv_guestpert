import "../sass/testimonial.scss";
import "./view/importsvg"; 

import Search from "./model/search";
import {pagination} from "./view/pagination_rest";
import {loadSpinner, removeSpinner} from "./view/spinner";
import {message} from "./view/errorMessage";
import {renderCards} from "./view/pg-fr_testimonials";


// State of the guestpert Page
const state = {};


//////////////////////////////////
//---- Universal selectors ----//
////////////////////////////////

const topicsSection = document.querySelector('.testimonial');
const testimonialBox = topicsSection.querySelector('.testimonial__card-box');
const pageSection = document.querySelector('.page');
const pageList = pageSection.querySelector('.page__list');
const showNewsletterBtn = document.getElementById('show-newsletter')
const newsletterList = document.querySelector('.newsletter__box')



///////////////////////////////////////////////
////---- Pagination functionality ----////
///////////////////////////////////////////////
state.paginationCls = new pagination(7) // 2 is Element per page

// On clicking other pages
pageList.addEventListener('click', function (e) {
    const target = e.target;
    if (!target.classList.contains('clickable')) { return };

    const pageNum = target.getAttribute('data-num');
    init(pageNum);
});


///////////////////////////////////////
////---- Show Hide Newsletters ----////
//////////////////////////////////////

showNewsletterBtn.addEventListener('click', e=>{

    newsletterList.classList.toggle('u-display-none');
    if(newsletterList.classList.contains('u-display-none')){
        e.target.textContent = 'show Newsletters'
    }else{
        e.target.textContent = 'hide Newsletters'
    }
})

//////////////////////////
////---- Functions ----////
//////////////////////////

// Show cards function
async function init(pageNum) {

    // Prepare UI
    loadSpinner(testimonialBox);
    
    // Get Data from Server
    state.testimonialSearch = new Search(`/fetchAllTestimonials?pageNum=${pageNum}`);
    state.testimonialSearchRes = await state.testimonialSearch.getResults();

    // Render Error page in UI
    removeSpinner(testimonialBox);

    if (!state.testimonialSearchRes) {

        // Render Error message
        const msg = 'Error in fethcing Testimonials. Please Reload the page';
        message(5000, false, msg);

        // Return Error
        return;
    };

    // Make change in UI
    renderCards(testimonialBox, state.testimonialSearchRes.data.dataArr);

    // Pagingation
    state.paginationCls.changeVals(+state.testimonialSearchRes.data.totalItems);
    state.paginationCls.changePage(+state.testimonialSearchRes.data.pageNum);
}
init(1);


const logoutBtn = document.querySelector('.header-bar__link-logout')
import { logoutFunc } from './lib/autoLogout'
if(logoutBtn){
    logoutFunc(() => {
        const btnId = logoutBtn.getAttribute('data-id')
        if(btnId=='adtyspnce'){
            window.location.href = `/admin_login`
        }else{
            window.location.href = `/login`
        }
    })
} 