import uniqid from "uniqid";
import { initTinymce } from '../utils/tinyActions';



export function removeBtnGenSettings(container, btnClass) {
    container.addEventListener('click', (e) => {
        if ((e.target.classList.contains(btnClass))) {
            const btn = e.target;
            const box = btn.parentElement.parentElement;
            btn.parentElement.parentElement.parentElement.removeChild(box);
        }
    })
}

export function removeElement(container, btnClass, removeClass) {
    container.addEventListener('click', (e) => {
        if ((e.target.classList.contains(btnClass))) {
            const btn = e.target;
            const box = btn.closest(`.${removeClass}`);
            container.removeChild(box);
        }
    })
}


export function addNewFields(btn, container, label, inpType, inpName, spans = '') {
    btn.addEventListener('click', function () {
        const id = uniqid();
        const markup = `
        <div class="u-mar-t-1">
            <label for="${id}" class="form__label">
                <span class="form__label--caps">${label}</span>
                <span class="form__label--details">Avoid any special characters.</span>
            </label>
            ${spans}
            <div class="edit__input-plus-btn">
                <input type="${inpType}" name="${inpName}" class="form__control edit__change-inputs" id="${id}" required>
                <div class="edit__btn-change btn_remove">Remove</div>
            </div>
        </div>
        `
        container.insertAdjacentHTML('beforeend', markup);
    })
}


export function addNewEmail(btn, container, label, inpType, inpName, spans = '') {
    btn.addEventListener('click', function () {
        const id1 = uniqid();
        const id2 = uniqid();
        const markup = `
        <div class="email__group" style="margin-top: 3rem;">
            <div class="u-mar-t-1">
                <label for="email_${id1}" class="form__label">
                    <span class="form__label--caps">email</span>
                </label>
                <div class="edit__input-plus-btn">
                    <input type="email" name="otherEmail" class="form__control edit__change-inputs" id="email_${id1}" required>
                </div>
            </div>
            <div class="u-mar-t-1">
                <label for="emailTitle_${id2}" class="form__label">
                    <span class="form__label--caps">email title</span>
                </label>
                <div class="edit__input-plus-btn">
                    <input type="text" name="emailTitle" class="form__control edit__change-inputs" id="<emailTitle_${id2}" required>
                    <div class="edit__btn-change btn_remove">Remove</div>
                </div>
            </div>
        </div>
        `
        container.insertAdjacentHTML('beforeend', markup);
    })
}


export function addNewUpcomingAppearances(btn, container) {
    btn.addEventListener('click', function () {
        const id1 = uniqid();
        const id2 = uniqid();
        const markup = `
        <div class="u-mar-t-3 box">
            <div class="u-mar-t-1">
                <label for="${id1}" class="form__label">
                    <span class="form__label--caps">Guestpert</span>
                    <span class="form__label--details">Maximum 50 characters allwed</span>
                </label>
                <div class="edit__input-plus-btn">
                    <input type="text" name="upcomingAppearanceTag" class="form__control edit__change-inputs" id="${id1}" required>
                    <div class="edit__btn-change btn_remove">Remove</div>
                </div>
            </div>
            <div class="u-mar-t-1">
                <label for="${id2}" class="form__label">
                    <span class="form__label--caps">guestpert title</span>
                </label>
                <div class="edit__input-plus-btn">
                    <input type="text" name="upcomingAppearanceTitle" class="form__control edit__change-inputs" id="${id2}" required>
                </div>
            </div>
        </div>
        `
        container.insertAdjacentHTML('beforeend', markup);
    })
}




export function addNewPara(btn, container, csrf) {
    // Error handling
    if (!btn) { return }

    btn.addEventListener('click', function () {
        const id1 = uniqid();
        const id2 = uniqid();
        const id3 = uniqid();
        const id4 = uniqid();
        const id5 = uniqid();
        const markup = `
        <form action="/admin/founderDetails?id=${id5}&&_csrf=${csrf}" method="POST" class="edit__form-container u-mar-t-3" enctype="multipart/form-data">

            <div class="form__group--settings">
                <label for="${id1}" class="form__label">
                    <span class="form__label--caps">paragraph</span>
                </label>
                <textarea class="form__control" name="paragraph" id="${id1}"></textarea>
            </div>

            <div class="form__group--img-inp" id="inpFile-box">
                <label for="${id2}" class="form__label form__label--inpFile">
                    <span class="form__label--caps">Image</span>
                    <span class="form__inp-img">Choose Image</span>
                    <span class="form__inp-img--adrss "></span>
                    <span class="form__label--details u-mar-t-1">Image should be of max 200 KB</span>
                </label>
                <input type="file" class="inputImgFile" id="${id2}" name="imageFile">
            </div>
            <div class="form__group--img-inp" id="inpFile-box">
                <label for="${id3}" class="form__label form__label--inpFile">
                    <span class="form__label--caps">Image</span>
                    <span class="form__inp-img">Choose Image</span>
                    <span class="form__inp-img--adrss "></span>
                    <span class="form__label--details u-mar-t-1">Image should be of max 200 KB</span>
                </label>
                <input type="file" class="inputImgFile" id="${id3}" name="imageFile">
            </div>
            <div class="form__group--img-inp" id="inpFile-box">
                <label for="${id4}" class="form__label form__label--inpFile">
                    <span class="form__label--caps">Image</span>
                    <span class="form__inp-img">Choose Image</span>
                    <span class="form__inp-img--adrss "></span>
                </label>
                <span class="form__label--details u-mar-t-1">Image should be of max 200 KB</span>
                <input type="file" class="inputImgFile" id="${id4}" name="imageFile">
            </div>

            <!-- ------ Other emails box ------ -->

            <div class="row">
                <button type="submit" class="form__submit-btn--orange u-mar-r-1 form__submit-btn">save</button>
                <button type="submit" data-id="${id5}" class="button prima form__submit-btn">Delete</button>
            </div>
        </form>
        `



        // Error handling
        if (!container) { return }
        container.insertAdjacentHTML('beforeend', markup);

        initTinymce(
            {
                selector: `#${id1}`,
                menubar: !1,
                skin: "CUSTOM",
                content_css: "CUSTOM",
                plugins: " preview lists fullscreen",
                toolbar: "undo redo preview fullscreen bold bullist italic alignjustify",
                advlist_bullet_styles: "disc",
            }
        )
    })
}

