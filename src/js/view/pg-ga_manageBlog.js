import {loadSpinner} from './spinner';
import {message} from './errorMessage';
import {removeImg} from './choose_img';

import {setContent} from '../utils/tinyActions';


////////////////////////
// --- Selectors --- //
const animeBox = document.getElementById('animations');


///////////////////////////////
// --- Export Functions --- //


//--- Render Edit Functionality ---//
export const renderEdit = (editSection, dataObj)=>{
    
    // Get the fields to be edited
    const topicTitle = editSection.querySelector('#title');
    const activeStats = editSection.querySelector('#activeStats');
    const inpFile = editSection.querySelector('#inpFile-box');

    // Fill up the contents
    topicTitle.value = dataObj.title;
    activeStats.checked = dataObj.activeStats;
    removeImg(inpFile);
    //--- To be applied on tinyMCE---//
    // tinymce.activeEditor.setContent(dataObj.content);
    setContent('content', dataObj.content)

    // Put class in show blogs btn
    const backBtn = editSection.querySelector('.edit__show-cards');
    backBtn.classList.add('editMode');

    // Change text content of submit btn
    const submitBtn = editSection.querySelector('.form__submit-btn');
    submitBtn.textContent = 'Save';

    editSection.style.display='block';
}


//--- Remove Edit Fields ---//
export const removeFields = (editSection)=>{

    // Get the fields to be edited
    const topicTitle = editSection.querySelector('#title');
    const activeStats = editSection.querySelector('#activeStats');
    const inpFile = editSection.querySelector('#inpFile-box');

    // Fill up the contents
    topicTitle.value = '';
    activeStats.checked = true;
    removeImg(inpFile);
    //--- To be applied on tinyMCE---//
    // tinymce.activeEditor.setContent('');
    setContent('content', '')

    // Change text content of submit btn
    const submitBtn = editSection.querySelector('.form__submit-btn');
    submitBtn.textContent = 'Create';
}



// Render Error Functionality--
export const renderError = (blogSection)=>{

    // display the main section
    blogSection.style.display ='flex';

    const msg = 'Sorry, Your request cannot be completed. Please try later';
    message(5000, false, msg);
}