const { domain } = require('../utils/domain')
import { rateStarReadOnly } from './star-rating'
import { setPrev } from './star-rating'
import dateFormat from 'dateformat'


export const renderCards = (parentBox, dataArr) => {

    if (!parentBox) { return }

    parentBox.innerHTML = '';
    if (dataArr.length < 1) {
        const markup = `<p class="paragraph__not-found u-center-text">No Such Guestpert available</p>`;
        parentBox.insertAdjacentHTML('beforeend', markup)
    }
    else {
        dataArr.forEach((cur) => {
            parentBox.insertAdjacentHTML('beforeend', markup(cur, domain))
        })
    }
}


export const renderEnquiryCards = (parentBox, dataArr = []) => {

    if (!parentBox) { return }

    parentBox.innerHTML = '';
    if (dataArr.length < 1) {
        const markup = `<p class="paragraph__not-found u-center-text">No Inquiry Availabe</p>`;
        parentBox.insertAdjacentHTML('beforeend', markup)
    }
    else {
        dataArr.forEach((cur) => {
            parentBox.insertAdjacentHTML('beforeend', enquiryMarkup(cur, domain))
        })
    }
}


export const clearForm = (parentBox) => {

    parentBox.querySelector('#media').value = ''
    parentBox.querySelector('#show').value = ''
    parentBox.querySelector('#airDate').value = ''
    parentBox.querySelector('#location').value = ''
    parentBox.querySelector('#topic').value = ''
    parentBox.querySelector('#ratingValue').value = ''
    setPrev(0)
    parentBox.querySelectorAll('.form__group--settings').forEach(c=>{c.style.display = 'flex'})
    parentBox.querySelectorAll('input').forEach(c=>{c.required=true})
    parentBox.querySelector('#ratingValue').required=false;
    parentBox.querySelector('.smileybox').querySelectorAll('input').forEach(c=>c.required=false);
}

export const showPast = (parentBox) => {
    setPrev(0)
    parentBox.querySelector('#ratingValue').value = ''
    parentBox.querySelector('#ratingContainer').classList.remove('u-display-none')
    parentBox.querySelector('#note_rating').classList.add('u-display-none')
}
export const showFuture = (parentBox) => {
    setPrev(0)
    parentBox.querySelector('#ratingValue').value = ''
    parentBox.querySelector('#note_rating').classList.remove('u-display-none');
    parentBox.querySelector('#ratingContainer').classList.add('u-display-none');
}

export const showRatingForm = (parentBox) =>{

    parentBox.querySelectorAll('.form__group--settings').forEach(c=>{c.style.display = 'none'})
    parentBox.querySelectorAll('input').forEach(c=>{c.required=false})
    parentBox.querySelector('#ratingContainer').style.display = 'flex';
    setPrev(0)
    parentBox.querySelector('#ratingValue').value = ''
} 



function enquiryMarkup(cur) {

    const starMarkup = rateStarReadOnly(+cur.rating);

    let markup = `
    <li>
    <p class="info-list__key">Ratings</p>
    <div class="info-list__value">
        <div class="star-rate__reading">
            ${starMarkup}
        </div>
    </div>
    </li>
    `
    if (cur.rating.length < 1 && new Date(cur.airDate) < new Date()) {
        markup = `
        <div class="users__card-btns">
            <button data-id="${cur._id}" class="users__card-btn-edit button__ghost button__ghost--sm">Rate Guestpert</button>
        </div>
        `;
    }else if(cur.rating.length < 1 && new Date(cur.airDate) > new Date()){
        markup=`
        <li>
        <p class="info-list__key">Ratings</p>
        <div class="info-list__value">
            <div class="star-rate__reading">
                Please come back after ${dateFormat(cur.airDate, 'dS mmm yyyy')} to rate the guestpert
            </div>
        </div>
        </li>
        `
    }



    return `
    <li class="enquiry">
    <div class="enquiry__title">
        <span class="txt-uppercase">${cur.show}</span>
    </div>
    <ul class="info-list u-mar-t-2">
        <li>
            <p class="info-list__key">Created</p>
            <p class="info-list__value">${dateFormat(cur.createdAt, 'dS mmm yyyy')}</p>
        </li>
        <li>
            <p class="info-list__key">Air Date</p>
            <p class="info-list__value">${cur.airDate?dateFormat(cur.airDate, 'dS mmm yyyy'):''}</p>
        </li>
        <li>
            <p class="info-list__key">Media</p>
            <p class="info-list__value">${cur.media}</p>
        </li>
        <li>
            <p class="info-list__key">Show</p>
            <p class="info-list__value">${cur.show}</p>
        </li>
        <li>
            <p class="info-list__key">Topic</p>
            <p class="info-list__value">${cur.topic}</p>
        </li>
        ${markup}
    </ul>
    </li>
    `
}



function markup(cur) {

    return `
    <div class="guestpert" data-id="${cur._id}">
        
        <div class="guestpert__img">
            <img src="${domain}${cur.imgSrc}" class="fluid-img"
                alt="guestpert image">
        </div>
        <div class="guestpert__info">
            <span class="guestpert__name">${cur.fullName}</span>
            <span class="guestpert__title">${cur.title}</span>
        </div>
    </div>
    `
}