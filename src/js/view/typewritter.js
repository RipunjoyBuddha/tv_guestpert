class TypeWriter{
    constructor(txtElement, sentences, wait=7000){
        this.txtElement = txtElement;
        this.sentences = sentences;
        this.txt = '';
        this.wordIndex = 0;
        this.wait = parseInt(wait, 10);
        this.type();
        this.isDeleting = false;
    }

    type(){
        // const sentencesArr = this.sentences.split(',');
    
        // Current index of word
        const current = this.wordIndex % this.sentences.length;

        // Get full text of current word
        const fulltxt = this.sentences[current];
        
        
        // Check if deleting
        if(this.isDeleting){
            // Remove a character
            this.txt = fulltxt.substring(0, this.txt.length-1)
        }
        else{
            // Add a character
            this.txt = fulltxt.substring(0, this.txt.length+1)
        }
        
        // Insert txt into element
        this.txtElement.innerHTML = `<span class="txt">${this.txt}</span>`

        // InitialType Speed
        let typeSpeed = 100;

        if(this.isDeleting){
            typeSpeed /= 5;
        }

        // If word is complete
        if(!this.isDeleting && this.txt === fulltxt ){
            // Make pause at end
            typeSpeed = this.wait;

            // Set delete to true
            this.isDeleting = true;
        }else if(this.isDeleting && this.txt === ''){
            this.isDeleting = false;

            // Move to next word
            this.wordIndex++;
            
            // Pause before start typing
            typeSpeed = 100;
        }

        setTimeout(()=> this.type(), typeSpeed)
    }
}




// Init App
export function typeFunc(txtElement, sentences, wait){

    // Init TypeWriter
    new TypeWriter(txtElement, sentences, wait);
}
