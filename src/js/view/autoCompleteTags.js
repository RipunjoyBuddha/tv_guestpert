export const autoComplete = () => {

    // Keyboard Functionality
    keyboardFunc()

    // On clicking the form
    for (const select of document.querySelectorAll('.tag__autoComplete')) {
        select.addEventListener('click', function (e) {
            if (e.target.classList.contains('custom-option')) {
                clickTask(e.target)
            }
        })
    }

    // Disappear on Clicking outside
    disappear();
}


function disappear() {
    window.addEventListener('click', function (e) {
        for (const select of document.querySelectorAll('.tag-box')) {
            if (!select.contains(e.target)) {
                select.querySelector('.custom-select').classList.remove('open');
            }
        }
    })
}


function clickTask(thisEl) {
    const parent = thisEl.closest('.tag-box');
    if (!parent) { return };

    // Get textContent
    const tag = thisEl.textContent;

    // Check validity of tag
    const chk = /^[a-z\d]{1,30}$/i;
    if (!chk.test(tag) && tag.length < 1) { return };

    // Add to Tag List
    addToList(parent, tag);
}



function keyboardFunc() {
    window.addEventListener('keydown', (e) => {
        if (e.keyCode === 38 || e.keyCode === 40 || e.keyCode === 13) {

            for (const select of document.querySelectorAll('.tag__autoComplete')) {
                if (select.querySelector('.custom-select').classList.contains('open')) {
                    e.preventDefault();

                    ////--- When down arrow is pressed ---///
                    if (e.keyCode === 40) {
                        const option = select.querySelectorAll('.custom-option');
                        if (!option || option.length <= 0) { return };

                        let index = 0;
                        if (index == undefined) { index = 0 };

                        option.forEach((cur, i) => {
                            if (cur.classList.contains('selected')) {
                                if (i < option.length - 1) {
                                    index = +i + 1;
                                    cur.classList.remove('selected');
                                }
                                else {
                                    index = +i;
                                }
                            }
                        })

                        if (index === option.length) { return };
                        if (index >= 0) {
                            option[index].classList.add("selected");
                        }
                    }

                    ////--- When up arrow is pressed ---///
                    else if (e.keyCode === 38) {
                        const option = select.querySelectorAll('.custom-option');
                        if (option.length <= 0) { return };

                        let index;
                        option.forEach((cur, i) => {
                            if (cur.classList.contains('selected')) {
                                if (i > 0) {
                                    index = +i - 1;
                                    cur.classList.remove('selected');
                                }
                                else {
                                    index = 0;
                                }
                            }
                        });

                        if (index < 0 || index === undefined) { return };
                        if (index === option.length) { return };
                        if (index >= 0) {
                            option[index].classList.add("selected");
                        }
                    }


                    ///--- Enter is pressed ---///
                    else if (e.keyCode === 13) {
                        // Set in main box
                        select.querySelectorAll('.custom-option').forEach(c => {
                            if (c.classList.contains('selected')) {
                                const tag = c.textContent;
                                addToList(select.closest('.tag-box'), tag);
                            }
                        })

                    }
                }
            }
        }

        //--- Escape is pressed ---//
        else if (e.keyCode === 27) {

            // Returning if input is not active
            if (!document.activeElement.classList.contains('tags-input__main-input')) { return };

            for (const parent of document.querySelectorAll('.tag-box')) {
                parent.querySelector('.tags-input__main-input').value = '';
                parent.querySelector('.tags-input__main-input').blur();
                parent.querySelector('.custom-select').classList.remove('open');
            }
        }
    })
}



//////////////////////////////////
//-- Function to add to List --//
function addToList(parent, tag) {

    ///-- Selectors --///
    const hiddenInput = parent.querySelector('.tags-input__hidden-input');
    const mainInput = parent.querySelector('.tags-input__main-input');

    if (!hiddenInput || !mainInput) { return };

    // Form markup
    const markup = (tag) => {
        return `<span class="tag" data-tag="${tag}">${tag}<span class="close">&#10007;</span></span>`
    };

    parent.querySelector('.tags-input').insertAdjacentHTML('beforeend', markup(tag));

    // Getting Previous inputs
    const hiddenValue = hiddenInput.value;

    // Storing hidden inputs
    hiddenInput.value = `${hiddenValue} ${tag}`;
    mainInput.value = '';

    // Remove class open
    parent.querySelector('.custom-select').classList.remove('open');
}
