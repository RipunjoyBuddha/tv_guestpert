import { domain } from "../utils/domain";
import dateFormat from 'dateformat'

const popup = document.querySelector('.popup');
const moreDetailsBox = popup.querySelector('.popup__users-info');

export const moreDetailsSubAdmin = (moreDetails_btns) => {
    for (const detailBtn of moreDetails_btns) {
        detailBtn.addEventListener('click', function () {
            const allInfo = this.parentElement.parentElement.querySelector('.users__hidden-info');

            const img = allInfo.querySelector('img').getAttribute('src');
            const id = detailBtn.getAttribute('data-id');
            const name = `${allInfo.querySelector('.subAdmin__firstName').getAttribute('data-firstName')} ${allInfo.querySelector('.subAdmin__lastName').getAttribute('data-lastName')}`;
            const username = allInfo.querySelector('.subAdmin__username').getAttribute('data-username');
            const email = allInfo.querySelector('.subAdmin__email').getAttribute('data-email');
            const phone = allInfo.querySelector('.subAdmin__phone').getAttribute('data-phone');
            const address = allInfo.querySelector('.subAdmin__address').getAttribute('data-address');
            const joined = allInfo.querySelector('.subAdmin__joined').getAttribute('data-joined');
            const active = allInfo.querySelector('.subAdmin__active').getAttribute('data-active');

            if (img) {
                moreDetailsBox.querySelector('img').setAttribute('src', img)
            } else {
                moreDetailsBox.querySelector('img').setAttribute('src', '')
            }

            if (id.length > 0) {
                moreDetailsBox.querySelector('#popup__subAdmin-id').querySelector('.all-details__value').textContent = id;
            } else {
                moreDetailsBox.querySelector('#popup__subAdmin-id').style.display = 'none'
            }

            if (name.length > 0) {
                moreDetailsBox.querySelector('#popup__subAdmin-name').querySelector('.all-details__value').textContent = name;
            } else {
                moreDetailsBox.querySelector('#popup__subAdmin-name').style.display = 'none'
            }

            if (username.length > 0) {
                moreDetailsBox.querySelector('#popup__subAdmin-username').querySelector('.all-details__value').textContent = username;
            } else {
                moreDetailsBox.querySelector('#popup__subAdmin-username').style.display = 'none'
            }

            if (email.length > 0) {
                moreDetailsBox.querySelector('#popup__subAdmin-email').querySelector('.all-details__value').textContent = email;
            } else {
                moreDetailsBox.querySelector('#popup__subAdmin-email').style.display = 'none'
            }

            if (phone.length > 0) {
                moreDetailsBox.querySelector('#popup__subAdmin-phone').querySelector('.all-details__value').textContent = phone;
            } else {
                moreDetailsBox.querySelector('#popup__subAdmin-phone').style.display = 'none'
            }

            if (address.length > 0) {
                moreDetailsBox.querySelector('#popup__subAdmin-address').querySelector('.all-details__value').textContent = address;
            } else {
                moreDetailsBox.querySelector('#popup__subAdmin-address').style.display = 'none'
            }

            if (joined.length > 0) {
                moreDetailsBox.querySelector('#popup__subAdmin-joined').querySelector('.all-details__value').textContent = joined;
            } else {
                moreDetailsBox.querySelector('#popup__subAdmin-joined').style.display = 'none'
            }

            if (active.length > 0) {
                moreDetailsBox.querySelector('#popup__subAdmin-active').querySelector('.all-details__value').textContent = active;
            } else {
                moreDetailsBox.querySelector('#popup__subAdmin-active').style.display = 'none'
            }

            popup.classList.add('popup__active');
            moreDetailsBox.style.display = 'block';
        })
    }
}

export const moreDetailGuestpert = (detailBtn) => {

    const allInfo = detailBtn.parentElement.parentElement.querySelector('.users__hidden-info');

    const img = allInfo.querySelector('img').getAttribute('src');
    const id = detailBtn.getAttribute('data-id');
    const name = `${allInfo.querySelector('.guestpert__prefix').getAttribute('data-prefix')} ${allInfo.querySelector('.guestpert__firstName').getAttribute('data-firstName')} ${allInfo.querySelector('.guestpert__lastName').getAttribute('data-lastName')}`;
    const username = allInfo.querySelector('.guestpert__username').getAttribute('data-username');
    const title = allInfo.querySelector('.guestpert__title').getAttribute('data-title');
    const primaryExpertise = allInfo.querySelector('.guestpert__primary-expertise').getAttribute('data-primary-expertise');

    const address = allInfo.querySelector('.guestpert__address').getAttribute('data-address');
    const city = allInfo.querySelector('.guestpert__city').getAttribute('data-city');
    const state = allInfo.querySelector('.guestpert__state').getAttribute('data-state');
    const email = allInfo.querySelector('.guestpert__email').getAttribute('data-email');
    const phone = allInfo.querySelector('.guestpert__phone').getAttribute('data-phone');
    const website = allInfo.querySelector('.guestpert__website').getAttribute('data-website');
    const fax = allInfo.querySelector('.guestpert__fax').getAttribute('data-fax');

    const role = allInfo.querySelector('.guestpert__role').getAttribute('data-role');
    const membership = allInfo.querySelector('.guestpert__membership').getAttribute('data-membership');
    const joined = allInfo.querySelector('.guestpert__joined').getAttribute('data-joined');
    const expiry = allInfo.querySelector('.guestpert__expiry').getAttribute('data-expiry');
    const active = allInfo.querySelector('.guestpert__active').getAttribute('data-active');
    const premium = allInfo.querySelector('.guestpert__premium').getAttribute('data-premium');


    if (img) {
        moreDetailsBox.querySelector('img').setAttribute('src', img)
    } else {
        moreDetailsBox.querySelector('img').setAttribute('src', '')
    }

    if (id) {
        moreDetailsBox.querySelector('#popup__guestpert-id').querySelector('.all-details__value').textContent = id;
    } else {
        moreDetailsBox.querySelector('#popup__guestpert-id').style.display = 'none'
    }

    if (name.length > 0) {
        moreDetailsBox.querySelector('#popup__guestpert-name').querySelector('.all-details__value').textContent = name;
    } else {
        moreDetailsBox.querySelector('#popup__guestpert-name').style.display = 'none'
    }

    if (username.length > 0) {
        moreDetailsBox.querySelector('#popup__guestpert-username').querySelector('.all-details__value').textContent = username;
    } else {
        moreDetailsBox.querySelector('#popup__guestpert-username').style.display = 'none'
    }

    if (title.length > 0) {
        moreDetailsBox.querySelector('#popup__guestpert-title').querySelector('.all-details__value').textContent = title;
    } else {
        moreDetailsBox.querySelector('#popup__guestpert-title').style.display = 'none'
    }

    if (primaryExpertise.length > 0) {
        moreDetailsBox.querySelector('#popup__guestpert-expertise').querySelector('.all-details__value').textContent = primaryExpertise;
    } else {
        moreDetailsBox.querySelector('#popup__guestpert-expertise').style.display = 'none'
    }

    if (address.length > 0) {
        moreDetailsBox.querySelector('#popup__guestpert-address').querySelector('.all-details__value').textContent = address;
    } else {
        moreDetailsBox.querySelector('#popup__guestpert-address').style.display = 'none'
    }

    if (city.length > 0) {
        moreDetailsBox.querySelector('#popup__guestpert-city').querySelector('.all-details__value').textContent = city;
    } else {
        moreDetailsBox.querySelector('#popup__guestpert-city').style.display = 'none'
    }

    if (state.length > 0) {
        moreDetailsBox.querySelector('#popup__guestpert-state').querySelector('.all-details__value').textContent = state;
    } else {
        moreDetailsBox.querySelector('#popup__guestpert-state').style.display = 'none'
    }

    if (email.length > 0) {
        moreDetailsBox.querySelector('#popup__guestpert-email').querySelector('.all-details__value').textContent = email;
    } else {
        moreDetailsBox.querySelector('#popup__guestpert-email').style.display = 'none'
    }

    if (phone.length > 0) {
        moreDetailsBox.querySelector('#popup__guestpert-phone').querySelector('.all-details__value').textContent = phone;
    } else {
        moreDetailsBox.querySelector('#popup__guestpert-phone').style.display = 'none'
    }

    if (website.length > 0) {
        moreDetailsBox.querySelector('#popup__guestpert-website').querySelector('.all-details__value').textContent = website;
    } else {
        moreDetailsBox.querySelector('#popup__guestpert-website').style.display = 'none'
    }

    if (fax.length > 0) {
        moreDetailsBox.querySelector('#popup__guestpert-fax').querySelector('.all-details__value').textContent = fax;
    } else {
        moreDetailsBox.querySelector('#popup__guestpert-fax').style.display = 'none'
    }

    if (role.length > 0) {
        moreDetailsBox.querySelector('#popup__guestpert-role').querySelector('.all-details__value').textContent = role;
    } else {
        moreDetailsBox.querySelector('#popup__guestpert-role').style.display = 'none'
    }

    if (membership.length > 0) {
        moreDetailsBox.querySelector('#popup__guestpert-membership').querySelector('.all-details__value').textContent = membership;
    } else {
        moreDetailsBox.querySelector('#popup__guestpert-membership').style.display = 'none'
    }

    if (joined.length > 0) {
        moreDetailsBox.querySelector('#popup__guestpert-joined').querySelector('.all-details__value').textContent = joined;
    } else {
        moreDetailsBox.querySelector('#popup__guestpert-joined').style.display = 'none'
    }

    if (expiry.length > 0) {
        moreDetailsBox.querySelector('#popup__guestpert-expiry').querySelector('.all-details__value').textContent = expiry;
    } else {
        moreDetailsBox.querySelector('#popup__guestpert-expiry').style.display = 'none'
    }

    if (active.length > 0) {
        moreDetailsBox.querySelector('#popup__guestpert-active').querySelector('.all-details__value').textContent = active;
    } else {
        moreDetailsBox.querySelector('#popup__guestpert-active').style.display = 'none'
    }

    if (premium.length > 0) {
        moreDetailsBox.querySelector('#popup__guestpert-premium').querySelector('.all-details__value').textContent = premium;
    } else {
        moreDetailsBox.querySelector('#popup__guestpert-premium').style.display = 'none'
    }

    popup.classList.add('popup__active');
    moreDetailsBox.style.display = 'block';
}


///////////////////////////////////////////
///---- About Page More Details ----///

export const moreDetailProducer = (producer) => {
    
    const markup = `
    <div class="popup__users-img">
        <img src="${domain}${producer.imgSrc}" alt="producer image" class="fluid-img">
    </div>

    <ul class="popup__all-details u-mar-t-2">
        <li id="popup__guestpert-id">
            <p class="all-details__key">Id</p>
            <p class="all-details__value">${producer._id}</p>
        </li>
        <li id="popup__guestpert-name">
            <p class="all-details__key">name</p>
            <p class="all-details__value">${producer.prefix} ${producer.fullName} ${producer.suffix}</p>
        </li>
        <li id="popup__guestpert-username">
            <p class="all-details__key">User name</p>
            <p class="all-details__value">${producer.username}</p>
        </li>
        <li id="popup__guestpert-title">
            <p class="all-details__key">title</p>
            <p class="all-details__value">${producer.title}</p>
        </li>
        <li id="popup__guestpert-expertise">
            <p class="all-details__key">expertise</p>
            <p class="all-details__value">${producer.primaryExpertise}</p>
        </li>
        <li id="popup__guestpert-address">
            <p class="all-details__key">Address</p>
            <p class="all-details__value">${producer.address}</p>
        </li>
        <li id="popup__guestpert-email">
            <p class="all-details__key">Email</p>
            <p class="all-details__value">${producer.email}</p>
        </li>
        <li id="popup__guestpert-phone">
            <p class="all-details__key">Phone</p>
            <p class="all-details__value">${producer.phone}</p>
        </li>
        <li id="popup__guestpert-website">
            <p class="all-details__key">website</p>
            <p class="all-details__value">${producer.website}</p>
        </li>
        <li id="popup__guestpert-fax">
            <p class="all-details__key">fax</p>
            <p class="all-details__value">${producer.fax}</p>
        </li>
        <li id="popup__guestpert-role">
            <p class="all-details__key">role</p>
            <p class="all-details__value">${producer.role}</p>
        </li>
        <li id="popup__guestpert-joined">
            <p class="all-details__key">Joined</p>
            <p class="all-details__value">${dateFormat(producer.createdAt, 'dS mmm yyyy')}</p>
        </li>
    </ul>
    `
    moreDetailsBox.innerHTML=markup;
    popup.classList.add('popup__active');
    moreDetailsBox.style.display = 'block';
}



///////////////////////////////////////////
///---- About Page More Details ----///

export const aboutMember = (card = null) => {

    if (!card) return;

    // Get the details
    let id = card.getAttribute('id');
    if (!id) { id = '' };
    const imgSrc = card.querySelector('.card__person-photo').getAttribute('src');
    const name = card.querySelector('.person__name').textContent;
    const role = card.querySelector('.person__role').textContent;
    const personDetails = card.querySelector('.person__details').innerHTML;

    // Set Markup
    const markup = `
    <div class="popup__content-wrapper">
        <div class="popup__content">
            
            <div class="popup__close-icon-container">
                <a class="popup__close--icon"></a>
            </div>

            <div class="popup__person-img-wrapper">
                <div class="popup__person-img-container">
                    <img src="${imgSrc}" alt="${name} image" class="popup__person-img fluid-img">
                </div>
            </div>

            <div class="popup__content-txt">
                <h2 class="heading-secondary u-mar-b-5">${name}</h2>
                <h3 class="heading-tertiary u-mar-b-2">${role}</h3>
                
                <div class="popup__content-sm">
                    ${personDetails}
                </div>
            </div>
        </div>
    </div>
    `

    popup.innerHTML = markup;
    popup.classList.add('popup__active')
}



export const closeMoreDetail = () => {
    popup.addEventListener('click', function (e) {

        if (!e.target.classList.contains('popup')) return;
        popup.classList.remove('popup__active');
        moreDetailsBox.style.display = 'none';
    })
}

export const vanish = (popup) => {
    popup.classList.remove('popup__active');
    popup.innerHTML = ''
}