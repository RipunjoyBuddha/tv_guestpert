const { domain } = require('../utils/domain')
import { rateStarReadOnly } from './star-rating'
import { setPrev } from './star-rating'
import dateFormat from 'dateformat'


export const renderCards = (parentBox, dataArr) => {

    if (!parentBox) { return }

    parentBox.innerHTML = '';
    if (dataArr.length < 1) {
        const markup = `<p class="paragraph__not-found u-center-text">No Such Guestpert available</p>`;
        parentBox.insertAdjacentHTML('beforeend', markup)
    }
    else {
        dataArr.forEach((cur) => {
            parentBox.insertAdjacentHTML('beforeend', markup(cur, domain))
        })
    }
}


export const renderEnquiryCards = (parentBox, dataArr) => {

    if (!parentBox) { return }

    parentBox.innerHTML = '';
    if (dataArr.length < 1) {
        const markup = `<p class="paragraph__not-found u-center-text">No Inquiry Availabe</p>`;
        parentBox.insertAdjacentHTML('beforeend', markup)
    }
    else {
        dataArr.forEach((cur) => {
            parentBox.insertAdjacentHTML('beforeend', enquiryMarkup(cur, domain))
        })
    }
}


export const renderEdit = (dataObj, editsection)=>{
    
    editsection.querySelector('#media').value = dataObj.media
    editsection.querySelector('#show').value = dataObj.show
    editsection.querySelector('#topic').value = dataObj.topic
    editsection.querySelector('#location').value = dataObj.location?dataObj.location:''
    editsection.querySelector('#airDate').value = dateFormat(dataObj.airDate, 'yyyy-mm-dd')
    editsection.querySelector('#ratingValue').value = +dataObj.rating
    setPrev(+dataObj.rating)
}


export const clearForm = (parentBox) => {

    parentBox.querySelector('#media').value = ''
    parentBox.querySelector('#show').value = ''
    parentBox.querySelector('#airDate').value = ''
    parentBox.querySelector('#location').value = ''
    parentBox.querySelector('#topic').value = ''
    parentBox.querySelector('#ratingValue').value = ''
    setPrev(0)
}



function enquiryMarkup(cur) {
    
    const starMarkup = rateStarReadOnly(+cur.rating);

    return `
    <li class="enquiry">
    <div class="enquiry__title">
        <span class="txt-uppercase">${cur.show}</span>
    </div>
    <ul class="info-list u-mar-t-2">
        <li>
            <p class="info-list__key">Date</p>
            <p class="info-list__value">${dateFormat(cur.createdAt, 'dS mmm yyyy')}</p>
        </li>
        <li>
            <p class="info-list__key">Media</p>
            <p class="info-list__value">${cur.media}</p>
        </li>
        <li>
            <p class="info-list__key">Show</p>
            <p class="info-list__value">${cur.show}</p>
        </li>
        <li>
            <p class="info-list__key">Topic</p>
            <p class="info-list__value">${cur.topic}</p>
        </li>
        <li>
            <p class="info-list__key">Ratings</p>
            <div class="info-list__value">
                <div class="star-rate__reading">
                    ${starMarkup}
                </div>
            </div>
        </li>
    </ul>
    <div class="enquiry-btns u-mar-b-2">
                    <button class="button__primary edit_enquiry" data-id="${cur._id}">Edit</button>
                    <button class="button__primary guestpertView" data-id="${cur._id}">${cur.guestpertView?'Remove From Guestpert': 'Send To Guestpert'}</button>
        </div>
    </li>
    `
}



function markup(cur) {
    
    let unreadMarkup = `<span class="enquiryNum">${cur.unreadInquiries}</span>`
    !cur.unreadInquiries||cur.unreadInquiries<1?unreadMarkup='':''

    return `
    <div class="guestpert" data-id="${cur._id}">
        ${unreadMarkup}
        <div class="guestpert__img">
            <img src="${domain}${cur.imgSrc}" class="fluid-img"
                alt="guestpert image">
        </div>
        <div class="guestpert__info">
            <span class="guestpert__name">${cur.fullName}</span>
            <span class="guestpert__title">${cur.title}</span>
        </div>
    </div>
    `
}