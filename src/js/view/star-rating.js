const smileyBox = document.querySelector('.smileybox')
const ratingInput = document.querySelector('#ratingValue')

export function ratingStar(input){
    
    const checkValue = smileyBox.querySelectorAll("input");
    const checkStar = smileyBox.querySelectorAll("label");
  
    let checkCount = 0;
    for(let i=0; i<checkValue.length; i++){
        if(checkValue[i]==input){
            checkCount = i+1;
        }
    }
    
    for(let j=0; j<checkCount; j++){
        checkValue[j].checked = true;
        checkStar[j].className = "rated";
    }
    
    for(let k=checkCount; k<checkValue.length; k++){
        checkValue[k].checked = false;
        checkStar[k].className = "check"
    }
    ratingInput.value= checkCount
}


export function rateStarReadOnly(n){

    if(!n)n=0;

    const star = []
    for(let i = 0; i<n; i++){
        star.push(`<label for="r${i+1}" class="rated"><input type="checkbox" id="r${i+1}"/></label>`)
    } 
    for(let i = 10-(10-n); i<10; i++){
        star.push(`<label for="r${i+1}" class="check"><input type="checkbox" id="r${i+1}"/></label>`)
    }
    
    return star.join('')
}



export function setPrev(n){
    const checkValue = smileyBox.querySelectorAll("input");
    const checkStar = smileyBox.querySelectorAll("label");
    
    let checkCount = n;
    
    for(let j=0; j<checkCount; j++){
        checkValue[j].checked = true;
        checkStar[j].className = "rated";
        
    }
    
    for(let k=checkCount; k<checkValue.length; k++){
        checkValue[k].checked = false;
        checkStar[k].className = "check"   
    }
    
}