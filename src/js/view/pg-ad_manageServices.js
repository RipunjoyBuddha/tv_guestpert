import {setContent} from '../utils/tinyActions';

//////////////////////////////////
//--- Render Edit ---//
const edit= document.querySelector('.edit');
const editStore = {
    title: edit.querySelector('#serviceTitle'),
    shortDescription: edit.querySelector("#shortDescription"),
    active: edit.querySelector('#active')
};

export const renderEdit = (dataObj)=>{
   
    // Fill the inputs with data
    editStore.title.value=dataObj.title;
    editStore.shortDescription.value = dataObj.shortDescription;
    setContent('description', dataObj.description);
    editStore.active.checked = dataObj.active;
}

export const clearFields = ()=>{
    
    // Fill the inputs with data
    editStore.title.value='';
    editStore.shortDescription.value = '';
    setContent('description', '');
    editStore.active.checked = true;
}
