const addressBar = document.getElementById('address-bar');
const menuBtn = document.getElementById('hamburger-menu');
const sidebar = document.getElementById('sidebar');
const tvgLogo = document.getElementById('header-bar__logo')

const menuBtns = document.querySelectorAll('.hamburger-btn');


export const hamburger = ()=>{
    let addressBarOffsetTop;

    if(addressBar){
        addressBarOffsetTop = +addressBar.offsetTop + +addressBar.offsetHeight;
    };

    if(typeof(addressBarOffsetTop)=='number' && menuBtn){
        window.addEventListener('scroll', ()=>{
            if(window.pageYOffset > addressBarOffsetTop){
                menuBtn.style.opacity=1;
                menuBtn.style.display='flex';
            }
            else{
                menuBtn.style.opacity=0;
                menuBtn.style.display='none';
            }
        })
    }
}

export const slideMenu =()=>{
    if(menuBtns.length>0){
        for(const btn of menuBtns){
            btn.addEventListener('click', function(){
                if(sidebar){
                    sidebar.classList.add('side-nav__active')
                    if(tvgLogo){
                        tvgLogo.classList.add('header-bar__logo-anime');
                    }
                }
            })
        }
    }
    window.addEventListener('click', function(e){
        if(!e.target.closest('.hamburger-btn')){
            if(sidebar){
                sidebar.classList.remove('side-nav__active');
                if(tvgLogo){
                    tvgLogo.classList.remove('header-bar__logo-anime');
                }
            }
        }
    }) 
}