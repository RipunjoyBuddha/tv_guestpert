import { domain } from "../utils/domain";

//////////////////////////
//--- Render Cards ---//
export const renderCards = (parentBox, dataArr) => {
    if (!parentBox) { return }

    parentBox.innerHTML = '';

    if(dataArr.length<1){
        const markup = `<p class="paragraph__not-found">No Testimonial Available</p>`;
        parentBox.insertAdjacentHTML('beforeend', markup)
        
    }
    else{
        dataArr.forEach((cur) => {
            parentBox.insertAdjacentHTML('beforeend', markup(cur,  domain, 'add-to-home'))
        })
    }
}


//////////////////////////////////
//--- Render HomePage Cards ---//
export const renderHomePageCards = (parentBox, card)=>{
    if (!parentBox || !card) { return }

    // Getting rid of "no testimonials on home page"
    if(parentBox.querySelectorAll('.card-2').length<1){
        const el = document.querySelector('.paragraph__not-found');
    
        if(el){
            parentBox.removeChild(el);
        }
    }
    
    // Make change in the button
    const cardBtn = card.querySelector('.add-to-home');
    cardBtn.classList.add('remove-from-home');
    cardBtn.classList.remove('add-to-home');
    cardBtn.textContent = 'remove';

    parentBox.appendChild(card);
}



//////////////////////////////////
//--- Remove HomePage Cards ---//
export const removeCardsFromHome = (parentBox, card)=>{
    parentBox.removeChild(card);

    // Check homepage testimonials number
    if(parentBox.querySelectorAll('.card-2').length<1){
        const markup = `<p class="paragraph__not-found">No Testimonial Available on home page</p>`;
        parentBox.insertAdjacentHTML('beforeend', markup)
    }
}



function markup(cur, domain, task) {

    let btnTxt = 'remove';

    if(task === 'add-to-home'){
        btnTxt = 'add to home';
    }

    const markup = `
        <div class="card-2 testimonial__card" >
                        
            <div class="testimonial__card-img">
                <img src="${domain}${cur.imgSrc}" alt="${cur.fullName} image" class="fluid-img">
            </div>

            <p class="paragraph-primary txt-capitalize u-center-text testimonial__guestpert">${cur.fullName}</p>
            <div class="testimonial__info u-mar-t-2 ">
                ${cur.content}
            </div>
            
            <div class="testimonial__card-btns">
                <button data-id="${cur._id}" class="${task} button__primary">${btnTxt}</button>
            </div>
        </div>
    `
    return markup
}