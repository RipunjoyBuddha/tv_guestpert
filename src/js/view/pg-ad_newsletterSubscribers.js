/////////////////////////
//--- Render Cards ---//
export const renderCards = (parentBox, dataArr) => {
    if (!parentBox) { return }

    parentBox.innerHTML = '';

    if(dataArr.length<1){
        const markup = `<p class="paragraph__not-found">No Subscribers Found</p>`;
        parentBox.insertAdjacentHTML('beforeend', markup)
        
    }
    else{
        dataArr.forEach((cur) => {
            parentBox.insertAdjacentHTML('beforeend', markup(cur));
        })
    }
}


function markup(cur) {

    let blockBtn = 'block';
    if(!cur.activeStats){blockBtn='unblock'};


    !cur.activeStats?cur.activeStats='not active':cur.activeStats='active';

    const markup = `
        <div class="card-3 newsletter-subscriber__card">
            <ul class="newsletter-subscriber__info">
                <li>
                    <p class="newsletter-subscriber__key">Email</p>
                    <p class="newsletter-subscriber__value newsletter-email">${cur.email}</p>
                </li>
                <li>
                    <p class="newsletter-subscriber__key">Status</p>
                    <p class="newsletter-subscriber__value txt-capitalize">${cur.activeStats}</p>
                </li>
            </ul>

            <div class="newsletter-subscriber__card-btns">
                <button data-id="${cur._id}" data-command="${blockBtn}" class="newsletter-subscriber__card-btn-block button__primary">${blockBtn}</button>
                <button data-id="${cur._id}" class="newsletter-subscriber__card-btn-remove button__ghost button__ghost--sm">Remove</button>
            </div>  
        </div>  
    `
    return markup
}