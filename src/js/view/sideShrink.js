class Shrink{
    constructor(txtElement, namesArr, designationArr, wait){
        this.txtElement = txtElement;
        this.namesArr= namesArr;
        this.designationArr = designationArr;
        this.wait = wait;
        this.index = 0;
        this.num = 0;
        this.shrink();
    }

    shrink(){
        let waitTime = this.wait;
        ////---- For num = index
        if(this.num === this.index){
            const current = this.index % this.namesArr.length;
            const curName = this.namesArr[current];
            const curDesignation = this.designationArr[current]; 

            if(this.designationArr.length>0){
                this.txtElement.innerHTML = `
                <div class="ribon-sm__content-txt-container">
                    <span class="ribon-sm__primary-content">
                        ${curName}
                    </span>
                    <span class="caption-text-sm ribon-sm__caption u-text-left">
                        ${curDesignation}
                    </span>
                </div>
                `;
            }
            else{
                this.txtElement.innerHTML = `
                <div class="ribon-sm__content-txt-container">
                    <span>
                        ${curName}
                    </span>
                </div>
                `;
            }

            this.txtElement.classList.remove('ribon-sm__shrink');
            this.index++;
        }
        
        else if(this.num !== this.index){
            this.txtElement.classList.add('ribon-sm__shrink');
            this.num = this.index;
            waitTime = 300;
        }

        setTimeout(()=> this.shrink(), waitTime)
        ////---- For num !== index
    }
}






// Init App
export function shrinkFunc(txtElement, namesArr, designationArr, wait){

    // Init TypeWriter
    const x = new Shrink(txtElement, namesArr, designationArr, wait);
}