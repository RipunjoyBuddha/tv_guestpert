import {updateContent} from '../vendor/form-dropdown';
import { message } from './errorMessage';
import {setContent} from '../utils/tinyActions';

// Selectors
const editSection = document.querySelector('#editSection');

const updateDropdown = new updateContent(editSection.querySelector('#custom-options-wrapper'));




// --------------------------- //
//////--- Render Edit ------/////
// --------------------------- //
export const renderEdit = (dataObj)=>{

    // Get the fields
    const subjectInput = editSection.querySelector('#emailSubject');
    
    // Place data in the fields
    updateDropdown.setContent(dataObj.segment.toLowerCase());
    subjectInput.value = dataObj.subject;
    // tinymce will be active
    setContent('emailContent', dataObj.emailContent);
    // emailContent.setContent(dataObj.emailContent);

    // Put class in show blogs btn
    const backBtn = editSection.querySelector('.edit__show-cards');
    backBtn.classList.add('editMode');

    editSection.style.display='block';
}



// /////////////////////////// //
//////--- Render Error ------/////
// /////////////////////////// //
export const renderError = (topicsSection)=>{

    // popup error msg
    const msg = 'Failed to process your request. Try again later';
    message(5000, false, msg);

    topicsSection.style.display = 'flex';
}


// /////////////////////////// //
//////--- Remove Fields------/////
// /////////////////////////// //
export const removeFields = ()=>{

    // Get the fields to be edited
    const subjectInput = editSection.querySelector('#emailSubject');
    // const emailContent = tinymce.get("#emailContent");

    // Place data in the fields
    updateDropdown.removeContent();
    subjectInput.value = '';
    setContent('emailContent', '');
    // emailContent.setContent('');
}