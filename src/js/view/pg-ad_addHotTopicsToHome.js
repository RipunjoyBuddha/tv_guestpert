import { domain } from "../utils/domain";

//////////////////////////
//--- Render Cards ---//
export const renderCards = (parentBox, dataArr) => {
    if (!parentBox) { return }

    parentBox.innerHTML = '';

    if(dataArr.length<1){
        const markup = `<p class="paragraph__not-found">No Topics available</p>`;
        parentBox.insertAdjacentHTML('beforeend', markup)
        
    }
    else{
        dataArr.forEach((cur) => {
            parentBox.insertAdjacentHTML('beforeend', markup(cur,  domain, 'add-to-home'))
        })
    }
}

//////////////////////////////////
//--- Render HomePage Cards ---//
export const renderHomePageCards = (parentBox, obj)=>{
    if (!parentBox || !obj) { return }
    
    parentBox.insertAdjacentHTML('beforeend', markup(obj, '', 'remove-from-home'));
}



//////////////////////////////////
//--- Remove HomePage Cards ---//
export const removeCardsFromHome = (parentBox, card)=>{
    parentBox.removeChild(card);
}





function markup(cur, domain, task) {

    let btnTxt = 'remove';

    if(task === 'add-to-home'){
        btnTxt = 'add to home';
    }

    const markup = `
        <div class="card-6 thin">
            <div class="thin__img">
                <img src="${domain}${cur.imgSrc}"  alt="guestpert image" class="fluid-img">
            </div>
            <div class="thin__info">
                <p class="thin__main-txt color-txt-dark">${cur.topicTitle}</p>
                <p class="thin__sm-txt">${cur.category}</p>
            </div>
            <div class="thin__btn-container">
                <button data-id="${cur._id}" class="${task} button__primary">${btnTxt}</button>
            </div>
        </div>  
    `
    return markup
}