

export const chooseImg = (topBox , inpImg= undefined)=>{
    if(!inpImg){
        inpImg = topBox.querySelector('#choose-img-file');
    }
    const imgAdrss = topBox.querySelector('.form__inp-img--adrss');
    
    inpImg.addEventListener('change', function(){
        const file = this.files[0];
        const name = file.name;
        if(!file){
            imgAdrss.textContent = '';
        }
        else{
            imgAdrss.textContent = name;
        }
    });

}

export const chooseImgMul = (parentContainer)=>{
    parentContainer.addEventListener('click', (e)=>{
        if(e.target.closest('.form__label--inpFile')){
            const topBox = e.target.closest('.form__label--inpFile');
            const imgAdrss = topBox.querySelector('.form__inp-img--adrss');
            const inpImg = topBox.parentElement.querySelector('.inputImgFile');
            
           

            if(!inpImg){return}

            inpImg.addEventListener('change', function(){
                const file = this.files[0];
                const name = file.name;
                if(!file){
                    imgAdrss.textContent = '';
                }
                else{
                    imgAdrss.textContent = name;
                }
            });
            
        }
        else{
            return
        }
    
    })
}

export const removeImg = (topBox , inpImg= undefined)=>{

    if(!inpImg){
        inpImg = topBox.querySelector('#choose-img-file');
    }
    const imgAdrss = topBox.querySelector('.form__inp-img--adrss');

    // Change the values

    inpImg.value = '';
    imgAdrss.textContent='';
}