export function tagsFunc(editSection){
    const tagOuterBox = editSection.querySelector('.tags-input');
    const mainInput = tagOuterBox.querySelector('.tags-input__main-input');
    const hiddenInput = tagOuterBox.querySelector('.tags-input__hidden-input');

    if(!tagOuterBox || !mainInput || !hiddenInput){
        console.log('Some fields are missing');
        return
    }

    // Style the box
    mainInput.addEventListener('focus',()=>{
        tagOuterBox.classList.add('tags-input__focus');
    })
    mainInput.addEventListener('focusout',()=>{
        tagOuterBox.classList.remove('tags-input__focus');
    })

    // Add Tag
    addTag(tagOuterBox, mainInput, hiddenInput);
    
    // Remove Tag
    removeTag(tagOuterBox, hiddenInput);
}

export class updateTag{
    constructor(parentEl){
        this.tagOuterBox = parentEl;
        this.tagHiddenInput = parentEl.querySelector('.tags-input__hidden-input');
    }

    setContent(tagArr){

        // Set Array of tags
        this.tagsArr = tagArr;

        const markup = (tag)=>{
            const m = `
            <span class="tag" data-tag="${tag}">${tag}<span class="close">&#10007;</span></span>
            `
            return m ;
        }

        // Remove all previous
        this.tagOuterBox.querySelectorAll('.tag').forEach(c=>c.parentElement.removeChild(c));

        // Set new
        this.tagsArr.forEach(c=>{
            this.tagOuterBox.insertAdjacentHTML('beforeend', markup(c));
        });
        
        // Set in hidden input
        if(this.tagsArr.length>0){
            this.tagHiddenInput.value= this.tagsArr.join(' ');
        }
    }


    removeContent(){
        this.tagOuterBox.querySelectorAll('.tag').forEach(c=>c.parentElement.removeChild(c));
        this.tagHiddenInput.value= '';
    }
}


function addTag(tagOuterBox, mainInput, hiddenInput){
    const markup =(tag)=>{
        return `<span class="tag" data-tag="${tag}">${tag}<span class="close">&#10007;</span></span>`
    } ;
    const chk = /^[a-z\d]{1,50}$/i;
    
    window.addEventListener('keydown',(e)=>{
        if(e.keyCode === 9 || e.keyCode===32 && tagOuterBox.classList.contains('tags-input__focus')){
            e.preventDefault();
            
            if(mainInput.value.length>0){
                if(chk.test(mainInput.value)){
                    error.removeError(tagOuterBox);

                    // Putting in show box
                    tagOuterBox.insertAdjacentHTML('beforeend', markup(mainInput.value));

                    // Getting Previous inputs
                    const hiddenValue = hiddenInput.value;

                    // Storing hidden inputs
                    hiddenInput.value = `${hiddenValue} ${mainInput.value}`;
                    mainInput.value='';

                    // Removing open class
                    tagOuterBox.parentElement.querySelector('.custom-select').classList.remove('open');
                }
                else{
                    const msg = 'Tag can contain only alphabets , numbers and length should be of max 50 character';
                    mainInput.value='';
                    tagOuterBox.parentElement.querySelector('.custom-select').classList.remove('open');
                    error.addError(tagOuterBox, msg)
                }
            }
        }
    })
}


function removeTag(tagOuterBox, hiddenInput){
    tagOuterBox.addEventListener('click', function(e){
        if(e.target.closest('.close')){
            const targetEl =  e.target.closest('.tag');
            const targetContent = String(targetEl.getAttribute('data-tag')); 
            
            // Remove content from hidden input
            if(!targetContent){return}
            const res = hiddenInput.value.split(' ').filter(cur=> cur!=targetContent).join(' ');
            hiddenInput.value= res;
            
            // Remove from UI
            targetEl.parentElement.removeChild(targetEl);
        }
    })
}

const error = {
    addError : (tagOuterBox, msg)=>{
        const msgEl = tagOuterBox.parentElement.querySelector('.form__error-msg');
        msgEl.textContent = msg;
        msgEl.classList.add('form__error-msg--show');
    },
    removeError: (tagOuterBox)=>{
        const msgEl = tagOuterBox.parentElement.querySelector('.form__error-msg');
        if(msgEl.classList.contains('form__error-msg--show')){
            msgEl.textContent = '';
            msgEl.classList.remove('form__error-msg--show');
        }
    }
}