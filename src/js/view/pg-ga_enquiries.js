import {rateStarReadOnly} from './star-rating'
import dateFormat from 'dateformat'

export const renderEnquiryCards = (parentBox, dataArr)=>{
    
    if (!parentBox) { return }

    parentBox.innerHTML = '';
    if (dataArr.length < 1) {
        const markup = `<p class="paragraph__not-found u-center-text">No Inquiry Availabe</p>`;
        parentBox.insertAdjacentHTML('beforeend', markup)
    }
    else {
        dataArr.forEach((cur) => {
            parentBox.insertAdjacentHTML('beforeend', enquiryMarkup(cur))
        })
    }
}


function enquiryMarkup(cur) {
    let star = true
    if(cur.rating.length==0){star=false}
    const starMarkup =  rateStarReadOnly(+cur.rating);

    return `
    <li class="enquiry">
    <div class="enquiry__title">
        <span>${cur.show}</span>
    </div>
    <ul class="info-list u-mar-t-2">
        <li>
            <p class="info-list__key">Date</p>
            <p class="info-list__value">${dateFormat( cur.createdAt, 'dS mmm yyyy')}</p>
        </li>
        <li>
            <p class="info-list__key">Media</p>
            <p class="info-list__value">${cur.media}</p>
        </li>
        <li>
            <p class="info-list__key">Show</p>
            <p class="info-list__value">${cur.show}</p>
        </li>
        <li>
            <p class="info-list__key">Topic</p>
            <p class="info-list__value">${cur.topic}</p>
        </li>
        ${
            !star?'':(
            `<li>
                <p class="info-list__key">Ratings</p>
                <div class="info-list__value">
                    <div class="star-rate__reading">
                        ${starMarkup}
                    </div>
                </div>
            </li>`)
        }
    </ul>
    </li>
    `
}