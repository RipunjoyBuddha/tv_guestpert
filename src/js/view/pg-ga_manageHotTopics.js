import {updateContent} from '../vendor/form-dropdown';
import {updateTag} from './tags';
import {message} from './errorMessage';
import {setContent} from '../utils/tinyActions';

// Selectors
const editSection = document.querySelector('.edit');

const updateDropdown = new updateContent(editSection.querySelector('#custom-options-wrapper'));
const updateTags = new updateTag(editSection.querySelector('.tags-input'));


export const renderEdit = (dataObj)=>{
    
    // Get the fields to be edited
    const title = editSection.querySelector('#title');
    const activeStats = editSection.querySelector('#activeStats');

    // set the fields
    updateDropdown.setContent(dataObj.category);
    title.value = dataObj.title;
    updateTags.setContent(dataObj.tags);
    activeStats.checked = dataObj.active;
    // tinymce will be active
    setContent('openingPoint',dataObj.openingSpeakingPoint);
    setContent('content',dataObj.content);

    // Put class in show blogs btn
    const backBtn = editSection.querySelector('.edit__show-cards');
    backBtn.classList.add('editMode');

    // Change text content of submit btn
    const submitBtn = editSection.querySelector('.form__submit-btn');
    submitBtn.textContent = 'Save';

    // Show edit Section
    editSection.style.display='block';
}



export const removeFields = ()=>{

    // Get the fields to be edited
    const title = editSection.querySelector('#title');
    const activeStats = editSection.querySelector('#activeStats');

    // Fill up the contents
    updateTags.removeContent();
    updateDropdown.removeContent();
    activeStats.checked = true;
    title.value = '';
    setContent('openingPoint', '')
    setContent('content', '')


    // Change text content of submit btn
    const submitBtn = editSection.querySelector('.form__submit-btn');
    submitBtn.textContent = 'Create';
}


export const renderError = (topicSection)=>{

    // display the main section
    topicSection.style.display ='flex';

    const msg = 'Sorry, Your request cannot be completed. Please try later';
    message(5000, false, msg);
}