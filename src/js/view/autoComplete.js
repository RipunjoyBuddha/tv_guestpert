export const autoComplete = (restFeature = false) => {

    // Keyboard Functionality
    keyboardFunc(restFeature)

    // On clicking the form
    for (const select of document.querySelectorAll('.form__autocomplete')) {
        select.addEventListener('click', function (e) {
            if (e.target.classList.contains('custom-option')) {
                clickTask(e.target, restFeature)
            }
        })
    }

    // Disappear on Clicking outside
    disappear(restFeature);
}


function disappear(restFeature) {
    window.addEventListener('click', function (e) {
        for (const select of document.querySelectorAll('.search-box')) {
            if (!select.contains(e.target)) {
                if(select.querySelector('.form__search-input')){
                    select.querySelector('.form__search-input').value = '';
                }
                if (restFeature === false && select.querySelector('.form__hidden-input')) {
                    select.querySelector('.form__hidden-input').value = '';
                }
                select.querySelector('.custom-select').classList.remove('open');
            }
        }
    })
}


function clickTask(thisEl, restFeature) {
    if (restFeature === true) { return };

    const parent = thisEl.closest('.search-box');
    if(!parent){return};

    parent.querySelector('.custom-select').classList.remove('open');
    parent.querySelector('.form__search-input').value = thisEl.textContent;
    parent.querySelector('.form__hidden-input').value = `${thisEl.getAttribute('data-id')}:tiai`;
    parent.querySelector('form').submit();
}


function keyboardFunc(restFeature) {
    window.addEventListener('keydown', (e) => {
        if (e.keyCode === 38 || e.keyCode === 40 || e.keyCode === 13) {
            for (const select of document.querySelectorAll('.form__autocomplete')) {
                if (select.querySelector('.custom-select').classList.contains('open')) {
                    e.preventDefault();

                    ////--- When down arrow is pressed ---///
                    if (e.keyCode === 40) {
                        const option = select.querySelectorAll('.custom-option');
                        if (!option || option.length <= 0) { return };

                        let index = 0;
                        if (index == undefined) { index = 0 };

                        option.forEach((cur, i) => {
                            if (cur.classList.contains('selected')) {
                                if (i < option.length - 1) {
                                    index = +i + 1;
                                    cur.classList.remove('selected');
                                }
                                else {
                                    index = +i;
                                }
                            }
                        })

                        if (index === option.length) { return };
                        if (index >= 0) {
                            option[index].classList.add("selected");
                            option[index].closest('.search-box').querySelector('.form__search-input').value = option[index].textContent;
                        }
                    }

                    ////--- When up arrow is pressed ---///
                    else if (e.keyCode === 38) {
                        const option = select.querySelectorAll('.custom-option');
                        if (option.length <= 0) { return };

                        let index;
                        option.forEach((cur, i) => {
                            if (cur.classList.contains('selected')) {
                                if (i > 0) {
                                    index = +i - 1;
                                    cur.classList.remove('selected');
                                }
                                else {
                                    index = 0;
                                }
                            }
                        });

                        if (index < 0 || index === undefined) { return };
                        if (index === option.length) { return };
                        if (index >= 0) {
                            option[index].classList.add("selected");
                            option[index].closest('.search-box').querySelector('.form__search-input').value = option[index].textContent;
                        }
                    }


                    ///--- Enter is pressed ---///
                    else if (e.keyCode === 13 && restFeature === false) {
                        for (const select of document.querySelectorAll('.custom-select')) {

                            select.querySelectorAll('.custom-option').forEach(c=>{
                                if(c.classList.contains('selected')){
                                    select.closest('.search-box').querySelector('.form__hidden-input').value= `${c.getAttribute('data-id')}:tiai`;
                                }
                            })

                            if (select.classList.contains('open')) {
                                select.classList.remove('open');

                                select.closest('.search-box').querySelector('form').submit();
                            }
                        }
                    }

                    
                }
            }
        }
        
        //--- Escape is pressed ---//
        else if(e.keyCode===27){

            // Returning if input is not active
            if (!document.activeElement.classList.contains('form__search-input')) {return};
            for(const parent of document.querySelectorAll('.search-box')){
                parent.querySelector('.form__search-input').value='';
                parent.querySelector('.form__hidden-input').value='';
                parent.querySelector('.form__search-input').blur();
                parent.querySelector('.custom-select').classList.remove('open');
            }
        }
    })
}




const filterArr = document.querySelectorAll('.hero__filter-btn');

export const autoCompleteClickPress = (init) => {
    // Keyboard Functionality
    keyboardFuncRest(init)

    // On clicking the form
    for (const select of document.querySelectorAll('.form__autocomplete')) {
        select.addEventListener('click', function (e) {
            if (e.target.classList.contains('custom-option')) {
                clickTaskRest(e.target, init)
            }
        })
    }
}

function clickTaskRest(thisEl, init) {

    // Saving from error
    if (!thisEl) { return };

    // Selecting main box above
    const parent = thisEl.closest('.search-box');
    if (!parent) { return };

    // Removing option box if exists
    if (parent.querySelector('.custom-select')) {
        parent.querySelector('.custom-select').classList.remove('open');
    }
    // Putting the hidden input value
    if (parent.querySelector('.form__hidden-input')) {
        parent.querySelector('.form__hidden-input').value = `${thisEl.getAttribute('data-id')}:tiai`;
    }
    // Removing the active filter
    if (filterArr && filterArr.length > 0) {
        filterArr.forEach(c => c.classList.remove('hero__filter-btn--active'));
    }

    // Removing maininput value
    if (parent.querySelector('.form__search-input')) {
        parent.querySelector('.form__search-input').value = '';
    }

    init(1);
}


function keyboardFuncRest(init) {
    window.addEventListener('keydown', (e) => {
        if (e.keyCode === 13) {

            // Select all the custom options box
            for (const select of document.querySelectorAll('.custom-select')) {
                e.preventDefault();

                // Check if search input is active
                if (!document.activeElement.classList.contains('form__search-input')) { return }

                // Remove the custom option box from UI
                select.classList.remove('open');

                // Select the main parent
                const parent = select.closest('.search-box');
                if (!parent) { return };

                let activeValue;

                // Declaring value to active value from search input
                activeValue = parent.querySelector('.form__search-input').value


                // Changing active value to guestpert id if available
                select.querySelectorAll('.custom-option').forEach(c => {
                    if (c.classList.contains('selected')) {
                        activeValue = `${c.getAttribute('data-id')}:tiai`;
                    }
                })

                // checking if active value is available
                if (!activeValue) { return };
                parent.querySelector('.form__hidden-input').value = activeValue;

                // Removing filter btns active property
                filterArr.forEach(c => c.classList.remove('hero__filter-btn--active'));

                // Remove everything from search Input
                parent.querySelector('.form__search-input').value = '';

                parent.querySelector('.form__search-input').blur();

                // calling init
                init(1);
            }

        }
    }
    )
}


export const removeHiddenInput=(parent)=>{
    const hiddenInput = parent.querySelector('.form__hidden-input');
    if(!hiddenInput)return;

    hiddenInput.value='';
}