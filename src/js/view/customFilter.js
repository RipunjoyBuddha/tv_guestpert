// The filter functionality takes 5 arguments
// searchbar , cards to be suffled , limit to show, class of txt 1 and class of txt 2
// nf_msg is the not found message
export function filter(searchBar, cards, limit, txt1=undefined, txt2=undefined, nf_msg=undefined){
    searchBar.addEventListener('keyup', function(){
        const keyword = this.value;
        let arr = [];
        cards.forEach((cur)=>{
            let str1='';
            let str2='';

            if(txt1 && cur.querySelector(txt1)){
                str1 = cur.querySelector(txt1).textContent;
            }
            if(txt2 && cur.querySelector(txt2)){
                str2 = cur.querySelector(txt2).textContent;
            }
            const str = `${str1} ${str2}`;
            const index = str.toLowerCase().indexOf(keyword);
            if(index>=0){
                const obj = {
                    i: index,
                    c: cur
                };
                arr.push(obj);
            }
                cur.style.display='none';
        })
        if(arr.length>0){
            arr = arr.sort((a,b)=> a.i - b.i);
            for(let i = 0; i<= limit-1; i++){
                if(arr[i]){
                    arr[i].c.style.order=arr[i].i;
                    arr[i].c.style.display='block';
                }
            }
            if(nf_msg){
                nf_msg.style.display='none';
            }
        }else{
            if(nf_msg){
                nf_msg.style.display='block';
            }
        }
    })
}


export function filterHotTopic(keyword, cards, cls, limit, nf_msg=undefined){
    let arr = [];
    cards.forEach((cur)=>{
        let str='';

        if(cur.querySelector(`.${cls}`)){
            str = cur.querySelector(`.${cls}`).textContent;
        }

        const index = str.toLowerCase().indexOf(keyword.toLowerCase());
        if(index>=0){
            const obj = {
                i: index,
                c: cur
            };
            arr.push(obj);
        }
            cur.style.display='none';
    })
    if(arr.length>0){
        arr = arr.sort((a,b)=> a.i - b.i);
        for(let i = 0; i<= limit-1; i++){
            if(arr[i]){
                arr[i].c.style.order=arr[i].i;
                arr[i].c.style.display='block';
            }
        }
        if(nf_msg){
            nf_msg.style.display='none';
        }
    }else{
        if(nf_msg){
            nf_msg.style.display='block';
        }
    }
}