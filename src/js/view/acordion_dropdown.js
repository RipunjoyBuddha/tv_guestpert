const btn = document.querySelectorAll('.faq__question');

btn.forEach(cur =>{
    cur.addEventListener('click', animate)
})

function animate(){
    // Check if the element is active or inactive
    const after = this.querySelector('.after');
    const before = this.querySelector('.before');
    const answer = this.parentNode.querySelector('.faq__answer');

    const check = after.classList.contains('after--inactive');
        // if(check == true) then (+ icon)
        // if(check == false) then (- icon)

    // If (- icon) make the element (+ icon) 
    if(!check){
        const video = answer.querySelector('iframe');
        if(video){
            const url = video.src;
            video.src = url;
        }

        after.classList.add('after--inactive');
        before.classList.add('before--inactive');
        answer.classList.add('faq__answer--inactive');
        return 
    }
    
    // If (+ icon)
    // Make every element (+ icon)
    btn.forEach(cur =>{
        cur.querySelector('.after').classList.add('after--inactive');
        cur.querySelector('.before').classList.add('before--inactive');
        cur.parentNode.querySelector('.faq__answer').classList.add('faq__answer--inactive');

        const video = cur.parentNode.querySelector('.faq__answer').querySelector('iframe');
        if(video){
            const url = video.src;
            video.src = url;
        }
    }) 
    
    // Make this element (- icon)
    after.classList.remove('after--inactive');
    before.classList.remove('before--inactive');
    answer.classList.remove('faq__answer--inactive');

}