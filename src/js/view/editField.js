import { removeImg } from './choose_img';
import dateFormat from "dateformat";
import {setContent} from '../utils/tinyActions';

/////////////////////////////////////
//---- Edit SubAdmin ---//
/////////////////////////////////////

export const editSubAdmin = (edit_btns, editSection, topicsSection, csrf) => {

    const editFormBasic = editSection.querySelector('.edit__form-basics');
    const editFormCredential = editSection.querySelector('.edit__form-credentials');

    // Get all the edit btns
    for (const editBtn of edit_btns) {
        editBtn.addEventListener('click', function () {
            const dataId = editBtn.getAttribute('data-id');

            editFormBasic.setAttribute('action', `/admin/editAdminBasics?id=${dataId}&&_csrf=${csrf}`)
            editFormCredential.setAttribute('action', `/admin/editAdminCredentials?id=${dataId}&&_csrf=${csrf}`)

            // Get all the info in html
            const allInfo = this.parentElement.parentElement.querySelector('.users__hidden-info');

            const id = editBtn.getAttribute('data-id');
            const firstName = allInfo.querySelector('.subAdmin__firstName').getAttribute('data-firstName');
            const lastName = allInfo.querySelector('.subAdmin__lastName').getAttribute('data-lastName');
            const username = allInfo.querySelector('.subAdmin__username').getAttribute('data-username');
            const email = allInfo.querySelector('.subAdmin__email').getAttribute('data-email');
            const phone = allInfo.querySelector('.subAdmin__phone').getAttribute('data-phone');
            const address = allInfo.querySelector('.subAdmin__address').getAttribute('data-address');
            const active = allInfo.querySelector('.subAdmin__active').getAttribute('data-active');

            // Put the respective values to edit section
            editSection.querySelector('#edit_firstName').value = firstName;
            editSection.querySelector('#edit_lastName').value = lastName;
            editSection.querySelector('#edit_address').value = address;
            editSection.querySelector('#edit_phone').value = phone;
            editSection.querySelector('#edit_email').value = email;
            editSection.querySelector('#edit_username').value = username;
            editSection.querySelector('#delete-subAdmin-btn').setAttribute('data-id', id);
            if (active === 'yes' || active == 'Yes' || active == 'YES') {
                editSection.querySelector('#edit_active').checked = true;
            }

            // Display the edit section and hide rest
            topicsSection.style.display = 'none';
            editSection.style.display = 'block';
        })
    }
}

/////////////////////////////////////
//---- Edit Slides ---//
/////////////////////////////////////

export const editSlides = (edit_btns, editSection, topicsSection, csrf) => {
    const editForm = editSection.querySelector('.edit__form-container');
    for (const editBtn of edit_btns) {
        editBtn.addEventListener('click', function(e) {

            const id = e.target.getAttribute('data-slide');

            // Get all the info in html
            const curSlide = editBtn.parentElement;

            const title = curSlide.querySelector('.slide-heading').textContent;
            const caption = curSlide.querySelector('#caption').getAttribute('data-caption');
            let vidLink = '';
            if(curSlide.querySelector('.slide-vidLink')){
                vidLink = curSlide.querySelector('.slide-vidLink').textContent;
            } 
            const anchor = curSlide.querySelector('.slide-anchor').textContent;
            const active = curSlide.querySelector('.slide-active').textContent;
            const slide = editBtn.getAttribute('data-slide');

            // Put the respective values to edit section
            if (title) {
                editSection.querySelector('#slideTitle').value = title;
            };

            // To be edited
            setContent('slideCaption', caption)

            if (vidLink) {
                editSection.querySelector('#videoLink').value = vidLink;
            }
            if (anchor) {
                editSection.querySelector('#anchor').value = anchor;
            }
            if (active === 'yes' || active == 'Yes' || active == 'YES') {
                editSection.querySelector('#active').checked = true;
            }
            editSection.querySelector('#edit__send-button').setAttribute('data-slide', slide);

            // Display the edit section and hide rest
            topicsSection.style.display = 'none';
            editSection.style.display = 'block';

            editForm.setAttribute('action', `/admin/editSlides?_csrf=${csrf}&&id=${id}`)
        })
    }

    editSection.querySelector('.edit__show-cards').addEventListener('click', () => {
        topicsSection.style.display = 'block';
        editSection.style.display = 'none';
    })
}

/////////////////////////////////////
//---- Edit HotTopic Categories ---//
/////////////////////////////////////

export const ht_Categories = (edit_btns, editSection, topicsSection, pageSection, csrf) => {

    // Edit Box--------
    const editBox = editSection.querySelector('#inpFile-box');
    const form = editSection.querySelector('#createEdit_form');

    for (const editBtn of edit_btns) {
        editBtn.addEventListener('click', function (e) {
            const curCard = this.parentElement.parentElement;
            const id = e.target.getAttribute('data-id')
            const catTitle = curCard.querySelector('.ht-categories__main-txt').textContent;
            const active = curCard.querySelector('.active-stats').getAttribute('data-active');

            if (catTitle) {
                editSection.querySelector('#catTitle').value = catTitle;
            };

            // Making the choose img value null
            removeImg(editBox);


            if (active === 'true') {
                editSection.querySelector('#active').checked = true;
            } else if (active === 'false') {
                editSection.querySelector('#active').checked = false;
            }

            // Set the form action attribute
            form.setAttribute('action', `/admin/editCategoryHotTopics?id=${id}&&_csrf=${csrf}`)

            // Display the edit section and hide rest
            topicsSection.style.display = 'none';
            pageSection.style.display = 'none';
            editSection.style.display = 'block';

        })

        editSection.querySelector('.edit__show-cards').addEventListener('click', () => {
            editSection.querySelector('#catTitle').value = '';
            editSection.querySelector('#active').checked = true;

            // Making the choose img value null
            removeImg(editBox);

            // Set the form action attribute
            form.setAttribute('action', '');

            topicsSection.style.display = 'flex';
            pageSection.style.display = 'flex';
            editSection.style.display = 'none';
        })
    }
}


/////////////////////////////////////
//---- Edit Membership ---//
/////////////////////////////////////

export const editMembership = (edit_btns, editSection, topicsSection, csrf) => {
    for (const editBtn of edit_btns) {
        editBtn.addEventListener('click', function () {
            const id = this.getAttribute('data-id');
            const editForm = editSection.querySelector('.edit__form-container');
            const priceInfo = this.parentElement.querySelector('.membership__price').getAttribute('data-price').replace('$', '');
            const titleInfo = this.parentElement.querySelector('.membership__title').textContent;

            editSection.querySelector('#membershiptitle').value = titleInfo;
            editSection.querySelector('#membershipprice').value = +priceInfo;
            editForm.setAttribute('action', `/admin/editMembership?id=${id}&&_csrf=${csrf}`);

            // Display the edit section and hide rest
            topicsSection.style.display = 'none';
            editSection.style.display = 'block';
        })
    }

    editSection.querySelector('.edit__show-cards').addEventListener('click', () => {
        topicsSection.style.display = 'flex';
        editSection.style.display = 'none';
    })
}

/////////////////////////////////////
//---- Edit Appearance ---//
/////////////////////////////////////

export const editAppearance = (dataObj, editSection, topicsSection) => {

    editSection.querySelector('#videoTitle').value = dataObj.videoTitle;
    editSection.querySelector('#videoLink').value = dataObj.videoLink;
    
    // Display the edit section and hide rest
    topicsSection.style.display = 'none';
    editSection.style.display = 'block';
};

export const removeAppearance = (editSection)=>{
    editSection.querySelector('#videoTitle').value = '';
    editSection.querySelector('#videoLink').value = '';
}


/////////////////////////////////////
//---- Edit Guestpert Books ---//
/////////////////////////////////////

export const editGuestpertBooks = (edit_btn_class, editSection, topicsSection, csrf, guestpertId) => {
    const form = editSection.querySelector('.edit__form-container');
    // Select inputFile container
    const inpFile = editSection.querySelector('#inpFile-box');

    topicsSection.addEventListener('click', (e) => {

        if (e.target.classList.contains(edit_btn_class)) {
            const btn = e.target;
            const bookId = btn.getAttribute('data-id');
            const box = btn.parentElement.parentElement;

            //  grab everything
            let bookTitle, publishedDate, activeStats, price, bestsellerStats;

            if (box.querySelector('.products__heading')) {
                bookTitle = box.querySelector('.products__heading').textContent;
            };

            if (box.querySelector('.publishedDate')) {
                publishedDate = box.querySelector('.publishedDate').textContent;
            };

            if (box.querySelector('.activeStats')) {
                activeStats = box.querySelector('.activeStats').textContent;
            };



            // // Put it into 
            if (bookTitle) {
                editSection.querySelector('#booktitle').value = bookTitle;
            }
            if (publishedDate) {
                editSection.querySelector('#publishDate').value = dateFormat(publishedDate, 'yyyy-mm-dd');
            }
            if (activeStats === 'yes' || activeStats == 'Yes' || activeStats == 'YES') {
                editSection.querySelector('#active').checked = true;
            }

            removeImg(inpFile);

            // Display the edit section and hide rest
            topicsSection.style.display = 'none';
            editSection.style.display = 'block';

            form.setAttribute('action', `/guestpert/editBlogs?_csrf=${csrf}&&guestpertId=${guestpertId}&&bookId=${bookId}`)
        }
    })

    editSection.querySelector('.edit__show-cards').addEventListener('click', () => {
        editSection.querySelector('#booktitle').value = '';
        editSection.querySelector('#publishDate').value = '';
        editSection.querySelector('#price').value = '';
        editSection.querySelector('#active').checked = true;
        editSection.querySelector('#bestseller').checked = true;
        removeImg(inpFile);

        topicsSection.style.display = 'flex';
        editSection.style.display = 'none';
    })
}


/////////////////////////////////////
//---- Edit Guestpert Demo Reel ---//
/////////////////////////////////////

export const editGuestpertReel = (edit_btn_class, editSection, topicsSection, csrf, guestpertId) => {
    topicsSection.addEventListener('click', (e) => {
        const form = editSection.querySelector('.edit__form-container');
        if (e.target.classList.contains(edit_btn_class)) {
            const btn = e.target;
            const reelId = btn.getAttribute('data-id');
            const box = btn.parentElement.parentElement;

            //  grab everything
            let title, activeStats, urlFrame;

            if (box.querySelector('.reel__title')) {
                title = box.querySelector('.reel__title').textContent;
            };

            if (box.querySelector('iframe')) {
                const url = box.querySelector('iframe').getAttribute('src');
                urlFrame = `<iframe width="560" height="315" src="${url}" frameborder="0" allowfullscreen></iframe>`
            };

            if (box.querySelector('.activeStats')) {
                activeStats = box.querySelector('.activeStats').textContent;
            };


            // Put it into 
            if (title) {
                editSection.querySelector('#reeltitle').value = title;
            }
            if (urlFrame) {
                editSection.querySelector('#link').value = urlFrame;
            }
            if (activeStats === 'yes' || activeStats == 'Yes' || activeStats == 'YES') {
                editSection.querySelector('#active').checked = true;
            } else{
                editSection.querySelector('#active').checked = false;
            }

            // Display the edit section and hide rest
            topicsSection.style.display = 'none';
            editSection.style.display = 'block';

            form.setAttribute('action', `/guestpert/editDemoReel?_csrf=${csrf}&&guestpertId=${guestpertId}&&reelId=${reelId}`)
        }
    })

    editSection.querySelector('.edit__show-cards').addEventListener('click', () => {
        editSection.querySelector('#reeltitle').value = '';
        editSection.querySelector('#link').value = '';
        editSection.querySelector('#active').checked = true;

        topicsSection.style.display = 'flex';
        editSection.style.display = 'none';
    })
}