import { domain } from "../utils/domain";



/////////////////////////
//--- Render Cards ---//
export const renderCards = (parentBox, dataArr) => {
    if (!parentBox) { return }

    parentBox.innerHTML = '';

    if (dataArr.length < 1) {
        const markup = `<p class="paragraph__not-found">No Testimonials Found</p>`;
        parentBox.insertAdjacentHTML('beforeend', markup)

    }
    else {
        dataArr.forEach((cur) => {
            parentBox.insertAdjacentHTML('beforeend', markup(cur));
        })
    }
}



//////////////////////////
//--- Card Markup ---//---
//////////////////////////

function markup(cur) {

    const markup = `
    <div class="feedback__card u-mar-t-5 testimonial__card">
    
    <!-- Left hand side box -->
    <div class="feedback__img-wrapper">
        <div class="feedback__profile-img">
            <img src="${domain}${cur.imgSrc}" alt="image" class="fluid-img">
        </div>
    </div>

    <!-- Right hand side box -->
    <div class="feedback__content-container">
        
        <div class="feedback__content">
             ${cur.content}
        </div>

        <div class="feedback__video-container u-mar-t-3">
            ${cur.vidLink.join(' ')}
        </div>
        
        <p class="paragraph-tertiary color-txt-dark u-mar-t-3 text-bold-1">${cur.name} , ${cur.title}</p>
    </div>

</div>
    `
    return markup
}