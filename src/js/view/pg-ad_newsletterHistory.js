import dateformat from "dateformat";

//////////////////////////
//--- Render Cards ---//
export const renderCards = (parentBox, dataArr) => {
    if (!parentBox) { return }

    parentBox.innerHTML = '';

    if(dataArr.length<1){
        const markup = `<p class="paragraph__not-found">No History Found</p>`;
        parentBox.insertAdjacentHTML('beforeend', markup)
        
    }
    else{
        dataArr.forEach((cur) => {
            parentBox.insertAdjacentHTML('beforeend', markup(cur));
        })
    }
}


function markup(cur) {

    const markup = `
    <div class="newsletter-history__card">
        <ul class="newsletter-history__info">
            <li>
                <p class="newsletter-history__key">Subject</p>
                <p class="newsletter-history__value">${cur.subject}</p>
            </li>
            <li>
                <p class="newsletter-history__key">Content</p>
                <div class="newsletter-history__value">
                    ${cur.content}
                </div>
            </li>
            <li>
                <p class="newsletter-history__key">Posted</p>
                <p class="newsletter-history__value">${dateformat(cur.history, 'dd mmm yyyy')}</p>
            </li>
        </ul>
    </div>
    `
    return markup
}