const pageContainer = document.getElementById('page__list');

export class pagination {
    constructor(totalItems, elPerPage=2, cb=(a)=>{},  pageNumList=pageContainer, totalPageChk=false) {
        this.pageShow = 5;
        this.elPerPage = elPerPage;
        this.pageNumList = pageNumList;
        this.totalItems = totalItems;
        this.totalPages = Math.ceil(this.totalItems / this.elPerPage);
        this.cb=cb;
        this.formPage(this.totalPages, this.pageShow, this.pageNumList, this.cb);
        this.changePage(this.pageShow, this.pageNumList, this.styleEl, this.cb);
        this.styleEl(this.totalPages);
        this.totalPageChk = totalPageChk;
    }

    ///////////////////////////////////////////
    ////-- Form pages at the begining --////
    ///////////////////////////////////////////

    async formPage(totalPages, pageShow, pageNumList, cb) {

        const totalItems = await cb(1, this.elPerPage);
        if(totalItems === Error){return};
        
        
        if(totalItems){
            totalPages = Math.ceil(totalItems / this.elPerPage);
            this.totalPages= totalPages;
        }
        
        
        const prevNext_markup = `
                <a class="page__num page__next clickable" data-num="2">Next</a>
        `;

        function markup(i, j, cls = '') {
            return `
            <a class="page__num ${cls}" data-num="${j}">${i}</a>
            `
        }

        // If total pages < 5
        if (totalPages > 1 && totalPages <= pageShow) {
            pageNumList.innerHTML = prevNext_markup;
            for (let i = 1; i <= totalPages; i++) {
                if (i === 1) {
                    pageNumList.insertAdjacentHTML('beforeend', markup(i, i, 'page__num--active'))
                } else {
                    pageNumList.insertAdjacentHTML('beforeend', markup(i, i, 'clickable'))
                }
            }
            pageNumList.style.display = 'flex';
        }

        // If total pages > 5
        else if (totalPages > pageShow) {
            pageNumList.innerHTML = prevNext_markup;
            for (let i = 1; i <= pageShow; i++) {
                if (i === 1) {
                    pageNumList.insertAdjacentHTML('beforeend', markup(i, i, 'page__num--active'))
                } else if (i === pageShow - 1) {
                    pageNumList.insertAdjacentHTML('beforeend', markup('...', '', ''))
                }
                else if (i === pageShow) {
                    pageNumList.insertAdjacentHTML('beforeend', markup(totalPages, totalPages, 'clickable'))
                }
                else {
                    pageNumList.insertAdjacentHTML('beforeend', markup(i, i, 'clickable'))
                }
            }
            pageNumList.style.display = 'flex';
        }

        // if total pages = 1 or less than 1
        else {
            pageNumList.style.display = 'none';
        }
    }


    ///////////////////////////////////////////
    ////-- Change pages --////
    ///////////////////////////////////////////

    changePage(pageShow, pageNumList, styleFunc, cb) {
        pageNumList.addEventListener('click', async (e) => {
            if (e.target.classList.contains('clickable')) {

                
                
                // If data-num is absent
                if(!e.target.getAttribute('data-num')){
                    return
                }

                let pageNum = +e.target.getAttribute('data-num');
                
                pageNum<1?pageNum=1:pageNum=pageNum;
                pageNum>this.totalPages?pageNum=this.totalPages:pageNum=pageNum;

                const changedtotalItems = await cb(pageNum, this.elPerPage);
                if(changedtotalItems === Error){return};

                if(changedtotalItems){
                    this.totalItems = changedtotalItems;
                    this.totalPages = Math.ceil(this.totalItems / this.elPerPage);
                }

                const prevNext_markup = `
                <a class="page__num page__prev clickable" data-num="${pageNum-1}">Prev</a>
                <a class="page__num page__next clickable" data-num="${pageNum+1}">Next</a>
                `;

                function markup(i, j, cls = '') {
                    return `
                    <a class="page__num ${cls}" data-num="${j}">${i}</a>
                    `
                }

                pageNumList.innerHTML = prevNext_markup;

                // if this.totalPages are less than 5
                if (this.totalPages <= pageShow) {
                    for (let i = 1; i <= this.totalPages; i++) {
                        if (i === pageNum) {
                            pageNumList.insertAdjacentHTML('beforeend', markup(i, i, 'page__num--active'))
                        } else {
                            pageNumList.insertAdjacentHTML('beforeend', markup(i, i, 'clickable'))
                        }
                    }
                    pageNumList.style.display = 'flex';
                }


                // if total pages are more than 5
                else{
                    if(pageNum > 1 && pageNum < this.totalPages){
                        const md_p = pageNum-1;
                        const md_n = pageNum+1;
                        const numBtwfrst = md_p - 1 -1;
                        const numBtwlst = this.totalPages -1 -md_n;
                        
                        pageNumList.insertAdjacentHTML('beforeend', markup(1, 1, 'clickable'));

                        if(numBtwfrst <= 0){}
                        else if(numBtwfrst === 1){
                            pageNumList.insertAdjacentHTML('beforeend', markup(md_p-1, md_p-1, 'clickable'));
                        }else{
                            pageNumList.insertAdjacentHTML('beforeend', markup('...', '', ''));
                        };
                        
                        if(numBtwfrst < 0){}
                        else{
                            pageNumList.insertAdjacentHTML('beforeend', markup(md_p, md_p, 'clickable'));
                        };
                        pageNumList.insertAdjacentHTML('beforeend', markup(pageNum, pageNum, 'page__num--active'));
                        
                        if(numBtwlst < 0){}
                        else {
                            pageNumList.insertAdjacentHTML('beforeend', markup(md_n, md_n, 'clickable'));
                        };

                        if(numBtwlst<=0){}
                        else if(numBtwlst===1){
                            pageNumList.insertAdjacentHTML('beforeend', markup(md_n+1, md_n+1, 'clickable'));
                        }else{
                            pageNumList.insertAdjacentHTML('beforeend', markup('...', '', ''));
                        };
                        pageNumList.insertAdjacentHTML('beforeend', markup(this.totalPages, this.totalPages, 'clickable'));
                    }
                    else if (pageNum===1){
                        for (let i = 1; i <= pageShow; i++) {
                            if (i === 1) {
                                pageNumList.insertAdjacentHTML('beforeend', markup(i, i, 'page__num--active'))
                            } else if (i === pageShow - 1) {
                                pageNumList.insertAdjacentHTML('beforeend', markup('...', '', ''))
                            }
                            else if (i === pageShow) {
                                pageNumList.insertAdjacentHTML('beforeend', markup(this.totalPages, this.totalPages, 'clickable'))
                            }
                            else {
                                pageNumList.insertAdjacentHTML('beforeend', markup(i, i, 'clickable'))
                            }
                        }
                    }
                    else{
                        for (let i = 1; i <= pageShow; i++) {
                            if (i === 1) {
                                pageNumList.insertAdjacentHTML('beforeend', markup('1', '1', 'clickable'))
                            }
                            else if (i === 2) {
                                pageNumList.insertAdjacentHTML('beforeend', markup('...', '', ''))
                            }
                            else if (i === pageShow){
                                pageNumList.insertAdjacentHTML('beforeend', markup(this.totalPages, this.totalPages, 'page__num--active'))
                            }
                            else {
                                pageNumList.insertAdjacentHTML('beforeend', markup(this.totalPages-pageShow+i, this.totalPages-pageShow+i, 'clickable'))
                            }
                        }
                    }
                }

                if(pageNum){
                    styleFunc(this.totalPages, this);
                }
            }
            else {
                return
            }
        })
    }



    
    ///////////////////////////////////////////
    ////-- Style the page number box --////
    ///////////////////////////////////////////
    styleEl(lastPage, obj=this) {
        
        // Prev and Next button
        const prevBtn = obj.pageNumList.querySelector('.page__prev');
        const nextBtn = obj.pageNumList.querySelector('.page__next');
        
        if(obj.pageNumList.querySelector('.page__num--active')){
            const activeEl = obj.pageNumList.querySelector('.page__num--active');
            const pageNum = activeEl.getAttribute('data-num');

            if (lastPage > 1 && +pageNum &&prevBtn) {
                if ( +pageNum=== 1) {
                    prevBtn.style.display = 'none';
                } else if ( +pageNum=== lastPage  && nextBtn) {
                    nextBtn.style.display = 'none';
                }
                else {
                    prevBtn.style.display = 'flex'
                    nextBtn.style.display = 'flex'
                }
            }
        }
    }
}


export function showCards(cards, style='block'){

    return function (pageNum, pageLimit){
        cards.forEach(cur=> cur.style.display='none');
        const ul= pageLimit*pageNum-1;
        const ll = pageLimit*pageNum - pageLimit;
        
        for( let i = ll; i<=ul; i++){
            if(cards[i]){
                cards[i].style.display=style;
            }
        }
    }
}
