import dateformat from "dateformat";
import {removeImg} from './choose_img';
import {setContent} from '../utils/tinyActions';

//////////////////////////
//--- Render Cards ---//
export const renderCards = (parentBox, dataArr) => {
    if (!parentBox) { return }

    parentBox.innerHTML = '';

    if(dataArr.length<1){
        const markup = `<p class="paragraph__not-found">No Blogs Available</p>`;
        parentBox.insertAdjacentHTML('beforeend', markup);
        
    }
    else{
        dataArr.forEach((cur) => {
            parentBox.insertAdjacentHTML('beforeend', markup(cur))
        })
    }
}


//--- Render Edit Functionality ---//
const editSection = document.querySelector('#editBlog');

const editStore = {
    title: editSection.querySelector('#title'),
    inpFile: editSection.querySelector('#inpFile-box'),
    activeStats: editSection.querySelector('#active-stats'),
    backBtn: editSection.querySelector('.edit__show-cards')
}



//////////////////////////////
////---- Render Edit ----////
/////////////////////////////

export const renderEdit = (dataObj)=>{

    // Fill up the contents
    editStore.title.value = dataObj.title;
    editStore.activeStats.checked = dataObj.active;
    removeImg(editStore.inpFile);
    //--- To be applied on tinyMCE---//
    // tinymce.activeEditor.setContent(dataObj.content);
    setContent('content', dataObj.content);

    // Put class in show blogs btn
    editStore.backBtn.classList.add('editMode');
}


//--- Remove Edit Fields ---//
export const removeFields = ()=>{

    // Fill up the contents
    editStore.title.value = '';
    editStore.activeStats.checked = true;
    setContent('content', '');
    removeImg(editStore.inpFile);
    //--- To be applied on tinyMCE---//
    

    // Remove class from show blogs btn
    editStore.backBtn.classList.remove('editMode');
}



function markup(cur) {

    // Change vals
    cur.activeStats==true?cur.active='yes':cur.active='no';

    const markup = `
    <li class="hot-topics__cards-wrapper">
        <div class="card-4 hot-topics__cards">
            <div class="hot-topics__title-container u-center-text">

                <!-- Title of Hot Topic -->
                <p class="hot-topics__title">
                    ${cur.title}
                </p>

                <!-- 3D Look -->
                <svg class="hot-topics__icon hot-topics__icon--top-left">
                    <use xlink:href="/images/sprite.svg#left-top"></use>
                </svg>
                <svg class="hot-topics__icon hot-topics__icon--bottom-left">
                    <use xlink:href="/images/sprite.svg#left-bottom"></use>
                </svg>
                <svg class="hot-topics__icon hot-topics__icon--top-right">
                    <use xlink:href="/images/sprite.svg#right-top"></use>
                </svg>
                <svg class="hot-topics__icon hot-topics__icon--bottom-right">
                    <use xlink:href="/images/sprite.svg#right-bottom"></use>
                </svg>
            </div>

            <!-- Duplicate hot topic title -->
            <div class="hot-topics__title-duplicate u-center-text">
                <p class="hot-topics__title">
                    ${cur.title}
                </p>
            </div>

            <ul class="info-list u-mar-t-2">
                <li>
                    <p class="info-list__key">Created</p>
                    <p class="info-list__value">${dateformat(cur.createdAt, 'dd mmm yyyy')}</p>
                </li>
                <li>
                    <p class="info-list__key">Modified</p>
                    <p class="info-list__value">${dateformat(cur.modifiedAt, 'dd mmm yyyy')}</p>
                </li>
                <li>
                    <p class="info-list__key">Active</p>
                    <p class="info-list__value">${cur.active}</p>
                </li>
            </ul>

            <div class="u-mar-b-2">
                <button data-id="${cur._id}" class="hot-topics__card-btn-edit button__primary">Edit</button>
                <button data-id="${cur._id}" class="hot-topics__card-btn-delete button__ghost button__ghost--sm">Delete</button>
            </div>
        </div> 
    </li> 
    `
    return markup
}