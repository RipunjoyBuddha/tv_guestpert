let clearTimer;

export const message = (time, result, message)=>{

    if(clearTimer){clearTimeout(clearTimer)};
    
    const el = document.querySelector('.message');
    const eltext = el.querySelector('.message__txt');
    eltext.textContent = message;

    if(result == true){
        eltext.classList.remove('message__txt--error');
        eltext.classList.add('message__txt--success')
    }
    else if(result == false){
        eltext.classList.remove('message__txt--success');
        eltext.classList.add('message__txt--error');
    }
    el.style.display = 'flex';
    clearTimer = setTimeout(()=>{
        el.style.display = 'none';
        eltext.classList.remove('message__txt--success');
        eltext.classList.remove('message__txt--error');
    }, time) 
}