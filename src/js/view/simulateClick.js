export const simulateClick = function (elem) {
	// Create our event (with options)
	const evt = new MouseEvent('click', {
		bubbles: true,
		cancelable: true,
		view: window
	});
	// If cancelled, don't dispatch our event
	const canceled = !elem.dispatchEvent(evt);
};