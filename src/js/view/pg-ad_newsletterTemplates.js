/////////////////////////
//--- Render Cards ---//
export const renderCards = (parentBox, dataArr) => {
    if (!parentBox) { return }

    parentBox.innerHTML = '';

    if(dataArr.length<1){
        const markup = `<p class="paragraph__not-found">No Newsletter Found</p>`;
        parentBox.insertAdjacentHTML('beforeend', markup)
        
    }
    else{
        dataArr.forEach((cur) => {
            parentBox.insertAdjacentHTML('beforeend', markup(cur));
        })
    }
}


///////////////////////////
//--- Markup Function ---//

function markup(cur) {

    const markup = `
    <div class="card-4 faty__card">
        <div class="faty__head">
            <p class="paragraph-tertiary color-txt-dark u-center-text">${cur.title}</p>
        </div>
        
        <div class="faty__card-btns">
            <button data-id="${cur._id}" class="btn-send button__primary">${cur.active?'unpost':'post'}</button>
            <button data-id="${cur._id}" class="faty__card-btn-remove button__ghost button__ghost--sm">Remove</button>
        </div>
    </div>
    `
    return markup
}