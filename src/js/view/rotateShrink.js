const hotTopics = document.querySelector('.featured__hot-topics');
const guestPerts = document.querySelector('.featured__guestperts')



class rotateShrink {
    //---- Construct object
    constructor(txtElement, useArray, concatValue) {
        this.txtElement = txtElement;
        this.index = 0;
        this.num = 0;
        this.concat = concatValue;
        this.objEl = {};
        this.contentObj = useArray;
        this.animate();
    }

    animate() {
        let waitTime = 5000;
        //---- Growing animation
        if (this.num === this.index) {
            //------ Select the image and content
            const content = this.contentObj[0];
            this.objEl = content;
            
            //------ Remove first element and add it to last
            this.contentObj.shift();
            

            //------ Put content to element
            if (this.concat) {
                this.txtElement.setAttribute('data-id', content.guestpertId)
                this.txtElement.innerHTML = `
                <img src="${content.imgSrc}" alt="Guestpert Image"
                class="gallery__img ">
                <div class="gallery__about-img">
                <p3 class="caption-text-bg text-bold-1">${content.title1.substring(0, 16)}...</p3>
                    <p3 class="caption-text-sm ">${content.title2}</p3>
                    </div>
                    `;
            }
            else {
                this.txtElement.setAttribute('data-id', content.guestpertId)
                this.txtElement.innerHTML = `
                <img src="${content.imgSrc}" alt="Guestpert Image"
                class="gallery__img ">
                <div class="gallery__about-img">
                    <p3 class="caption-text-bg text-bold-1">${content.title1}</p3>
                    <p3 class="caption-text-sm ">${content.title2}</p3>
                </div>
                `;
            }

            //------ index++
            this.index++;

            //------wait Time
            waitTime = Math.random() * 10000;
            waitTime += 4000;


            //------ Remove Class
            this.txtElement.classList.remove('gallery__rotate-shrink')
        }


        //---- Shrinking and pushing to main array
        else {
            //---- Add class to shrink
            this.txtElement.classList.add('gallery__rotate-shrink');

            //---- Making index = num
            this.index = this.num;

            //---- Pushing item to main array
            this.contentObj.push(this.objEl);

            //---- WaitTime
            waitTime = 300;
        }

        setTimeout(() => this.animate(), waitTime);
    }
}


export function rotateShrinkFunc() {

    function createClass(section, object, featureConcat) {
        section.querySelectorAll('.gallery__img-container').forEach(cur => {
            const txtEl = cur.querySelector('.gallery__img-frame');
            new rotateShrink(txtEl, object, featureConcat);
        })
    }

    //---- Works for Section HotTopics 
    //-- 2nd argument specify the name the array
    //-- Compulsory to give 3 arguments. 3rd can be skipped if subString not needed
    createClass(hotTopics, hotTopicsObj, true);

    //---- Works for Section Guestperts
    createClass(guestPerts, guestPertObj, false);

}

// Array of all content objects
function arrOfObj(section) {
    return Array.from(section.querySelectorAll('.gallery__modal-img')).map(el => {
        const imgSrc = el.src;
        const title1 = el.getAttribute('data-title1');
        const title2 = el.getAttribute('data-title2');
        const guestpertId = el.getAttribute('data-id')
        return {
            imgSrc,
            title1,
            title2,
            guestpertId
        }
    })
}

//-- Create new for a new section . Argument is the section element mentioned in top of page
const hotTopicsObj = [...arrOfObj(hotTopics)];
const guestPertObj = [...arrOfObj(guestPerts)];


