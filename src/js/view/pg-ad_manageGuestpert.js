import dateformat from "dateformat";
import { domain } from "../utils/domain";
import {setContent} from '../utils/tinyActions';
import {updateContent} from '../vendor/form-dropdown_manageGuestperts';
import dateFormat from 'dateformat';


//////////////////////////
//--- Render Cards ---//
export const renderCards = (parentBox, dataArr) => {
    if (!parentBox) { return }

    parentBox.innerHTML = '';

    if (dataArr.length < 1) {
        const markup = `<p class="paragraph__not-found">No Such Guestpert available</p>`;
        parentBox.insertAdjacentHTML('beforeend', markup)

    }
    else {
        dataArr.forEach((cur) => {
            parentBox.insertAdjacentHTML('beforeend', markup(cur, domain))
        })
    }
}



//////////////////////////
//--- Render Edit ---//

const editStore = {
    editSection: document.querySelector('#editSection'),
    guestpertImg: editSection.querySelector('#edit_guestpert-img'),
    firstName: editSection.querySelector('#edit_firstname'),
    lastName: editSection.querySelector('#edit_lastName'),
    prefix: editSection.querySelector('#edit_prefix'),
    sufix: editSection.querySelector('#edit_sufix'),
    title: editSection.querySelector('#edit_title'),
    tagline: editSection.querySelector('#edit_tagline'),
    primaryExpertise: editSection.querySelector('#edit_primaryexpertise'),
    extendedExpertise: editSection.querySelector('#edit_expertise'),

    membershipForm : editSection.querySelector('.edit__form-container--memberships'),
    expiryDate: editSection.querySelector('#edit_expiryDate'),
    active: editSection.querySelector('#active'),

    address: editSection.querySelector('#edit_address'),
    city: editSection.querySelector('#edit_city'),
    state: editSection.querySelector('#edit_state'),
    email: editSection.querySelector('#edit_email'),
    zipCode: editSection.querySelector('#edit_zipCode'),
    country: editSection.querySelector('#edit_country'),
    phone: editSection.querySelector('#edit_phone'),
    website: editSection.querySelector('#edit_website'),
    fax: editSection.querySelector('#edit_fax'),
    rssfeed: editSection.querySelector('#edit_rssfeed'),
    username: editSection.querySelector('#edit_username'),
    premium: editSection.querySelector('#premium'),

    delBtn: editSection.querySelector('#delete-guestpert-btn'),

    otherFields: editSection.querySelector('#edit__otherFields'),

    // hotTopics: editse
};

export const renderEdit = (dataObj) => {
    console.log(editStore.active)
    // Set the content
    editStore.guestpertImg.setAttribute('src', `${domain}${dataObj.imgSrc}`);
    editStore.firstName.value = dataObj.firstName;
    editStore.lastName.value = dataObj.lastName;
    editStore.prefix.value = dataObj.prefix;
    editStore.sufix.value = dataObj.suffix;
    editStore.title.value = dataObj.title;
    editStore.premium.checked = dataObj.premium;

    editStore.tagline.value = dataObj.tagLine;
    editStore.primaryExpertise.value = dataObj.primaryExpertise;
    editStore.extendedExpertise.value = dataObj.expandedExpertise;

    const dropDownEdit = new updateContent(editStore.membershipForm);
    dropDownEdit.removeContent();
    dropDownEdit.setContent(dataObj.membership);
    editStore.expiryDate.value = dataObj.membershipExpiryDate?dateFormat(dataObj.membershipExpiryDate, 'yyyy-mm-dd'):'';
    editStore.active.checked = dataObj.membershipOrder?true:false;

    editStore.address.value = dataObj.address;
    editStore.city.value = dataObj.city;
    editStore.state.value = dataObj.state;
    editStore.email.value = dataObj.email;
    editStore.zipCode.value = dataObj.zipCode;
    editStore.country.value = dataObj.country;
    editStore.phone.value = dataObj.phone;
    editStore.website.value = dataObj.website;
    editStore.fax.value = dataObj.fax;
    editStore.rssfeed.value = dataObj.rssfeed;

    editStore.username.value = dataObj.username;

    setContent('biography', dataObj.biography);

    // Put the Id in delBtn
    editStore.delBtn.setAttribute('data-id', dataObj._id);

    // Editing Other Fields
    editStore.otherFields.querySelector('#hot-topics').setAttribute('href', `/admin/editGuestpertOtherFields/hotTopics?guestpertId=${dataObj._id}`)
    editStore.otherFields.querySelector('#media').setAttribute('href', `/admin/editGuestpertOtherFields/media?guestpertId=${dataObj._id}`)
    editStore.otherFields.querySelector('#demo-reel').setAttribute('href', `/admin/editGuestpertOtherFields/demoReel?guestpertId=${dataObj._id}`)
    editStore.otherFields.querySelector('#blogs').setAttribute('href', `/admin/editGuestpertOtherFields/blogs?guestpertId=${dataObj._id}`)
    editStore.otherFields.querySelector('#books').setAttribute('href', `/admin/editGuestpertOtherFields/books?guestpertId=${dataObj._id}`) 

    // Changing Form Actions
    
}


// Clear edit
export const clearEdit = ()=>{

    // Set the content
    editStore.guestpertImg.setAttribute('src', `${domain}images/user.6a49468980ebb03c04e35b656f3fa264.png`);
    editStore.firstName.value = '';
    editStore.lastName.value = '';
    editStore.prefix.value = '';
    editStore.sufix.value = '';
    editStore.title.value = '';
    editStore.tagline.value='';
    editStore.primaryExpertise.value = '' ;
    editStore.extendedExpertise.value = '' ;

    editStore.address.value=''
    editStore.city.value = '';
    editStore.state.value = '';
    editStore.email.value = '';
    editStore.phone.value = '';
    editStore.website.value = '';
    editStore.fax.value = '';
    editStore.rssfeed.value = '';

    setContent('biography', '');

    // Put the Id in delBtn
    editStore.delBtn.setAttribute('data-id', '');

    // Editing Other Fields
    editStore.otherFields.querySelector('#hot-topics').setAttribute('href', '');
    editStore.otherFields.querySelector('#media').setAttribute('href', '');
    editStore.otherFields.querySelector('#demo-reel').setAttribute('href', '');
    editStore.otherFields.querySelector('#blogs').setAttribute('href', '');
    editStore.otherFields.querySelector('#books').setAttribute('href', '');
}





function markup(cur, domain) {

    const today = new Date();
    let expiryDate = new Date(cur.membershipExpiryDate);

    // Change vals
    if(cur.membershipExpiryDate.length>0){
        expiryDate<today ? cur.active = 'no' : cur.active = 'yes';
        expiryDate = dateformat(cur.membershipExpiryDate, 'd mmm yyyy');
    }else{
        cur.active='no';
        expiryDate = '';
    }
    if(!cur.membershipOrder){
        cur.active = 'no'
    }
    
    
    const markup = `
    <div class="card-3 users__card ">
    <div class="users__card-heading">
        <div class="users__person">
            <p class="paragraph-primary users__person-name">${cur.prefix} ${cur.firstName} ${cur.lastName} ${cur.suffix}</p>
        </div>
        <div class="users__person-title">
            <p class="txt-capitalize">${cur.title}</p>
            <p class="txt-uppercase">${cur.primaryExpertise}</p>
        </div>
        <div class="users__card-img users__card-img--position-right">
            <img src="${domain}${cur.imgSrc}" alt="${cur.firstName} ${cur.lastName} image" class="fluid-img">
        </div>
    </div>

    <div class="users__info">
        <ul class="info-list u-mar-t-2">
            <li>
                <p class="info-list__key">User name</p>
                <p class="info-list__value">${cur.username}</p>
            </li>
            <li>
                <p class="info-list__key">Joined</p>
                <p class="info-list__value">${dateformat(cur.createdAt, 'd mmm yyyy')}</p>
            </li>
            <li>
                <p class="info-list__key">Role</p>
                <p class="info-list__value">${cur.role}</p>
            </li>
            <li>
                <p class="info-list__key">Membership</p>
                <p class="info-list__value">${cur.membership}</p>
            </li>
            <li>
                <p class="info-list__key">Expiry</p>
                <p class="info-list__value">${expiryDate}</p>
            </li>
            <li>
                <p class="info-list__key">Active</p>
                <p class="info-list__value">${cur.active}</p>
            </li>
            <li>
                <p class="info-list__key">Premium</p>
                <p class="info-list__value">${cur.premium?'Yes':'No'}</p>
            </li>
        </ul>
    </div>
    <div class="users__hidden-info">
        <img src="${domain}${cur.imgSrc}" class="hide-img">

        <div class="guestpert__prefix" data-prefix="${cur.prefix}"></div>
        <div class="guestpert__firstName" data-firstName="${cur.firstName}"></div>
        <div class="guestpert__lastName" data-lastName="${cur.lastName}"></div>
        <div class="guestpert__suffix" data-sufix="${cur.suffix}"></div>
        <div class="guestpert__nickname" data-nickname="${cur.nickname}"></div>
        <div class="guestpert__username" data-username="${cur.username}"></div>
        <div class="guestpert__title" data-title="${cur.title}"></div>
        <div class="guestpert__primary-expertise" data-primary-expertise="${cur.primaryExpertise}"></div>

        <div class="guestpert__address" data-address="${cur.address}"></div>
        <div class="guestpert__city" data-city="${cur.city}"></div>
        <div class="guestpert__state" data-state="${cur.state}"></div>
        <div class="guestpert__email" data-email="${cur.email}"></div>
        <div class="guestpert__phone" data-phone="${cur.phone}"></div>
        <div class="guestpert__website" data-website="${cur.website}"></div>
        <div class="guestpert__fax" data-fax="${cur.fax}"></div>
        <div class="guestpert__rssfeedlink" data-rssFeedLink="${cur.rssfeedlink}"></div>

        <div class="guestpert__role" data-role="${cur.role}"></div>
        <div class="guestpert__membership" data-membership="${cur.membership}"></div>
        <div class="guestpert__joined" data-joined="${dateformat(cur.createdAt, 'd mmm yyyy')}"></div>
        <div class="guestpert__expiry" data-expiry="${expiryDate}"></div>
        <div class="guestpert__active" data-active="${cur.active}"></div>
        <div class="guestpert__premium" data-premium="${cur.premium?'Yes':'No'}"></div>
    </div>
    <div class="users__card-btns">
        <button data-id="${cur._id}" class="users__card-btn-edit button__primary">Edit</button>
        <button data-id="${cur._id}" class="users__card-btn-details button__ghost button__ghost--sm">More
            Details</button>
    </div>
</div>
    `
    return markup
}