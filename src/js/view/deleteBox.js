const popup = document.querySelector('.popup');
const delBox = popup.querySelector('.popup__del-box');
const delBtn = delBox.querySelector('.popup__del-btn');
const input_id = delBox.querySelector('.card-id');


export const delfunc = (delBtn) => {
    if (delBtn) {
        delBtn.addEventListener('click', function (e) {
            const id = this.getAttribute('data-id');

            ////---- show del box
            input_id.value = id;
            popup.classList.add('popup__active');
            delBox.style.display = 'block';
        })
    }
}


export const delfuncAdv = (btn) => {

    const id = btn.getAttribute('data-id');

    ////---- show del box
    input_id.value = id;
    popup.classList.add('popup__active');
    delBox.style.display = 'block';
}


/////////////////////////////////////
////---- Delete Function Rest-----////
/////////////////////////////////////
export const delfuncRest = (btn) => {

    // Get id from the button
    const id = btn.getAttribute('data-id');
    if (!id) return;

    // put id in del btn
    delBtn.setAttribute('data-id', id);

    // Show del box
    popup.classList.add('popup__active');
    delBox.style.display = 'block';
}



export const cancelfunc = () => {
    delBox.querySelector('.btn__cancel').addEventListener('click', (e) => {

        // Prevent from submitting form
        e.preventDefault();

        removeDelBox();
    })

    popup.addEventListener('click', function (e) {
        if (e.target == popup) {

            // dosomething
            removeDelBox();
        }
        else {
            return
        }
    })
}


export const removeDelBox = () => {
    popup.classList.remove('popup__active');
    delBox.style.display = 'none';
    delBtn.setAttribute('data-id', '');
    if (input_id) {
        input_id.value = '';
    }
}

