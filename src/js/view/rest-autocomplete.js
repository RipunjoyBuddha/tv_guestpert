////////////////////////////
//--- Render Search UI ---//

export function renderSearchUI(arr, inputEl){
    
    if(arr.length<=0){return};
    let parentBox = inputEl.closest('.search-box');
    
    let parent = parentBox.querySelector('.custom-options');
    
    parent.innerHTML='';
    
    for(let i = 0; i<7; i++){
        const val = arr[i];
        if(!val){break};
        parent.insertAdjacentHTML('beforeend', markup(val.id, val.name));
    }
    parent.parentElement.classList.add('open');
}


////////////////////////////
//--- Render Tags UI ---//

export function renderTagsUI(arr, inputEl){

    // Return if array length is less than 1
    if(arr.length<1){return};
    let parentBox = inputEl.closest('.tag-box');

    let parent = parentBox.querySelector('.custom-options');

    parent.innerHTML='';
    
    for(let i = 0; i<7; i++){
        const val = arr[i];
        if(!val){break};
        parent.insertAdjacentHTML('beforeend', markup(val, val));
    }
    parent.parentElement.classList.add('open');
}


function markup(id, name){
    const markup = `
    <span class="custom-option" data-id="${id}">${name}</span>
    `;
    return markup;
}