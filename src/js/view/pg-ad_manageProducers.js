import dateformat from "dateformat";
import { domain } from "../utils/domain";


//////////////////////////
//--- Render Cards ---//
export const renderCards = (parentBox, dataArr) => {
    if (!parentBox) { return }

    parentBox.innerHTML = '';

    if (dataArr.length < 1) {
        const markup = `<p class="paragraph__not-found">No Such Producers available</p>`;
        parentBox.insertAdjacentHTML('beforeend', markup)
    }
    else {
        dataArr.forEach((cur) => {
            parentBox.insertAdjacentHTML('beforeend', markup(cur, domain))
        })
    }
}



//////////////////////////
//--- Render Edit ---//

const editStore = {
    editSection: document.querySelector('#editSection'),
    guestpertImg: editSection.querySelector('#edit_guestpert-img'),
    firstName: editSection.querySelector('#edit_firstname'),
    lastName: editSection.querySelector('#edit_lastName'),
    prefix: editSection.querySelector('#edit_prefix'),
    sufix: editSection.querySelector('#edit_sufix'),
    title: editSection.querySelector('#edit_title'),
    tagline: editSection.querySelector('#edit_tagline'),

    address: editSection.querySelector('#edit_address'),
    company: editSection.querySelector('#edit_company'),
    country: editSection.querySelector('#edit_country'),
    phone: editSection.querySelector('#edit_phone'),
    mobile: editSection.querySelector('#edit_mobile'),
    website: editSection.querySelector('#edit_website'),
    fax: editSection.querySelector('#edit_fax'),
    rssfeed: editSection.querySelector('#edit_rssfeed'),
    username: editSection.querySelector('#edit_username'),
    email: editSection.querySelector('#edit_email'),
    

    delBtn: editSection.querySelector('#delete-producer-btn')
};


export const renderEdit = (dataObj) => {

    // Set the content
    editStore.guestpertImg.setAttribute('src', `${domain}${dataObj.imgSrc}`);
    editStore.firstName.value = dataObj.firstName;
    editStore.lastName.value = dataObj.lastName;
    editStore.prefix.value = dataObj.prefix;
    editStore.sufix.value = dataObj.suffix;
    editStore.title.value = dataObj.title;

    editStore.address.value = dataObj.address;
    editStore.email.value = dataObj.email;
    editStore.mobile.value = dataObj.mobile;
    editStore.phone.value = dataObj.phone;
    editStore.website.value = dataObj.website;
    editStore.fax.value = dataObj.fax;
    editStore.company.value = dataObj.company;

    editStore.username.value = dataObj.username;

    // Put the Id in delBtn
    editStore.delBtn.setAttribute('data-id', dataObj._id);
    
}


// Clear edit
export const clearEdit = ()=>{

    // Set the content
    editStore.guestpertImg.setAttribute('src', `${domain}images/user.png`);
    editStore.firstName.value = '';
    editStore.lastName.value = '';
    editStore.prefix.value = '';
    editStore.sufix.value = '';
    editStore.title.value = '';

    editStore.address.value=''
    editStore.email.value = '';
    editStore.phone.value = '';
    editStore.website.value = '';
    editStore.fax.value = '';

    // Put the Id in delBtn
    editStore.delBtn.setAttribute('data-id', '');
}





function markup(cur, domain) {
    
    const markup = `
    <div class="card-3 users__card ">
    <div class="users__card-heading">
        <div class="users__person">
            <p class="paragraph-primary users__person-name">${cur.prefix} ${cur.firstName} ${cur.lastName} ${cur.suffix}</p>
        </div>
        <div class="users__person-title">
            <p class="txt-capitalize">${cur.title}</p>
        </div>
        <div class="users__card-img users__card-img--position-right">
            <img src="${domain}${cur.imgSrc}" alt="${cur.firstName} ${cur.lastName} image" class="fluid-img">
        </div>
    </div>

    <div class="users__info">
        <ul class="info-list u-mar-t-2">
            <li>
                <p class="info-list__key">User name</p>
                <p class="info-list__value">${cur.username}</p>
            </li>
            <li>
                <p class="info-list__key">Joined</p>
                <p class="info-list__value">${dateformat(cur.createdAt, 'dS mmm yyyy')}</p>
            </li>
            <li>
                <p class="info-list__key">Role</p>
                <p class="info-list__value">${cur.role}</p>
            </li>
        </ul>
    </div>
    
    <div class="users__card-btns">
        <button data-id="${cur._id}" class="users__card-btn-edit button__primary">Edit</button>
        <button data-id="${cur._id}" class="users__card-btn-details button__ghost button__ghost--sm">More
            Details</button>
    </div>
</div>
    `
    return markup
}