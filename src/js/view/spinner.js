export const prepareForLoad = (parentBox, animeBox)=>{

    // Hide the main section
    parentBox.style.display='none';

    // Render spinner
    loadSpinner(animeBox);
}

// Load spinner
export const loadSpinner = (parentbox) => {

    const markup = `
    <div class="spinner-box">
        <div class="spinner" id="loading"></div>
    </div>
    `;

    if(parentbox){
        parentbox.innerHTML = markup;
    }else{
        // console.log('parent is not defined')
    }
}

// Remove Spinner
export const removeSpinner=(parentbox)=>{
    if(parentbox){
        parentbox.innerHTML=''
    }
}


// Load large spinner
export const loadLargeSpinner=(parentBox)=>{
    const markup =`
    <div class="spinner-box--large">
    <div class="spinner" id="loading--white"></div>
    </div> `;

    if(!parentBox){return}

    parentBox.innerHTML = markup;
}