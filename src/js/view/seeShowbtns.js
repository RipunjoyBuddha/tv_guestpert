const createBtn = document.querySelector('.btn-createNew');
const seeBtn = document.querySelectorAll('.edit__show-cards');
const editBtn = document.querySelector('.btn-editFeature');



export const seeShow = (topicsSection, editSection1, editSection2=undefined)=>{
    
    if(!topicsSection){
        return;
    }
    
    if(!editSection1){
        editSection1 = document.querySelector('.edit');
    }
    const page = document.querySelector('.edit');
    
    createBtn.addEventListener('click', function(){
        topicsSection.style.display='none'
        editSection1.style.display='block'
    })

    if(editBtn){
        editBtn.addEventListener('click', function(){
            topicsSection.style.display='none'
            editSection2.style.display='block'
        })
    }
    
    for(const see of seeBtn){
        see.addEventListener('click', function(){
            topicsSection.style.display='flex'
            editSection1.style.display='none'
            if(editSection2){
                editSection2.style.display='none';
            }
        })
    }
}


// See show adv will take all topicSections as an array and make appear only the desired edit section
export const seeShow_adv = (mainSectionsArr, editSection)=>{

    // Disappear everything in mainSection
    mainSectionsArr.forEach(c=>c.style.display='none');

    // Make editSection Visible
    editSection.style.display='block'
}



export const showBtnClick = (mainSectionsArr, editSection)=>{
    
    // Make the main sections appear
    mainSectionsArr.forEach(c=>c.style.display='flex');

    // Disappear the editSection
    editSection.style.display='none';
}