function requireAll(r) {
    r.keys().forEach(r);
  }
export default requireAll(require.context('../../images/svg/', true, /\.svg$/));