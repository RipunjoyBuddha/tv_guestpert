export const change = (popup, popupBox) => {
    return {
        showUp: () => {
            popup.classList.add('popup__active');
            popupBox.style.display = 'block'
        },
        cancel: (msg, mainInp)=>{
            popup.classList.remove('popup__active');
            popupBox.style.display = 'none';
            msg.style.display='none';
            msg.textContent='';
            mainInp.value = '';
        }
    }
}

