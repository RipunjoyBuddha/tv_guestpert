import { domain } from "../utils/domain";
import { updateContent } from "../vendor/form-dropdown";
import uniqid from "uniqid";
import {setContent} from '../utils/tinyActions';



/////////////////////////
//--- Render Cards ---//
export const renderCards = (parentBox, dataArr) => {
    if (!parentBox) { return }

    parentBox.innerHTML = '';

    if (dataArr.length < 1) {
        const markup = `<p class="paragraph__not-found">No Testimonials Found</p>`;
        parentBox.insertAdjacentHTML('beforeend', markup)

    }
    else {
        dataArr.forEach((cur) => {
            parentBox.insertAdjacentHTML('beforeend', markup(cur));
        })
    }
}




//////////////////////////
//--- Render Edit ---//---
//////////////////////////

const editSection = document.querySelector('.edit');

const editStore = {
    imgPreview: editSection.querySelector('#preview-img'),
    category: editSection.querySelector('.custom-select-wrapper'),
    name: editSection.querySelector('#name'),
    title: editSection.querySelector('#title'),
    active: editSection.querySelector('#create_active'),
    vidLinkSection: editSection.querySelector('.testimonial-video__link-container')
};


const dropdownUpdate = new updateContent(editStore.category);

export const renderEdit = (dataObj) => {

    // Making the vid Link array
    let vidLinkArr = dataObj.vidLink.map(c => {
        const uId = uniqid();
        
        c = c.replace(/'/g, `"`);

        const vidLinkMarkup = `
                <div class="u-mar-t-1">
                    <label for="${uId}" class="form__label">
                        <span class="form__label--caps">Video Link</span>
                    </label>
                    <div>
                        <span class="span color-txt-dark">Put only embed code with &lt;iframe&gt; tag for vimeo and youtube</span>
                    </div>
                    <div>
                        <span class="span color-txt-dark"> eg. &lt;iframe width=&quot;560&quot; height=&quot;315&quot; src=&quot;https://www.youtube.com/embed/aJDqelOW6ho&quot; frameborder=&quot;0&quot; allowfullscreen&gt;&lt;/iframe&gt;</span>
                    </div>
                    <div class="edit__input-plus-btn">
                        <input type="text" class="form__control edit__change-inputs" id="${uId}" value='${c}' required>
                        <div class="edit__btn-change btn_remove">Remove</div>
                    </div>
                </div>
            `;

        return vidLinkMarkup;
    });


    if (vidLinkArr.length < 1) {

        const uId = uniqid();

        const mrkup = `
        <div class="u-mar-t-1">
            <label for="${uId}" class="form__label">
                <span class="form__label--caps">Video Link</span>
            </label>
            <div>
                <span class="span color-txt-dark">Put only embed code with &lt;iframe&gt; tag for vimeo and youtube</span>
            </div>
            <div>
                <span class="span color-txt-dark"> eg. &lt;iframe width=&quot;560&quot; height=&quot;315&quot; src=&quot;https://www.youtube.com/embed/aJDqelOW6ho&quot; frameborder=&quot;0&quot; allowfullscreen&gt;&lt;/iframe&gt;</span>
            </div>
            <div class="edit__input-plus-btn">
                <input type="text" class="form__control edit__change-inputs" id="${uId}">
                <div class="edit__btn-change btn_remove">Remove</div>
            </div>
        </div>
        `
        vidLinkArr = [mrkup];
    }

    // Set the content
    editStore.imgPreview.setAttribute('src', `${domain}${dataObj.imgSrc}`);
    dropdownUpdate.setContent(dataObj.role);
    setContent('content', dataObj.content);
    editStore.name.value = dataObj.name;
    editStore.title.value = dataObj.title;
    editStore.active.checked = dataObj.active;
    if(vidLinkArr && vidLinkArr.length>0){
        editStore.vidLinkSection.innerHTML = vidLinkArr.join(' ');
    }
}

//////////////////////////
//--- Remove Edit ---//---
//////////////////////////

export const clearEditFields = (defaultImg) => {

    const uId = uniqid();

    const mrkup = `
        <div class="u-mar-t-1">
            <label for="${uId}" class="form__label">
                <span class="form__label--caps">Video Link</span>
            </label>
            <div>
                <span class="span color-txt-dark">Put only embed code with &lt;iframe&gt; tag for vimeo and youtube</span>
            </div>
            <div>
                <span class="span color-txt-dark"> eg. &lt;iframe width=&quot;560&quot; height=&quot;315&quot; src=&quot;https://www.youtube.com/embed/aJDqelOW6ho&quot; frameborder=&quot;0&quot; allowfullscreen&gt;&lt;/iframe&gt;</span>
            </div>
            <div class="edit__input-plus-btn">
                <input type="text" name="videoLink" class="form__control edit__change-inputs" id="${uId}">
                <div class="edit__btn-change btn_remove">Remove</div>
            </div>
        </div>
        `
    let vidLinkArr = [mrkup];


    // Set the content
    editStore.imgPreview.setAttribute('src', defaultImg);
    dropdownUpdate.removeContent();
    editStore.name.value = '';
    editStore.title.value = '';
    setContent('content', '')
    editStore.active.checked = true ;
    editStore.vidLinkSection.innerHTML = vidLinkArr.join(' ');

}



//////////////////////////
//--- Card Markup ---//---
//////////////////////////

function markup(cur) {

    let vid = '';
    if(cur.vidLink){
        vid = cur.vidLink.join(' ')
    }

    const markup = `
    <div class="feedback__card u-mar-t-5 testimonial__card">
    
    <!-- Left hand side box -->
    <div class="feedback__img-wrapper">
        <div class="feedback__profile-img">
            <img src="${domain}${cur.imgSrc}" alt="image" class="fluid-img">
        </div>
    </div>

    <!-- Right hand side box -->
    <div class="feedback__content-container">
        
        <div class="feedback__content">
             ${cur.content}
        </div>

        <div class="feedback__video-container u-mar-t-3">
            ${vid}
        </div>
        
        <p class="paragraph-tertiary color-txt-dark u-mar-t-3 text-bold-1">${cur.name}</p>
        <p class="paragraph-tertiary color-txt-dark">${cur.title}</p>
        
        <div class="testimonial__btn-container">
            <button data-id="${cur._id}" class="button__primary btn-testimonial-edit">Edit</button>
            <button data-id="${cur._id}" class="button__primary btn-testimonial-delete">Delete</button>
        </div>
    </div>

</div>
    `
    return markup
}