import { domain } from "../utils/domain";
import dateformat from "dateformat";

//////////////////////////
//--- Render Cards ---//
export const renderCards = (parentBox, dataArr) => {
    if (!parentBox) { return }

    parentBox.innerHTML = '';

    if(dataArr.length<1){
        const markup = `<p class="paragraph__not-found">No Tags Available</p>`;
        parentBox.insertAdjacentHTML('beforeend', markup)
        
    }
    else{
        dataArr.forEach((cur) => {
            parentBox.insertAdjacentHTML('beforeend', markup(cur));
        })
    }
}



///////////////////////////////////
//--- Render Edit ---//
const edit= document.querySelector('.edit');
const editStore = {
    keyword: edit.querySelector('#tag-keyword'),
    active: edit.querySelector('#active')
};

export const renderEdit = (dataObj)=>{

    dataObj.active==='yes'?dataObj.active=true:dataObj.active=false;

    // Fill the inputs with data
    editStore.keyword.value=dataObj.keyword;
    editStore.active.checked=dataObj.active;
}


export const removeFields = ()=>{
    
    // Fill the inputs with data
    editStore.keyword.value='';
    editStore.active.checked= true;
}



function markup(cur) {

    cur.active?cur.active='yes':cur.active='no'

    const markup = `
        <div class="card-4 thin">
            <ul class="info-list books__info u-mar-t-2">
                <li>
                    <p class="info-list__key">Keyword</p>
                    <p class="info-list__value tag-keyword">${cur.keyword}</p>
                </li>
                <li>
                    <p class="info-list__key">Created</p>
                    <p class="info-list__value">${dateformat(cur.created, 'dd mmm yyyy')}</p>
                </li>
                <li>
                    <p class="info-list__key">Modified</p>
                    <p class="info-list__value">${dateformat(cur.modified, 'dd mmm yyyy')}</p>
                </li>
                <li>
                    <p class="info-list__key">Active</p>
                    <p class="info-list__value tag-activeStats">${cur.active}</p>
                </li>
            </ul>
            <div class="tags__card-btns u-flexCenter u-mar-t-2">
                <button data-id="${cur._id}" class="tags__card-btn-edit button__primary">Edit</button>
                <button data-id="${cur._id}" class="tags__card-btn-remove button__ghost button__ghost--sm">Remove</button>
            </div>
        </div>
    `
    return markup
}