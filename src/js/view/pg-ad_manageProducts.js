import { domain } from "../utils/domain";
import dateformat from "dateformat";
import {updateContent, setThirdParty, removeThirdParty} from "../vendor/form-dopdown_manageProds";
import {removeImg} from "../view/choose_img";
import {setContent} from '../utils/tinyActions';

//////////////////////////
//--- Render Active Cards ---//
export const renderActiveCards = (parentBox, dataArr) => {
    if (!parentBox) { return }

    parentBox.innerHTML = '';

    if(dataArr.length<1){
        const markup = `<p class="paragraph__not-found">No Products Available</p>`;
        parentBox.insertAdjacentHTML('beforeend', markup)
        
    }
    else{
        dataArr.forEach((cur) => {
            parentBox.insertAdjacentHTML('beforeend', markup(cur,  domain, 'active-card'))
        })
    }
};


//////////////////////////
//--- Render In Active Cards ---//
export const renderInactiveCards = (parentBox, dataArr) => {
    if (!parentBox) { return }

    parentBox.innerHTML = '';

    if(dataArr.length<1){
        const markup = `<p class="paragraph__not-found">No Books Available</p>`;
        parentBox.insertAdjacentHTML('beforeend', markup)
        
    }
    else{
        dataArr.forEach((cur) => {
            parentBox.insertAdjacentHTML('beforeend', markup(cur,  domain, 'inactive-card'))
        })
    }
};




///////////////////////////////////
//--- Render Delete Markup ---//
export const renderDel = (parentBox, id)=>{

    if(!id || !parentBox)return;

    const markup = `
        <div class="edit__form-container u-mar-t-3" id="delete-product">
            <div>
                <p class="paragraph-secondary">Do you want to delete this Product? Please remember that you will not
                    be
                    able to retrive this product once you delete it.</p>
                <button id="delete-product-btn" data-id="${id}"
                    class="form__submit-btn--pink form__submit-btn u-mar-t-2 delete-product">Delete</button>
            </div>
        </div>
    `;

    parentBox.insertAdjacentHTML('beforeend', markup);
}

export const removeDel = (parentBox)=>{

    // Select the delete box
    const del = parentBox.querySelector('#delete-product');

    // Remove element
    // parentBox.removeChild(del);
}





///////////////////////////////////
//--- Render Edit ---//
const edit= document.querySelector('.edit');
const editStore = {
    paymentMode: edit.querySelector('.custom-select-wrapper'),
    titleInp: edit.querySelector('#productTitle'),
    type: edit.querySelector('#productType'),
    audioLink: edit.querySelector('#audioLink'),
    // description: tinymce.get("#productDescription"),
    inpFile: edit.querySelector('#inpFile-box'),
    active: edit.querySelector('#active'),
    bestSeller: edit.querySelector('#bestSeller'),
    currency: edit.querySelector('#currency'),
    price: edit.querySelector('#product_price')
};

const dropdownUpdate = new updateContent(editStore.paymentMode);

export const renderEdit = (dataObj, thirdpartyBox)=>{
    // Fill the inputs with data
    if(dataObj.paymentMode){
        dropdownUpdate.setContent(dataObj.paymentMode);
        if(dataObj.paymentMode==='thirdparty'){
            setThirdParty(thirdpartyBox, dataObj.thirdpartyUrl);
        }else{
            removeThirdParty(thirdpartyBox);
        }
    }
    if(dataObj.title){
        editStore.titleInp.value= dataObj.title;
    }else{
        editStore.titleInp.value= '';
    }
    if(dataObj.audioLink){
        editStore.audioLink.value = dataObj.audioLink;
    }else{
        editStore.audioLink.value = '';
    }
    if(dataObj.productType){
        editStore.type.value = dataObj.productType;
    }else{
        editStore.type.value = '';
    }
    if(dataObj.description){
        setContent('productDescription', dataObj.description);
    }else{
        setContent('productDescription', '');
    }
    removeImg(editStore.inpFile);

    active.checked = dataObj.active;
    
    bestSeller.checked = dataObj.bestSeller;

    if(dataObj.currency){
        editStore.currency.value = dataObj.currency;
    }else{
        editStore.currency.value = '';
    }
    if(dataObj.price){
        editStore.price.value = dataObj.price;
    }else{
        editStore.price.value = '';
    }
}

export const renderRemove = ()=>{

    // Fill the inputs with data
    dropdownUpdate.removeContent();
    editStore.titleInp.value= '';
    editStore.type.value = '';
    setContent('productDescription', '');
    removeImg(editStore.inpFile);
    active.checked = true;
    bestSeller.checked = false;
    editStore.currency.value = '';
    editStore.price.value = '';
}






function markup(cur, domain, cardType) {

    if(cur.length<=0)return;

    // Set best seller
    let bestSellerMarkup = `<div class="products__best-seller">Best Seller</div>`;
    !cur.bestSeller?bestSellerMarkup='':'';

    // Control Output
    let btns = `
        <button data-id="${cur._id}" class="products__card-btn-edit button__primary">Edit</button>
        <button data-id="${cur._id}" class="products__card-btn-remove button__ghost button__ghost--sm">Remove</button>
    `;
    let moreDetails =`
        <li>
            <p class="info-list__key">Product Type</p>
            <p class="info-list__value">${cur.productType}</p>
        </li>
        <li>
            <p class="info-list__key">payment Mode</p>
            <p class="info-list__value">${cur.paymentMode}</p>
        </li>
        <li>
            <p class="info-list__key">Active</p>
            <p class="info-list__value">${cur.active?'yes':'no'}</p>
        </li>
        <li>
            <p class="info-list__key">Price</p>
            <p class="info-list__value">$${cur.price}</p>
        </li>
    `;

    if(cardType === 'inactive-card'){

        btns=`
            <div class="u-absCenter" style="width:100%">
                <button data-id="${cur._id}" class="button__oval ">Add to Shop</button>
            </div>
        `;

        moreDetails = `
        <li>
            <p class="info-list__key">Published</p>
            <p class="info-list__value">${dateformat(cur.published, 'dd mmm yyyy')}</p>
        </li>
        `
    }
    
    const markup = `
    <div class="product_details__card-wrapper collection-products">
    <div class="card-4 products__cards products">
        <div class="products__cards-details">
            <div class="products__title-container">
                <h3 class="products__heading">${cur.title}</h3>
            </div>

            <ul class="info-list u-mar-t-2 u-mar-b-4">
                ${moreDetails}
                <li>
                    <p class="info-list__key">Created</p>
                    <p class="info-list__value">${dateformat(cur.createdAt, 'dd mmm yyyy')}</p>
                </li>
            </ul>

            <div class="products__card-btns u-mar-b-1">
                ${btns}
            </div>

            <div class="products__effect card-4__effect"></div>
        </div>
        <div class="products__img-wrapper">
            <div class="products__img-container">
                <img src="${domain}${cur.imgSrc}" alt="image"
                    class="fluid-img">
            </div>
            ${bestSellerMarkup}
        </div>

    </div>
</div>
    `
    return markup
}