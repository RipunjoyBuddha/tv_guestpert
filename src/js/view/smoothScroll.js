const smoothScroll = class{
    constructor(target, duration, navbarHeight){
        this.target = document.querySelector(target);
        this.navbarHeight = navbarHeight;
        this.targetPosition = (+this.target.offsetTop) - (+this.navbarHeight);
        this.startPosition = window.pageYOffset;
        this.distance = +this.targetPosition - (+this.startPosition);
        this.startTime = null;
        this.duration = duration;
        this.init();
    }

    init(){
        const animation = ()=>{
            if(this.startTime === null)this.startTime = Date.now();
            const timeElapsed = Date.now() - this.startTime;
            const run = ease(timeElapsed, this.startPosition, this.distance, this.duration);
            window.scrollTo(0, run);
            if(timeElapsed < this.duration){
                requestAnimationFrame(animation) 
            }
            
            
        }

        function ease(t, b, c, d) {
            t /= d/2;
            if (t < 1) return c/2*t*t*t + b;
            t -= 2;
            return c/2*(t*t*t + 2) + b;
        };

        requestAnimationFrame(animation);
    }

}

export function scroll(target, duration, navbarHeight){
    new smoothScroll(target, duration, navbarHeight)
}