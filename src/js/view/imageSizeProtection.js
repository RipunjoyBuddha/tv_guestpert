

export const imgSizeProtection = (form, inputs=[{id: 'image', maxSize: 500000}])=>{

    let result = true;
    for(const inp of inputs){
        const imageFile = form.querySelector(`#${inp.id}`).files[0];
        if(!imageFile)return true;
        if(imageFile && inp.maxSize < imageFile.size){
            result = false;
        }
    }

    return result;
}
