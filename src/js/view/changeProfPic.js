export const changeDp = (image, popup)=>{
    
    const popupBox = popup.querySelector('.popup__change-profile-picture');
    const cancelbtn = popupBox.querySelector('.btn_cancel');
    const inpimg = popup.querySelector('#image-input');
    const imgPreview = popup.querySelector('#imagePreview');
    const imgsrc = imgPreview.getAttribute('src') ;
    

    // Make the popup and box visible
    image.addEventListener('click', function(){
        const previmg = this.querySelector('img').getAttribute('src');
        imgPreview.setAttribute('src', previmg);
        popup.classList.add('popup__active');
        popupBox.style.display = 'flex';
    })
    
    
    // Disappear popup and box
    cancelbtn.addEventListener('click', (e)=>{
        
        // Prevent default
        e.preventDefault()
        
        popup.classList.remove('popup__active');
        popupBox.style.display = 'none';
        imgPreview.setAttribute('src', imgsrc);
        window.scrollTo(0, 0);
    })
    
    
    popup.addEventListener('click', function (e) {
        if (e.target == popup) {
            // dosomething
            popup.classList.remove('popup__active');
            popupBox.style.display='none';
        }
        else {
            return
        }
    })
    

    // Show in the sample box funtionality
    inpimg.addEventListener('change', function(){
        const file = this.files[0];

        if(file){
            const reader = new FileReader();
            reader.addEventListener('load', function(){
                
                imgPreview.setAttribute('src', this.result)
            });
            reader.readAsDataURL(file);
        }else{
            imgPreview.setAttribute('src', imgsrc);
        }
    })
}


export const changeDp_withoutPopup = (section)=>{
    
    // Grab required fields
    const inpimg = section.querySelector('#img-input');
    const imgPreview = section.querySelector('#preview-img');
    const imgsrc = imgPreview.getAttribute('src') ;

    // Show in the sample box funtionality
    inpimg.addEventListener('change', function(){
        const file = this.files[0];

        if(file){
            const reader = new FileReader();
            reader.addEventListener('load', function(){
                
                imgPreview.setAttribute('src', this.result)
            });
            reader.readAsDataURL(file);
        }else{
            // imgPreview.setAttribute('src', imgsrc);
        }
    })
}


export const removeDp = (box, defaultImg)=>{

    // Get fields
    const imgPreview = box.querySelector('#preview-img');
    const inpimg = box.querySelector('#img-input');

    // Make changes
    imgPreview.setAttribute('src', defaultImg);
    inpimg.value = '';
}