import {setContent} from '../utils/tinyActions';

export const renderEdit = (obj, editSection) => {

    // Get the Fields
    const question = editSection.querySelector('#question');
    const answerTitle = editSection.querySelector('#answerTitle');
    const videoLink = editSection.querySelector('#videoLink');
    // const answer = tinymce.get('#answer');
    const active = editSection.querySelector('#active');


    // Place the data in the Fields
    question.value = obj.question;
    if(obj.answerTitle){
        answerTitle.value = obj.answerTitle;
    }
    videoLink.value=obj.videoLink;
    setContent('answer', obj.answer);
    active.checked = obj.activeStats;

    // Display editSection
    editSection.style.display = 'block';
}


// /////////////////////////// //
//////--- Remove Fields------/////
// /////////////////////////// //
export const removeFields = (editSection) => {

    // Get the Fields
    const question = editSection.querySelector('#question');
    const answerTitle = editSection.querySelector('#answerTitle');
    const videoLink = editSection.querySelector('#videoLink');
    // const answer = tinymce.get('#answer');
    const active = editSection.querySelector('#active');

    // Place data in the fields
    question.value = '';
    videoLink.value='';
    answerTitle.value = '';
    setContent('answer', '')
    active.checked = true;
}