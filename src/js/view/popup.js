const videoEl = document.querySelector('.popup').querySelector('iframe');

export const playVideo = (url)=>{
    // Load the url to play video
    videoEl.src = url;
}

export const pauseVideo = ()=>{
    videoEl.src = "";
}