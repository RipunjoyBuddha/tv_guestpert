import {updateTag} from './tags';
import {message} from './errorMessage';
import dateFormat from 'dateformat';

// Selectors
const editSection = document.querySelector('.edit');
const submitBtn = editSection.querySelector('.form__submit-btn');

const updateTags = new updateTag(editSection.querySelector('.tags-input'));

export const renderEdit = (dataObj)=>{
    
    // Get the fields to be edited
    const appDate = editSection.querySelector('#date');
    const appTopic = editSection.querySelector('#appTopic');
    const programTitle = editSection.querySelector('#programTitle');
    const vidLink = editSection.querySelector('#videoLink');
    const activeStats = editSection.querySelector('#activeStats');
    

    // set the fields
    appDate.value = dateFormat(dataObj.appearanceDate, 'yyyy-mm-dd');
    appTopic.value = dataObj.topic;
    programTitle.value = dataObj.title;
    vidLink.value = dataObj.videoLink;
    updateTags.setContent(dataObj.tags);
    activeStats.checked = dataObj.active;

    // Put class in show blogs btn
    const backBtn = editSection.querySelector('.edit__show-cards');
    backBtn.classList.add('editMode');

    // Change text content of submit btn
    submitBtn.textContent = 'Save';
    
    // Show edit Section
    editSection.style.display='block';
}




// Remove everything from edit section
export const removeFields = ()=>{
    
    // Get the fields to be edited
    const appDate = editSection.querySelector('#date');
    const appTopic = editSection.querySelector('#appTopic');
    const programTitle = editSection.querySelector('#programTitle');
    const vidLink = editSection.querySelector('#videoLink');
    const activeStats = editSection.querySelector('#activeStats');
    
    // Fill up the contents
    appDate.value = '';
    appTopic.value = '';
    programTitle.value = '';
    vidLink.value = '';
    activeStats.checked = true;
    updateTags.removeContent();

    // Change btn txt
    submitBtn.textContent = 'Create';
}


// -- Render Error -- //
export const renderError = (topicSection)=>{

    // display the main section
    topicSection.style.display ='flex';

    const msg = 'Sorry, Your request couldn\'t be completed. Please try later';
    message(5000, false, msg);
}