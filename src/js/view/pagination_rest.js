const pageContainer = document.getElementById('page__list');

export class pagination {
    constructor(elPerPage = 2, pageNumList = pageContainer) {
        this.pageShow = 5;
        this.elPerPage = elPerPage;
        this.pageNumList = pageNumList;
        this.totalItems = 5;
        this.totalPages = Math.ceil(this.totalItems / this.elPerPage);
    }


    ///////////////////////////
    ////-- Change pages --////
    /////////////////////////

    changePage(pageNum) {

        // If data-num is absent
        if (!pageNum || this.totalPages < 2) {
            this.pageNumList.innerHTML = '';
            return
        }
        
        // Check error in pageNum and Fix it
        pageNum < 1 ? pageNum = 1 : pageNum = pageNum;
        pageNum > this.totalPages ? pageNum = this.totalPages : pageNum = pageNum;
        
        const prevNext_markup = `
        <a class="page__num page__prev clickable" data-num="${pageNum - 1}">Prev</a>
        <a class="page__num page__next clickable" data-num="${pageNum + 1}">Next</a>
        `;
        
        function markup(i, j, cls = '') {
            return `
            <a class="page__num ${cls}" data-num="${j}">${i}</a>
            `
        }
        
        // Render previous next markup
        this.pageNumList.innerHTML = prevNext_markup;
        
 
        // if this.totalPages are less than 5
        if (this.totalPages <= this.pageShow) {
            for (let i = 1; i <= this.totalPages; i++) {
                if (i === pageNum) {
                    this.pageNumList.insertAdjacentHTML('beforeend', markup(i, i, 'page__num--active'))
                } else {
                    this.pageNumList.insertAdjacentHTML('beforeend', markup(i, i, 'clickable'))
                }
            }
            this.pageNumList.style.display = 'flex';
        }
        
        
        // if total pages are more than 5
        else {
            if (pageNum > 1 && pageNum < this.totalPages) {
                const md_p = pageNum - 1;
                const md_n = pageNum + 1;
                const numBtwfrst = md_p - 1 - 1;
                const numBtwlst = this.totalPages - 1 - md_n;
                
                this.pageNumList.insertAdjacentHTML('beforeend', markup(1, 1, 'clickable'));

                if (numBtwfrst <= 0) { }
                else if (numBtwfrst === 1) {
                    this.pageNumList.insertAdjacentHTML('beforeend', markup(md_p - 1, md_p - 1, 'clickable'));
                } else {
                    this.pageNumList.insertAdjacentHTML('beforeend', markup('...', '', ''));
                };

                if (numBtwfrst < 0) { }
                else {
                    this.pageNumList.insertAdjacentHTML('beforeend', markup(md_p, md_p, 'clickable'));
                };
                this.pageNumList.insertAdjacentHTML('beforeend', markup(pageNum, pageNum, 'page__num--active'));

                if (numBtwlst < 0) { }
                else {
                    this.pageNumList.insertAdjacentHTML('beforeend', markup(md_n, md_n, 'clickable'));
                };

                if (numBtwlst <= 0) { }
                else if (numBtwlst === 1) {
                    this.pageNumList.insertAdjacentHTML('beforeend', markup(md_n + 1, md_n + 1, 'clickable'));
                } else {
                    this.pageNumList.insertAdjacentHTML('beforeend', markup('...', '', ''));
                };
                this.pageNumList.insertAdjacentHTML('beforeend', markup(this.totalPages, this.totalPages, 'clickable'));
            }
            else if (pageNum === 1) {
                for (let i = 1; i <= this.pageShow; i++) {
                    if (i === 1) {
                        this.pageNumList.insertAdjacentHTML('beforeend', markup(i, i, 'page__num--active'))
                    } else if (i === this.pageShow - 1) {
                        this.pageNumList.insertAdjacentHTML('beforeend', markup('...', '', ''))
                    }
                    else if (i === this.pageShow) {
                        this.pageNumList.insertAdjacentHTML('beforeend', markup(this.totalPages, this.totalPages, 'clickable'))
                    }
                    else {
                        this.pageNumList.insertAdjacentHTML('beforeend', markup(i, i, 'clickable'))
                    }
                }
            }
            else {
                for (let i = 1; i <= this.pageShow; i++) {
                    if (i === 1) {
                        this.pageNumList.insertAdjacentHTML('beforeend', markup('1', '1', 'clickable'))
                    }
                    else if (i === 2) {
                        this.pageNumList.insertAdjacentHTML('beforeend', markup('...', '', ''))
                    }
                    else if (i === this.pageShow) {
                        this.pageNumList.insertAdjacentHTML('beforeend', markup(this.totalPages, this.totalPages, 'page__num--active'))
                    }
                    else {
                        this.pageNumList.insertAdjacentHTML('beforeend', markup(this.totalPages - this.pageShow + i, this.totalPages - this.pageShow + i, 'clickable'))
                    }
                }
            }
            this.pageNumList.style.display = 'flex';
        }

        this.styleEl(this.totalPages);
    }




    ///////////////////////////////////////////
    ////-- Style the page number box --////
    ///////////////////////////////////////////
    styleEl(lastPage) {

        // Prev and Next button
        const prevBtn = this.pageNumList.querySelector('.page__prev');
        const nextBtn = this.pageNumList.querySelector('.page__next');

        if (this.pageNumList.querySelector('.page__num--active')) {
            const activeEl = this.pageNumList.querySelector('.page__num--active');
            const pageNum = activeEl.getAttribute('data-num');

            if (lastPage > 1 && +pageNum && prevBtn) {
                if (+pageNum === 1) {
                    prevBtn.style.display = 'none';
                } else if (+pageNum === lastPage && nextBtn) {
                    nextBtn.style.display = 'none';
                }
                else {
                    prevBtn.style.display = 'flex'
                    nextBtn.style.display = 'flex'
                }
            }
        }
    }


    ////////////////////////////
    ////-- Change Values --////
    ///////////////////////////

    changeVals(totalItems) {
        this.totalPages = Math.ceil(totalItems / this.elPerPage);
    }

    activePageNum(){
        const activePg = Array.from(this.pageNumList.querySelectorAll('.page__num')).filter(c=>c.classList.contains('page__num--active'))[0];
        if(!activePg){return 1};

        const pgNum = activePg.getAttribute('data-num');
        return pgNum;
    }
}
