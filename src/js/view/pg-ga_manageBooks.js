import {message} from './errorMessage';
import {setContent} from '../utils/tinyActions'

///////////////////////////////
// --- Export Functions --- //


//--- Render Edit Functionality ---//
export const renderEdit = (editSection, dataObj)=>{
    
    // Get the fields to be edited
    const topicTitle = editSection.querySelector('#booktitle');
    const published = editSection.querySelector('#publishDate');
    const activeStats = editSection.querySelector('#active');

    

    // Fill up the contents
    topicTitle.value = dataObj.title;
    activeStats.checked = dataObj.active;
    published.value = dataObj.published
    //--- To be applied on tinyMCE---//
    setContent('description', dataObj.description)

    // Put class in show blogs btn
    const backBtn = editSection.querySelector('.edit__show-cards');
    backBtn.classList.add('editMode');

    editSection.style.display='block';

    // Change text content of submit btn
    const submitBtn = editSection.querySelector('.form__submit-btn');
    submitBtn.textContent = 'Save';
}


//--- Remove Edit Fields ---//
export const removeFields = (editSection)=>{

    // Get the fields to be edited
    const topicTitle = editSection.querySelector('#booktitle');
    const published = editSection.querySelector('#publishDate');
    const activeStats = editSection.querySelector('#active');

     // Fill up the contents
     topicTitle.value = '';
     activeStats.checked = true;
     published.value = '';
     //--- To be applied on tinyMCE---//
     setContent('description', '')

    // Change text content of submit btn
    const submitBtn = editSection.querySelector('.form__submit-btn');
    submitBtn.textContent = 'Create';
}



// Render Error Functionality--
export const renderError = ()=>{
    const msg = 'Sorry, Your request cannot be completed. Please try later';
    message(5000, false, msg);
}