import Glide from '@glidejs/glide';

export const headerCarousel = ()=>{
    const slide = new Glide('.header__slide', {
        type: 'carousel',
        startAt: 0,
        perView: 1,
        gap:0,
        autoplay: 6000,
        hoverpause: false,
    }).mount();

    return slide;
}

export const featuredBooksCarousel = ()=>{
    new Glide('.glide', {
        type: 'carousel',
        startAt: 0,
        perView: 3,
        focusAt: 'center',
        gap:10,
        autoplay: 3000,
        hoverpause: false,
    }).mount();
}


export const jaquiesAppearance = ()=>{
    new Glide('.glide',{
        type: 'carousel',
        startAt: 0,
        perView: 1
    }).mount()
}