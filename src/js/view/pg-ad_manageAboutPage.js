import { domain } from '../utils/domain';
import { removeDp } from './changeProfPic';
import {setContent} from '../utils/tinyActions';

export const renderEdit = (obj, editSection) => {

    // Get the Fields
    const firstName = editSection.querySelector('#firstName');
    const lastName = editSection.querySelector('#lastName');
    const title = editSection.querySelector('#title');
    const img = editSection.querySelector('#preview-img');
    const hiddenId = editSection.querySelector('#hidden-id');

    
    // Place the data in the Fields
    firstName.value = obj.firstName;
    lastName.value = obj.lastName;
    title.value = obj.title;
    hiddenId.value = obj.id

    if (obj.memberType === 'prime') {
        img.setAttribute('src', `${domain}${obj.imgSrc}`);
        // tinymce will be active
        setContent('description', obj.description)
        // description.setContent(obj.description);
    }

    // Display editSection
    editSection.style.display = 'block';
}



// /////////////////////////// //
//////--- Remove Fields------/////
// /////////////////////////// //
export const removeFields = (editSection, defaultImg) => {

    // Get the fields to be edited
    const removeImg = editSection.querySelector('.changeDP');
    const firstName = editSection.querySelector('#firstName');
    const lastName = editSection.querySelector('#lastName');
    const title = editSection.querySelector('#title');
    const hiddenId = editSection.querySelector('#hidden-id');

    // Place data in the fields
    firstName.value = '';
    lastName.value = '';
    title.value = '';
    hiddenId.value = '';
    if (editSection.id === 'editPrimeMembers') {
        removeDp(editSection, defaultImg);
        setContent('description')
    }
}