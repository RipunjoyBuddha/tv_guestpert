import {domain} from "../utils/domain";

export const renderCards = (parentBox, dataArr) => {
    if (!parentBox) { return }

    dataArr.forEach((cur, i) => {
        parentBox.insertAdjacentHTML('beforeend', markup(cur, i))
    })
}

function markup(cur, i) {
    let tempLine1 = ``;
    let tempLine2 = ``;
    let goldenLine = ``;

    if (cur.expertise) {
        tempLine1 = `<p class="paragraph-secondary text-bold-2">${cur.expertise}</p>`;
    }
    if (cur.premium) {
        tempLine2 = `<img src="${domain}/images/Tvg-logo.png" alt="premium" class="card__premium-logo">`;
    }

    if (i % 2 !== 0) {
        if ((i - 3) % 4 === 0) {
            goldenLine = `<img src="${domain}/images/golden_line-right.png" alt="background_picture" class="goldenline__right goldenline">`;
        }
        else if ((i - 1) % 4 === 0) {
            goldenLine = `<img src="${domain}/images/golden_line-left.png" alt="background_picture" class="goldenline__left goldenline">`;
        }
    }

    const markup = `
        <div class="card">
            <div class="card__marker card__marker--${cur.package} u-mar-t-2">${cur.package}</div>
            <div class="card__person-wrapper ">
                <div class="card__person-photo-container">
                    ${goldenLine}
                    <img src="${domain}${cur.imgSrc}" alt="${cur.name} image" class="fluid-img card__person-photo">
                </div>
                <div class="card__person-content">
                    <h2 class="heading-secondary u-mar-b-2">${cur.name}</h2>
                    <h2 class="heading-tertiary">${cur.title}</h2>
                    <!-- below line is temporary -->
                    ${tempLine1}
                    
                    <p class="paragraph-tertiary u-mar-b-3 u-mar-t-3 color-txt-dark">${cur.tagLine}</p>
                    <a href="/guestperts/${cur._id}" class="button__ghost button__ghost--sm">Read More</a>
                    
                    <!-- below line is temporary -->
                    ${tempLine2}
                </div>
            </div>
        </div>
    `
    return markup;
}


