import "../sass/ad_manageProducers.scss";
import "./view/importsvg";

import Search from './model/search';
import remove from './model/deleteCard';
import {prepareForLoad , removeSpinner, loadLargeSpinner} from './view/spinner';
import {pagination} from './view/pagination_rest';
import {renderCards, renderEdit, clearEdit} from './view/pg-ad_manageProducers';
import {moreDetailProducer, closeMoreDetail} from './view/moreDetailsPopup';
import {seeShow_adv, showBtnClick} from './view/seeShowbtns';
import {message} from './view/errorMessage';
import {changeDp_withoutPopup, changeDp} from './view/changeProfPic';
import {delfuncRest, removeDelBox, cancelfunc} from './view/deleteBox';
import { hamburger, slideMenu } from "./view/sidebar";

const state = {}

///////////////////////////////////////////////
////---- Universal Selectors ----////
///////////////////////////////////////////////
const searchBar = document.querySelector('#search-input');
const hiddenInput = searchBar.parentElement.querySelector('#hidden-input');

const topicsSection = document.querySelector('.producers');
const producerBox = topicsSection.querySelector('#producers-box');
const createBtn = topicsSection.querySelector('#create-btn');

const showProducersBtn = document.querySelectorAll('.btn__show-cards');

const animation = document.getElementById('animation');
const createsection = document.querySelector('#create-section');
const editSection = document.querySelector('#editSection');
const edit_dp = editSection.querySelector('.edit__image-container');
const delBtn = editSection.querySelector('#delete-producer-btn');

const pageSection = document.querySelector('.page');
const pageList = pageSection.querySelector('#page__list');

const popup = document.querySelector('.popup');
const delBox = popup.querySelector('.popup__del-box');
const popupProfilePic = popup.querySelector('.popup__change-profile-picture');

const csrf = document.querySelector('#csrf').getAttribute('data-csrf');

const form__credentials = editSection.querySelector('.edit__form-container--credentials');
const create__form = createsection.querySelector('.edit__form-container')


///////////////////////////////////////////
///--- Hamburger and Sidebar functionality ---///
///////////////////////////////////////////
hamburger();
slideMenu();


///////////////////////////////////////////
////---- Pagination functionality ----////
//////////////////////////////////////////
state.paginationCls = new pagination(12) // 12 is Element per page

// On clicking other pages
pageList.addEventListener('click', function (e) {
    const target = e.target;
    if (!target.classList.contains('clickable')) { return };

    const pageNum = target.getAttribute('data-num');
    init(pageNum);
});


///////////////////////////////////////////////
////---- On clicking create and see btns ----////
///////////////////////////////////////////////
createBtn.addEventListener('click', ()=>{
    seeShow_adv([topicsSection, pageSection], createsection);
})

for(const btn of showProducersBtn){
    btn.addEventListener('click', function(){
        showBtnClick([topicsSection, pageSection], this.parentElement.parentElement);
    })
}


/////////////////////////////////////
////---- change Profile pic ----////
////////////////////////////////////
changeDp_withoutPopup(createsection);
// When profile picture is changed 
changeDp(edit_dp, popup);



/////////////////////////////////////////
////---- Search bar functionality ----////
/////////////////////////////////////////
const regEx = /^[\w]{1,60}$/i;

searchBar.addEventListener('keyup', function (e) {

    const chk = ['', 8, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 96, 97, 98, 99, 100, 101, 102, 103, 104, 105, 106, 222, 186]
    const k = chk.indexOf(e.keyCode);

    // When ecape is pressed
    if (e.keyCode === 27) {
        e.preventDefault();
        this.value = '';
        hiddenInput.value = '';
        this.blur();
    };

    // Validation 1
    if (!k || k < 0) { return };

    if (this.value.length === 0) {
        hiddenInput.value = '';
        init(1);
    };

    // Validation 2
    if (this.value.length < 2 || !regEx.test(this.value)) { return };

    // Get the value
    const value = this.value;

    // Put text into hidden input
    hiddenInput.value = value;

    // call init
    init(1);
})


///////////////////////////////////
////---- Editing Guestpert ----////
///////////////////////////////////
producerBox.addEventListener('click', async (e)=>{

    // Prevent from clicking outside
    if (!e.target.classList.contains('users__card-btn-edit')) return;

    // store the id
    const id = e.target.getAttribute('data-id');

    // Render Spinner
    loadLargeSpinner(animation);

    // Search
    state.search = new Search(`/admin/manageGuestperts/searchEdit?id=${id}`);
    state.result = await state.search.getResults();
    
    // Remove spinner
    removeSpinner(animation);

    // If error occours
    if (!state.result) {

        // display topics Section
        topicsSection.style.display = 'flex';

        // display error message
        message(5000, false, 'Your request could not be completed. Please try again later.')

        // stop next
        return
    }

    // Render Edit
    renderEdit(state.result.data);

    // Set hrefs in forms
    // SetEditForms(state.result.data._id);

    seeShow_adv([topicsSection, pageSection], editSection);

    SetEditForms(state.result.data._id)
})


//------- Edit Forms -------//


function SetEditForms (id){
    const form__profilePic = popupProfilePic.querySelector('form');
    const form__personal = editSection.querySelector('.edit__form-container--personal');
    const form__contact = editSection.querySelector('.edit__form-container--contact');

    form__profilePic.setAttribute('action', `/admin/editProducer/profilePic?id=${id}&&_csrf=${csrf}`)
    form__personal.setAttribute('action', `/admin/editProducer/personal?id=${id}&&_csrf=${csrf}`)
    form__contact.setAttribute('action', `/admin/editProducer/contact?id=${id}&&_csrf=${csrf}`)
    form__credentials.setAttribute('action', `/admin/editProducer/credentials?id=${id}&&_csrf=${csrf}`)
}


form__credentials.addEventListener('submit', e=>{

    e.preventDefault();
    
    // Get new Password and confirm password
    const password = form__credentials.querySelector('#edit_newpassword').value
    const confirmPassword = form__credentials.querySelector('#edit_cnfrmpassword').value

    if(password !== confirmPassword){
        message(5000, false, 'Password and confirmPassword don\'t match')
        return
    }

    e.target.submit()
})


const editImgForm = popupProfilePic.querySelector('form')

editImgForm.addEventListener('submit', e=>{

    e.preventDefault()
    const imgInp = editImgForm.querySelector('#image-input').files[0]

    if(imgInp && imgInp.size>1000*500){
        message(5000, false, 'Image file is too big. Please reduce it\'s size')
        return
    }

    e.target.submit()
})

create__form.addEventListener('submit', e=>{

    e.preventDefault();
    
    // Get new Password and confirm password
    const password = create__form.querySelector('#create_newpassword').value
    const confirmPassword = create__form.querySelector('#create_cnfrmpassword').value
    const imgInp = create__form.querySelector('#img-input').files[0]

    if(imgInp && imgInp.size>1000*500){
        message(5000, false, 'Image file is too big. Please reduce it\'s size')
        return
    }

    if(password !== confirmPassword){
        message(5000, false, 'Password and confirmPassword don\'t match')
        return
    }

    e.target.submit()
})


///////////////////////////////////////////
////---- On clicking More details ----////
/////////////////////////////////////////
producerBox.addEventListener('click', (e)=>{

    // Protect from clicking outside
    if(!e.target.classList.contains('users__card-btn-details'))return;
    
    // Extract the Id
    const extractedId = e.target.getAttribute('data-id');
    const producerDetails = state.pageSearchRes.data['dataArr'].filter(c=>c._id==extractedId)[0]
    
    // Call details function
    moreDetailProducer(producerDetails)
})
closeMoreDetail();


//////////////////////////////
////---- Delete a Guestpert ----////
//////////////////////////////
delBtn.addEventListener('click', function(){

    // Call the del popup box
    delfuncRest(this)
})
cancelfunc();


/////////////////////////////////////
//---- On deleting a Guestpert ----//

delBox.addEventListener('click', async (e)=>{
    
    // Avoid from clicking outside 
    if(!e.target.classList.contains('popup__del-btn'))return;

    
    // Store the id
    state.delId = e.target.getAttribute('data-id');
    if(!state.delId)return;


    // Disappear popup
    removeDelBox();


    // Render Spinner
    loadLargeSpinner(animation);

    // Del request to server
    state.remove = new remove(`/admin/manageProducers/deleteProducer?id=${state.delId}&&_csrf=${csrf}`);
    state.result = await state.remove.getResults();

    // Remove spinner
    removeSpinner(animation);

    // If error occours
    if (!state.result) {
        
        // display error message
        message(5000, false, 'Your request could not be completed. Please try again later.')
        
        // stop next
        return
    }
    
    // Get active page number
    state.activePage = state.paginationCls.activePageNum();


    // Make the topicSection active
    showBtnClick([topicsSection, pageSection], editSection);

    // Clear the edit section
    clearEdit()

    // Call init
    init(state.activePage);
})


///////////////////////////////
////---- init function ----////
///////////////////////////////

// Show cards function
async function init(pageNum) {
    // Get keywords from input
    let keywords = hiddenInput.value;
    
    // Prepare UI
    prepareForLoad(producerBox, animation);
    
    // Get Data from Server
    state.pageSearch = new Search(`/admin/searchProducers?pageNum=${pageNum}&&keywords=${keywords}`);
    state.pageSearchRes = await state.pageSearch.getResults();
    
    // Remove Spinner
    removeSpinner(animation);

    // Display producer cards
    producerBox.style.display='flex';
    
    if (!state.pageSearchRes) {

        // Render Error message
        const msg = 'Error in fethcing data'; 
        message(5000, false, msg);

        // Return Error
        return;
    };
    
    // // Make change in UI
    renderCards(producerBox, state.pageSearchRes.data.dataArr);

    // Pagingation
    state.paginationCls.changeVals(+state.pageSearchRes.data.totalItems);
    state.paginationCls.changePage(+state.pageSearchRes.data.pageNum);
}
init(1)


const errMsg = document.querySelector('.message');
window.addEventListener('load', e=>{
    setTimeout(()=>{
        if(!errMsg)return;
        errMsg.style.display='none';
        errMsg.querySelector('.message__txt').textContent='';
    }, 5000)
})


import { logoutFunc } from './lib/autoLogout'
logoutFunc(() => {
    window.location.href = `/admin_login`
})