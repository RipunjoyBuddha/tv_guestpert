import "../sass/ad_newsletterTemplate.scss";
import "./view/importsvg";

import Search from "./model/search";
import remove from "./model/deleteCard";
import send from "./model/send";
import {pagination} from "./view/pagination_rest";
import {loadSpinner, removeSpinner, loadLargeSpinner} from "./view/spinner";
import {message} from "./view/errorMessage";
import {seeShow_adv, showBtnClick} from "./view/seeShowbtns";
import {delfuncRest , cancelfunc, removeDelBox} from "./view/deleteBox";
import {renderCards} from "./view/pg-ad_newsletterTemplates";
import { hamburger, slideMenu } from "./view/sidebar";
import {chooseImg} from "./view/choose_img";
const inputFile = document.querySelector('#inpFile-box');

///////////////////////////////////////////
///--- Hamburger and Sidebar functionality ---///
///////////////////////////////////////////
hamburger();
slideMenu();

///////////////////////////////////////////////
////---- Universal Selectors ----////
///////////////////////////////////////////////
const cardsSection = document.querySelector('.newsletter');
const cardBox = cardsSection.querySelector('#newsletter__cardBox');
const newsletterForm = document.querySelector('.edit__form-container');

const pageSection = document.querySelector('.page');
const pageList = pageSection.querySelector('.page__list');

const editSection = document.querySelector('.edit');
const backBtn = editSection.querySelector('.edit__show-cards');
const delBox = document.querySelector('.popup__del-box');
const csrf = document.querySelector('#csrf').getAttribute('data-csrf');


////////////////////////////////////////
//---- State of the current Page ----//
//////////////////////////////////////
const state = {};


///////////////////////////////////////////
////---- Pagination functionality ----////
//////////////////////////////////////////
state.paginationCls = new pagination(12) // 2 is Element per page

// On clicking other pages
pageList.addEventListener('click', function (e) {
    const target = e.target;

    if (!target.classList.contains('clickable')) { return };
    const pageNum = target.getAttribute('data-num');
    init(pageNum)
});




//////////////////////////////////////////////////
////---- On clicking create and back btns ----////
/////////////////////////////////////////////////
cardsSection.addEventListener('click', (e)=>{

    if(!e.target.classList.contains('btn-createNew'))return;

    // Call seeshow 
    seeShow_adv([cardsSection, pageSection], editSection);
    newsletterForm.setAttribute('action', `/admin/createNewsletter?_csrf=${csrf}`)
})


backBtn.addEventListener('click', function(){
    showBtnClick([cardsSection, pageSection], editSection);
})


////////////////////////////////////////
////---- On clicking delete btn ----////
////////////////////////////////////////

cardsSection.addEventListener('click', async (e) => {

    // Prevent from clicking outside
    if (!e.target.classList.contains('faty__card-btn-remove')) return;

    // Get the id
    const id = e.target.getAttribute('data-id');
    if(!id)return;
    
    // Store card
    state.activeCard = e.target.closest('.faty__card');

    // call delfunc Rest
    delfuncRest(e.target);
});

cancelfunc();




//////////////////////////////////////
////---- Pressing remove Btn ----////
/////////////////////////////////////
delBox.addEventListener('click', async (e) => {

    // Preventing from clicking outside
    if (!e.target.classList.contains('popup__del-btn')) { return };

    // store the id
    const id = e.target.getAttribute('data-id');
    
    // Remove the popup 
    removeDelBox();

    // Render Spinner
    loadLargeSpinner(animation);

    // Search
    state.removeTemplates = new remove(`/admin/newsletterTemplates/delete?id=${id}&&_csrf=${csrf}`);
    state.removeTemplatesRes = await state.removeTemplates.getResults();

    // Remove spinner
    removeSpinner(animation);

    // If error occours
    if (!state.removeTemplatesRes) {

        // display error message
        message(5000, false, 'Could not delete this newsletter. Please try again later');

        // Cancel active card
        state.activeCard = null;

        // stop next
        return
    }
    
    message(5000, true, 'The newsletter template have been successfully deleted');

    // Disappear the card
    if(state.activeCard){
        state.activeCard.parentElement.removeChild(state.activeCard);
    }
    
})

///////////////////////////////////////////////
////---- Choose Img functionality----////
///////////////////////////////////////////////
chooseImg(inputFile)


//////////////////////////////////////////
////---- On clicking Send button ----////
/////////////////////////////////////////

cardsSection.addEventListener('click', async (e) => {

    // Prevent from clicking outside
    if (!e.target.classList.contains('btn-send')) return;

    // Get the id
    const id = e.target.getAttribute('data-id');
    if(!id)return;

    // Render Spinner
    loadLargeSpinner(animation);

    // Search
    state.sendTemplate = new send(`/admin/newsletterTemplates/send?id=${id}&&_csrf=${csrf}`);
    state.sendTemplate.result = await state.sendTemplate.getResults();

    // Remove spinner
    removeSpinner(animation);

    // If error occours
    if (!state.sendTemplate.result) {

        // display error message
        message(5000, false, 'Sorry, Could not post Newsletter. Please try again later');
        
        // stop next
        return
    }

    e.target.textContent = state.sendTemplate.result.data.textContent;
    
    // Show success message
    message(5000, true, 'Newsletter posted successfully');
    
});



///////////////////////////////
////---- init function ----////
///////////////////////////////

// Show cards function
async function init(pageNum) {

    // Prepare UI
    loadSpinner(cardBox);

    // Get Data from Server
    state.nwsltrTmplteSearch = new Search(`/admin/newsletterTemplates/search?pageNum=${pageNum}`);
    state.nwsltrTmplteSearchRes = await state.nwsltrTmplteSearch.getResults();
    
    // Remove Spinner
    removeSpinner(cardBox);

    
    if (!state.nwsltrTmplteSearchRes) {

        // Render Error message
        const msg = 'Error in fethcing templates';
        message(5000, false, msg);

        // Return Error
        return;
    };

    // // Make change in UI
    renderCards(cardBox, state.nwsltrTmplteSearchRes.data.dataArr);

    // Pagingation
    state.paginationCls.changeVals(+state.nwsltrTmplteSearchRes.data.totalItems);
    state.paginationCls.changePage(+state.nwsltrTmplteSearchRes.data.pageNum);
}
init(1)


const errMsg = document.querySelector('.message');

window.addEventListener('load', e=>{
    setTimeout(()=>{
        if(!errMsg)return;
        errMsg.style.display='none';
        errMsg.querySelector('.message__txt').textContent='';
    }, 5000)
})



import { logoutFunc } from './lib/autoLogout'
logoutFunc(() => {
    window.location.href = `/admin_login`
})