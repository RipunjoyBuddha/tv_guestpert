import "../sass/ad_manageTestimonials.scss";
import "./view/importsvg";
import { initTinymce } from './utils/tinyActions';

import "./vendor/form-dropdown";
import Search from "./model/search";
import remove from "./model/deleteCard";
import { pagination } from "./view/pagination_rest";
import { removeBtnGenSettings, addNewFields } from "./view/addRemoveElement";
import { loadSpinner, removeSpinner, loadLargeSpinner } from "./view/spinner";
import { message } from "./view/errorMessage";
import { renderCards, renderEdit, clearEditFields } from "./view/pg-ad_manageTestimonials";
import { seeShow_adv, showBtnClick } from "./view/seeShowbtns";
import { changeDp_withoutPopup } from "./view/changeProfPic";
import { delfuncRest, cancelfunc, removeDelBox } from "./view/deleteBox";
import { hamburger, slideMenu } from "./view/sidebar";
import uniqid from 'uniqid';

///////////////////////////////////////////
///--- Hamburger and Sidebar functionality ---///
///////////////////////////////////////////
hamburger();
slideMenu();


// State of the guestpert Page
const state = {};

const tempId = uniqid();

///////////////////////////
//---- Universal selectors ----//
/////////////////////////
const videoLinkContainer = document.querySelector('.testimonial-video__link-container');
const topicsSection = document.querySelector('.testimonial');
const testimonialBox = topicsSection.querySelector('.testimonial__card-box');
const btnCreate = topicsSection.querySelector('.btn-createNew');

const editsection = document.querySelector('.edit');
const testimonialForm = editsection.querySelector('.edit__form-container');
const pageSection = document.querySelector('.page');
const pageList = pageSection.querySelector('.page__list');
const backBtn = editsection.querySelector('.edit__show-cards');
const delBox = document.querySelector('.popup__del-box');
const csrf = document.querySelector('#csrf').getAttribute('data-csrf');
const editForm = editsection.querySelector('.edit__form-container')


//-------- Protection from submitting large images --------- //
editForm.addEventListener('submit', e=>{

    e.preventDefault()

    const imgFile = editForm.querySelector('#img-input').files[0]

    if(imgFile && imgFile.size > 500*1000){
        message(5000, false, 'Too large image file. Please reduce image file size')
        return
    }

    e.target.submit()
})

///////////////////////////////////////////////
////---- Pagination functionality ----////
///////////////////////////////////////////////
state.paginationCls = new pagination(7) // 7 is Element per page

// On clicking other pages
pageList.addEventListener('click', function (e) {
    const target = e.target;
    if (!target.classList.contains('clickable')) { return };

    const pageNum = target.getAttribute('data-num');
    init(pageNum);
});




////////////////////////////////////
////---- change Profile pic ----////
////////////////////////////////////
changeDp_withoutPopup(editsection);

/////--- Store the default image ---/////
state.defaultImg = editsection.querySelector('#preview-img').getAttribute('src');


// /////////////////////////////////
// //---- Remove viedo links ----//
// ///////////////////////////////
const removeBtnClass = 'btn_remove';
removeBtnGenSettings(videoLinkContainer, removeBtnClass);


//////////////////////////////////
////---- Editing Guestpert ----////
///////////////////////////////////
testimonialBox.addEventListener('click', async (e) => {

    // Prevent from clicking outside
    if (!e.target.classList.contains('btn-testimonial-edit')) return;

    // store the id
    const id = e.target.getAttribute('data-id');
    if (!id) return;

    // Render Spinner
    loadLargeSpinner(animation);

    // Search
    state.searchEdit = new Search(`/admin/manageTestimonials/edit?id=${id}`);
    state.editResult = await state.searchEdit.getResults();

    // Remove spinner
    removeSpinner(animation);

    // If error occours
    if (!state.editResult) {

        // display error message
        message(5000, false, 'Your request could not be completed. Please try again later.')
        // stop next
        return
    }

    // Render Edit
    renderEdit(state.editResult.data);

    // Set Edit Mode
    backBtn.setAttribute('data-mode', 'edit');

    // Set create url in the form
    testimonialForm.setAttribute('action', `/admin/editTestimonial?id=${id}&&tempId=${tempId}&&_csrf=${csrf}`);

    seeShow_adv([topicsSection, pageSection], editsection);
})




// ///////////////////////////
// //--- Add new Fields ---//
// /////////////////////////
const newLinkBtn = document.querySelector('#settings__btn-link');
const markup = `<div>
<span class="span color-txt-dark">Put only embed code with &lt;iframe&gt; tag for vimeo and youtube</span>
</div>
<div>
<span class="span color-txt-dark"> eg. &lt;iframe width=&quot;560&quot; height=&quot;315&quot; src=&quot;https://www.youtube.com/embed/aJDqelOW6ho&quot; frameborder=&quot;0&quot; allowfullscreen&gt;&lt;/iframe&gt;</span>
</div>`
addNewFields(newLinkBtn, videoLinkContainer, 'video link', 'text', 'videoLink', markup);




///////////////////////////////////////
//---- On clicking create btn ----////
//////////////////////////////////////
btnCreate.addEventListener('click', () => {
    
    // Set create url in the form
    testimonialForm.setAttribute('action', `/admin/createTestimonial?tempId=${tempId}&&_csrf=${csrf}`);
    
    // Display edit section
    seeShow_adv([topicsSection, pageSection], editsection);
})


///////////////////////////////////////
//---- On clicking Back btn ----////
//////////////////////////////////////

backBtn.addEventListener('click', async function () {

    // Remove Edit Fields
    if (this.getAttribute('data-mode') == 'edit') {

        // Clear the fields
        clearEditFields(state.defaultImg)
        
        // Remove btn-mode
        backBtn.setAttribute('data-mode', '');
    }

    // Render Spinner
    loadLargeSpinner(animation);
    
    // Delete the array of images with the tempId
    const removeTempArr = new remove(`/admin/tempTestimonialsArr/delete?tempId=${tempId}&&_csrf=${csrf}`);
    await removeTempArr.getResults();

    // Render Spinner
    removeSpinner(animation);

    // display main Section
    showBtnClick([topicsSection, pageSection], editsection);
})




///////////////////////////////////////
//---- On clicking delete btn ----////
//////////////////////////////////////
topicsSection.addEventListener('click', e => {

    // Prevent clicking from outside
    if (!e.target.classList.contains('btn-testimonial-delete')) return;

    delfuncRest(e.target);
})

cancelfunc();



/////////////////////////////////////
//---- On deleting a Guestpert ----//

delBox.addEventListener('click', async (e) => {

    // Avoid from clicking outside 
    if (!e.target.classList.contains('popup__del-btn')) return;


    // Store the id
    state.delId = e.target.getAttribute('data-id');
    if (!state.delId) return;


    // Disappear popup
    removeDelBox();


    // Render Spinner
    loadLargeSpinner(animation);

    // Del request to server
    state.removeTestimonial = new remove(`/admin/manageTestimonials/delete?id=${state.delId}&&_csrf=${csrf}`);
    state.removeResult = await state.removeTestimonial.getResults();

    // Remove spinner
    removeSpinner(animation);

    // If error occours
    if (!state.removeResult) {

        // display error message
        message(5000, false, 'Your request could not be completed. Please try again later.')

        // stop next
        return
    }

    // Get active page number
    state.activePage = state.paginationCls.activePageNum();

    // Call init
    init(state.activePage);
})



//////////////////////////
////---- Functions ----////
//////////////////////////

// Show cards function
async function init(pageNum) {

    // Prepare UI
    loadSpinner(testimonialBox);

    // Get Data from Server
    state.testimonialSearch = new Search(`/admin/manageTestimonials/search?pageNum=${pageNum}`);
    state.testimonialSearchRes = await state.testimonialSearch.getResults();

    // Render Error page in UI
    removeSpinner(testimonialBox);

    if (!state.testimonialSearchRes) {

        // Render Error message
        const msg = 'Error in fethcing Testimonials';
        message(5000, false, msg);

        // Return Error
        return;
    };

    // Make change in UI
    renderCards(testimonialBox, state.testimonialSearchRes.data.dataArr);

    // Pagingation
    state.paginationCls.changeVals(+state.testimonialSearchRes.data.totalItems);
    state.paginationCls.changePage(+state.testimonialSearchRes.data.pageNum);
}
init(1);


initTinymce(tinymceObj(tempId))

function tinymceObj(id) {
    return {
        selector: "#content",
        menubar: !1,
        skin: "CUSTOM",
        content_css: "CUSTOM",
        plugins: "preview image lists fullscreen paste",
        paste_as_text: true,
        toolbar: "undo redo image preview fullscreen bold italic alignjustify",
        advlist_bullet_styles: "disc",
        images_upload_url: `/admin/imageUpload/testimonials?tempId=${id}&&_csrf=${csrf}`,
        image_dimensions: false,
    }
}

const errMsg = document.querySelector('.message');
window.addEventListener('load', e=>{
    setTimeout(()=>{
        if(!errMsg)return;
        errMsg.style.display='none';
        errMsg.querySelector('.message__txt').textContent='';
    }, 5000)
})



import { logoutFunc } from './lib/autoLogout'
logoutFunc(() => {
    window.location.href = `/admin_login`
})