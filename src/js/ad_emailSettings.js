import "../sass/ad_emailSettings.scss";
import "./view/importsvg";

import { seeShow } from "./view/seeShowbtns";
import { pagination, showCards } from "./view/pagination";

import { initTinymce } from './utils/tinyActions';

import "./vendor/form-dropdown";
import { toArray } from "tinymce";
import { hamburger, slideMenu } from "./view/sidebar";

///////////////////////////////////////////
///--- Hamburger and Sidebar functionality ---///
///////////////////////////////////////////
hamburger();
slideMenu();


//////////////////////////////////////
//--- Universal Selectors ---//
//////////////////////////////////////
const editSection = document.querySelector('.edit');
const EmailForm = editSection.querySelector('.edit__form-container');
const animeBox = document.getElementById('animations');
const topicsSection = document.querySelector('.email-template');
const csrf = document.querySelector('#csrf').getAttribute('data-csrf');


///////////////////////////////////////////////
////---- On clicking create and see btns ----////
///////////////////////////////////////////////
const page = document.querySelector('.page');
seeShow(topicsSection);
seeShow(page);

document.querySelector('.btn-createNew').addEventListener('click', e => {

    // Make edit route available in action
    EmailForm.setAttribute('action', `/admin/createEmailSettings?_csrf=${csrf}`);
})

///////////////////////////////////////////////
////---- Pagination functionality ----////
///////////////////////////////////////////////
const cards = toArray(document.querySelectorAll('.email-template__cards'));
const cardNumb = cards.length;

// Show cards function
const pageFunc = showCards(cards);

// Calling the class pagination 
new pagination(cardNumb, 12, pageFunc) // cardNumb is total cards, 2 is limit to show Show Cards is the callback


////////////////////////////////////////
///////////////////////////////////////
/////---- REST Features ----///////////
//////////////////////////////////////
//////////////////////////////////////



import Search from './model/search';
import { prepareForLoad, removeSpinner } from './view/spinner';
import { renderEdit, renderError, removeFields } from './view/pg-ad_emailSettings';

// State of the home Page
const state = {};

// Clicking on Edit Button
topicsSection.addEventListener('click', async (e) => {

    // Preventing from clicking outside
    if (!e.target.classList.contains('edit-email')) { return };

    // store the button
    const btn = e.target;
    if (!btn) { return };

    // store the id
    const id = btn.getAttribute('data-id');

    // Render Spinner
    prepareForLoad(topicsSection, animeBox);

    // Search
    state.search = new Search(`/admin/emailSettings/edit?id=${id}`);
    state.result = await state.search.getResults();
    // console.log(state.result)

    // Remove spinner
    removeSpinner(animeBox);

    // If error occours
    if (!state.result) {
        // call error function
        renderError(topicsSection);

        // stop next
        return
    }

    // Make edit route available in action
    EmailForm.setAttribute('action', `/admin/editEmailSettings?id=${id}&&_csrf=${csrf}`);

    // Render Edit
    renderEdit(state.result.data);
})


/////////////////////////////////////////////
// --- Clicking the show Emails btn ---//
///////////////////////////////////////////
document.querySelector('.edit__show-cards').addEventListener('click', function () {

    // Do nothing if not the button
    if (!this.classList.contains('editMode')) { return };

    // Remove Fields
    removeFields();

    // Make edit route available in action
    EmailForm.setAttribute('action', '');

    // Remove class from button
    this.classList.remove('editMode');
});


const errMsg = document.querySelector('.message');

window.addEventListener('load', e => {
    setTimeout(() => {
        if (!errMsg) return;
        errMsg.style.display = 'none';
        errMsg.querySelector('.message__txt').textContent = '';
    }, 5000)
})

///////////////////////////////
// --- tiny mce settings ---//
///////////////////////////////


initTinymce(
    {
        selector: "#emailContent",
        menubar: !1,
        skin: "CUSTOM",
        content_css: "CUSTOM",
        plugins: "preview lists fullscreen link paste",
        relative_urls: false,
        remove_script_host: false,
        document_base_url: 'https://www.tvguestpert.com',
        paste_as_text: true,
        toolbar: "undo redo preview fullscreen bold italic alignjustify link"
    }
)


import { logoutFunc } from './lib/autoLogout'
logoutFunc(() => {
    window.location.href = `/admin_login`
})