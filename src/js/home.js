import "../sass/home.scss";
import "./view/importsvg";
import "regenerator-runtime/runtime";


import { headerCarousel, featuredBooksCarousel } from './view/glide';
import * as popup from './view/popup';
import { typeFunc } from './view/typewritter';
import { shrinkFunc } from './view/sideShrink';
import { rotateShrinkFunc } from './view/rotateShrink';
import { autoComplete, removeHiddenInput } from './view/autoComplete';


////////////////////////////////////////
///////////////////////////////////////
/////---- Frontend Animations ----////
/////////////////////////////////////
////////////////////////////////////

//---------------------------//
//------ Carousel Header ------///
const headerSlider = headerCarousel();


//---------------------------//
//------ Carousel Featured Books ----------///
featuredBooksCarousel();


//---------------------------//
//------ Popup -----///
function popupPlay() {
    if (this.dataset.url) {
        // ---- play the youtube video
        popup.playVideo(this.dataset.url);
    }

    // ---- Pause the Carousel
    headerSlider.pause();
}

function popupPause() {
    //---- Pause the youtube video
    popup.pauseVideo();

    //---- Play the carousel
    headerSlider.play();
}

/////////---- Grab all play buttons and trigger function
const playBtn = document.querySelectorAll('.button__play');
playBtn.forEach(btn => {
    btn.addEventListener('click', popupPlay)
})

/////////---- Grab close function and trigger pause function
document.querySelector('.popup__close').addEventListener('click', popupPause);



//---------------------------//
//------ Type Writter -----///

//---- Init on DOM Load
document.addEventListener('onload', typeFunc);

//---- Get the sentences in an array
const type_sentences = Array.from(document.querySelectorAll('.company-tags__modal-txt')).map(el => el.textContent);

//---- Get the txt Element and wait
const type_txtEl = document.querySelector('.company-tags__txt-type');
const type_wait = type_txtEl.getAttribute('data-wait');

//---- Call typeFunction 
typeFunc(type_txtEl, type_sentences, type_wait);



//------ Ribon small Upcoming GuestPerts-----///

const ribonUpcomingGuestpert = document.querySelector('.ribon__upcoming-guestpert');

if (ribonUpcomingGuestpert) {
    //---- Init on DOM Load
    document.addEventListener('onload', shrinkFunc);

    //---- Get Names and designations
    const guestPerts_Arr = Array.from(ribonUpcomingGuestpert.querySelectorAll('.ribon-sm__upcoming-modals'));

    const guestPerts_NameArr = guestPerts_Arr.map(el => el.textContent);
    const guestPerts_DesignationArr = guestPerts_Arr.map(el => el.getAttribute('data-designation'));

    //---- Get the element and waiting time
    const shrink_txtEl = ribonUpcomingGuestpert.querySelector('.ribon-sm__content');
    const shrink_wait = shrink_txtEl.getAttribute('data-wait');

    shrinkFunc(shrink_txtEl, guestPerts_NameArr, guestPerts_DesignationArr, shrink_wait)
}



//------ Ribon small Hot Topics-----///

const ribonHotTopics = document.querySelector('.ribon__hotTopics');

if (ribonHotTopics) {

    // Get text content 
    const hotTopicsArr = Array.from(ribonHotTopics.querySelectorAll('.ribon-sm__hotTopics-modals')).map(el => el.textContent);
    const shrink_txtEl = ribonHotTopics.querySelector('.ribon-sm__content');
    const shrink_wait = shrink_txtEl.getAttribute('data-wait');

    shrinkFunc(shrink_txtEl, hotTopicsArr, [], shrink_wait)
}



//------Trigger all Gallery Animation-----//
rotateShrinkFunc();

//------ Input Autocomplete -----//
autoComplete()


// ---------------------------------------
const featuredGuestperts = document.querySelector('.featured__guestperts');
const guestpertGallery = featuredGuestperts.querySelector('.gallery');

guestpertGallery.addEventListener('click', e => {
    const target = e.target.closest('.gallery__img-frame');
    if (!target) return;
    const guestpertId = target.getAttribute('data-id');
    window.location.href=`/guestperts/${guestpertId}`
    
})


//  ------------------------------------
const featuredHotTopics = document.querySelector('.featured__hot-topics');
const hotTopicsGallery = featuredHotTopics.querySelector('.gallery');

hotTopicsGallery.addEventListener('click', e => {
    const target = e.target.closest('.gallery__img-frame');
    if (!target) return;
    const hotTopicId = target.getAttribute('data-id');
    window.location.href=`/readTopic/${hotTopicId}`
})

////////////////////////////////////////
///////////////////////////////////////
/////---- REST Features ----//////////
/////////////////////////////////////
////////////////////////////////////

import Search from './model/search';
import { renderSearchUI } from './view/rest-autocomplete';

const regEx = /^[\w"': ]{1,60}$/i;

// State of the home Page
const state = {};

const controlAutoComplete = async (el) => {

    // Finding the type of form (guestpert / hottopic)
    const type = el.getAttribute('name');

    // Assigning url
    let url;
    if (type === 'guestpert-keyword') {
        url = 'searchguestpert';
    }
    else if (type === 'hotTopic-keyword') {
        url = 'searchHotTopics'
    }

    // Checking url
    if (!url) { return }

    // Search
    state.search = new Search(`/${url}/${el.value}`);
    state.result = await state.search.getResults();

    // Render in UI
    if (!state.result) { return }
    renderSearchUI(state.result.data.data, el);
}


document.querySelectorAll('.form__search-input').forEach(cur => {
    cur.addEventListener('keyup', async function (e) {

        // Search input form
        if (!document.activeElement.classList.contains('form__search-input')) { return };

        const chk = ['', 8, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 96, 97, 98, 99, 100, 101, 102, 103, 104, 105, 106, 222, 186]
        const k = chk.indexOf(e.keyCode);

        if (k < 0 || !k) { return };

        // Remove hidden input if something is written
        const parent = cur.parentElement;
        removeHiddenInput(parent);

        if (this.value.length > 1) {

            // RegExp
            if (!regEx.test(this.value)) { return };

            // Get Results from api
            controlAutoComplete(this);

        }
        else if (this.value.length < 1) {
            this.closest('.search-box').querySelector('.custom-select').classList.remove('open');
        }
    })
})

const logoutBtn = document.querySelector('.header-bar__link-logout')
import { logoutFunc } from './lib/autoLogout'
if(logoutBtn){
    logoutFunc(() => {
        const btnId = logoutBtn.getAttribute('data-id')
        if(btnId=='adtyspnce'){
            window.location.href = `/admin_login`
        }else{
            window.location.href = `/login`
        }
    })
} 
