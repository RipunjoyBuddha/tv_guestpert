import "../sass/guestpert.scss";
import "./view/importsvg";

import { pagination } from "./view/pagination_rest";
import Search from './model/search';
import { loadSpinner, removeSpinner } from './view/spinner';
import { renderCards } from './view/pg-guestperts';
import {renderSearchUI} from './view/rest-autocomplete';
import { autoComplete, autoCompleteClickPress } from './view/autoComplete';


//////////////////////////////////////
////---- Universal Selectors ----////
/////////////////////////////////////
const cardsSection = document.querySelector('.guestperts');
const filterArr = document.querySelectorAll('.hero__filter-btn');
let searchInput = document.querySelector('#search-input');
let hiddenInput = document.querySelector('#hidden-input');
const pageList = document.querySelector('#page__list');



//------ Input Autocomplete -----//
autoComplete(true);
autoCompleteClickPress(init);


// State of the guestpert Page
const state = {};

///////////////////////////////////////////////
////---- Pagination functionality ----////
///////////////////////////////////////////////
state.paginationCls = new pagination(6) // 6 is Element per page
window.addEventListener('load', e=>{
    searchInput = document.querySelector('#search-input');
    hiddenInput = document.querySelector('#hidden-input');
    init(1);
})

// On clicking other pages
pageList.addEventListener('click', function (e) {
    const target = e.target;
    if (!target.classList.contains('clickable')) { return };

    const pageNum = target.getAttribute('data-num');
    init(pageNum);
});


///////////////////////////////////////////////
////---- Search bar functionality ----////
///////////////////////////////////////////////

const regEx = /^[\w"': ]{1,60}$/i;

const controlAutoComplete = async (el) => {
    // Search
    state.search = new Search(`/searchguestpert/${el.value}`);
    state.result = await state.search.getResults();

    // Render in UI
    if (!state.result) { return }
    renderSearchUI(state.result.data.data, el);
}

document.querySelectorAll('.form__search-input').forEach(cur => {
    cur.addEventListener('keyup', async function (e) {
        const chk = ['', 8, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 96, 97, 98, 99, 100, 101, 102, 103, 104, 105, 106, 222, 186]
        const k = chk.indexOf(e.keyCode);

        if (k < 0 || !k) { return };

        if (this.value.length > 1) {
            // Get Results from api
            controlAutoComplete(this);

            // RegExp
            if (!regEx.test(this.value)) { return };
        }
        else if (this.value.length === 0) {
            this.closest('.search-box').querySelector('.custom-select').classList.remove('open');
        }
    })
})


// ---- When Search button is clicked --- //
document.querySelector('#form-search-btn').addEventListener('click', () => {

    // Stop function if nothing is there
    if (searchInput.value.length < 1) { return }

    // Prepare for calling init function
    hiddenInput.value = searchInput.value;

    // Call init
    init(1);

    // After init is called
    searchInput.value = '';
    document.querySelectorAll('.custom-select').forEach(c => c.classList.remove('open'));
    filterArr.forEach(c => c.classList.remove('hero__filter-btn--active'));
})



///////////////////////////////////////////////
////---- Filter functionality ----////
///////////////////////////////////////////////

for (const btn of filterArr) {
    btn.addEventListener('click', function (e) {

        // Disable clicking active btn
        if (btn.classList.contains('hero__filter-btn--active')) { return };

        // Remove active class from previous
        filterArr.forEach(cur => cur.classList.remove('hero__filter-btn--active'));

        // Clear the hidden input 
        hiddenInput.value = '';

        // Add active class to this btn
        this.classList.add('hero__filter-btn--active');

        // Call initPagination
        init(1)

    })
}



/////////////////////////////
////---- Functions ----////
///////////////////////////

// Show cards function
async function init(pageNum) {

    // Get active Filter
    let filter = '';
    for (const fltr of filterArr) {
        if (fltr.classList.contains('hero__filter-btn--active')) {
            filter = fltr.textContent.toLowerCase();
        }
    }

    // Get keywords from input
    let keywords = hiddenInput.value;

    // Prepare UI
    loadSpinner(cardsSection)

    // Get Data from Server
    state.pageSearch = new Search(`/searchguestpert?pageNum=${pageNum}&&filter=${filter}&&keywords=${keywords}`);
    state.pageSearchRes = await state.pageSearch.getResults();

    if (!state.pageSearchRes) {
        // Render Error page in UI
        removeSpinner(cardsSection);

        // Render Error message

        // Return Error
        return;
    };

    // Prepare for change
    removeSpinner(cardsSection);

    // Make change in UI
    renderCards(cardsSection, state.pageSearchRes.data.dataArr);

    // Pagingation
    state.paginationCls.changeVals(+state.pageSearchRes.data.totalItems);

    state.paginationCls.changePage(+state.pageSearchRes.data.pageNum);
}




/////////////////////////////
////---- See show Menu ----////
///////////////////////////

const seeShowBtn = document.querySelector('.hero__btn-see-show');
const seeShowMenu = document.querySelector('.guestpert-list');
const seeShowBox = seeShowMenu.querySelector('.guestpert-list__box');

seeShowBtn.addEventListener('click', async e=>{
    if(!state.showMenu){
        state.showMenu = true;
    }else{
        state.showMenu = !state.showMenu;
    }

    showHideMenu()
})

async function renderMenu(){
    
    const x = new Search(`/allGuestperts`);
    state.allGuestperts = await x.getResults();
    
    if(!state.allGuestperts)return;
    
    const newArr = state.allGuestperts.data.dataArr.map(c=>`<li><a href="/guestperts/${c._id}">${c.name}</a></li>`);
    
    seeShowBox.innerHTML = newArr.join(' ')
}

function showHideMenu(){
    if(state.showMenu){
        seeShowMenu.classList.add('guestpert-list__active');
    }else{
        seeShowMenu.classList.remove('guestpert-list__active');
    }
}

renderMenu();


import { logoutFunc } from './lib/autoLogout'
logoutFunc(() => {
    window.location.href = `/login`
})