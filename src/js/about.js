import "../sass/about.scss";
import svg from "./view/importsvg";

import {aboutMember, vanish} from "./view/moreDetailsPopup";

window.addEventListener('click', e=>{

    // --- Prevent from clicking outside
    if(!e.target.classList.contains('btn-read-more'))return;
    
    // Get the card
    const card  = e.target.closest('.card');
    
    // Call aboutMember
    aboutMember(card);
})

window.addEventListener('click', e=>{
    // --- Prevent from clicking outside
    if(!e.target.classList.contains('popup'))return;
    vanish(e.target);
})


window.addEventListener('click', e=>{
    // --- Prevent from clicking outside
    if(!e.target.classList.contains('popup__close--icon'))return;
    vanish(e.target.closest('.popup'));
})


const logoutBtn = document.querySelector('.header-bar__link-logout')
import { logoutFunc } from './lib/autoLogout'
if(logoutBtn){
    logoutFunc(() => {
        const btnId = logoutBtn.getAttribute('data-id')
        if(btnId=='adtyspnce'){
            window.location.href = `/admin_login`
        }else{
            window.location.href = `/login`
        }
    })
} 