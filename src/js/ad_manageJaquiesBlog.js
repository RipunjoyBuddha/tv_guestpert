import "../sass/ad_manageJaquiesBlog.scss";
import "./view/importsvg";

import uniqid from 'uniqid'

import Search from './model/search';
import remove from './model/deleteCard';
import {showBtnClick, seeShow_adv} from "./view/seeShowbtns";
import {pagination} from './view/pagination_rest';
import {loadLargeSpinner, loadSpinner, removeSpinner} from './view/spinner';
import {message} from "./view/errorMessage";
import {renderCards, renderEdit, removeFields} from "./view/pg-ad_manageJaquiesBlog";
import {chooseImg} from "./view/choose_img";
import {delfuncRest, removeDelBox, cancelfunc} from "./view/deleteBox";
import { hamburger, slideMenu } from "./view/sidebar";
import {initTinymce} from './utils/tinyActions';

///////////////////////////////////////////
///--- Hamburger and Sidebar functionality ---///
///////////////////////////////////////////
hamburger();
slideMenu();

/////////////////////////////////////
////---- Universal Selectors ----////
/////////////////////////////////////
const topicsSection = document.querySelector('.blogs');
const editsection = document.querySelector('.edit');
const pageSection = document.querySelector('.page');
const blogForm = editsection.querySelector('form');
const pageList = pageSection.querySelector('#page__list');
const blogsBox = topicsSection.querySelector('#all-blogs-box');
const animeBox = document.querySelector('#animations');
const inputBox = editsection.querySelector('#inpFile-box');
const popup = document.querySelector('.popup');
const csrf = document.querySelector('#csrf').getAttribute('data-csrf');

const pageUniqId = uniqid();


/*****************************************/
/***** Protect from posting large image */
blogForm.addEventListener('submit', e=>{

    e.preventDefault()
    const imgFile = blogForm.querySelector('#choose-img-file').files[0]

    if(imgFile && imgFile.size > 500*1000){
        message(5000, false, 'Too large image file. Please reduce image file size')
        return
    }

    e.target.submit()
})



///////////////////////////////////////////////
////---- On clicking create and see btns ----////
//////////////////////////////////////////////

const createBtn = document.querySelector('.btn-createNew');
createBtn.addEventListener('click', e=>{
    seeShow_adv([pageSection, topicsSection], editsection)
    blogForm.setAttribute('action', `/admin/createFounderBlog?_csrf=${csrf}&&tempId=${pageUniqId}`)
})

// State of the guestpert Page
const state = {};

// Choose image functionality
chooseImg(inputBox)



///////////////////////////////////////////
////---- Pagination functionality ----////
//////////////////////////////////////////
state.paginationCls = new pagination(12) // 12 is Element per page

// On clicking other pages
pageList.addEventListener('click', function (e) {
    const target = e.target;
    if (!target.classList.contains('clickable')) { return };

    const pageNum = target.getAttribute('data-num');
    init(pageNum);
});


// Show cards function
async function init(pageNum) {

    // Prepare UI
    loadSpinner(blogsBox);

    // Get Data from Server
    state.search = new Search(`/admin/searchFounderBlogs?pageNum=${pageNum}`);
    state.searchRes = await state.search.getResults();

    // Remove Spinner
    removeSpinner(blogsBox);
    
    if (!state.searchRes) {

        // Render Error message
        const msg = 'Error in fethcing blogs. Try to reload the page'; 
        message(5000, false, msg);

        // Return Error
        return;
    };

    // Make change in UI
    renderCards(blogsBox , state.searchRes.data.dataArr);

    // Pagingation
    state.paginationCls.changeVals(+state.searchRes.data.totalItems);
    state.paginationCls.changePage(+state.searchRes.data.pageNum);
}
init(1)



//////////////////////////////////
// --- Clicking the edit btn ---//
////////////////////////////////
topicsSection.addEventListener('click', async function(e){

    // Don't do anything if clicked outside
    if(!e.target.classList.contains('hot-topics__card-btn-edit')){return};
    
    // store the button
    const btn = e.target;
    if(!btn){return};

    // store the id
    const id = btn.getAttribute('data-id');

    // Render Spinner
    loadLargeSpinner(animeBox);
    
    // Search
    state.searchBlog = new Search(`/admin/manageFounderBlogs/edit?id=${id}`);
    state.result = await state.searchBlog.getResults();

    // Remove spinner
    removeSpinner(animeBox);

    // If error occours
    if(!state.result){
     
        // Render Error message
        const msg = 'Error occoured in editing . Please try again later'; 
        message(5000, false, msg);
        
        // stop next
        return
    }

    // Set Url
    blogForm.setAttribute('action', `/admin/editFounderBlog?id=${id}&&_csrf=${csrf}&&tempId=${pageUniqId}`)

    // Render Edit
    renderEdit(state.result.data);
    
    // Hide the main sections
    seeShow_adv([topicsSection, pageSection], editsection);

});



/////////////////////////////////////////
////---- On clicking delete btn ----////
///////////////////////////////////////
topicsSection.addEventListener('click', (e)=>{
    if(e.target.classList.contains('hot-topics__card-btn-delete')){

        // Store the card
        state.activeCard = e.target.closest('.hot-topics__cards-wrapper');
        
        // Call del func
        delfuncRest(e.target);
    }
})

/////////////////////////////////////////
////---- On clicking cancel btn ----////
///////////////////////////////////////
cancelfunc();


////////////////////////////////////////
// --- Clicking the back button ---//
///////////////////////////////////////

document.querySelector('.edit__show-cards').addEventListener('click', async function(){

    // Remove Fields
    removeFields();

    // Render Spinner
    loadLargeSpinner(animeBox);

    // Delete the array of images with the tempId
    const removeTempArr = new remove(`/admin/tempBlogsArr/delete?tempId=${pageUniqId}&&_csrf=${csrf}`);
    await removeTempArr.getResults();

    // Render Spinner
    removeSpinner(animeBox);

    // Setting Form url
    blogForm.setAttribute('action', '')
    showBtnClick([topicsSection, pageSection], editsection);
})



///////////////////////////////////
// --- Clicking the remove btn ---//
////////////////////////////////////
popup.addEventListener('click', async (e)=>{
    
    // Avoid from clicking outside 
    if(!e.target.classList.contains('popup__del-btn'))return;

    // Store the id
    state.delId = e.target.getAttribute('data-id');
    
    // Disappear popup
    removeDelBox();

    // Render Spinner
    loadLargeSpinner(animeBox);

    // Del request to server
    state.remove = new remove(`/admin/manageFounderBlogs/delete?id=${state.delId}&&_csrf=${csrf}`);
    state.result = await state.remove.getResults();

    // Remove spinner
    removeSpinner(animeBox);

    // If error occours
    if (!state.result) {

        // display error message
        message(5000, false, 'Blog couldn\'t be deleted. Please try again later.')
        
        // stop next
        return
    }
    
    // Display success message
    message(5000, true, 'Your blog have been successfully deleted');
    
    
    // Remove the card from topicSection
    state.activeCard.parentElement.removeChild(state.activeCard);

    // disappear the state.activeCard
    state.activeCard = null;
    
})



const errMsg = document.querySelector('.message');

window.addEventListener('load', e=>{
    setTimeout(()=>{
        if(!errMsg)return;
        errMsg.style.display='none';
        errMsg.querySelector('.message__txt').textContent='';
    }, 5000)
})

///////////////////////////////
// --- tiny mce settings ---//
///////////////////////////////


initTinymce(
    { 
        selector: "#content",
        menubar: !1,
        skin: "CUSTOM",
        content_css: "CUSTOM",
        plugins: "preview image lists fullscreen link paste",
        paste_as_text: true,
        toolbar: "undo redo image preview fullscreen bullist bold italic alignjustify link",
        advlist_bullet_styles: "disc",
        images_upload_url: `/admin/founderBlogUploadPic?tempId=${pageUniqId}&&_csrf=${csrf}`,
        image_dimensions: false,
    }
)


import { logoutFunc } from './lib/autoLogout'
logoutFunc(() => {
    window.location.href = `/admin_login`
})