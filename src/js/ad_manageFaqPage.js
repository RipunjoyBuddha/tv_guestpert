import "../sass/ad_manageFaqPage.scss";
import "./view/importsvg";

import { initTinymce } from './utils/tinyActions';

import { seeShow } from "./view/seeShowbtns";
import { delfuncRest, cancelfunc, removeDelBox } from "./view/deleteBox";
import { message } from "./view/errorMessage";
import { hamburger, slideMenu } from "./view/sidebar";

///////////////////////////////////////////
///--- Hamburger and Sidebar functionality ---///
///////////////////////////////////////////
hamburger();
slideMenu();


// State of the Page
const state = {};

/////////////////////////////////////////////
//---- Universal Selectors ----////
/////////////////////////////////////////////
const cardsSection = document.querySelector('.faq');
const editSection = document.getElementById('edit-section');
const form = editSection.querySelector('.edit__form-container');
const animeBox = document.getElementById('animations');
const showCardsBtn = editSection.querySelector('.edit__show-cards');
const popup = document.querySelector('.popup');
const createBtn = document.querySelector('.btn-createNew');
const csrf = document.querySelector('#csrf').getAttribute('data-csrf');

///////////////////////////////////////////////
////---- On clicking create and see btns ----////
///////////////////////////////////////////////
seeShow(cardsSection);
createBtn.addEventListener('click', e => {
    form.setAttribute('action', `/admin/createFaq?_csrf=${csrf}`);
})

///////////////////////////////////////////////
////---- On clicking cancel btn ----////
///////////////////////////////////////////////
cancelfunc();


////////////////////////////////////////
////---- Clicking Remove Button ----////
////////////////////////////////////////
cardsSection.addEventListener('click', function (e) {

    // Prevent from clicking outside
    if (!e.target.classList.contains('faty__card-btn-remove')) return;

    // Store the card
    state.activeCard = e.target.closest('.faty__card');

    // Show the popup
    delfuncRest(e.target);
})


///////////////////////////////////////////////
///////////////////////////////////////////////
////---- Rest Features ----//// ----------
///////////////////////////////////////////////
///////////////////////////////////////////////

import Search from './model/search';
import remove from './model/deleteCard';
import { prepareForLoad, removeSpinner } from './view/spinner';
import { renderEdit, removeFields } from './view/pg-ad_faq';


// Edit functionality
cardsSection.addEventListener('click', async (e) => {

    // Prevent from clicking outside
    if (!e.target.classList.contains('faty__card-btn-edit')) return;

    // store the button
    const btn = e.target;
    if (!btn) { return };

    // store the id
    const id = btn.getAttribute('data-id');

    // Render Spinner
    prepareForLoad(cardsSection, animeBox);

    // Search
    state.search = new Search(`/admin/manageFaq/edit?id=${id}`);
    state.result = await state.search.getResults();

    // Remove spinner
    removeSpinner(animeBox);

    // If error occours
    if (!state.result) {

        // display topics Section
        cardsSection.style.display = 'flex';

        // display error message
        message(5000, false, 'Your request could not be completed. Please try again later.')
        // stop next
        return
    }

    form.setAttribute('action', `/admin/editFaq?id=${state.result.data._id}&&_csrf=${csrf}`);

    // Render Edit
    renderEdit(state.result.data, editSection);

    // Change the edit mode
    showCardsBtn.setAttribute('data-mode', 'edit');

    // Change Form title
    editSection.querySelector('.edit__form-heading').textContent = 'Create & Edit FAQ';
})




/////////////////////////////////////////
// --- Clicking the show cards Btn ---//
////////////////////////////////////////

showCardsBtn.addEventListener('click', function () {

    // Do nothing if not the button
    if (this.getAttribute('data-mode') !== 'edit') { return };

    // Remove Fields
    removeFields(editSection);

    // Do nothing if not the button
    this.setAttribute('data-mode', '');
})



////////////////////////////////////
// --- Clicking the remove btn ---//
////////////////////////////////////
popup.addEventListener('click', async (e) => {

    // Avoid from clicking outside 
    if (!e.target.classList.contains('popup__del-btn')) return;

    // Store the id
    state.delId = e.target.getAttribute('data-id');

    // Disappear popup
    removeDelBox();

    // Render Spinner
    prepareForLoad(cardsSection, animeBox);

    // Del request to server
    state.remove = new remove(`/admin/manageFaq/remove?id=${state.delId}&&_csrf=${csrf}`);
    state.result = await state.remove.getResults();

    // Remove spinner
    removeSpinner(animeBox);

    // If error occours
    if (!state.result) {

        cardsSection.style.display = 'flex';

        // display error message
        message(5000, false, 'Your request could not be completed. Please try again later.')

        // stop next
        return
    }

    // Remove the card from topicSection
    state.activeCard.parentElement.removeChild(state.activeCard);

    // disappear the state.activeCard
    state.activeCard = null;

    // Display card Section
    cardsSection.style.display = 'flex';
})

initTinymce({
    selector: "#answer",
    menubar: !1,
    skin: "CUSTOM",
    content_css: "CUSTOM",
    plugins: "preview image lists fullscreen paste",
    paste_as_text: true,
    toolbar: "undo redo image preview fullscreen bold italic alignjustify",
    advlist_bullet_styles: "disc",
    images_upload_url: `/admin/imageUpload/servicePage?_csrf=${csrf}`,
    image_dimensions: false,
})


import { logoutFunc } from './lib/autoLogout'
logoutFunc(() => {
    window.location.href = `/admin_login`
})