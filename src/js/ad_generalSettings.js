import "../sass/ad_generalSettings.scss";
import "./view/importsvg";
import { removeElement, removeBtnGenSettings,addNewFields, addNewEmail} from "./view/addRemoveElement";
import { hamburger, slideMenu } from "./view/sidebar";

///////////////////////////////////////////
///--- Hamburger and Sidebar functionality ---///
///////////////////////////////////////////
hamburger();
slideMenu();


///////////////////////////
//---- Universal selectors ----//
/////////////////////////
const otherEmailContainer = document.querySelector('.settings__other-emails');
const mailAddressContainer = document.querySelector('.settings__mailAddress');


///////////////////////////
//---- Remove email and Mail Address ----//
/////////////////////////
const removeBtnClass = 'btn_remove';
removeElement(otherEmailContainer, removeBtnClass, 'email__group');
removeBtnGenSettings(mailAddressContainer, removeBtnClass);


///////////////////////////
//--- Add new Fields ---//
/////////////////////////
const newEmailBtn = document.querySelector('#settings__btn-newEmail');
const addMailAddressBtn = document.querySelector('#settings__btn-newMailAddress');

addNewEmail(newEmailBtn, otherEmailContainer, 'email', 'email', 'otherEmail');
addNewFields(addMailAddressBtn, mailAddressContainer, 'mail address', 'text', 'mailAddress');


const errMsg = document.querySelector('.message');

window.addEventListener('load', e=>{
    setTimeout(()=>{
        if(!errMsg)return;
        errMsg.style.display='none';
        errMsg.querySelector('.message__txt').textContent='';
    }, 5000)
})

import { logoutFunc } from './lib/autoLogout'
logoutFunc(() => {
    window.location.href = `/admin_login`
})