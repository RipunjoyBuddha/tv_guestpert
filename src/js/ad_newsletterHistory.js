import "../sass/ad_newsletterHistory.scss";
import svg from "./view/importsvg";

import { pagination } from "./view/pagination_rest";
import Search from './model/search';
import { loadSpinner, removeSpinner} from './view/spinner';
import {message} from "./view/errorMessage";
import {renderCards} from "./view/pg-ad_newsletterHistory";
import { hamburger, slideMenu } from "./view/sidebar";

///////////////////////////////////////////
///--- Hamburger and Sidebar functionality ---///
///////////////////////////////////////////
hamburger();
slideMenu();

/////////////////////////////////////
////---- Universal Selectors ----////
/////////////////////////////////////
const topicSection = document.querySelector('.newsletter-history__card-box');
const pageList = document.querySelector('.page__list');

// State of the guestpert Page
const state = {};


///////////////////////////////////////////
////---- Pagination functionality ----////
//////////////////////////////////////////
state.paginationCls = new pagination(2) // 2 is Element per page
init(1);

// On clicking other pages
pageList.addEventListener('click', function (e) {
    const target = e.target;
    if (!target.classList.contains('clickable')) { return };

    const pageNum = target.getAttribute('data-num');
    init(pageNum);
});


///////////////////////////
////---- Functions ----////
//////////////////////////

// Show cards function
async function init(pageNum) {

    // Prepare UI
    loadSpinner(topicSection);

    // Get Data from Server
    state.newsHistorySearch = new Search(`/admin/searchNewsletterHistory?pageNum=${pageNum}`);
    state.newsHistorySearchRes = await state.newsHistorySearch.getResults();


    if (!state.newsHistorySearchRes) {
        // Render Error page in UI
        removeSpinner(topicSection);

        // Render Error message
        const msg = 'Error in fethcing history. Please try again later';
        message(5000, false, msg);

        // Return Error
        return;
    };

    // Prepare for change
    removeSpinner(topicSection);

    // Make change in UI
    renderCards(topicSection, state.newsHistorySearchRes.data.dataArr);

    // Pagingation
    state.paginationCls.changeVals(+state.newsHistorySearchRes.data.totalItems);
    state.paginationCls.changePage(+state.newsHistorySearchRes.data.pageNum);
}



import { logoutFunc } from './lib/autoLogout'
logoutFunc(() => {
    window.location.href = `/admin_login`
})