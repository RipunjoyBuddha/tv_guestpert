import "../sass/ad_homePageGuestperts.scss";
import "./view/importsvg";


import { pagination } from "./view/pagination_rest";
import Search from './model/search';
import addCard from './model/addToRequest'
import { renderCards, renderHomePageCards, removeCardsFromHome } from './view/pg-ad_addGuestpertsToHome';
import { loadSpinner, removeSpinner, loadLargeSpinner } from './view/spinner';
import { message } from './view/errorMessage';
import { hamburger, slideMenu } from "./view/sidebar";

///////////////////////////////////////////
///--- Hamburger and Sidebar functionality ---///
///////////////////////////////////////////
hamburger();
slideMenu();


//////////////////////////////////////
////---- Universal Selectors ----////
////////////////////////////////////
const searchBar = document.querySelector('#search-input');
const hiddenInput = searchBar.parentElement.querySelector('#hidden-input');
const homepageGuestpertsSection = document.getElementById('homepage-guestperts-box');
const addNewSection = document.getElementById('add-new-box');
const pageList = document.querySelector('#page__list');
const animation = document.getElementById('animation');
const csrf = document.querySelector('#csrf').getAttribute('data-csrf');

// State of the guestpert Page
const state = {};


///////////////////////////////////////////
////---- Pagination functionality ----////
//////////////////////////////////////////
state.paginationCls = new pagination(24) // 24 is Element per page

// On clicking other pages
pageList.addEventListener('click', function (e) {
    const target = e.target;
    if (!target.classList.contains('clickable')) { return };

    const pageNum = target.getAttribute('data-num');
    init(pageNum);
});


///////////////////////////////////////////
////---- Search bar functionality ----////
/////////////////////////////////////////
const regEx = /^[\w]{1,60}$/i;

searchBar.addEventListener('keyup', function (e) {

    const chk = ['', 8, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 96, 97, 98, 99, 100, 101, 102, 103, 104, 105, 106, 222, 186]
    const k = chk.indexOf(e.keyCode);

    // When ecape is pressed
    if (e.keyCode === 27) {
        e.preventDefault();
        this.value = '';
        hiddenInput.value = '';
        this.blur();
    };

    // Validation 1
    if (!k || k < 0) { return };

    if (this.value.length === 0) {
        hiddenInput.value = '';
        init(1);
    };

    // Validation 2
    if (this.value.length < 2 || !regEx.test(this.value)) { return };

    // Get the value
    const value = this.value;

    // Put text into hidden input
    hiddenInput.value = value;

    // call init
    init(1);
})



//////////////////////////////
////---- Add To Home ----////
////////////////////////////

addNewSection.addEventListener('click', async (e) => {

    // Prevent from clicking outside
    if (!e.target.classList.contains('add-to-home')) { return };

    // Store the btn
    const btn = e.target

    // Get Id
    state.guestpertId = btn.getAttribute('data-id');

    // Start spinner
    loadLargeSpinner(animation);

    // Store the card details
    let obj = {
    id: state.guestpertId,
    imgSrc : btn.closest('.thin').querySelector('.thin__img img').getAttribute('src'),
    fullName : btn.closest('.thin').querySelector('.thin__main-txt').textContent,
    package: btn.closest('.thin').querySelector('.thin__sm-txt').textContent
    };
    state.activeCard = obj;

    // send rest req to backend
    state.addCard = new addCard(`/admin/addGuestpertsToHome?guestpertId=${state.guestpertId}&&_csrf=${csrf}`);
    state.addCardRes = await state.addCard.getResults();
    
    // Start spinner
    removeSpinner(animation);

    // If error occours
    if (!state.addCardRes) {

        // Render Error message
        const msg = 'Failed to process your request. Try again later';
        state.msg = message(5000, false, msg);

        // Return Error
        return;
    }

    // Show success message
    const msg = 'Your Changes have been made successfully';
    message(5000, true, msg);


    // Render books already on home page
    if (state.activeCard) {
        renderHomePageCards(homepageGuestpertsSection, state.activeCard)
    }

    // Call init 
    state.activePage = state.paginationCls.activePageNum();
    init(state.activePage);
})



///////////////////////////////////
////---- Remove from Home ----////
/////////////////////////////////
homepageGuestpertsSection.addEventListener('click', async (e) => {

    // Prevent from clicking outside
    if (!e.target.classList.contains('remove-from-home')) { return };

    // Save Minimum 6 Requirement
    if (homepageGuestpertsSection.querySelectorAll('.thin').length <= 6) {
        // Render Error message
        const msg = 'Sorry, Minimum 6 cards are required for homepage to work';
        state.msg = message(5000, false, msg);
        
        // Return Error
        return;
    }

    // Store the btn
    const btn = e.target

    // Get Id
    state.guestpertId = btn.getAttribute('data-id');
    if(!state.guestpertId){return}

    // Start spinner
    loadLargeSpinner(animation);

    // Store the card details
    state.activeCard = btn.closest('.thin');
    // send rest req to backend
    state.removeCard = new addCard(`/admin/removeGuestpertsFromHome?guestpertId=${state.guestpertId}&&_csrf=${csrf}`);
    state.removeCardRes = await state.removeCard.getResults();

    // Remove spinner
    removeSpinner(animation);

    // If error occours
    if (!state.removeCardRes) {

        // Render Error message
        const msg = 'Failed to process your request. Try again later';
        message(5000, false, msg);

        // Return Error
        return;
    }

    
    // Show success message
    const msg = 'Your Changes have been made successfully';
    message(5000, true, msg);

    // Remove the book from homePage Collection
    if (state.activeCard) {
        removeCardsFromHome(homepageGuestpertsSection, state.activeCard);
    }
})


///////////////////////////
////---- Functions ----////
//////////////////////////

// Show cards function
async function init(pageNum) {

    // Get keywords from input
    let keywords = hiddenInput.value;

    // Prepare UI
    loadSpinner(addNewSection);

    // Get Data from Server
    state.pageSearch = new Search(`/admin/searchGuestperts?pageNum=${pageNum}&&keywords=${keywords}`);
    state.pageSearchRes = await state.pageSearch.getResults();

    // Remove Spinner
    removeSpinner(addNewSection);
    
    if (!state.pageSearchRes) {

        // Render Error message
        const msg = 'Error in fethcing data';
        message(5000, false, msg);

        // Return Error
        return;
    };

    // // Make change in UI
    renderCards(addNewSection, state.pageSearchRes.data.dataArr);

    // Pagingation
    state.paginationCls.changeVals(+state.pageSearchRes.data.totalItems);
    state.paginationCls.changePage(+state.pageSearchRes.data.pageNum);
}
init(1)

const errMsg = document.querySelector('.message');

window.addEventListener('load', e=>{
    setTimeout(()=>{
        if(!errMsg)return;
        errMsg.style.display='none';
        errMsg.querySelector('.message__txt').textContent='';
    }, 5000)
})


import { logoutFunc } from './lib/autoLogout'
logoutFunc(() => {
    window.location.href = `/admin_login`
})