import "../sass/register.scss";
import "./view/importsvg";

const regForm = document.querySelector('.register__form');
regForm.addEventListener('submit', (e) => {
    e.preventDefault();
    const captcha = regForm.querySelector('#g-recaptcha-response').value;
    if (!captcha) return;

    regForm.submit()
})

const errMsg = document.querySelector('.message');

window.addEventListener('load', e=>{
    setTimeout(()=>{
        if(!errMsg)return;
        errMsg.style.display='none';
        errMsg.querySelector('.message__txt').textContent='';
    }, 5000)
})

const logoutBtn = document.querySelector('.header-bar__link-logout')
import { logoutFunc } from './lib/autoLogout'
if(logoutBtn){
    logoutFunc(() => {
        const btnId = logoutBtn.getAttribute('data-id')
        if(btnId=='adtyspnce'){
            window.location.href = `/admin_login`
        }else{
            window.location.href = `/login`
        }
    })
} 