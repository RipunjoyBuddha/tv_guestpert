import "../sass/ad_companyTags.scss";
import "./view/importsvg";
import {removeBtnGenSettings, addNewFields, addNewUpcomingAppearances, removeElement} from "./view/addRemoveElement";

import { hamburger, slideMenu } from "./view/sidebar";

///////////////////////////////////////////
///--- Hamburger and Sidebar functionality ---///
///////////////////////////////////////////
hamburger();
slideMenu();

///////////////////////////
//---- Universal selectors ----//
/////////////////////////
const companyTagsBox = document.querySelector('.companyTags__box');
const upcomingAppearancesBox = document.querySelector('.set-upcomingAppearances-tags');
const hotTopicsBox = document.querySelector('.setHotTopicsTags');


///////////////////////////
//---- Remove email and Mail Address ----//
/////////////////////////
const removeBtnClass = 'btn_remove';
removeBtnGenSettings(companyTagsBox, removeBtnClass);
removeBtnGenSettings(hotTopicsBox, removeBtnClass);
removeElement(upcomingAppearancesBox, removeBtnClass, 'box');


///////////////////////////
//--- New Fields ---//
/////////////////////////
const newTagsBtn = document.querySelector('#btn-newTag');
const newUpcomingAppearanceBtn = document.querySelector('#btn-newUpcomingAppearance');
const newHotTopicsBtn = document.querySelector('#btn-newHotTopic');

addNewFields(newTagsBtn, companyTagsBox, 'tag', 'text', 'companyTag');
addNewUpcomingAppearances(newUpcomingAppearanceBtn, upcomingAppearancesBox, 'Guestpert', 'text', 'upcomingAppearanceTag');
addNewFields(newHotTopicsBtn, hotTopicsBox, 'hot topic', 'text', 'hotTopicTag');


const errMsg = document.querySelector('.message'); 

window.addEventListener('load', e=>{
    setTimeout(()=>{
        if(!errMsg)return;
        errMsg.style.display='none';
        errMsg.querySelector('.message__txt').textContent='';
    }, 5000)
})


import { logoutFunc } from './lib/autoLogout'
logoutFunc(() => {
    window.location.href = `/admin_login`
})