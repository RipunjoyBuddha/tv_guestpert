import axios from "axios";

import {domain} from "../utils/domain";

export default class Search {
    constructor(url){
        this.url = url; 
    }

    async getResults(){
        try{
            const data = await axios(`${domain}${this.url}`);
            return data;
        }
        catch(error){
            return null;
        }
    }
}