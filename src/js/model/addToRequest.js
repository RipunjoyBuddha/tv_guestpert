import axios from "axios";
import {domain} from "../utils/domain";

export default class addCard {
    constructor(url){
        this.url = url; 
    }

    async getResults(){
        try{
            const data = await axios({
                url: `${domain}${this.url}`,
                method: 'put'
            })
            return data;
        }
        catch(error){
            return null;
        }
    }
}