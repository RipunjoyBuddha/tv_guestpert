import "../sass/ga_books.scss";
import "./view/importsvg";
import {seeShow} from "./view/seeShowbtns";
import {chooseImg } from "./view/choose_img";
import {delfunc, cancelfunc} from "./view/deleteBox";
import {initTinymce} from './utils/tinyActions';
import {loadLargeSpinner, removeSpinner} from './view/spinner';
import {renderError, renderEdit, removeFields} from './view/pg-ga_manageBooks';
import Search from './model/search';
import {message} from './view/errorMessage'


const state = {};

///////////////////////////////////////////////
////---- Universal Selector ----////
///////////////////////////////////////////////
const topicsSection = document.querySelector('.books');
const editSection = document.querySelector('.edit');
const csrf = document.querySelector('#csrf').getAttribute('data-csrf');
const guestpertId = document.querySelector('#guestpertId').getAttribute('data-guestpertId')
const createBtn = document.querySelector('.btn-createNew');
const form = document.querySelector('.edit__form-container');
const animeBox = document.getElementById('animations');


/********* Edit Form **********/
form.addEventListener('submit', e=>{

    e.preventDefault()
    const imgFile = form.querySelector('#choose-img-file').files[0]

    if(imgFile && imgFile.size > 200*1000){
        message(5000, false, 'Too large image file. Please reduce image file size')
        return
    }

    e.target.submit()
})

//////////////////////////////////////////////////
////---- On clicking create and see btns ----////
////////////////////////////////////////////////
seeShow(topicsSection);
createBtn.addEventListener('click', e=>{
    form.setAttribute('action', `/guestpert/createBooks?_csrf=${csrf}&&guestpertId=${guestpertId}`)
})

/////////////////////////////////////////////
//---- choose image functionality ----//
/////////////////////////////////////////////
const topBox = document.getElementById('inpFile-box');
chooseImg(topBox);


///////////////////////////////////////////////
////---- On clicking delete btn ----////
///////////////////////////////////////////////
for (const delBtn of document.querySelectorAll('.books__card-btn-delete')) {
    delfunc(delBtn)
}

///////////////////////////////////////////////
////---- On clicking cancel btn ----////
///////////////////////////////////////////////
cancelfunc();

///////////////////////////////////
// --- Clicking the edit btn ---//
////////////////////////////////
topicsSection.addEventListener('click', async function(e){

    // Don't do anything if clicked outside
    if(!e.target.classList.contains('books__card-btn-edit')){return};

    // store the button
    const btn = e.target;
    if(!btn){return};

    // store the id
    const id = btn.getAttribute('data-id');
    
    // Render Spinner
    loadLargeSpinner(animeBox)
    
    // // Search
    state.search = new Search(`/guestpert/manageBooks/edit?id=${id}`);
    state.result = await state.search.getResults();

    // If error occours
    if(!state.result){
        // Remove spinner
        removeSpinner(animeBox);
     
        // call error function
        renderError(topicsSection.parentElement);
        
        // stop next
        return
    }

    // // Remove spinner
    removeSpinner(animeBox);

    // Render Edit
    topicsSection.style.display = 'none';
    renderEdit(editSection, state.result.data);

    // Set form url
    form.setAttribute('action', `/guestpert/editBooks?id=${id}&&_csrf=${csrf}&&guestpertId=${guestpertId}`)
});

/////////////////////////////////////////
// --- Clicking the show blogs btn ---//
///////////////////////////////////////

document.querySelector('.edit__show-cards').addEventListener('click', function(){
    
    // Do nothing if not the button
    if(!this.classList.contains('editMode')){return};

    // Remove Fields
    removeFields(editSection);

    // Remove class from button
    this.classList.remove('editMode');
})



///////////////////////////////
// --- tiny mce settings ---//
///////////////////////////////


initTinymce(
    { 
        selector: "#description", 
        menubar: !1, 
        skin: "CUSTOM", 
        content_css: "CUSTOM", 
        plugins: "preview lists fullscreen paste",
        paste_as_text: true, 
        toolbar: "undo redo preview fullscreen bold italic alignjustify"
    }
)


const errMsg = document.querySelector('.message');

window.addEventListener('load', e=>{
    setTimeout(()=>{
        if(!errMsg)return;
        errMsg.style.display='none';
        errMsg.querySelector('.message__txt').textContent='';
    }, 5000)
})

import { logoutFunc } from './lib/autoLogout'
logoutFunc(() => {
    window.location.href = `/login`
})