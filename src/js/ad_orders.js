import "../sass/ad_orders.scss";
import "./view/importsvg";
import { hamburger, slideMenu } from "./view/sidebar";

///////////////////////////////////////////
///--- Hamburger and Sidebar functionality ---///
///////////////////////////////////////////
hamburger();
slideMenu();

const cards = document.querySelectorAll('.orders__card')
const cardNumb = cards.length;

// Show cards function
const pageFunc = showCards(cards, 'flex');

// Calling the class pagination 
new pagination(cardNumb, 12, pageFunc) // cardNumb is total cards, 12 is limit to show , Show Cards is the callback 


import { logoutFunc } from './lib/autoLogout'
logoutFunc(() => {
    window.location.href = `/admin_login`
})