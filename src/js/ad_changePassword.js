import "../sass/ad_changePassword.scss";
import "./view/importsvg";

import { hamburger, slideMenu } from "./view/sidebar";
import { message } from "./view/errorMessage";

hamburger();
slideMenu();


// Get the form
const form = document.querySelector('#form')
const newPassword = form.querySelector('#newPassword')
const cnfrmPassword = form.querySelector('#cnfrmpassword')


form.addEventListener('submit', e=>{

    e.preventDefault()

    if(newPassword.value !== cnfrmPassword.value){
        // display error message
        message(5000, false, 'Passwords doesn\'t match. Please try again')
        return
    }

    form.submit()
})

const errMsg = document.querySelector('.message'); 
window.addEventListener('load', e=>{
    setTimeout(()=>{
        if(!errMsg)return;
        errMsg.style.display='none';
        errMsg.querySelector('.message__txt').textContent='';
    }, 5000)
})

import { logoutFunc } from './lib/autoLogout'
logoutFunc(() => {
    window.location.href = `/admin_login`
})