import "../sass/pr_profileSettings.scss";
import "./view/importsvg";
import axios from 'axios';

import { changeDp } from "./view/changeProfPic";
import { change } from "./view/changeEmailPhone";
import { imgSizeProtection } from './view/imageSizeProtection';
import { message } from './view/errorMessage';

const popup = document.querySelector('.popup');
const emailPopupBox = popup.querySelector('.popup__change-email');
const changeDPBox = popup.querySelector('.popup__change-profile-picture')
const csrf = document.getElementById('csrf').getAttribute('data-csrf')
const credentialsForm = document.querySelector('.edit__form-container--credentials')
const imgInpForm = popup.querySelector('.popup__change-profile-picture')


imgInpForm.addEventListener('submit', e=>{

    e.preventDefault()

    const imgFile = imgInpForm.querySelector('#image-input').files[0]

    if(imgFile && imgFile.size > 500*1000){
        message(5000, false, 'Too large image file. Maximum 500 KB supported')
        return
    }

    e.target.submit()
})

///////////////////////////////////
//---- State of the whole page ----//
///////////////////////////////////
const state = {
    changeEmail: change(popup, emailPopupBox)
}

///////////////////////////////////
//---- Change profile picture----//
///////////////////////////////////

const image = document.querySelector('.edit__guestpert-image-container');

//--- When change btn is clicked ---//
changeDp(image, popup);


///////////////////////////////////
//---- Change Email----//
///////////////////////////////////
(function () {
    const changeEmailBtn = document.getElementById('change-email');
    const cancelbtn = emailPopupBox.querySelector('.btn_cancel');
    const popupMsg = emailPopupBox.querySelector('.popup__msg');
    const popupEmailinp = emailPopupBox.querySelector('.popup__email-input');
    const popupVerificationInp = emailPopupBox.querySelector('.popup__verification-input');
    const KeepChangesBtn = emailPopupBox.querySelector('#changeEmail')

    //--- When change btn is clicked ---//
    changeEmailBtn.addEventListener('click', function () {

        //--- Call the function showUP ---//
        state.changeEmail.showUp();
    })

    //--- When cancel btn is clicked ---//
    cancelbtn.addEventListener('click', (e) => {

        e.preventDefault()
        //--- Call the function cancel ---//
        state.changeEmail.cancel(popupMsg, popupEmailinp, popupVerificationInp);
        KeepChangesBtn.style.display = 'flex';
        popupMsg.style.display = 'none'
    })

    
    popup.addEventListener('click', e => {
        if (!e.target.classList.contains('popup')) return;
        state.changeEmail.cancel(popupMsg, popupEmailinp, popupVerificationInp);
        KeepChangesBtn.style.display = 'flex';
        popupMsg.style.display = 'none'
    })

    emailPopupBox.addEventListener('submit', async e => {

        e.preventDefault()
        const email = popupEmailinp.value;

        try {

            KeepChangesBtn.style.display = 'none';
            
            // Send email to backend for verifying
            await axios(`/producer/editEmail?_csrf=${csrf}`, {
                method: 'POST',
                data: {
                    email
                }
            })
            
            popupMsg.textContent = 'Verification link have beed sent in this email';
            popupMsg.style.display = 'inline-block'
            
        } catch (error) {
            KeepChangesBtn.style.display = 'flex';
            popupMsg.style.display = 'none'
            
            message(5000, false, error.response.data.message?error.response.data.message:'something went wrong. Please try again')
        }
        
    })
    
}())

///////////////////////////////////
//---- Credentials Form Submit----//
///////////////////////////////////

credentialsForm.addEventListener('submit', e=>{
    
    e.preventDefault()
    const curForm = e.target;
    const cnfrmPassword = e.target.querySelector('#cnfrmpassword').value
    const newPassword = e.target.querySelector('#newpassword').value
    
    if(cnfrmPassword !== newPassword){
        message(5000, false, 'Passwords doesn\'t match. Please check again')
        return
    }

    curForm.submit()
})


////////////////////////////////////////////
//---- Submitting ProfilePicture form----//
//////////////////////////////////////////

changeDPBox.addEventListener('submit', e => {
    e.preventDefault()
    const curForm = e.target;
    const tstPass = imgSizeProtection(curForm, [{ id: 'image-input', maxSize: 500000 }])

    if (!tstPass) {
        message(5000, false, 'Too large Image. Please resize the image')
        return
    }

    const imageFile = curForm.querySelector('#image-input').value;

    if (!imageFile) {
        message(5000, false, 'Please attach an image')
        return
    }

    curForm.submit()
})


import { logoutFunc } from './lib/autoLogout'
logoutFunc(() => {
    window.location.href = `/login`
})