import "../sass/ad_homePageBooksCarousel.scss";
import svg from "./view/importsvg";

import { pagination } from "./view/pagination_rest";
import Search from './model/search';
import addCard from './model/addToRequest'
import { renderCards, renderHomePageCards, removeCardsFromHome } from './view/pg-ad_addBookToHome';
import { loadSpinner, removeSpinner, loadLargeSpinner } from './view/spinner';
import { message } from './view/errorMessage';
import { hamburger, slideMenu } from "./view/sidebar";

///////////////////////////////////////////
///--- Hamburger and Sidebar functionality ---///
///////////////////////////////////////////
hamburger();
slideMenu();

///////////////////////////////////////////////
////---- Universal Selectors ----////
///////////////////////////////////////////////
const searchBar = document.querySelector('#search-input');
const hiddenInput = searchBar.parentElement.querySelector('.form__hidden-input');
const homepageBooksSection = document.querySelector('#homepage-books');
const addNewSection = document.querySelector('#addNew-box');
const pageList = document.querySelector('#page__list');
const animation = document.getElementById('animation');
const csrf = document.querySelector('#csrf').getAttribute('data-csrf');



// State of the guestpert Page
const state = {};


///////////////////////////////////////////
////---- Pagination functionality ----////
//////////////////////////////////////////
state.paginationCls = new pagination(24) // 24 is Element per page
init(1); // 1 is the page number

// On clicking other pages
pageList.addEventListener('click', function (e) {
    const target = e.target;
    if (!target.classList.contains('clickable')) { return };

    const pageNum = target.getAttribute('data-num');
    init(pageNum);
});



///////////////////////////////////////////
////---- Search bar functionality ----////
/////////////////////////////////////////
const regEx = /^[\w]{1,60}$/i;

searchBar.addEventListener('keyup', function (e) {

    const chk = ['', 8, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 96, 97, 98, 99, 100, 101, 102, 103, 104, 105, 106, 222, 186]
    const k = chk.indexOf(e.keyCode);

    // When ecape is pressed
    if (e.keyCode === 27) {
        e.preventDefault();
        this.value = '';
        hiddenInput.value = '';
        this.blur();
    };

    // Validation 1
    if (!k || k < 0) { return };

    if (this.value.length === 0) {
        hiddenInput.value = '';
        init(1);
    };

    // Validation 2
    if (this.value.length < 2 || !regEx.test(this.value)) { return };

    // Get the value
    const value = this.value;

    // Put text into hidden input
    hiddenInput.value = value;

    // call init
    init(1);
})


//////////////////////////////
////---- Add To Home ----////
////////////////////////////

addNewSection.addEventListener('click', async (e) => {

    // Prevent from clicking outside
    if (!e.target.classList.contains('add-to-home')) { return };

    // Store the btn
    const btn = e.target

    // Get Id
    state.bookId = btn.getAttribute('data-id');

    // Start spinner
    loadLargeSpinner(animation);

    // Store the card details
    if (btn.closest('.thin').querySelector('.thin__lrg-img img')) {
        state.activeImg = btn.closest('.thin').querySelector('.thin__lrg-img img').getAttribute('src');
    }

    // send rest req to backend
    state.addCard = new addCard(`/admin/addBooksToHome?bookId=${state.bookId}&&_csrf=${csrf}`);
    state.addCardRes = await state.addCard.getResults();

    // Start spinner
    removeSpinner(animation);

    // If error occours
    if (!state.addCardRes) {

        // Render Error message
        const msg = 'Failed to process your request. Try again later';
        state.msg = message(5000, false, msg);

        // Return Error
        return;
    }

    // Show success message
    const msg = 'Your Changes have been made successfully';
    message(5000, true, msg);


    // Render books already on home page
    if (state.activeImg && state.bookId) {
        
        renderHomePageCards(homepageBooksSection, { imgSrc: state.activeImg, id: state.bookId, bookTitle: 'book Image' })
    }

    // Call init 
    state.activePage = state.paginationCls.activePageNum();
    init(state.activePage);
})


///////////////////////////////////
////---- Remove from Home ----////
/////////////////////////////////
homepageBooksSection.addEventListener('click', async (e) => {

    // Prevent from clicking outside
    if (!e.target.classList.contains('remove-from-home')) { return };
    // Save Minimum 4 Requirement
    if (homepageBooksSection.querySelectorAll('.thin').length <= 4) {
        // Render Error message
        const msg = 'Sorry, Minimum 4 cards are required for homepage to work';
        state.msg = message(5000, false, msg);
        
        // Return Error
        return;
    }
    
    // Store the btn
    const btn = e.target
    
    // Get Id
    state.bookId = btn.getAttribute('data-id');
    
    if(!state.bookId){return}

    // Start spinner
    loadLargeSpinner(animation);

    // Store the card details
    state.card = btn.closest('.thin');

    // send rest req to backend
    state.removeCard = new addCard(`/admin/removeBooksFromHome?bookId=${state.bookId}&&_csrf=${csrf}`);
    state.removeCardRes = await state.removeCard.getResults();

    // Remove spinner
    removeSpinner(animation);

    // If error occours
    if (!state.removeCardRes) {

        // Render Error message
        const msg = 'Failed to process your request. Try again later';
        message(5000, false, msg);

        // Return Error
        return;
    }

    // Show success message
    const msg = 'Your Changes have been made successfully';
    message(5000, true, msg);

    // Remove the book from homePage Collection
    if (state.card) {
        removeCardsFromHome(homepageBooksSection, state.card);
    }
})



////////////////////////////
////---- Functions ----////
//////////////////////////

// Show cards function
async function init(pageNum) {

    // Get keywords from input
    let keywords = hiddenInput.value;

    // Prepare UI
    loadSpinner(addNewSection);

    // Get Data from Server
    state.pageSearch = new Search(`/admin/searchBooks?pageNum=${pageNum}&&keywords=${keywords}`);
    state.pageSearchRes = await state.pageSearch.getResults();


    if (!state.pageSearchRes) {
        // Render Error page in UI
        removeSpinner(addNewSection);

        // Render Error message
        const msg = 'Error in fethcing data';
        message(5000, false, msg);

        // Return Error
        return;
    };

    // Prepare for change
    removeSpinner(addNewSection);

    // Make change in UI
    renderCards(addNewSection, state.pageSearchRes.data.dataArr);

    // Pagingation
    state.paginationCls.changeVals(+state.pageSearchRes.data.totalItems);
    state.paginationCls.changePage(+state.pageSearchRes.data.pageNum);
}


import { logoutFunc } from './lib/autoLogout'
logoutFunc(() => {
    window.location.href = `/admin_login`
})