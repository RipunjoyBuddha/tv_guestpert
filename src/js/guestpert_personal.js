import "../sass/guestpert_personal.scss";
import "./view/importsvg";

import { scroll } from "./view/smoothScroll";
import { pagination, showCards } from "./view/pagination";

const scrollBar = document.querySelector('.scroll-bar__wrapper');
const scrollBarDuplicate = document.querySelector('.scroll-bar__duplicate');


/////////////////////////////
////---- Pagination ----////
///////////////////////////

//-- Main Function --//
function mulPagination(sectionCls, cardsCls, limit, cardStyle='block') {
    const section = document.querySelector(`.${sectionCls}`);
    if(!section)return;
    const cards = section.querySelectorAll(`.${cardsCls}`);
    const page = section.querySelector('.page__list');
    const cardNumb = cards.length;
    // console.log(cardStyle)
    // Show cards function
    const pageFunc = showCards(cards, cardStyle);
    
    
    // Calling the class pagination 
    new pagination(cardNumb, limit , pageFunc, page )
}

//-- upcoming Appearance --//
mulPagination('media__upcoming-appearances', 'media__cards', 4);

//-- Prior Appearance --//
mulPagination('media__prior-appearances', 'media__cards', 4);

//-- Hot Topics --//
mulPagination('hot-topics', 'hot-topics__cards', 4, 'flex');

//-- Blogs --//
mulPagination('blog', 'blog__card', 2, 'flex');

//-- Demo Reel --//
mulPagination('demo-reel', 'demo-reel__cards', 4);

//-- Books --//
mulPagination('books', 'books__cards', 4, 'flex');



////////////////////////////////////
////---- Smooth Scroll ----////
////////////////////////////////////

document.querySelectorAll('.scroll-bar__btn').forEach(btn => {
    btn.addEventListener('click', (e) => {
        e.preventDefault();

        const target = btn.getAttribute('href').replace('#', '.');
        scroll(target, 1000, scrollBar.offsetHeight - 1);
    })
})

////////////////////////////////////
////---- Sticky Nav ----////
////////////////////////////////////
let scrollBarTop = scrollBar.offsetTop;

window.addEventListener('resize', () => {
    if (scrollBar.classList.contains('scroll-bar__fixed')) {
        scrollBarTop = scrollBarDuplicate.offsetTop;
    }
    else {
        scrollBarTop = scrollBar.offsetTop;
    }
});

window.addEventListener('scroll', () => {
    if (window.pageYOffset > scrollBarTop) {
        scrollBar.classList.add('scroll-bar__fixed');
        scrollBarDuplicate.style.display = 'block';
    }
    else {
        scrollBar.classList.remove('scroll-bar__fixed');
        scrollBarDuplicate.style.display = 'none';
    }

})


import { logoutFunc } from './lib/autoLogout'
logoutFunc(() => {
    window.location.href = `/login`
})