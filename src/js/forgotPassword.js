import "../sass/forgotPassword.scss";
import "./view/importsvg";

import { domain } from './utils/domain';

import axios from 'axios';

// -------------------
const state = { click: true };

// /////////////////////////////------
// Universal Selectors ---------------

const resetForm = document.querySelector('.forgot__form');
const resetInput = resetForm.querySelector('.forgot__input');
const forgotMsg = document.querySelector('.forgot__msg');
const csrf = document.querySelector('#csrf').getAttribute('data-csrf');




// When resetForm is submitted
resetForm.addEventListener('submit', async (e) => {

    e.preventDefault();

    if (state.click === false || resetInput.value === '') return;

    // Send the form
    const resetValue = resetInput.value;

    // Prevent from clicking
    state.click = false;
    try {
        await axios.post(`${domain}/resetPasswordRequest?_csrf=${csrf}`, {
            username: resetValue
        })
    } catch (error) {
        state.click = true
        return
    }

    forgotMsg.textContent = 'A reset link have been sent in the respective email address. The link is valid only for 10 minutes';
    forgotMsg.style.display = 'block';
    resetForm.style.display = 'none';
})


const logoutBtn = document.querySelector('.header-bar__link-logout')
import { logoutFunc } from './lib/autoLogout'
if(logoutBtn){
    logoutFunc(() => {
        const btnId = logoutBtn.getAttribute('data-id')
        if(btnId=='adtyspnce'){
            window.location.href = `/admin_login`
        }else{
            window.location.href = `/login`
        }
    })
} 