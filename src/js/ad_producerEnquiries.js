import "../sass/ad_producerEnquiries.scss";
import "./view/importsvg";

import Search from './model/search';
import send from './model/addToRequest'
import { ratingStar } from './view/star-rating'
import { prepareForLoad, removeSpinner } from './view/spinner';
import { pagination } from './view/pagination_rest';
import { renderCards, renderEnquiryCards, renderEdit, clearForm } from './view/pg-ad_enquiries';
import { message } from './view/errorMessage';
import { hamburger, slideMenu } from "./view/sidebar";

const state = {}

/////////////////////////////////////////////////
///--- Hamburger and Sidebar functionality ---///
////////////////////////////////////////////////
hamburger();
slideMenu();

///////////////////////////////////////////////
////---- Universal Selectors ----////
///////////////////////////////////////////////
const searchBar = document.querySelector('#search-input');
const smileyBox = document.querySelector('.smileybox');
const guestpertBox = document.querySelector('#guestpert-box');
const enquiriesBox = document.querySelector('.allEnquiries');
const pageListGuestperts = document.querySelector('#page__list-guestperts');
const pageListEnquiries = document.querySelector('#page__list-enquiries');
const animation = document.getElementById('animation');
const createBtn = document.querySelector('.btn-createNew');
const backBtnToEnquiries = document.querySelector('.btn-backToEnquiries')
const backBtnToGuestpert = document.querySelector('.btn-backToGuestperts')


const guestpertSection = document.querySelector('.guestperts')
const allEnquiriesSection = document.querySelector('.enquiries-section')
const editSection = document.querySelector('.edit');

const csrf = document.querySelector('#csrf').getAttribute('data-csrf')

///////////////////////////////////////////
////---- Pagination functionality ----////
//////////////////////////////////////////
state.paginationCls_1 = new pagination(24, pageListGuestperts) // 24 is Element per page
state.paginationCls_2 = new pagination(12, pageListEnquiries) // 12 is Element per page

// On clicking other pages
pageListGuestperts.addEventListener('click', function (e) {
    const target = e.target;
    if (!target.classList.contains('clickable')) { return };

    const pageNum = target.getAttribute('data-num');
    init(pageNum);
});

pageListEnquiries.addEventListener('click', function (e) {
    const target = e.target;
    if (!target.classList.contains('clickable')) { return };

    const pageNum = target.getAttribute('data-num');
    getEnquiries(pageNum);
});


////////////////////////////////////////////
////---- Clicking Guestperts Cards ----////
//////////////////////////////////////////
guestpertBox.addEventListener('click', async e => {

    if (!e.target.closest('.guestpert')) return

    const guestpertId = e.target.closest('.guestpert').getAttribute('data-id')
    if (!guestpertId) return;

    state.selectedGuestpert = guestpertId;

    guestpertSection.classList.add('u-display-none')
    allEnquiriesSection.classList.remove('u-display-none')

    getEnquiries(1)
})


createBtn.addEventListener('click', e => {

    const id = state.selectedGuestpert;
    allEnquiriesSection.classList.add('u-display-none')

    editSection.style.display = 'block'
    editSection.querySelector('.form__submit-btn').textContent = 'send inquiry'
    clearForm(editSection)
    editSection.querySelector('.edit__form-container--enquiry').setAttribute('action', `/admin/createEnquiry?guestpertId=${id}&&_csrf=${csrf}`)
})


//////////////////////////////////////////////////
////---- Clicking EditEnquiries Button ----////
////////////////////////////////////////////////
allEnquiriesSection.addEventListener('click', e=>{

    if(!e.target.classList.contains('edit_enquiry'))return
    
    const id = e.target.getAttribute('data-id')
    allEnquiriesSection.classList.add('u-display-none')
    
    const currentEnquiry = state.enquirySearchRes.data.dataArr.filter(c=>c._id===id)[0]
    editSection.querySelector('.form__submit-btn').textContent = 'save inquiry'
    
    renderEdit(currentEnquiry, editSection)

    editSection.style.display = 'block'
    editSection.querySelector('.edit__form-container--enquiry').setAttribute('action', `/admin/editEnquiry?guestpertId=${id}&&_csrf=${csrf}`)
})


//////////////////////////////////////////////////
////---- Clicking SendToGuestpert Button ----////
////////////////////////////////////////////////
allEnquiriesSection.addEventListener('click', async e=>{

    if(!e.target.classList.contains('guestpertView'))return

    const id = e.target.getAttribute('data-id')
    
    const x = new send(`/admin/changeGuestpertView?_csrf=${csrf}&&guestpertId=${id}`)
    const result = await x.getResults()
    
    if (!result) {
        // Render Error message
        const msg = 'Something went wrong. Please try again later';
        message(5000, false, msg);

        // Return Error
        return;
    };

    message(5000, true, 'Successfully changes are applied')
    e.target.textContent = result.data.textContent
})

////////////////////////////////////////
////---- Clicking Back Buttons ----////
//////////////////////////////////////

backBtnToGuestpert.addEventListener('click', e => {
    guestpertSection.classList.remove('u-display-none')
    allEnquiriesSection.classList.add('u-display-none')

    pageListEnquiries.style.display = 'none'
    enquiriesBox.innerHTML=''
    init(1)

    clearForm(editSection)
})

backBtnToEnquiries.addEventListener('click', () => {
    allEnquiriesSection.classList.remove('u-display-none')
    editSection.style.display = 'none'
})


///////////////////////////////////
////---- Rating of stars ----////
///////////////////////////////////
smileyBox.addEventListener('click', e => {

    if (e.target.classList.contains('rated') || e.target.classList.contains('str-inp')) {

        let input = e.target
        if (e.target.classList.contains('rated')) {
            input = e.target.querySelector('input')
        }
        ratingStar(input)
    }

})



/////////////////////////////////////////
////---- Search bar functionality ----////
/////////////////////////////////////////
const regEx = /^[\w]{1,60}$/i;

searchBar.addEventListener('keyup', function (e) {

    const chk = ['', 8, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 96, 97, 98, 99, 100, 101, 102, 103, 104, 105, 106, 222, 186]
    const k = chk.indexOf(e.keyCode);

    // When ecape is pressed
    if (e.keyCode === 27) {
        e.preventDefault();
        this.value = '';
        searchBar.value = '';
        this.blur();
    };

    // Validation 1
    if (!k || k < 0) { return };

    if (this.value.length === 0) {
        searchBar.value = '';
        init(1);
    };

    // Validation 2
    if (this.value.length < 2 || !regEx.test(this.value)) { return };

    // Get the value
    const value = this.value;

    // Put text into hidden input
    searchBar.value = value;

    // call init
    init(1);
})


////////////////////////////////////////
////---- Get Enquiries function ----////
//////////////////////////////////////
async function getEnquiries(pageNum) {

    let guestpertId = state.selectedGuestpert
    if (!guestpertId || !pageNum) {
        guestpertSection.classList.remove('u-display-none')
        allEnquiriesSection.classList.add('u-display-none')

        clearForm(editSection)
        return
    }

    // Prepare UI
    prepareForLoad(enquiriesBox, animation);

    // Get Data from Server
    const enquirySearch = new Search(`/admin/getEnquiries?guestpertId=${guestpertId}&&pageNum=${pageNum}`);
    state.enquirySearchRes = await enquirySearch.getResults();

    // Remove Spinner
    removeSpinner(animation);

    if (!state.enquirySearchRes) {

        // Render Error message
        const msg = 'Error in fethcing data';
        message(5000, false, msg);

        guestpertSection.classList.remove('u-display-none')
        allEnquiriesSection.classList.add('u-display-none')

        clearForm(editSection)

        // Return Error
        return;
    };

    renderEnquiryCards(enquiriesBox, state.enquirySearchRes.data.dataArr)
    enquiriesBox.style.display='block'

    // Pagingation
    state.paginationCls_2.changeVals(+state.enquirySearchRes.data.totalItems);
    state.paginationCls_2.changePage(+state.enquirySearchRes.data.pageNum);
}


///////////////////////////////
////---- init function ----////
///////////////////////////////

// Show cards function
async function init(pageNum) {

    // Get keywords from input
    let keywords = searchBar.value;

    // Prepare UI
    prepareForLoad(guestpertBox, animation);

    // Get Data from Server
    state.pageSearch = new Search(`/admin/enquiriesWithGuestperts?pageNum=${pageNum}&&keywords=${keywords}`);
    state.pageSearchRes = await state.pageSearch.getResults();

    // Remove Spinner
    removeSpinner(animation);

    // Display guestperts cards
    guestpertBox.style.display = 'flex';

    if (!state.pageSearchRes) {

        // Render Error message
        const msg = 'Error in fethcing data';
        message(5000, false, msg);

        // Return Error
        return;
    };

    // // Make change in UI
    renderCards(guestpertBox, state.pageSearchRes.data.dataArr);

    // Pagingation
    console.log(state.pageSearchRes)
    state.paginationCls_1.changeVals(+state.pageSearchRes.data.totalItems);
    state.paginationCls_1.changePage(+state.pageSearchRes.data.pageNum);
}
init(1)


const errMsg = document.querySelector('.message');
window.addEventListener('load', e => {
    setTimeout(() => {
        if (!errMsg) return;
        errMsg.style.display = 'none';
        errMsg.querySelector('.message__txt').textContent = '';
    }, 5000)
})



import { logoutFunc } from './lib/autoLogout'
logoutFunc(() => {
    window.location.href = `/admin_login`
})