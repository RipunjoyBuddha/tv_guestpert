// import tinymce from './tinymce'; // Comment it out during building

export const initTinymce = obj=> {
    return tinymce.init(obj)
};


export const setContent = (textareaId, content='')=>{
    return tinymce.get(textareaId).setContent(content)
}

export const getContent = (textareaId)=>{
    return tinymce.get(textareaId).getContent();
}