import "../sass/ga_demoReel.scss"; 
import "./view/importsvg" ;

import { delfunc, cancelfunc } from "./view/deleteBox";
import {seeShow} from "./view/seeShowbtns";
import {editGuestpertReel} from "./view/editField";


/////////////////////////////////////
////---- Universal Selector ----////
///////////////////////////////////
const topicsSection = document.querySelector('.reel');
const editSection = document.querySelector('.edit');
const createBtn = document.querySelector('.btn-createNew');
const form = document.querySelector('.edit__form-container');
const csrf = document.querySelector('#csrf').getAttribute('data-csrf');
const guestpertId = document.querySelector('#guestpertId').getAttribute('data-guestpertId')

///////////////////////////////////////////////
////---- On clicking create and see btns ----////
///////////////////////////////////////////////
seeShow(topicsSection);
createBtn.addEventListener('click', e=>{
    form.setAttribute('action', `/guestpert/createDemoReel?_csrf=${csrf}&&guestpertId=${guestpertId}`)
})

///////////////////////////////////////////////
////---- On clicking delete btn ----////
///////////////////////////////////////////////
for (const delBtn of document.querySelectorAll('.reel__card-btn-delete')) {
    delfunc(delBtn);
}

///////////////////////////////////////////////
////---- On clicking cancel btn ----////
///////////////////////////////////////////////
cancelfunc();

/////////////////////////////////////
////---- Edit Functionality ----////
///////////////////////////////////
editGuestpertReel('reel__card-btn-edit', editSection, topicsSection, csrf, guestpertId)

import { logoutFunc } from './lib/autoLogout'
logoutFunc(() => {
    window.location.href = `/login`
})