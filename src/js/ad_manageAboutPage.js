import "../sass/ad_manageAboutPage.scss";
import "./view/importsvg";

import {initTinymce} from './utils/tinyActions';

import {seeShow_adv, showBtnClick} from "./view/seeShowbtns";
import {message} from "./view/errorMessage";
import {changeDp_withoutPopup} from "./view/changeProfPic";
import {delfuncRest, cancelfunc, removeDelBox} from "./view/deleteBox";
import remove from "./model/deleteCard";

import { hamburger, slideMenu } from "./view/sidebar";

//////////////////////////////////////////////////
///--- Hamburger and Sidebar functionality ---///
////////////////////////////////////////////////
hamburger();
slideMenu();

// State of the Page
const state = {};

//////////////////////////////////////
////---- Universal Selectors ----////
////////////////////////////////////
const topicsSection = document.getElementById('about-section');
const editSectionPrime = document.getElementById('editPrimeMembers');
const editFormPrime = editSectionPrime.querySelector('.edit__form-container');
const editSectionOthers = document.getElementById('editOtherMembers');
const editFormOthers = editSectionOthers.querySelector('.edit__form-container');
const createBtns = topicsSection.querySelectorAll('.btn-createNew');
const backBtns = document.querySelectorAll('.btn__show-cards');
const animeBox = document.getElementById('animations');
const popup = document.querySelector('.popup');
const csrf = document.querySelector('#csrf').getAttribute('data-csrf');


/**************************************/
// Primary team Large Image protection //

editFormPrime.addEventListener('submit', e=>{
    e.preventDefault()
    const imgInp = editFormPrime.querySelector('#img-input').files[0]

    if(imgInp && imgInp.size > 500*1000){
        message(5000, false, 'Too large image. Please reduce image size')
        return
    }
    e.target.submit()
})


/////////////////////////////////////////////// 
////---- On clicking create and see btns ----////
///////////////////////////////////////////////
for (const btn of createBtns){
    
    // Add event listener click
    btn.addEventListener('click', function(){
        
        // Store the btn type
        const type = this.getAttribute('data-type');

        if(type === 'prime'){
            seeShow_adv([topicsSection], editSectionPrime);
            editFormPrime.setAttribute('action', `/admin/createTeam/primary?_csrf=${csrf}`)
        }
        else if(type === 'others'){
            seeShow_adv([topicsSection], editSectionOthers);
            editFormOthers.setAttribute('action', `/admin/createTeam/others?_csrf=${csrf}`)
        }
        else{return}
    })
}

///////////////////////////////////////////////
////---- Change Profile picture in form ----////
///////////////////////////////////////////////
changeDp_withoutPopup(editSectionPrime);



/////////////////////////////////////////
////---- On clicking delete btn ----////
///////////////////////////////////////
topicsSection.addEventListener('click', (e)=>{
    if(e.target.classList.contains('faty__card-btn-delete')){

        // Store the card
        state.activeCard = e.target.closest('.faty__card')

        // Call del func
        delfuncRest(e.target);
    }
})

/////////////////////////////////////////
////---- On clicking cancel btn ----////
///////////////////////////////////////
cancelfunc();


///////////////////////////////////////////////
///////////////////////////////////////////////
////---- Rest Features ----//// ----------
///////////////////////////////////////////////
///////////////////////////////////////////////

import Search from './model/search';
import { prepareForLoad, removeSpinner } from './view/spinner';
import { renderEdit, removeFields} from './view/pg-ad_manageAboutPage';




// Store default guestpert image
state.defaultImg = editSectionPrime.querySelector('#preview-img').getAttribute('src');


topicsSection.addEventListener('click', async (e) => {

    // Preventing from clicking outside
    if (!e.target.classList.contains('faty__card-btn-edit')) { return };

    // store the button
    const btn = e.target;
    if (!btn) { return };

    // store the id
    const id = btn.getAttribute('data-id');

    // Render Spinner
    prepareForLoad(topicsSection, animeBox);

    // Search
    state.search = new Search(`/admin/manageAbout/edit?id=${id}`);
    state.result = await state.search.getResults();
    
    // Remove spinner
    removeSpinner(animeBox);

    // If error occours
    if (!state.result) {

        // display topics Section
        topicsSection.style.display='flex';
        
        // display error message
        message(5000, false, 'Your request could not be completed. Please try again later.')
        // stop next
        return
    }
    
    // Render Edit
    if(state.result.data.memberType === 'prime'){
        renderEdit(state.result.data, editSectionPrime);
        editSectionPrime.querySelector('.btn__show-cards').setAttribute('data-mode', 'edit');
        editFormPrime.setAttribute('action', `/admin/editTeam/primary/${state.result.data.id}?edit=true&&_csrf=${csrf}`)

    }else if(state.result.data.memberType === 'others'){
        
        renderEdit(state.result.data, editSectionOthers);
        editSectionOthers.querySelector('.btn__show-cards').setAttribute('data-mode', 'edit');
        editFormOthers.setAttribute('action', `/admin/editTeam/others/${state.result.data.id}?edit=true&&_csrf=${csrf}`)
    }
    else{
        topicsSection.style.display='flex';
    }
})



/////////////////////////////////////////////
// --- Clicking the show Teams btn ---//
///////////////////////////////////////////

for( const btn of backBtns){
    
    // Add click event Listenter
    btn.addEventListener('click', function(){

        const editSection =  this.parentElement.parentElement;
        
        // Do nothing if not the button
        if (this.getAttribute('data-mode')!=='edit') { 

            // Appear of all main section
            showBtnClick([topicsSection], editSection);

            return 
        };

        // Current edit section

        // Remove Fields
        removeFields(editSection, state.defaultImg);

        // Appear of all main section
        showBtnClick([topicsSection], editSection);

        // Remove mode from button
        this.setAttribute('data-mode', '');
    })
}


////////////////////////////////////
// --- Clicking the remove btn ---//
////////////////////////////////////
popup.addEventListener('click', async (e)=>{
    
    // Avoid from clicking outside 
    if(!e.target.classList.contains('popup__del-btn'))return;

    
    // Store the id
    state.delId = e.target.getAttribute('data-id');
    

    // Disappear popup
    removeDelBox();

    // Render Spinner
    prepareForLoad(topicsSection, animeBox);

    // Del request to server
    state.remove = new remove(`/admin/manageAbout/deleteMember?id=${state.delId}&&_csrf=${csrf}`);
    state.result = await state.remove.getResults();

    // Remove spinner
    removeSpinner(animeBox);

    // If error occours
    if (!state.result) {

        topicsSection.style.display='flex';
        
        // display error message
        message(5000, false, 'Your request could not be completed. Please try again later.')
        
        // stop next
        return
    }
    
    // Remove the card from topicSection
    state.activeCard.parentElement.removeChild(state.activeCard);

    // disappear the state.activeCard
    state.activeCard = null;
    
    // Display topicSection
    topicsSection.style.display='flex';
})



///////////////////////////////
// --- tiny mce settings ---//
///////////////////////////////


initTinymce(
    { 
        selector: "#description", 
        menubar: !1, 
        skin: "CUSTOM", 
        content_css: "CUSTOM", 
        plugins: "code preview paste lists fullscreen", 
        paste_as_text: true,
        toolbar: "undo redo preview fullscreen bold italic alignjustify"
    }
)


import { logoutFunc } from './lib/autoLogout'
logoutFunc(() => {
    window.location.href = `/admin_login`
})