import "../sass/ga_enquiries.scss"; 
import "./view/importsvg" ;

import Search from './model/search';
import { prepareForLoad, removeSpinner} from './view/spinner';
import { pagination } from './view/pagination_rest';
import { renderEnquiryCards} from './view/pg-ga_enquiries';
import { message } from './view/errorMessage';

const state = {}

const animation = document.getElementById('animation');
const enquiriesBox = document.querySelector('.allEnquiries');
const pageListEnquiries = document.querySelector('#page__list-enquiries');

///////////////////////////////////////////
////---- Pagination functionality ----////
//////////////////////////////////////////
state.paginationCls_2 = new pagination(12, pageListEnquiries) // 12 is Element per page

pageListEnquiries.addEventListener('click', function (e) {
    const target = e.target;
    if (!target.classList.contains('clickable')) { return };

    const pageNum = target.getAttribute('data-num');
    getEnquiries(pageNum);
});


////////////////////////////////////////
////---- Get Enquiries function ----////
//////////////////////////////////////
async function getEnquiries(pageNum) {

    // Prepare UI
    prepareForLoad(enquiriesBox, animation);

    // Get Data from Server
    const enquirySearch = new Search(`/guestpert/searchEnquiries?pageNum=${pageNum}`);
    state.enquirySearchRes = await enquirySearch.getResults();

    // Remove Spinner
    removeSpinner(animation);

    if (!state.enquirySearchRes) {

        // Render Error message
        const msg = 'Error in fethcing data';
        message(5000, false, msg);

        // Return Error
        return;
    };

    renderEnquiryCards(enquiriesBox, state.enquirySearchRes.data.dataArr)
    enquiriesBox.style.display='block'

    // Pagingation
    state.paginationCls_2.changeVals(+state.enquirySearchRes.data.totalItems);
    state.paginationCls_2.changePage(+state.enquirySearchRes.data.pageNum);

}
getEnquiries(1)

import { logoutFunc } from './lib/autoLogout'
logoutFunc(() => {
    window.location.href = `/login`
})