import "../sass/ad_homePageHotTopics.scss";
import "./view/importsvg";
import { hamburger, slideMenu } from "./view/sidebar";

///////////////////////////////////////////
///--- Hamburger and Sidebar functionality ---///
///////////////////////////////////////////
hamburger();
slideMenu();


import { logoutFunc } from './lib/autoLogout'
logoutFunc(() => {
    window.location.href = `/admin_login`
})