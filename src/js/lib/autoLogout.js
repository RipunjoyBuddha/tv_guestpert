import axios from 'axios'

import {domain} from '../utils/domain'

const endTime = 1000*60*10

const callLogout = async (cb = ()=>{})=>{
    
    try {
        await axios(`${domain}/logout_rest`)
        cb()
    } catch (error) {
    }
}

export const logoutFunc = (cb) => {

    let timer = setInterval( ()=>{callLogout(cb)}, endTime)

    const resetTimer = (e) => {
        clearInterval(timer)
        timer = setInterval( ()=>{callLogout(cb)}, endTime)
    }

    window.addEventListener('mousemove', resetTimer)
    window.addEventListener('click', resetTimer)
    window.addEventListener('scroll', resetTimer)
    window.addEventListener('keydown', resetTimer)
}