import "../sass/ad_subAdmin.scss";
import "./view/importsvg";
import {seeShow} from "./view/seeShowbtns";
import {moreDetailsSubAdmin, closeMoreDetail} from "./view/moreDetailsPopup";
import {editSubAdmin} from "./view/editField";
import {chooseImg} from "./view/choose_img";
import {delfunc, cancelfunc} from "./view/deleteBox";
import { hamburger, slideMenu } from "./view/sidebar";
import {message} from './view/errorMessage';

///////////////////////////////////////////
///--- Hamburger and Sidebar functionality ---///
///////////////////////////////////////////
hamburger();
slideMenu();


///////////////////////////////////////////////
////---- Universal selectors ----////
///////////////////////////////////////////////
const topicsSection = document.querySelector('.subAdmins');
const editsection = document.querySelector('#editSubAdmin');
const createSection = document.querySelector('#createSubAdmin');
const csrf = document.querySelector('#csrf').getAttribute('data-csrf');
const createForm = createSection.querySelector('.edit__form-container')
const editBasicsForm = editsection.querySelector('.edit__form-basics')


/*****************************/
// Form Submit Image protection
/*****************************/

createForm.addEventListener('submit', e=>{

    e.preventDefault()
    const imgInp = createForm.querySelector('#create_choose-img-file').files[0]

    if(!imgInp){
        message(5000, false, 'Please provide an image')
        return
    }
    
    if(imgInp && imgInp.size>1000*1000){
        message(5000, false, 'Too large Image file, Please reduce it.')
        return
    }
    
    createForm.submit()
})

editBasicsForm.addEventListener('submit', e=>{
    
    e.preventDefault()
    const imgInp = editBasicsForm.querySelector('#edit_choose-img-file').files[0]

    if(imgInp && imgInp.size>1000*1000){
        message(5000, false, 'Too large Image file, Please reduce it.')
        return
    }
    editBasicsForm.submit()
})

///////////////////////////////////////////////
////---- On clicking create and see btns ----////
///////////////////////////////////////////////
const createsection = document.querySelector('#createSubAdmin');
seeShow(topicsSection, createsection, editsection);



////////////////////////////////////////////
////---- On clicking More details ----////
//////////////////////////////////////////
const moreDetails_btns = document.querySelectorAll('.users__card-btn-details');
moreDetailsSubAdmin(moreDetails_btns);
closeMoreDetail();



////////////////////////////////////////////
////---- On clicking Edit btn ----////
//////////////////////////////////////////
const edit_btns = document.querySelectorAll('.users__card-btn-edit');
editSubAdmin(edit_btns, editsection, topicsSection, csrf);



////////////////////////////////////////////
////---- Deleting the Admin ----////
//////////////////////////////////////////
const delBtn = editsection.querySelector('#delete-subAdmin-btn');
delfunc(delBtn);
cancelfunc();


////////////////////////////////////////////
////---- Choose Image Functionality ----////
//////////////////////////////////////////
const createInputFileBox = createSection.querySelector('#create_inpFile-box');
const createInpImg = createInputFileBox.querySelector('#create_choose-img-file');

const editInputFileBox = editsection.querySelector('#edit_inpFile-box');
const editInpImg = editInputFileBox.querySelector('#edit_choose-img-file');


chooseImg(createInputFileBox, createInpImg);
chooseImg(editInputFileBox, editInpImg);


const errMsg = document.querySelector('.message');

window.addEventListener('load', e=>{
    setTimeout(()=>{
        if(!errMsg)return;
        errMsg.style.display='none';
        errMsg.querySelector('.message__txt').textContent='';
    }, 5000)
})



import { logoutFunc } from './lib/autoLogout'
logoutFunc(() => {
    window.location.href = `/admin_login`
})