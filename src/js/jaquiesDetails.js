import "../sass/jaquiesDetails.scss";
import "./view/importsvg";

import { jaquiesAppearance } from "./view/glide";
import { pagination, showCards } from "./view/pagination";

const toScroll = document.querySelector('#scroll').getAttribute('data-scroll');

jaquiesAppearance();
///////////////////////////////////////////////
////---- Pagination functionality ----////
///////////////////////////////////////////////
const cards = document.querySelectorAll('.blog__card')
const cardNumb = cards.length;


// Show cards function
const pageFunc = showCards(cards);

// Calling the class pagination 
new pagination(cardNumb, 2, pageFunc) // cardNumb is total cards, 2 is limit to show , Show Cards is the callback 

window.addEventListener('load', ()=>{
    if (toScroll === 'true') {
        setTimeout(() => {
            const scrollamount = document.querySelector('.blog').offsetTop;
            window.scrollTo(0, scrollamount-50);
        }, 500);
    }
})

const logoutBtn = document.querySelector('.header-bar__link-logout')
import { logoutFunc } from './lib/autoLogout'
if(logoutBtn){
    logoutFunc(() => {
        const btnId = logoutBtn.getAttribute('data-id')
        if(btnId=='adtyspnce'){
            window.location.href = `/admin_login`
        }else{
            window.location.href = `/login`
        }
    })
} 