import "../sass/ad_manageMembership.scss";
import "./view/importsvg";
import "./vendor/form-dropdown2";

import {seeShow} from "./view/seeShowbtns";
import {editMembership} from "./view/editField";
import { hamburger, slideMenu } from "./view/sidebar";

///////////////////////////////////////////
///--- Hamburger and Sidebar functionality ---///
///////////////////////////////////////////
hamburger();
slideMenu();
//////////////////////////////////
///--- Universal Selectors ---///
////////////////////////////////
const topicSection = document.querySelector('.membership');
const createFeature = document.querySelector('#createFeature');
const editFeature = document.querySelector('#editFeature');
const editMembershipSection = document.querySelector('#editMembership');
const btnEditMembership = document.querySelectorAll('.btn-editMembership');
const csrf = document.querySelector('#csrf').getAttribute('data-csrf');


seeShow(topicSection, createFeature, editFeature);

editMembership(btnEditMembership, editMembershipSection, topicSection, csrf);

const errMsg = document.querySelector('.message');
window.addEventListener('load', e=>{
    setTimeout(()=>{
        if(!errMsg)return;
        errMsg.style.display='none';
        errMsg.querySelector('.message__txt').textContent='';
    }, 5000)
})


import { logoutFunc } from './lib/autoLogout'
logoutFunc(() => {
    window.location.href = `/admin_login`
})