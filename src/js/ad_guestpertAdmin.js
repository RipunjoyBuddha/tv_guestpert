import "../sass/ad_guestpertAdmin.scss";
import "./view/importsvg";
import { hamburger, slideMenu } from "./view/sidebar";

///////////////////////////////////////////
///--- Hamburger and Sidebar functionality ---///
///////////////////////////////////////////
hamburger();
slideMenu();

const errMsg = document.querySelector('.message');

window.addEventListener('load', e=>{
    setTimeout(()=>{
        if(!errMsg)return;
        errMsg.style.display='none';
        errMsg.querySelector('.message__txt').textContent='';
    }, 5000)
})


import { logoutFunc } from './lib/autoLogout'
logoutFunc(() => {
    window.location.href = `/admin_login`
})