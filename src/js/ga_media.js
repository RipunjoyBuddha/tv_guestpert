import "../sass/ga_media.scss"; 
import "./view/importsvg" ;

import {delfunc, cancelfunc} from "./view/deleteBox";
import { seeShow } from "./view/seeShowbtns";
import {tagsFunc} from "./view/tags";
import {autoComplete} from './view/autoCompleteTags';


///////////////////////////////////////////////
////---- Universal Selectors ----////
///////////////////////////////////////////////
const editSection = document.querySelector('.edit');
const topicsSection = document.querySelector('.media');
const animeBox = document.getElementById('animations');
const csrf = document.querySelector('#csrf').getAttribute('data-csrf');
const guestpertId = document.querySelector('#guestpertId').getAttribute('data-guestpertId')
const createBtn = document.querySelector('.btn-createNew');
const form = document.querySelector('.edit__form-container');


///////////////////////////////////////////////
////---- On clicking create and see btns ----////
///////////////////////////////////////////////
seeShow(topicsSection);
createBtn.addEventListener('click', e=>{
    form.setAttribute('action', `/guestpert/createMedia?_csrf=${csrf}&&guestpertId=${guestpertId}`)
})

///////////////////////////////////////////////
////---- Tags Functionality ----////
///////////////////////////////////////////////
tagsFunc(editSection);


///////////////////////////////////////////////
////---- On clicking delete btn ----////
///////////////////////////////////////////////
for (const delBtn of document.querySelectorAll('.media__card-btn-delete')) {
    delfunc(delBtn)
}

///////////////////////////////////////////////
////---- On clicking cancel btn ----////
///////////////////////////////////////////////
cancelfunc();

autoComplete()


////////////////////////////////////////
///////////////////////////////////////
/////---- REST Features ----///////////
//////////////////////////////////////
//////////////////////////////////////

import Search from './model/search';
import {renderTagsUI} from './view/rest-autocomplete';
import { prepareForLoad, removeSpinner } from './view/spinner';
import { renderEdit, renderError, removeFields } from './view/pg-ga_manageMedia';

// State of the home Page
const state = {};

// Clicking on Edit Button
topicsSection.addEventListener('click', async (e) => {

    // Preventing from clicking outside
    if (!e.target.classList.contains('media__card-btn-edit')) { return };

    // store the button
    const btn = e.target;
    if (!btn) { return };

    // store the id
    const id = btn.getAttribute('data-id');

    // Render Spinner
    prepareForLoad(topicsSection, animeBox);

    // Search
    state.search = new Search(`/guestpert/manageMedia/edit?id=${id}`);
    state.result = await state.search.getResults();

    // If error occours
    if (!state.result) {
        // Remove spinner
        removeSpinner(animeBox);

        // call error function
        renderError(topicsSection);

        // stop next
        return
    }

    // Remove spinner
    removeSpinner(animeBox);

    // Render Edit
    renderEdit(state.result.data);

    form.setAttribute('action', `/guestpert/editMedia?id=${id}&&_csrf=${csrf}&&guestpertId=${guestpertId}`)
});


/////////////////////////////////////////////
// --- Clicking the show HotTopics btn ---//
///////////////////////////////////////////
document.querySelector('.edit__show-cards').addEventListener('click', function () {

    // Do nothing if not the button
    if (!this.classList.contains('editMode')) { return };

    // Remove Fields
    removeFields();

    // Remove class from button
    this.classList.remove('editMode');
});



///////////////////////////
// --- AutoComplete ---//
/////////////////////////

const regEx = /^[\w]{1,60}$/i;

document.querySelector('.tags-input__main-input').addEventListener('keyup', async function (e) {

    // Return if not available
    if (!document.activeElement.classList.contains('tags-input__main-input') || this.value.length < 2) { return };
    
    const chk = ['', 8, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 96, 97, 98, 99, 100, 101, 102, 103, 104, 105, 106, 222, 186]
    const k = chk.indexOf(e.keyCode);
    
    if (k < 0 || !k) { return };
    
    // RegExp
    if (!regEx.test(this.value)) { return };
    
    // Get Keyword
    const keyword = this.value;
    
    // Search
    state.autoComplete = new Search(`/guestpert/search/tagsList?tag=${keyword}`);
    state.autoCompleteResult = await state.autoComplete.getResults();
    
    if (!state.autoCompleteResult) { return };
    
    // Render in UI
    renderTagsUI(state.autoCompleteResult.data, this);
})


const errMsg = document.querySelector('.message');

window.addEventListener('load', e=>{
    setTimeout(()=>{
        if(!errMsg)return;
        errMsg.style.display='none';
        errMsg.querySelector('.message__txt').textContent='';
    }, 5000)
})

import { logoutFunc } from './lib/autoLogout'
logoutFunc(() => {
    window.location.href = `/login`
})