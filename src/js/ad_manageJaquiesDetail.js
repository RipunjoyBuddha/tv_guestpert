import "../sass/ad_manageJaquiesDetail.scss";
import "./view/importsvg";

import { initTinymce } from './utils/tinyActions';
import { hamburger, slideMenu } from "./view/sidebar";
import uniqid from 'uniqid'

///////////////////////////////////////////
///--- Hamburger and Sidebar functionality ---///
///////////////////////////////////////////
hamburger();
slideMenu();

/////////////////////////////////////
//--- Universal Selectors ---//
/////////////////////////////////////
const csrf = document.querySelector('#csrf').getAttribute('data-csrf');
const id = uniqid();


document.querySelector('.edit__form-container').setAttribute('action', `/admin/founderDetails?_csrf=${csrf}&&tempId=${id}`)


////////////////////////
//--- Init TinyMce ---//
////////////////////////


initTinymce(
    {
        selector: `#content`,
        menubar: !1,
        skin: "CUSTOM",
        content_css: "CUSTOM",
        plugins: "preview image lists fullscreen paste",
        paste_as_text: true,
        toolbar: "undo redo image preview fullscreen bold italic alignjustify",
        advlist_bullet_styles: "disc",
        images_upload_url: `/admin/imageUpload/founderDetails?tempId=${id}&&_csrf=${csrf}`,
        image_dimensions: true,
    }
)



const errMsg = document.querySelector('.message');
window.addEventListener('load', e => {
    setTimeout(() => {
        if (!errMsg) return;
        errMsg.style.display = 'none';
        errMsg.querySelector('.message__txt').textContent = '';
    }, 5000)
})


import { logoutFunc } from './lib/autoLogout'
logoutFunc(() => {
    window.location.href = `/admin_login`
})