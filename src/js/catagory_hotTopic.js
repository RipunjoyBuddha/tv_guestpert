import "../sass/catagory_hotTopic.scss";
import "./view/importsvg";

import {filterHotTopic} from "./view/customFilter";

/////////////////////////////////////
// --- Universal Selectors ---//
/////////////////////////////////////
const menuBox = document.querySelector('#menu-box');
const menu = document.querySelector('#menu');
const menuLinks = menu.querySelectorAll('li');
const viewAllBtn = document.querySelector('#view-all-btn');
const cards = document.querySelectorAll('.hot-topics__cards-wrapper');
const byGuestpert = document.querySelector('#byGuestpert');

//--- Show and unshow menu --//
menuBox.addEventListener('mouseover',()=>{
    menu.style.display='block'
})
menuBox.addEventListener('mouseout', ()=>{
    menu.style.display='none'
})

//--- Clicking menu btns --//
for(const btn of menuLinks){
    btn.addEventListener('click', function(){
        const keyword = btn.textContent;
        filterHotTopic(keyword, cards, 'guestPert_name', 2000);
        byGuestpert.textContent=keyword;
    })
}

//--- Clicking view all btn --//
viewAllBtn.addEventListener('click', function(){
    filterHotTopic('', cards, 'guestPert_name', 16);
    byGuestpert.textContent='All';
})


const logoutBtn = document.querySelector('.header-bar__link-logout')
import { logoutFunc } from './lib/autoLogout'
if(logoutBtn){
    logoutFunc(() => {
        const btnId = logoutBtn.getAttribute('data-id')
        if(btnId=='adtyspnce'){
            window.location.href = `/admin_login`
        }else{
            window.location.href = `/login`
        }
    })
} 