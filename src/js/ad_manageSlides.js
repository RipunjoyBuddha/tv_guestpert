import "../sass/ad_manageSlides.scss";
import "./view/importsvg";
import {editSlides} from "./view/editField"
import {chooseImg} from "./view/choose_img";
import { hamburger, slideMenu } from "./view/sidebar";
import {message} from './view/errorMessage'
import {initTinymce} from './utils/tinyActions';

///////////////////////////////////////////
///--- Hamburger and Sidebar functionality ---///
///////////////////////////////////////////
hamburger();
slideMenu();


///////////////////////////////////////////////
////---- Universal selectors ----////
///////////////////////////////////////////////
const slidesSection = document.querySelector('.slides');
const editsection = document.querySelector('#editSlides');
const csrf = document.querySelector('#csrf').getAttribute('data-csrf');
const editForm = editsection.querySelector('form')

chooseImg(editsection.querySelector('#inpFile-box'));

/********* Edit Form **********/
editForm.addEventListener('submit', e=>{

    e.preventDefault()
    const imgFile = editForm.querySelector('#choose-img-file').files[0]

    if(imgFile && imgFile.size > 400*1000){
        message(5000, false, 'Too large image file. Please reduce image file size')
        return
    }

    e.target.submit()
})


/////////////////////////////////
//---- Make edit button work----//
/////////////////////////////////
const editBtn = document.querySelectorAll('.slides__edit-btn');
editSlides(editBtn, editsection, slidesSection, csrf)

const errMsg = document.querySelector('.message');

window.addEventListener('load', e=>{
    setTimeout(()=>{
        if(!errMsg)return;
        errMsg.style.display='none';
        errMsg.querySelector('.message__txt').textContent='';
    }, 5000)
})


import { logoutFunc } from './lib/autoLogout'
logoutFunc(() => {
    window.location.href = `/admin_login`
})


///////////////////////////////
// --- tiny mce settings ---//
///////////////////////////////


initTinymce(
    { 
        selector: "#slideCaption",
        menubar: !1,
        skin: "CUSTOM",
        content_css: "CUSTOM",
        plugins: "preview lists fullscreen link paste",
        paste_as_text: true,
        toolbar: "undo redo preview fullscreen bold italic link",
        advlist_bullet_styles: "disc",
    }
)