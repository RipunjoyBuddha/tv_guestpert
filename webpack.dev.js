const path = require('path');

const merge = require('webpack-merge');
const HtmlWebpackPlugin = require("html-webpack-plugin");

const common = require('./webpack.common');

module.exports = merge(common, {
    mode: "development",
    output: {
        filename: "js/[name].bundle.js",
        path: path.resolve(__dirname, 'dist')
    },
    
    devServer: {
        contentBase: path.join(__dirname, 'dist', 'templates'),
        compress: true,
        port: 9000
    },

    
    plugins: [

        ///////////////////////////////////////////////////
        //////*****Change below to add new page*****//////
        ///////////////////////////////////////////////////

        /////**** Set filename extension to .html only ****/////

        new HtmlWebpackPlugin({
            filename:  'index.html', //***Dont change this filename***// 
            template: 'src/templates/front/home.ejs', // template to use
            chunks: ['home'] // css and js to be attatched
        }),
        new HtmlWebpackPlugin({
            filename: 'about.html',
            template: 'src/templates/front/about.ejs',
            chunks: ['about']
        }),
        new HtmlWebpackPlugin({
            filename: 'guestpert.html',
            template: 'src/templates/front/guestpert.ejs',
            chunks: ['guestpert']
        }),
        new HtmlWebpackPlugin({
            filename: 'services.html',
            template: 'src/templates/front/services.ejs',
            chunks: ['services']
        }),
        new HtmlWebpackPlugin({
            filename: 'hot_topics.html',
            template: 'src/templates/front/hot_topics.ejs',
            chunks: ['hot_topics']
        }),
        new HtmlWebpackPlugin({
            filename: 'contact.html',
            template: 'src/templates/front/contact.ejs',
            chunks: ['contact']
        }),
        new HtmlWebpackPlugin({
            filename: 'catagory_hotTopic.html',
            template: 'src/templates/front/catagory_hotTopic.ejs',
            chunks: ['catagory_hotTopic']
        }),
        new HtmlWebpackPlugin({
            filename: 'content_hotTopics.html',
            template: 'src/templates/front/content_hotTopics.ejs',
            chunks: ['content_hotTopics']
        }),
        new HtmlWebpackPlugin({
            filename: 'guestpert_personal.html',
            template: 'src/templates/front/guestpert_personal.ejs',
            chunks: ['guestpert_personal']
        }),
        new HtmlWebpackPlugin({
            filename: 'faq.html',
            template: 'src/templates/front/faq.ejs',
            chunks: ['faq']
        }),
        new HtmlWebpackPlugin({
            filename: 'shop.html',
            template: 'src/templates/front/shop.ejs',
            chunks: ['shop']
        }),
        new HtmlWebpackPlugin({
            filename: 'product_details.html',
            template: 'src/templates/front/product_details.ejs',
            chunks: ['product_details']
        }),
        new HtmlWebpackPlugin({
            filename: 'testimonial.html',
            template: 'src/templates/front/testimonial.ejs',
            chunks: ['testimonial']
        }),
        new HtmlWebpackPlugin({
            filename: 'blog.html',
            template: 'src/templates/front/blog.ejs',
            chunks: ['blog']
        }),
        new HtmlWebpackPlugin({
            filename: 'jaquiesDetails.html',
            template: 'src/templates/front/jaquiesDetails.ejs',
            chunks: ['jaquiesDetails']
        }),
        new HtmlWebpackPlugin({
            filename: 'termsAndCondition.html',
            template: 'src/templates/front/termsAndCondition.ejs',
            chunks: ['termsAndCondition']
        }),
        new HtmlWebpackPlugin({
            filename: 'privacyPolicy.html',
            template: 'src/templates/front/privacyPolicy.ejs',
            chunks: ['privacyPolicy']
        }),
        new HtmlWebpackPlugin({
            filename: 'forgotPassword.html',
            template: 'src/templates/auth/forgotPassword.ejs',
            chunks: ['forgotPassword']
        }),
        new HtmlWebpackPlugin({
            filename: 'resetPassword.html',
            template: 'src/templates/auth/resetPassword.ejs',
            chunks: ['resetPassword']
        }),



        //---- Authorization below
        new HtmlWebpackPlugin({
            filename: 'login.html',
            template: 'src/templates/auth/login.ejs',
            chunks: ['login']
        }),
        new HtmlWebpackPlugin({
            filename: 'register.html',
            template: 'src/templates/auth/register.ejs',
            chunks: ['register']
        }),
        

        //---- Guestpert admin pannel below
        
        new HtmlWebpackPlugin({
            filename: 'ga_dashboard.html',
            template: 'src/templates/guestpertPannel/ga_dashboard.ejs',
            chunks: ['ga_dashboard']
        }),
        new HtmlWebpackPlugin({
            filename: 'ga_profileSettings.html',
            template: 'src/templates/guestpertPannel/ga_profileSettings.ejs',
            chunks: ['ga_profileSettings']
        }),
        new HtmlWebpackPlugin({
            filename: 'ga_hotTopicsSettings.html',
            template: 'src/templates/guestpertPannel/ga_hotTopicsSettings.ejs',
            chunks: ['ga_hotTopicsSettings']
        }),
        new HtmlWebpackPlugin({
            filename: 'ga_media.html',
            template: 'src/templates/guestpertPannel/ga_media.ejs',
            chunks: ['ga_media']
        }),
        new HtmlWebpackPlugin({
            filename: 'ga_blog.html',
            template: 'src/templates/guestpertPannel/ga_blog.ejs',
            chunks: ['ga_blog']
        }),
        new HtmlWebpackPlugin({
            filename: 'ga_books.html',
            template: 'src/templates/guestpertPannel/ga_books.ejs',
            chunks: ['ga_books']
        }),
        new HtmlWebpackPlugin({
            filename: 'ga_demoReel.html',
            template: 'src/templates/guestpertPannel/ga_demoReel.ejs',
            chunks: ['ga_demoReel']
        }),
        new HtmlWebpackPlugin({
            filename: 'ga_membership.html',
            template: 'src/templates/guestpertPannel/ga_membership.ejs',
            chunks: ['ga_membership']
        }),
        new HtmlWebpackPlugin({
            filename: 'ga_orders.html',
            template: 'src/templates/guestpertPannel/ga_orders.ejs',
            chunks: ['ga_orders']
        }),
        new HtmlWebpackPlugin({
            filename: 'ga_enquiries.html',
            template: 'src/templates/guestpertPannel/ga_enquiries.ejs',
            chunks: ['ga_enquiries']
        }),
        
        //---- Admin admin pannel below

        new HtmlWebpackPlugin({
            filename: 'ad_login.html',
            template: 'src/templates/auth/ad_login.ejs',
            chunks: ['ad_login']
        }),
        new HtmlWebpackPlugin({
            filename: 'ad_dashboard.html',
            template: 'src/templates/adminPannel/ad_dashboard.ejs',
            chunks: ['ad_dashboard']
        }),
        new HtmlWebpackPlugin({
            filename: 'ad_folders.html',
            template: 'src/templates/adminPannel/ad_folders.ejs',
            chunks: ['ad_folders']
        }),
        new HtmlWebpackPlugin({
            filename: 'ad_subAdmin.html',
            template: 'src/templates/adminPannel/ad_subAdmin.ejs',
            chunks: ['ad_subAdmin']
        }),
        new HtmlWebpackPlugin({
            filename: 'ad_generalSettings.html',
            template: 'src/templates/adminPannel/ad_generalSettings.ejs',
            chunks: ['ad_generalSettings']
        }),
        new HtmlWebpackPlugin({
            filename: 'ad_emailSettings.html',
            template: 'src/templates/adminPannel/ad_emailSettings.ejs',
            chunks: ['ad_emailSettings']
        }),
        new HtmlWebpackPlugin({
            filename: 'ad_frontEndUI.html',
            template: 'src/templates/adminPannel/ad_frontEndUI.ejs',
            chunks: ['ad_frontEndUI']
        }),
        new HtmlWebpackPlugin({
            filename: 'ad_homePageSettings.html',
            template: 'src/templates/adminPannel/ad_homePageSettings.ejs',
            chunks: ['ad_homePageSettings']
        }),
        new HtmlWebpackPlugin({
            filename: 'ad_manageSlides.html',
            template: 'src/templates/adminPannel/ad_manageSlides.ejs',
            chunks: ['ad_manageSlides']
        }),
        new HtmlWebpackPlugin({
            filename: 'ad_companyTags.html',
            template: 'src/templates/adminPannel/ad_companyTags.ejs',
            chunks: ['ad_companyTags']
        }),
        new HtmlWebpackPlugin({
            filename: 'ad_homePageHotTopics.html',
            template: 'src/templates/adminPannel/ad_homePageHotTopics.ejs',
            chunks: ['ad_homePageHotTopics']
        }),
        new HtmlWebpackPlugin({
            filename: 'ad_homePageGuestperts.html',
            template: 'src/templates/adminPannel/ad_homePageGuestperts.ejs',
            chunks: ['ad_homePageGuestperts']
        }),
        new HtmlWebpackPlugin({
            filename: 'ad_homePageBooksCarousel.html',
            template: 'src/templates/adminPannel/ad_homePageBooksCarousel.ejs',
            chunks: ['ad_homePageBooksCarousel']
        }),
        new HtmlWebpackPlugin({
            filename: 'ad_homePageTestimonials.html',
            template: 'src/templates/adminPannel/ad_homePageTestimonials.ejs',
            chunks: ['ad_homePageTestimonials']
        }),
        new HtmlWebpackPlugin({
            filename: 'ad_manageAboutPage.html',
            template: 'src/templates/adminPannel/ad_manageAboutPage.ejs',
            chunks: ['ad_manageAboutPage']
        }),
        new HtmlWebpackPlugin({
            filename: 'ad_manageHotTopicsCat.html',
            template: 'src/templates/adminPannel/ad_manageHotTopicsCat.ejs',
            chunks: ['ad_manageHotTopicsCat']
        }),
        new HtmlWebpackPlugin({
            filename: 'ad_manageContactPage.html',
            template: 'src/templates/adminPannel/ad_manageContactPage.ejs',
            chunks: ['ad_manageContactPage']
        }),
        new HtmlWebpackPlugin({
            filename: 'ad_manageServicePage.html',
            template: 'src/templates/adminPannel/ad_manageServicePage.ejs',
            chunks: ['ad_manageServicePage']
        }),
        new HtmlWebpackPlugin({
            filename: 'ad_manageFaqPage.html',
            template: 'src/templates/adminPannel/ad_manageFaqPage.ejs',
            chunks: ['ad_manageFaqPage']
        }),
        new HtmlWebpackPlugin({
            filename: 'ad_guestpertAdmin.html',
            template: 'src/templates/adminPannel/ad_guestpertAdmin.ejs',
            chunks: ['ad_guestpertAdmin']
        }),
        new HtmlWebpackPlugin({
            filename: 'ad_newsletterTemplate.html',
            template: 'src/templates/adminPannel/ad_newsletterTemplate.ejs',
            chunks: ['ad_newsletterTemplate']
        }),
        new HtmlWebpackPlugin({
            filename: 'ad_newsletterHistory.html',
            template: 'src/templates/adminPannel/ad_newsletterHistory.ejs',
            chunks: ['ad_newsletterHistory']
        }),
        new HtmlWebpackPlugin({
            filename: 'ad_newsletterSubscriber.html',
            template: 'src/templates/adminPannel/ad_newsletterSubscriber.ejs',
            chunks: ['ad_newsletterSubscriber']
        }),
        new HtmlWebpackPlugin({
            filename: 'ad_manageTags.html',
            template: 'src/templates/adminPannel/ad_manageTags.ejs',
            chunks: ['ad_manageTags']
        }),
        new HtmlWebpackPlugin({
            filename: 'ad_manageTestimonials.html',
            template: 'src/templates/adminPannel/ad_manageTestimonials.ejs',
            chunks: ['ad_manageTestimonials']
        }),
        new HtmlWebpackPlugin({
            filename: 'ad_manageGuestperts.html',
            template: 'src/templates/adminPannel/ad_manageGuestperts.ejs',
            chunks: ['ad_manageGuestperts']
        }),
        new HtmlWebpackPlugin({
            filename: 'ad_manageMembership.html',
            template: 'src/templates/adminPannel/ad_manageMembership.ejs',
            chunks: ['ad_manageMembership']
        }),
        new HtmlWebpackPlugin({
            filename: 'ad_manageProducts.html',
            template: 'src/templates/adminPannel/ad_manageProducts.ejs',
            chunks: ['ad_manageProducts']
        }),
        new HtmlWebpackPlugin({
            filename: 'ad_orders.html',
            template: 'src/templates/adminPannel/ad_orders.ejs',
            chunks: ['ad_orders']
        }),
        new HtmlWebpackPlugin({
            filename: 'ad_jaquiesAppearance.html',
            template: 'src/templates/adminPannel/ad_jaquiesAppearance.ejs',
            chunks: ['ad_jaquiesAppearance']
        }),
        new HtmlWebpackPlugin({
            filename: 'ad_manageJaquiesBlog.html',
            template: 'src/templates/adminPannel/ad_manageJaquiesBlog.ejs',
            chunks: ['ad_manageJaquiesBlog']
        }),
        new HtmlWebpackPlugin({
            filename: 'ad_manageJaquiesDetail.html',
            template: 'src/templates/adminPannel/ad_manageJaquiesDetail.ejs',
            chunks: ['ad_manageJaquiesDetail']
        }),
        new HtmlWebpackPlugin({
            filename: 'ad_manageProducers.html',
            template: 'src/templates/adminPannel/ad_manageProducers.ejs',
            chunks: ['ad_manageProducers']
        }),
        new HtmlWebpackPlugin({
            filename: 'ad_producerEnquiries.html',
            template: 'src/templates/adminPannel/ad_producerEnquiries.ejs',
            chunks: ['ad_producerEnquiries']
        }),
        new HtmlWebpackPlugin({
            filename: 'ad_changePassword.html',
            template: 'src/templates/adminPannel/ad_changePassword.ejs',
            chunks: ['ad_changePassword']
        }),



        new HtmlWebpackPlugin({
            filename: 'pr_enquiries.html',
            template: 'src/templates/producerPannel/pr_enquiries.ejs',
            chunks: ['pr_enquiries']
        }),
        new HtmlWebpackPlugin({
            filename: 'pr_profileSettings.html',
            template: 'src/templates/producerPannel/pr_profileSettings.ejs',
            chunks: ['pr_profileSettings']
        }),


        ///////////////////////////////////////////////////
        //////*****Change above to add new page*****//////
        ///////////////////////////////////////////////////      
    ],



    module: {
        rules: [ 
            {
                test:/\.scss$/,
                use: [
                    {
                        loader:"style-loader"
                    },
                    {
                        loader:"css-loader",
                        options: {
                            url:true
                        }
                    },
                    {
                        loader: 'resolve-url-loader',
                    },
                    {
                        loader:"sass-loader"
                    }
                ]
            },
            {
                test: /\.svg$/,
                loader: 'svg-sprite-loader',
                options: {
                    spriteFilename: 'imgs/sprite.svg',
                    runtimeCompat: true
                }
            }
        ]
    }
});