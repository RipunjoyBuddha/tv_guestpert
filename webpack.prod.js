const path = require('path');

const merge = require('webpack-merge');
const HtmlWebpackPlugin = require("html-webpack-plugin");
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const OPtimizeCssAssetsPlugin = require('optimize-css-assets-webpack-plugin');
const TerserPlugin = require('terser-webpack-plugin');
const SpriteLoaderPlugin = require('svg-sprite-loader/plugin');
const autoprefixer = require('autoprefixer');

const common = require('./webpack.common');


///////////////////////////////////////////////////
//////*****Src template location below*****//////
///////////////////////////////////////////////////

const templateFront = 'templates/front';
const templateAdmin = 'templates/adminPannel';
const templateguestpert = 'templates/guestPertPannel';
const templateProducer = 'templates/producerPannel'

///////////////////////////////////////////////////
//////*****Src template location above*****//////
///////////////////////////////////////////////////


module.exports = merge(common, {
    mode: "production",
    output: {
        filename: "public/js/[name].[contentHash].bundle.js",
        path: path.resolve(__dirname, 'dist')
    },

    optimization: {
        minimizer: [
            new OPtimizeCssAssetsPlugin(),
            new TerserPlugin()
        ]
    },

    plugins: [

        ///////////////////////////////////////////////////
        //////*****Change below to add new page*****//////
        ///////////////////////////////////////////////////

        new HtmlWebpackPlugin({
            filename: `${templateFront}/home.ejs`, //name and location of output template
            template: 'src/templates/front/home.ejs', // template to use
            minify: false,
            chunks: ['home'], // css and js to be attatched
        }),
        new HtmlWebpackPlugin({
            filename:  `${templateFront}/about.ejs`, //name and location of output template
            template: 'src/templates/front/about.ejs', // template to use
            minify: false,
            chunks: ['about'], // css and js to be attatched
        }),
        new HtmlWebpackPlugin({
            filename:  `${templateFront}/guestpert.ejs`, //name and location of output template
            template: 'src/templates/front/guestpert.ejs', // template to use
            minify: false,
            chunks: ['guestpert'], // css and js to be attatched
        }),
        new HtmlWebpackPlugin({
            filename:  `${templateFront}/services.ejs`, //name and location of output template
            template: 'src/templates/front/services.ejs', // template to use
            minify: false,
            chunks: ['services'], // css and js to be attatched
        }),
        new HtmlWebpackPlugin({
            filename:  `${templateFront}/contact.ejs`, //name and location of output template
            template: 'src/templates/front/contact.ejs', // template to use
            minify: false,
            chunks: ['contact'], // css and js to be attatched
        }),
        new HtmlWebpackPlugin({
            filename:  `${templateFront}/catagory_hotTopic.ejs`, //name and location of output template
            template: 'src/templates/front/catagory_hotTopic.ejs', // template to use
            minify: false,
            chunks: ['catagory_hotTopic'], // css and js to be attatched
        }),
        new HtmlWebpackPlugin({
            filename:  `${templateFront}/content_hotTopics.ejs`, //name and location of output template
            template: 'src/templates/front/content_hotTopics.ejs', // template to use
            minify: false,
            chunks: ['content_hotTopics'], // css and js to be attatched
        }),
        new HtmlWebpackPlugin({
            filename:  `${templateFront}/guestpert_personal.ejs`, //name and location of output template
            template: 'src/templates/front/guestpert_personal.ejs', // template to use
            minify: false,
            chunks: ['guestpert_personal'], // css and js to be attatched
        }),
        new HtmlWebpackPlugin({
            filename:  `${templateFront}/faq.ejs`, //name and location of output template
            template: 'src/templates/front/faq.ejs', // template to use
            minify: false,
            chunks: ['faq'], // css and js to be attatched
        }),
        new HtmlWebpackPlugin({
            filename:  `${templateFront}/shop.ejs`, //name and location of output template
            template: 'src/templates/front/shop.ejs', // template to use
            minify: false,
            chunks: ['shop'], // css and js to be attatched
        }),
        new HtmlWebpackPlugin({
            filename:  `${templateFront}/product_details.ejs`, //name and location of output template
            template: 'src/templates/front/product_details.ejs', // template to use
            minify: false,
            chunks: ['product_details'], // css and js to be attatched
        }),

        new HtmlWebpackPlugin({
            filename:  `${templateFront}/blog.ejs`, //name and location of output template
            template: 'src/templates/front/blog.ejs', // template to use
            minify: false,
            chunks: ['blog'], // css and js to be attatched
        }),

        new HtmlWebpackPlugin({
            filename:  `${templateFront}/jaquiesDetails.ejs`, //name and location of output template
            template: 'src/templates/front/jaquiesDetails.ejs', // template to use
            minify: false,
            chunks: ['jaquiesDetails'], // css and js to be attatched
        }),

        new HtmlWebpackPlugin({
            filename:  `${templateFront}/termsAndCondition.ejs`, //name and location of output template
            template: 'src/templates/front/termsAndCondition.ejs', // template to use
            minify: false,
            chunks: ['termsAndCondition'], // css and js to be attatched
        }),

        new HtmlWebpackPlugin({
            filename:  `${templateFront}/privacyPolicy.ejs`, //name and location of output template
            template: 'src/templates/front/privacyPolicy.ejs', // template to use
            minify: false,
            chunks: ['privacyPolicy'], // css and js to be attatched
        }),

        new HtmlWebpackPlugin({
            filename:  `${templateFront}/forgotPassword.ejs`, //name and location of output template
            template: 'src/templates/auth/forgotPassword.ejs', // template to use
            minify: false,
            chunks: ['forgotPassword'], // css and js to be attatched
        }),
        
        new HtmlWebpackPlugin({
            filename:  `${templateFront}/resetPassword.ejs`, //name and location of output template
            template: 'src/templates/auth/resetPassword.ejs', // template to use
            minify: false,
            chunks: ['resetPassword'], // css and js to be attatched
        }),

        //---- Authorization below
        new HtmlWebpackPlugin({
            filename:  `${templateFront}/login.ejs`, //name and location of output template
            template: 'src/templates/auth/login.ejs', // template to use
            minify: false,
            chunks: ['login'], // css and js to be attatched
        }),
        new HtmlWebpackPlugin({
            filename:  `${templateFront}/register.ejs`, //name and location of output template
            template: 'src/templates/auth/register.ejs', // template to use
            minify: false,
            chunks: ['register'], // css and js to be attatched
        }),

        //---- Guestpert admin pannel below

        new HtmlWebpackPlugin({
            filename:  `${templateguestpert}/ga_dashboard.ejs`, //name and location of output template
            template: 'src/templates/guestpertPannel/ga_dashboard.ejs', // template to use
            minify: false,
            chunks: ['ga_dashboard'], // css and js to be attatched
        }),
        // new HtmlWebpackPlugin({
        //     filename:  `${templateguestpert}/ga_profileSettings.ejs`, //name and location of output template
        //     template: 'src/templates/guestpertPannel/ga_profileSettings.ejs', // template to use
        //     minify: false,
        //     chunks: ['ga_profileSettings'], // css and js to be attatched
        // }),
        new HtmlWebpackPlugin({
            filename:  `${templateguestpert}/ga_hotTopicsSettings.ejs`, //name and location of output template
            template: 'src/templates/guestpertPannel/ga_hotTopicsSettings.ejs', // template to use
            minify: false,
            chunks: ['ga_hotTopicsSettings'], // css and js to be attatched
        }),
        new HtmlWebpackPlugin({
            filename:  `${templateguestpert}/ga_media.ejs`, //name and location of output template
            template: 'src/templates/guestpertPannel/ga_media.ejs', // template to use
            minify: false,
            chunks: ['ga_media'], // css and js to be attatched
        }),
        new HtmlWebpackPlugin({
            filename:  `${templateguestpert}/ga_blog.ejs`, //name and location of output template
            template: 'src/templates/guestpertPannel/ga_blog.ejs', // template to use
            minify: false,
            chunks: ['ga_blog'], // css and js to be attatched
        }),
        new HtmlWebpackPlugin({
            filename:  `${templateguestpert}/ga_books.ejs`, //name and location of output template
            template: 'src/templates/guestpertPannel/ga_books.ejs', // template to use
            minify: false,
            chunks: ['ga_books'], // css and js to be attatched
        }),
        new HtmlWebpackPlugin({
            filename:  `${templateguestpert}/ga_demoReel.ejs`, //name and location of output template
            template: 'src/templates/guestpertPannel/ga_demoReel.ejs', // template to use
            minify: false,
            chunks: ['ga_demoReel'], // css and js to be attatched
        }),
        new HtmlWebpackPlugin({
            filename:  `${templateguestpert}/ga_membership.ejs`, //name and location of output template
            template: 'src/templates/guestpertPannel/ga_membership.ejs', // template to use
            minify: false,
            chunks: ['ga_membership'], // css and js to be attatched
        }),
        new HtmlWebpackPlugin({
            filename:  `${templateguestpert}/ga_orders.ejs`, //name and location of output template
            template: 'src/templates/guestpertPannel/ga_orders.ejs', // template to use
            minify: false,
            chunks: ['ga_orders'], // css and js to be attatched
        }),

        new HtmlWebpackPlugin({
            filename:  `${templateguestpert}/ga_enquiries.ejs`, //name and location of output template
            template: 'src/templates/guestpertPannel/ga_enquiries.ejs', // template to use
            minify: false,
            chunks: ['ga_enquiries'], // css and js to be attatched
        }),

        //---- Admin Pannel below
        new HtmlWebpackPlugin({
            filename:  `${templateAdmin}/ad_login.ejs`, //name and location of output template
            template: 'src/templates/auth/ad_login.ejs', // template to use
            minify: false,
            chunks: ['ad_login'], // css and js to be attatched
        }),
        new HtmlWebpackPlugin({
            filename:  `${templateAdmin}/ad_dashboard.ejs`, //name and location of output template
            template: 'src/templates/adminPannel/ad_dashboard.ejs', // template to use
            minify: false,
            chunks: ['ad_dashboard'], // css and js to be attatched
        }),
        new HtmlWebpackPlugin({
            filename:  `${templateAdmin}/ad_subAdmin.ejs`, //name and location of output template
            template: 'src/templates/adminPannel/ad_subAdmin.ejs', // template to use
            minify: false,
            chunks: ['ad_subAdmin'], // css and js to be attatched
        }),
        new HtmlWebpackPlugin({
            filename:  `${templateAdmin}/ad_generalSettings.ejs`, //name and location of output template
            template: 'src/templates/adminPannel/ad_generalSettings.ejs', // template to use
            minify: false,
            chunks: ['ad_generalSettings'], // css and js to be attatched
        }),
        new HtmlWebpackPlugin({
            filename:  `${templateAdmin}/ad_emailSettings.ejs`, //name and location of output template
            template: 'src/templates/adminPannel/ad_emailSettings.ejs', // template to use
            minify: false,
            chunks: ['ad_emailSettings'], // css and js to be attatched
        }),
        new HtmlWebpackPlugin({
            filename:  `${templateAdmin}/ad_frontEndUI.ejs`, //name and location of output template
            template: 'src/templates/adminPannel/ad_frontEndUI.ejs', // template to use
            minify: false,
            chunks: ['ad_frontEndUI'], // css and js to be attatched
        }),
        new HtmlWebpackPlugin({
            filename:  `${templateAdmin}/ad_homePageSettings.ejs`, //name and location of output template
            template: 'src/templates/adminPannel/ad_homePageSettings.ejs', // template to use
            minify: false,
            chunks: ['ad_homePageSettings'], // css and js to be attatched
        }),
        new HtmlWebpackPlugin({
            filename:  `${templateAdmin}/ad_manageSlides.ejs`, //name and location of output template
            template: 'src/templates/adminPannel/ad_manageSlides.ejs', // template to use
            minify: false,
            chunks: ['ad_manageSlides'], // css and js to be attatched
        }),
        new HtmlWebpackPlugin({
            filename:  `${templateAdmin}/ad_companyTags.ejs`, //name and location of output template
            template: 'src/templates/adminPannel/ad_companyTags.ejs', // template to use
            minify: false,
            chunks: ['ad_companyTags'], // css and js to be attatched
        }),
        new HtmlWebpackPlugin({
            filename:  `${templateAdmin}/ad_homePageHotTopics.ejs`, //name and location of output template
            template: 'src/templates/adminPannel/ad_homePageHotTopics.ejs', // template to use
            minify: false,
            chunks: ['ad_homePageHotTopics'], // css and js to be attatched
        }),
        new HtmlWebpackPlugin({
            filename:  `${templateAdmin}/ad_homePageGuestperts.ejs`, //name and location of output template
            template: 'src/templates/adminPannel/ad_homePageGuestperts.ejs', // template to use
            minify: false,
            chunks: ['ad_homePageGuestperts'], // css and js to be attatched
        }),
        new HtmlWebpackPlugin({
            filename:  `${templateAdmin}/ad_homePageBooksCarousel.ejs`, //name and location of output template
            template: 'src/templates/adminPannel/ad_homePageBooksCarousel.ejs', // template to use
            minify: false,
            chunks: ['ad_homePageBooksCarousel'], // css and js to be attatched
        }),
        new HtmlWebpackPlugin({
            filename:  `${templateAdmin}/ad_homePageTestimonials.ejs`, //name and location of output template
            template: 'src/templates/adminPannel/ad_homePageTestimonials.ejs', // template to use
            minify: false,
            chunks: ['ad_homePageTestimonials'], // css and js to be attatched
        }),
        new HtmlWebpackPlugin({
            filename:  `${templateAdmin}/ad_manageAboutPage.ejs`, //name and location of output template
            template: 'src/templates/adminPannel/ad_manageAboutPage.ejs', // template to use
            minify: false,
            chunks: ['ad_manageAboutPage'], // css and js to be attatched
        }),
        new HtmlWebpackPlugin({
            filename:  `${templateAdmin}/ad_manageHotTopicsCat.ejs`, //name and location of output template
            template: 'src/templates/adminPannel/ad_manageHotTopicsCat.ejs', // template to use
            minify: false,
            chunks: ['ad_manageHotTopicsCat'], // css and js to be attatched
        }),
        new HtmlWebpackPlugin({
            filename:  `${templateAdmin}/ad_manageContactPage.ejs`, //name and location of output template
            template: 'src/templates/adminPannel/ad_manageContactPage.ejs', // template to use
            minify: false,
            chunks: ['ad_manageContactPage'], // css and js to be attatched
        }),
        new HtmlWebpackPlugin({
            filename:  `${templateAdmin}/ad_manageServicePage.ejs`, //name and location of output template
            template: 'src/templates/adminPannel/ad_manageServicePage.ejs', // template to use
            minify: false,
            chunks: ['ad_manageServicePage'], // css and js to be attatched
        }),
        new HtmlWebpackPlugin({
            filename:  `${templateAdmin}/ad_manageFaqPage.ejs`, //name and location of output template
            template: 'src/templates/adminPannel/ad_manageFaqPage.ejs', // template to use
            minify: false,
            chunks: ['ad_manageFaqPage'], // css and js to be attatched
        }),
        new HtmlWebpackPlugin({
            filename:  `${templateAdmin}/ad_guestpertAdmin.ejs`, //name and location of output template
            template: 'src/templates/adminPannel/ad_guestpertAdmin.ejs', // template to use
            minify: false,
            chunks: ['ad_guestpertAdmin'], // css and js to be attatched
        }),
        new HtmlWebpackPlugin({
            filename:  `${templateAdmin}/ad_newsletterTemplate.ejs`, //name and location of output template
            template: 'src/templates/adminPannel/ad_newsletterTemplate.ejs', // template to use
            minify: false,
            chunks: ['ad_newsletterTemplate'], // css and js to be attatched
        }),
        new HtmlWebpackPlugin({
            filename:  `${templateAdmin}/ad_newsletterHistory.ejs`, //name and location of output template
            template: 'src/templates/adminPannel/ad_newsletterHistory.ejs', // template to use
            minify: false,
            chunks: ['ad_newsletterHistory'], // css and js to be attatched
        }),
        new HtmlWebpackPlugin({
            filename:  `${templateAdmin}/ad_newsletterSubscriber.ejs`, //name and location of output template
            template: 'src/templates/adminPannel/ad_newsletterSubscriber.ejs', // template to use
            minify: false,
            chunks: ['ad_newsletterSubscriber'], // css and js to be attatched
        }),
        new HtmlWebpackPlugin({
            filename:  `${templateAdmin}/ad_manageTags.ejs`, //name and location of output template
            template: 'src/templates/adminPannel/ad_manageTags.ejs', // template to use
            minify: false,
            chunks: ['ad_manageTags'], // css and js to be attatched
        }),
        new HtmlWebpackPlugin({
            filename:  `${templateAdmin}/ad_manageTestimonials.ejs`, //name and location of output template
            template: 'src/templates/adminPannel/ad_manageTestimonials.ejs', // template to use
            minify: false,
            chunks: ['ad_manageTestimonials'], // css and js to be attatched
        }),
        //// new HtmlWebpackPlugin({
        //     filename:  `${templateAdmin}/ad_manageGuestperts.ejs`, //name and location of output template
        //     template: 'src/templates/adminPannel/ad_manageGuestperts.ejs', // template to use
        //     minify: false,
        //     chunks: ['ad_manageGuestperts'], // css and js to be attatched
        //// }),
        new HtmlWebpackPlugin({
            filename:  `${templateAdmin}/ad_manageMembership.ejs`, //name and location of output template
            template: 'src/templates/adminPannel/ad_manageMembership.ejs', // template to use
            minify: false,
            chunks: ['ad_manageMembership'], // css and js to be attatched
        }),
        new HtmlWebpackPlugin({
            filename:  `${templateAdmin}/ad_manageProducts.ejs`, //name and location of output template
            template: 'src/templates/adminPannel/ad_manageProducts.ejs', // template to use
            minify: false,
            chunks: ['ad_manageProducts'], // css and js to be attatched
        }),
        new HtmlWebpackPlugin({
            filename:  `${templateAdmin}/ad_orders.ejs`, //name and location of output template
            template: 'src/templates/adminPannel/ad_orders.ejs', // template to use
            minify: false,
            chunks: ['ad_orders'], // css and js to be attatched
        }),
        new HtmlWebpackPlugin({
            filename:  `${templateAdmin}/ad_jaquiesAppearance.ejs`, //name and location of output template
            template: 'src/templates/adminPannel/ad_jaquiesAppearance.ejs', // template to use
            minify: false,
            chunks: ['ad_jaquiesAppearance'], // css and js to be attatched
        }),
        new HtmlWebpackPlugin({
            filename:  `${templateAdmin}/ad_manageJaquiesBlog.ejs`, //name and location of output template
            template: 'src/templates/adminPannel/ad_manageJaquiesBlog.ejs', // template to use
            minify: false,
            chunks: ['ad_manageJaquiesBlog'], // css and js to be attatched
        }),
        new HtmlWebpackPlugin({
            filename:  `${templateAdmin}/ad_manageJaquiesDetail.ejs`, //name and location of output template
            template: 'src/templates/adminPannel/ad_manageJaquiesDetail.ejs', // template to use
            minify: false,
            chunks: ['ad_manageJaquiesDetail'], // css and js to be attatched
        }),
        new HtmlWebpackPlugin({
            filename:  `${templateAdmin}/ad_producerEnquiries.ejs`, //name and location of output template
            template: 'src/templates/adminPannel/ad_producerEnquiries.ejs', // template to use
            minify: false,
            chunks: ['ad_producerEnquiries'], // css and js to be attatched
        }),
        new HtmlWebpackPlugin({
            filename:  `${templateAdmin}/ad_changePassword.ejs`, //name and location of output template
            template: 'src/templates/adminPannel/ad_changePassword.ejs', // template to use
            minify: false,
            chunks: ['ad_changePassword'], // css and js to be attatched
        }),

        new HtmlWebpackPlugin({
            filename:  `${templateProducer}/pr_enquiries.ejs`, //name and location of output template
            template: 'src/templates/producerPannel/pr_enquiries.ejs', // template to use
            minify: false,
            chunks: ['pr_enquiries'], // css and js to be attatched
        }),
        new HtmlWebpackPlugin({
            filename:  `${templateProducer}/pr_profileSettings.ejs`, //name and location of output template
            template: 'src/templates/producerPannel/pr_profileSettings.ejs', // template to use
            minify: false,
            chunks: ['pr_profileSettings'], // css and js to be attatched
        }),

        ///////////////////////////////////////////////////
        //////*****Change above to add new page*****//////
        ///////////////////////////////////////////////////

        new MiniCssExtractPlugin({
            filename: "public/css/[name].[contentHash].css"
        }),

        new CleanWebpackPlugin(),
        new SpriteLoaderPlugin()
    ],

    module: {
        rules: [
            {
                test: /\.scss$/,
                use: [
                    MiniCssExtractPlugin.loader, //3. Extract css into files
                    "css-loader", // 3. Turns css into common js
                    {
                        loader: 'postcss-loader',
                        options: {
                            plugins: () => [autoprefixer()]
                        }
                    },
                    "sass-loader" // 1. Turns sass into css
                ]
            },
            {
                test: /\.js$/,
                exclude: /node_modules/,
                use: {
                    loader: 'babel-loader'
                }
            },
            {
                test: /\.svg$/,
                loader: 'svg-sprite-loader',
                options: {
                    extract: true,
                    spriteFilename: 'imgs/sprite.svg',

                }
            }
        ]
    },
});