const path = require('path');

const SpriteLoaderPlugin = require('svg-sprite-loader/plugin');

module.exports = {
    ///////////////////////////////////////////////////
    //////***** Edit Entry points below*****//////
    ///////////////////////////////////////////////////
    entry: {
        home: ["babel-polyfill","./src/js/home.js"],
        about: ["babel-polyfill","./src/js/about.js"],
        guestpert: ["babel-polyfill", "./src/js/guestpert.js"],
        services: ["babel-polyfill", "./src/js/services.js"],
        hot_topics: ["babel-polyfill", "./src/js/hot_topics.js"],
        contact: ["babel-polyfill", "./src/js/contact.js"],
        content_hotTopics: ["babel-polyfill","./src/js/content_hotTopics.js"],
        catagory_hotTopic: ["babel-polyfill", "./src/js/catagory_hotTopic.js"],
        guestpert_personal: ["babel-polyfill", "./src/js/guestpert_personal.js"],
        faq: ["babel-polyfill", "./src/js/faq.js"],
        shop: ["babel-polyfill", "./src/js/shop.js"],
        product_details: ["babel-polyfill", "./src/js/product_details.js"],
        testimonial: ["babel-polyfill", "./src/js/testimonial.js"],
        blog: ["babel-polyfill", "./src/js/blog.js"],
        login: ["babel-polyfill","./src/js/login.js"],
        register: ["babel-polyfill", "./src/js/register.js"],
        jaquiesDetails: ["babel-polyfill" ,"./src/js/jaquiesDetails.js"],
        forgotPassword: ["babel-polyfill" ,"./src/js/forgotPassword.js"],
        resetPassword: ["babel-polyfill" ,"./src/js/resetPassword.js"],
        termsAndCondition: ["babel-polyfill" ,"./src/js/termsAndCondition.js"],
        privacyPolicy: ["babel-polyfill" ,"./src/js/privacyPolicy.js"],

        ga_dashboard: [ "babel-polyfill" , "./src/js/ga_dashboard.js"],
        ga_profileSettings: ["babel-polyfill" , "./src/js/ga_profileSettings.js"],
        ga_hotTopicsSettings: ["babel-polyfill" ,"./src/js/ga_hotTopicsSettings.js"],
        ga_media: ["babel-polyfill", "./src/js/ga_media.js"],
        ga_blog: ["babel-polyfill" ,"./src/js/ga_blog.js"],
        ga_books: ["babel-polyfill" ,"./src/js/ga_books.js"],
        ga_demoReel: ["babel-polyfill" ,"./src/js/ga_demoReel.js"],
        ga_membership: ["babel-polyfill" ,"./src/js/ga_membership.js"],
        ga_orders: ["babel-polyfill" ,"./src/js/ga_orders.js"],
        ga_enquiries: ["babel-polyfill" ,"./src/js/ga_enquiries.js"],

        ad_login: ["babel-polyfill", "./src/js/ad_login.js"],
        ad_dashboard: ["babel-polyfill","./src/js/ad_dashboard.js"],
        ad_folders: ["babel-polyfill", "./src/js/ad_folders.js"],
        ad_subAdmin: ["babel-polyfill", "./src/js/ad_subAdmin.js"],
        ad_generalSettings: ["babel-polyfill", "./src/js/ad_generalSettings.js"],
        ad_emailSettings: ["babel-polyfill" , "./src/js/ad_emailSettings.js"],
        ad_frontEndUI: ["babel-polyfill", "./src/js/ad_frontEndUI.js"],
        ad_homePageSettings: ["babel-polyfill", "./src/js/ad_homePageSettings.js"],
        ad_manageSlides: ["babel-polyfill", "./src/js/ad_manageSlides.js"],
        ad_companyTags: ["babel-polyfill", "./src/js/ad_companyTags.js"],
        ad_homePageHotTopics: ["babel-polyfill" ,"./src/js/ad_homePageHotTopics.js"],
        ad_homePageGuestperts: ["babel-polyfill" ,"./src/js/ad_homePageGuestperts.js"],
        ad_homePageBooksCarousel:["babel-polyfill" , "./src/js/ad_homePageBooksCarousel.js"],
        ad_homePageTestimonials: ["babel-polyfill" ,"./src/js/ad_homePageTestimonials.js"],
        ad_manageAboutPage: ["babel-polyfill", "./src/js/ad_manageAboutPage.js"],
        ad_manageHotTopicsCat: ["babel-polyfill" ,"./src/js/ad_manageHotTopicsCat.js"],
        ad_manageContactPage: ["babel-polyfill" ,"./src/js/ad_manageContactPage.js"],
        ad_manageServicePage: ["babel-polyfill" ,"./src/js/ad_manageServicePage.js"],
        ad_manageFaqPage: ["babel-polyfill" ,"./src/js/ad_manageFaqPage.js"],
        ad_guestpertAdmin: ["babel-polyfill" ,"./src/js/ad_guestpertAdmin.js"],
        ad_newsletterTemplate: ["babel-polyfill" ,"./src/js/ad_newsletterTemplate.js"],
        ad_newsletterHistory: ["babel-polyfill" ,"./src/js/ad_newsletterHistory.js"],
        ad_newsletterSubscriber: ["babel-polyfill" ,"./src/js/ad_newsletterSubscriber.js"],
        ad_manageTags: ["babel-polyfill" ,"./src/js/ad_manageTags.js"],
        ad_manageTestimonials: ["babel-polyfill" , "./src/js/ad_manageTestimonials.js"],
        ad_manageGuestperts: ["babel-polyfill" ,"./src/js/ad_manageGuestperts.js"],
        ad_manageProducers: ["babel-polyfill" ,"./src/js/ad_manageProducers.js"],
        ad_manageMembership: ["babel-polyfill" ,"./src/js/ad_manageMembership.js"],
        ad_manageProducts: ["babel-polyfill" ,"./src/js/ad_manageProducts.js"],
        ad_orders: ["babel-polyfill" ,"./src/js/ad_orders.js"],
        ad_jaquiesAppearance: ["babel-polyfill" ,"./src/js/ad_jaquiesAppearance.js"],
        ad_manageJaquiesBlog: ["babel-polyfill" , "./src/js/ad_manageJaquiesBlog.js"],
        ad_manageJaquiesDetail: ["babel-polyfill", "./src/js/ad_manageJaquiesDetail.js"],
        ad_producerEnquiries: ["babel-polyfill", "./src/js/ad_producerEnquiries.js"],
        ad_changePassword: ["babel-polyfill", "./src/js/ad_changePassword"],

        pr_enquiries: ["babel-polyfill", "./src/js/pr_enquiries.js"],
        pr_profileSettings: ["babel-polyfill", "./src/js/pr_profileSettings.js"],
    },

    // guestpert.bbeb3991c14aed08333e.bundle
    ///////////////////////////////////////////////////
    //////***** Edit Entry points above *****//////
    ///////////////////////////////////////////////////

    module: {
        rules: [
            {
                test: /\.ejs$/,
                use: ["html-loader"]
            },
            {
                test: /\.(png|jpg|JPG|jpeg|gif)$/,
                use: {
                    loader: "file-loader",
                    options: {
                        name: "[name].[hash].[ext]",
                        outputPath: "imgs"
                    }
                }
            }
        ]
    },

    plugins: [
        new SpriteLoaderPlugin({
            plainSprite: true
        })
    ]
}

// { 
//     test: require.resolve('tinymce/tinymce'), 
//     use: [
//         {
//             loader:'imports?this=>window', 
//         },
//         {
//             loader:'exports?window.tinymce'
//         }
//     ]
// }, 
// { 
//     test: /tinymce\/(themes|plugins)\//,
//     use: {
//         loader: 'imports?this=>window'
//     }
// }